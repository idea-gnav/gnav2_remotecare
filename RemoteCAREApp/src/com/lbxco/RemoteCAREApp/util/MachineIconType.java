package com.lbxco.RemoteCAREApp.util;

import java.util.List;

import com.lbxco.RemoteCAREApp.entities.MstMachine;



public class MachineIconType {


	private MachineIconType() {}


	public static Integer stateMachineIconType(List<Object[]> dtcResults, MstMachine machine) {

		// アイコン区分
		// default:16
		Integer iconType = 16;


		// 警報レベル
		// ALERT_LV 1:なし, 2:通常, 3:重要
		boolean isDtcAlert2 = false;
		boolean isDtcAlert3 = false;

		if(dtcResults!=null) {
			for(int index=0; index < dtcResults.size(); index++) {

				Integer dtcLevel = 0;
				if(dtcResults.get(index)[0]!=null)
					dtcLevel = Integer.parseInt(dtcResults.get(index)[0].toString());	// ALERT_LV

				Integer dtcCount = 0;
				if(dtcResults.get(index)[1]!=null)
					dtcCount = Integer.parseInt(dtcResults.get(index)[1].toString());	// COUNT

				if(dtcLevel==2 && dtcCount>0)
					isDtcAlert2 = true;

				if(dtcLevel==3 && dtcCount>0)
					isDtcAlert3 = true;
			}

		}

		// 定期整備
		// 0:予告,  1:警告・経過
		boolean isMaintainece0 = false;
		boolean isMaintainece1 = false;

		if(machine!=null) {
			Integer maintaineceNotice = 0;
			if(machine.getYokokuCount()!=null)
				maintaineceNotice = machine.getYokokuCount();

			Integer maintaineceWarning = 0;
			if(machine.getKeyikokuCount()!=null)
				maintaineceWarning = machine.getKeyikokuCount();

			if(maintaineceNotice > 0)
				isMaintainece0 = true;

			if(maintaineceWarning > 0)
				isMaintainece1 = true;
		}

		// アイコン区分
		if(isDtcAlert2 && isDtcAlert3 && isMaintainece0 && isMaintainece1)
			iconType = 1;
		if(isDtcAlert2 && isDtcAlert3 && isMaintainece0 && !isMaintainece1)
			iconType = 2;
		if(isDtcAlert2 && isDtcAlert3 && !isMaintainece0 && isMaintainece1)
			iconType = 3;
		if(isDtcAlert2 && !isDtcAlert3 && isMaintainece0 && isMaintainece1)
			iconType = 4;
		if(!isDtcAlert2 && isDtcAlert3 && isMaintainece0 && isMaintainece1)
			iconType = 5;
		if(isDtcAlert2 && isDtcAlert3 && !isMaintainece0 && !isMaintainece1)
			iconType = 6;
		if(isDtcAlert2 && !isDtcAlert3 && isMaintainece0 && !isMaintainece1)
			iconType = 7;
		if(isDtcAlert2 && !isDtcAlert3 && !isMaintainece0 && isMaintainece1)
			iconType = 8;
		if(!isDtcAlert2 && isDtcAlert3 && isMaintainece0 && !isMaintainece1)
			iconType = 9;
		if(!isDtcAlert2 && isDtcAlert3 && !isMaintainece0 && isMaintainece1)
			iconType = 10;
		if(!isDtcAlert2 && !isDtcAlert3 && isMaintainece0 && isMaintainece1)
			iconType = 11;
		if(isDtcAlert2 && !isDtcAlert3 && !isMaintainece0 && !isMaintainece1)
			iconType = 12;
		if(!isDtcAlert2 && isDtcAlert3 && !isMaintainece0 && !isMaintainece1)
			iconType = 13;
		if(!isDtcAlert2 && !isDtcAlert3 && isMaintainece0 && !isMaintainece1)
			iconType = 14;
		if(!isDtcAlert2 && !isDtcAlert3 && !isMaintainece0 && isMaintainece1)
			iconType = 15;

		return iconType;
	}



}
