package com.lbxco.RemoteCAREApp.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PropertyUtil {

	private static final Logger logger = Logger.getLogger(PropertyUtil.class.getName());

	private PropertyUtil() { }

	/**
	 * 外部ファイル（properties）から key,value　読み込み
	 */
	public static String readPropertys(String filePath, String key, String defaultValue) throws Exception{

		Properties properties = new Properties();

		try {
			InputStream inputStream = new FileInputStream(filePath);
			properties.load(inputStream);
			inputStream.close();

			if(properties.getProperty(key)!=null)
				return properties.getProperty(key);
			else
				return defaultValue;

		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			throw new Exception("readPropertys error.");
		}

	}

}
