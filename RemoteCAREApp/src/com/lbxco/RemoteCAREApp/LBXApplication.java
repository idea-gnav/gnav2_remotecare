package com.lbxco.RemoteCAREApp;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;


@ApplicationPath("api")
public class LBXApplication extends Application {
}
