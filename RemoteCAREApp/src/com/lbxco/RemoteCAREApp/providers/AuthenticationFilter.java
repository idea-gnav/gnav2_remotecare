package com.lbxco.RemoteCAREApp.providers;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.ext.Provider;

import com.lbxco.RemoteCAREApp.annotations.Authorized;
import com.lbxco.RemoteCAREApp.ejbs.authentication.IAuthentication;
import com.lbxco.RemoteCAREApp.exceptions.UnauthorizedException;


@Provider
//@Authorized(value = "1")
public class AuthenticationFilter implements javax.ws.rs.container.ContainerRequestFilter {

	private static final Logger logger = Logger.getLogger(AuthenticationFilter.class.getName());

	@Inject
	IAuthentication authenticationService;

	@Context
	ResourceInfo resourceInfo;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {

		logger.info(requestContext.toString());

		Method resourceMethod = resourceInfo.getResourceMethod();
		Authorized methodAnnot = resourceMethod.getAnnotation(Authorized.class);

		//access is private!
		if (methodAnnot == null || methodAnnot.value().equals("Public") == false) {
			Cookie token = requestContext.getCookies().get("token");
			if (token == null || !authenticationService.verifyToken(token.getValue())) {
				throw new UnauthorizedException();
			}
		}
	}



}
