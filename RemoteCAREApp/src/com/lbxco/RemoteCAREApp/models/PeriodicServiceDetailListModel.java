package com.lbxco.RemoteCAREApp.models;

import java.util.ArrayList;

public class PeriodicServiceDetailListModel extends BaseModel {

	private Integer statusCode;
	private Integer serialNumber;
	private Integer machineModelCategory;
	private String manufacturerSerialNumber;
	private String lbxSerialNumber;
	private String customerManagementNo;
	private Integer iconType;
	private String customerManagementName;
	private String scmModel;
	private String lbxModel;
	private String latestLocation;
	private Double ido;
	private Double keido;
	private Double hourMeter;
	private Integer fuelLevel;
	private String latestUtcCommonDateTime;
	private Integer defLevelAdblueLevel;
	private ArrayList<PeriodicServiceDetailArrayListModel> serviceRequestList;




	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public Integer getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(Integer serialNumber) {
		this.serialNumber = serialNumber;
	}
	public Integer getMachineModelCategory() {
		return machineModelCategory;
	}
	public void setMachineModelCategory(Integer machineModelCategory) {
		this.machineModelCategory = machineModelCategory;
	}
	public String getManufacturerSerialNumber() {
		return manufacturerSerialNumber;
	}
	public void setManufacturerSerialNumber(String manufacturerSerialNumber) {
		this.manufacturerSerialNumber = manufacturerSerialNumber;
	}

	public String getLbxSerialNumber() {
		return lbxSerialNumber;
	}
	public void setLbxSerialNumber(String lbxSerialNumber) {
		this.lbxSerialNumber = lbxSerialNumber;
	}

	public String getCustomerManagementNo() {
		return customerManagementNo;
	}
	public void setCustomerManagementNo(String customerManagementNo) {
		this.customerManagementNo = customerManagementNo;
	}

	public Integer getIconType() {
		return iconType;
	}
	public void setIconType(Integer iconType) {
		this.iconType = iconType;
	}

	public String getCustomerManagementName() {
		return customerManagementName;
	}
	public void setCustomerManagementName(String customerManagementName) {
		this.customerManagementName = customerManagementName;
	}

	public String getScmModel() {
		return scmModel;
	}
	public void setScmModel(String scmModel) {
		this.scmModel = scmModel;
	}

	public String getLbxModel() {
		return lbxModel;
	}
	public void setLbxModel(String lbxModel) {
		this.lbxModel = lbxModel;
	}

	public String getLatestLocation() {
		return latestLocation;
	}
	public void setLatestLocation(String latestLocation) {
		this.latestLocation = latestLocation;
	}

	public Double getIdo() {
		return ido;
	}
	public void setIdo(Double ido) {
		this.ido = ido;
	}

	public Double getKeido() {
		return keido;
	}
	public void setKeido(Double keido) {
		this.keido = keido;
	}

	public Double getHourMeter() {
		return hourMeter;
	}
	public void setHourMeter(Double hourMeter) {
		this.hourMeter = hourMeter;
	}

	public String getLatestUtcCommonDateTime() {
		return latestUtcCommonDateTime;
	}
	public void setLatestUtcCommonDateTime(String latestUtcCommonDateTime) {
		this.latestUtcCommonDateTime = latestUtcCommonDateTime;
	}

	public Integer getFuelLevel() {
		return fuelLevel;
	}
	public void setFuelLevel(Integer fuelLevel) {
		this.fuelLevel = fuelLevel;
	}

	public Integer getDefLevelAdblueLevel() {
		return defLevelAdblueLevel;
	}
	public void setDefLevelAdblueLevel(Integer defLevelAdblueLevel) {
		this.defLevelAdblueLevel = defLevelAdblueLevel;
	}

	public ArrayList<PeriodicServiceDetailArrayListModel> getServiceRequestList() {
		return serviceRequestList;
	}
	public void setServiceRequestList(ArrayList<PeriodicServiceDetailArrayListModel> serviceRequestList) {
		this.serviceRequestList = serviceRequestList;
	}

}
