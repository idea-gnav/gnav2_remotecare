package com.lbxco.RemoteCAREApp.models;

public class StatisticsListModel{

	// **««« 2019/01/11  LBNÎì  tF[Y2¼ÇÁvÎ ÇÁ  «««**//
	private String countryCd;
	// **ªªª 2019/01/11  LBNÎì  tF[Y2¼ÇÁvÎ ÇÁ  ªªª**//
	private String groupTitle;
	private Double circleObjSize;
	private Double aggregateTime;
	private Double aggregateUnit;


	public StatisticsListModel() {}

	// **««« 2019/01/11  LBNÎì  tF[Y2¼ÇÁvÎ ÇÁ  «««**//
	// public StatisticsListModel(String groupTitle, Double circleObjSize, Double aggregateTime, Double aggregateUnit) {
	public StatisticsListModel(String groupTitle, Double circleObjSize, Double aggregateTime, Double aggregateUnit, String countryCd) {
		this.countryCd = countryCd;
	// **ªªª 2019/01/11  LBNÎì  tF[Y2¼ÇÁvÎ ÇÁ  ªªª**//
		this.groupTitle = groupTitle;
		this.circleObjSize = circleObjSize;
		this.aggregateTime =aggregateTime;
		this.aggregateUnit = aggregateUnit;

	}

	// **««« 2019/01/11  LBNÎì  tF[Y2¼ÇÁvÎ ÇÁ  «««**//
	public void setCountryCd(String countryCd) {
		this.countryCd = countryCd;
	}
	public String getCountryCd() {
		return countryCd;
	}
	// **ªªª 2019/01/11  LBNÎì  tF[Y2¼ÇÁvÎ ÇÁ  ªªª**//

	public void setGroupTitle(String groupTitle) {
		this.groupTitle = groupTitle;
	}
	public String getGroupTitle() {
		return groupTitle;
	}

	public void setCircleObjSize(Double circleObjSize) {
		this.circleObjSize = circleObjSize;
	}
	public Double getCircleObjSize() {
		return circleObjSize;
	}

	public void setAggregateTime(Double aggregateTime) {
		this.aggregateTime = aggregateTime;
	}
	public Double getAggregateTime() {
		return aggregateTime;
	}

	public void setAggregateUnit(Double aggregateUnit) {
		this.aggregateUnit = aggregateUnit;
	}
	public Double getAggregateUnit() {
		return aggregateUnit;
	}

}