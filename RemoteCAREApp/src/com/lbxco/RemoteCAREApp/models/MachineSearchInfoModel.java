package com.lbxco.RemoteCAREApp.models;

import java.util.ArrayList;
import java.util.List;

public class MachineSearchInfoModel {

	private Integer statusCode;
	private List<String> lbxModelList;
	private List<PsmModel> psmList;
	private String psmDisplay;
	private List<RmModel> rmList;
	private String rmDisplay;
	// **«««@2019/02/01  iDEARΊ  Xebv2@π·iXgΗΑ «««**//
	private ArrayList<MaintainecePartItemArrayListModel> maintainecePartItemList;
	// **ͺͺͺ@2019/01/07  iDEARΊ  Xebv2@π·iXgΗΑ ͺͺͺ**//
	private Integer dealerNameFlg;
	private Integer customerNameFlg;

	public MachineSearchInfoModel() {
	}

	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public List<String> getLbxModelList() {
		return lbxModelList;
	}
	public void setLbxModelList(List<String> lbxModelList) {
		this.lbxModelList = lbxModelList;
	}

	public String getPsmDisplay() {
		return psmDisplay;
	}
	public void setPsmDisplay(String psmDisplay) {
		this.psmDisplay = psmDisplay;
	}

	public List<PsmModel> getPsmList() {
		return psmList;
	}
	public void setPsmList(List<PsmModel> psmList) {
		this.psmList = psmList;
	}

	public String getRmDisplay() {
		return rmDisplay;
	}
	public void setRmDisplay(String rmDisplay) {
		this.rmDisplay = rmDisplay;
	}

	public List<RmModel> getRmList() {
		return rmList;
	}
	public void setRmList(List<RmModel> rmList) {
		this.rmList = rmList;
	}

	// **«««@2019/02/01  iDEARΊ  Xebv2@π·iXgΗΑ «««**//
	public ArrayList<MaintainecePartItemArrayListModel> getmaintainecePartItemList() {
		return maintainecePartItemList;
	}
	public void setMaintainecePartItemList(ArrayList<MaintainecePartItemArrayListModel> maintainecePartItemList) {
		this.maintainecePartItemList = maintainecePartItemList;
	}
	// **ͺͺͺ@2019/02/01  iDEARΊ  Xebv2@π·iXgΗΑ ͺͺͺ**//

	public Integer getDealerNameFlg() {
		return dealerNameFlg;
	}
	public void setDealerNameFlg(Integer dealerNameFlg) {
		this.dealerNameFlg = dealerNameFlg;
	}

	//customerName
	public Integer getCustomerNameFlg() {
		return customerNameFlg;
	}
	public void setCustomerNameFlg(Integer customerNameFlg) {
		this.customerNameFlg = customerNameFlg;
	}
}




