package com.lbxco.RemoteCAREApp.models;

import java.util.ArrayList;
import java.util.HashMap;

public class UserStatusModel {

	private Integer statusCode;
	private Integer unitSelect;
	private Integer kibanSelect;
	private Integer pdfFlg;
	private Integer appLogFlg;
	private Integer searchRangeDistance;
	private Integer statisticsFlg;
	// **↓↓↓ 2018/12/10 LBN石川 フェーズ2機械アイコン対応    ↓↓↓**//
	private ArrayList<HashMap<String,Object>> machineIconList;
	// **↑↑↑ 2018/12/10 LBN石川 フェーズ2機械アイコン対応    ↑↑↑**//
	// **↓↓↓ 2019/01/31  LBN石川  ステップ2フェーズ3代理店向けお知らせ 追加  ↓↓↓**//
	private Integer dealerNewsFlg;
	// **↑↑↑ 2019/01/31  LBN石川  ステップ2フェーズ3代理店向けお知らせ 追加  ↑↑↑**//

	public UserStatusModel(){
	}


	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public Integer getUnitSelect() {
		return unitSelect;
	}
	public void setUnitSelect(Integer unitSelect) {
		this.unitSelect = unitSelect;
	}

	public Integer getKibanSelect() {
		return kibanSelect;
	}
	public void setKibanSelect(Integer kibanSelect) {
		this.kibanSelect = kibanSelect;
	}

	public Integer getPdfFlg() {
		return pdfFlg;
	}
	public void setPdfFlg(Integer pdfFlg) {
		this.pdfFlg = pdfFlg;
	}

	public Integer getAppLogFlg() {
		return appLogFlg;
	}
	public void setAppLogFlg(Integer appLogFlg) {
		this.appLogFlg = appLogFlg;
	}

	public Integer getSearchRangeDistance() {
		return searchRangeDistance;
	}
	public void setSearchRangeDistance(Integer searchRangeDistance) {
		this.searchRangeDistance = searchRangeDistance;
	}

	public Integer getStatisticsFlg() {
		return statisticsFlg;
	}
	public void setStatisticsFlg(Integer statisticsFlg) {
		this.statisticsFlg = statisticsFlg;
	}

	// **↓↓↓ 2018/12/10 LBN石川 フェーズ2機械アイコン対応    ↓↓↓**//
	public ArrayList<HashMap<String, Object>> getMachineIconList() {
		return machineIconList;
	}
	public void setMachineIconList(ArrayList<HashMap<String, Object>> machineIconList) {
		this.machineIconList = machineIconList;
	}
	// **↑↑↑ 2018/12/10 LBN石川 フェーズ2機械アイコン対応    ↑↑↑**//

	// **↓↓↓ 2019/01/31  LBN石川  ステップ2フェーズ3代理店向けお知らせ 追加  ↓↓↓**//
	public Integer getDealerNewsFlg() {
		return dealerNewsFlg;
	}
	public void setDealerNewsFlg(Integer dealerNewsFlg) {
		this.dealerNewsFlg = dealerNewsFlg;
	}
	// **↑↑↑ 2019/01/31  LBN石川  ステップ2フェーズ3代理店向けお知らせ 追加  ↑↑↑**//


}
