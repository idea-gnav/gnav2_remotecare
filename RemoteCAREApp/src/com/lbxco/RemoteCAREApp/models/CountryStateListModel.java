package com.lbxco.RemoteCAREApp.models;

import java.util.List;

public class CountryStateListModel extends BaseModel{
	
	private Double mapIdo;
	private Double mapKeido;
	private Double mapScaleSize;
	private List<CountryListModel> countryList;
	
	public CountryStateListModel() {}
	
	
	public void setMapIdo(Double mapIdo) {
		this.mapIdo = mapIdo;
	}
	public Double getMapIdo() {
		return mapIdo;
	}
	
	public void setMapKeido(Double mapKeido) {
		this.mapKeido = mapKeido;
	}
	public Double getMapKeido() {
		return mapKeido;
	}
	
	//mapScaleSize;
	public void setMapScaleSize(Double mapScaleSize) {
		this.mapScaleSize = mapScaleSize;
	}
	public Double getMapScaleSize() {
		return mapScaleSize;
	}
	
	
	public void setCountryList(List<CountryListModel> countryList) {
		this.countryList = countryList;
	}
	public List<CountryListModel> getCountryList() {
		return countryList;
	}
	

}