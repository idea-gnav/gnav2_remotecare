package com.lbxco.RemoteCAREApp.models;

public class MachineDetailModel extends BaseModel {

	private Integer statusCode;
	private Integer serialNumber;
	private Integer machineModelCategory;
	private String managementNo;
	private String manufacturerSerialNumber;
	private String lbxSerialNumber;
	private String lbxModel;
    // **««« 2021/02/03 iDEARº J®«ÏXÎ «««**//
//  private Double engineSerialNumber;
    private String engineSerialNumber;
    // **ªªª 2021/02/03 iDEARº J®«ÏXÎ ªªª**//
	private String deliveryDate;
	private double hourMeter;
	private String latestLocation;
	private String latestUtcCommonDateTime;
	private Integer monitorFuelBar;
	private Integer defLevelAdblueLevel;
	private String customerName;
	private String dealerName;
	// **«««@2018/11/28  iDEARº  Xebv2@ãXdbÔÇÁ «««**//
	private String dealerTel;
	// **ªªª@2018/11/28  iDEARº  Xebv2@ãXdbÔÇÁ ªªª**//
	private String psm;
	// **«««@2018/04/11  LBNÎì  v]sïÇä  No34Î ÇÁ «««**//
	private String psmTel;
	// **ªªª@2018/04/11  LBNÎì  v]sïÇä  No34Î ÇÁ ªªª**//
	private String regionalManager;
	private String computerAVersion;
	private String computerBVersion;
	private String x4ControllerPartNo;
	private String x4ControllerSerialNo;
	private String q4000Version;
	private Double ido;
	private Double keido;
	private Integer favoriteDelFlg;
	// **«««@2018/07/24 LBNÎì  v]Î ¨CÉüèDTCðÌæ¾ ÇÁ «««**//
	private Integer dtcBadgeCount;
	// **ªªª@2018/07/24 LBNÎì  v]Î ¨CÉüèDTCðÌæ¾ ÇÁ ªªª**//
	// **««« 2018/12/10 LBNÎì tF[Y2@BACRÎ    «««**//
	private Integer iconType;
	// **ªªª 2018/12/10 LBNÎì tF[Y2@BACRÎ    ªªª**//

	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public Integer getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(Integer serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Integer getMachineModelCategory() {
		return machineModelCategory;
	}
	public void setMachineModelCategory(Integer machineModelCategory) {
		this.machineModelCategory = machineModelCategory;
	}

	public String getManagementNo() {
		return managementNo;
	}
	public void setManagementNo(String managementNo) {
		this.managementNo = managementNo;
	}

	public String getManufacturerSerialNumber() {
		return manufacturerSerialNumber;
	}
	public void setManufacturerSerialNumber(String manufacturerSerialNumber) {
		this.manufacturerSerialNumber = manufacturerSerialNumber;
	}

	public String getLbxSerialNumber() {
		return lbxSerialNumber;
	}
	public void setLbxSerialNumber(String lbxSerialNumber) {
		this.lbxSerialNumber = lbxSerialNumber;
	}

	public String getLbxModel() {
		return lbxModel;
	}
	public void setLbxModel(String lbxModel) {
		this.lbxModel = lbxModel;
	}

	// **««« 2021/02/03 iDEARº J®«ÏXÎ «««**//
//	public Integer getEngineSerialNumber() {
//		return engineSerialNumber;
//	}
//	public void setEngineSerialNumber(Integer engineSerialNumber) {
//		this.engineSerialNumber = engineSerialNumber;
//	}
	public String getEngineSerialNumber() {
		return engineSerialNumber;
	}
	public void setEngineSerialNumber(String engineSerialNumber) {
		this.engineSerialNumber = engineSerialNumber;
	}
	// **ªªª 2021/02/03 iDEARº J®«ÏXÎ ªªª**//

	public String getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public double getHourMeter() {
		return hourMeter;
	}
	public void setHourMeter(double hourMeter) {
		this.hourMeter = hourMeter;
	}

	public String getLatestLocation() {
		return latestLocation;
	}
	public void setLatestLocation(String latestLocation) {
		this.latestLocation = latestLocation;
	}

	public String getLatestUtcCommonDateTime() {
		return latestUtcCommonDateTime;
	}
	public void setLatestUtcCommonDateTime(String latestUtcCommonDateTime) {
		this.latestUtcCommonDateTime = latestUtcCommonDateTime;
	}

	public Integer getMonitorFuelBar() {
		return monitorFuelBar;
	}
	public void setMonitorFuelBar(Integer monitorFuelBar) {
		this.monitorFuelBar = monitorFuelBar;
	}

	public Integer getDefLevelAdblueLevel() {
		return defLevelAdblueLevel;
	}
	public void setDefLevelAdblueLevel(Integer defLevelAdblueLevel) {
		this.defLevelAdblueLevel = defLevelAdblueLevel;
	}

	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getDealerName() {
		return dealerName;
	}
	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}

	// **«««@2018/11/28  iDEARº  Xebv2@ãXdbÔÇÁ «««**//
	public String getDealerTel() {
		return dealerTel;
	}
	public void setDealerTel(String dealerTel) {
		this.dealerTel = dealerTel;
	}
	// **ªªª@2018/11/28  iDEARº  Xebv2@ãXdbÔÇÁ ªªª**//

	public String getPsm() {
		return psm;
	}
	public void setPsm(String psm) {
		this.psm = psm;
	}

	public String getPsmTel() {
		return psmTel;
	}
	public void setPsmTel(String psmTel) {
		this.psmTel = psmTel;
	}

	public String getRegionalManager() {
		return regionalManager;
	}
	public void setRegionalManager(String regionalManager) {
		this.regionalManager = regionalManager;
	}

	public String getComputerAVersion() {
		return computerAVersion;
	}
	public void setComputerAVersion(String computerAVersion) {
		this.computerAVersion = computerAVersion;
	}

	public String getComputerBVersion() {
		return computerBVersion;
	}
	public void setComputerBVersion(String computerBVersion) {
		this.computerBVersion = computerBVersion;
	}

	public String getX4ControllerPartNo() {
		return x4ControllerPartNo;
	}
	public void setX4ControllerPartNo(String x4ControllerPartNo) {
		this.x4ControllerPartNo = x4ControllerPartNo;
	}

	public String getX4ControllerSerialNo() {
		return x4ControllerSerialNo;
	}
	public void setX4ControllerSerialNo(String x4ControllerSerialNo) {
		this.x4ControllerSerialNo = x4ControllerSerialNo;
	}

	public String getQ4000Version() {
		return q4000Version;
	}
	public void setQ4000Version(String q4000Version) {
		this.q4000Version = q4000Version;
	}


	public Double getIdo() {
		return ido;
	}
	public void setIdo(Double ido) {
		this.ido = ido;
	}


	public Double getKeido() {
		return keido;
	}
	public void setKeido(Double keido) {
		this.keido = keido;
	}


	public Integer getFavoriteDelFlg() {
		return favoriteDelFlg;
	}
	public void setFavoriteDelFlg(Integer favoriteDelFlg) {
		this.favoriteDelFlg = favoriteDelFlg;
	}

	// **«««@2018/07/24 LBNÎì  v]Î ¨CÉüèDTCðÌæ¾ ÇÁ «««**//
	public Integer getDtcBadgeCount() {
		return dtcBadgeCount;
	}
	public void setDtcBadgeCount(Integer dtcBadgeCount) {
		this.dtcBadgeCount = dtcBadgeCount;
	}
	// **ªªª@2018/07/24 LBNÎì  v]Î ¨CÉüèDTCðÌæ¾ ÇÁ ªªª**//
	// **««« 2018/12/10 LBNÎì tF[Y2@BACRÎ    «««**//
	public Integer getIconType() {
		return iconType;
	}
	public void setIconType(Integer iconType) {
		this.iconType = iconType;
	}
	// **ªªª 2018/12/10 LBNÎì tF[Y2@BACRÎ    ªªª**//

}