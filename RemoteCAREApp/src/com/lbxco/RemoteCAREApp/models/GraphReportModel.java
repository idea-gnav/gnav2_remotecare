package com.lbxco.RemoteCAREApp.models;

import java.util.List;

//public class GraphReportModel extends BaseModel{
public class GraphReportModel{
	private double statusCode;
	private String yearMonth;
	private Integer serialNumber;
	private double machineModelCategory;
	private String manufacturerSerialNumber;
	private String lbxSerialNumber;
	private String customerManagementNo;
	private String customerManagementName;
	private String scmModel;
	private String lbxModel;
	private String latestLocation;
	private Double ido;
	private Double keido;
	private double hourMeter;
	private Integer fuelLevel;
	private String latestUtcCommonDateTime;
	private Integer defLevelAdblueLevel;

	private List<ReportOperatingListModel> OperatingPartialList;
	private List<ReportOperatingListModel> OperatingWeekTotalList;

	private double engineOperatingTimeTotal;
	private double machineOperatingTimeTotal;
	private double idlePercentageTotal;
	private double fuelConsumptionTotal;
	private double fuelEffciencyTotal;
	private double defConsumpitonTotal;
	// **««« 2018/12/10 LBNÎì tF[Y2@BACRÎ    «««**//
	private Integer iconType;
	// **ªªª 2018/12/10 LBNÎì tF[Y2@BACRÎ    ªªª**//


	public String getYearMonth() {
		return yearMonth;
	}
	public void setYearMonth(String yearMonth) {
		this.yearMonth = yearMonth;
	}
	public Integer getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(Integer serialNumber) {
		this.serialNumber = serialNumber;
	}
	public double getMachineModelCategory() {
		return machineModelCategory;
	}
	public void setMachineModelCategory(double machineModelCategory) {
		this.machineModelCategory = machineModelCategory;
	}
	public String getManufacturerSerialNumber() {
		return manufacturerSerialNumber;
	}
	public void setManufacturerSerialNumber(String manufacturerSerialNumber) {
		this.manufacturerSerialNumber = manufacturerSerialNumber;
	}
	public String getLbxSerialNumber() {
		return lbxSerialNumber;
	}
	public void setLbxSerialNumber(String lbxSerialNumber) {
		this.lbxSerialNumber = lbxSerialNumber;
	}
	public String getCustomerManagementNo() {
		return customerManagementNo;
	}
	public void setCustomerManagementNo(String customerManagementno) {
		this.customerManagementNo = customerManagementno;
	}
	public String getCustomerManagementName() {
		return customerManagementName;
	}
	public void setCustomerManagementName(String customerManagementName) {
		this.customerManagementName = customerManagementName;
	}
	public String getScmModel() {
		return scmModel;
	}
	public void setScmModel(String scmModel) {
		this.scmModel = scmModel;
	}
	public String getLbxModel() {
		return lbxModel;
	}
	public void setLbxModel(String lbxModel) {
		this.lbxModel = lbxModel;
	}
	public String getLatestLocation() {
		return latestLocation;
	}
	public void setLatestLocation(String latestLocation) {
		this.latestLocation = latestLocation;
	}
	public Double getIdo() {
		return ido;
	}
	public void setIdo(Double ido) {
		this.ido = ido;
	}
	public Double getKeido() {
		return keido;
	}
	public void setKeido(Double keido) {
		this.keido = keido;
	}
	public double getHourMeter() {
		return hourMeter;
	}
	public void setHourMeter(double hourMeter) {
		this.hourMeter = hourMeter;
	}
	public Integer getFuelLevel() {
		return fuelLevel;
	}
	public void setFuelLevel(Integer fuelLevel) {
		this.fuelLevel = fuelLevel;
	}
	public String getLatestUtcCommonDateTime() {
		return latestUtcCommonDateTime;
	}
	public void setLatestUtcCommonDateTime(String latestUtcCommonDateTime) {
		this.latestUtcCommonDateTime = latestUtcCommonDateTime;
	}
	public Integer getDefLevelAdblueLevel() {
		return defLevelAdblueLevel;
	}
	public void setDefLevelAdblueLevel(Integer defLevelAdblueLevel) {
		this.defLevelAdblueLevel = defLevelAdblueLevel;
	}
	public double getEngineOperatingTimeTotal() {
		return engineOperatingTimeTotal;
	}
	public void setEngineOperatingTimeTotal(double engineOperatingTimeTotal) {
		this.engineOperatingTimeTotal = engineOperatingTimeTotal;
	}
	public double getMachineOperatingTimeTotal() {
		return machineOperatingTimeTotal;
	}
	public void setMachineOperatingTimeTotal(double machineOperatingTimeTotal) {
		this.machineOperatingTimeTotal = machineOperatingTimeTotal;
	}
	public double getIdlePercentageTotal() {
		return idlePercentageTotal;
	}
	public void setIdlePercentageTotal(double idlePercentageTotal) {
		this.idlePercentageTotal = idlePercentageTotal;
	}
	public double getFuelConsumptionTotal() {
		return fuelConsumptionTotal;
	}
	public void setFuelConsumptionTotal(double fuelConsumptionTotal) {
		this.fuelConsumptionTotal = fuelConsumptionTotal;
	}
	public double getFuelEffciencyTotal() {
		return fuelEffciencyTotal;
	}
	public void setFuelEffciencyTotal(double fuelEffciencyTotal) {
		this.fuelEffciencyTotal = fuelEffciencyTotal;
	}
	public double getDefConsumpitonTotal() {
		return defConsumpitonTotal;
	}
	public void setDefConsumpitonTotal(double defConsumpitonTotal) {
		this.defConsumpitonTotal = defConsumpitonTotal;
	}
	public List<ReportOperatingListModel> getOperatingPartialList() {
		return OperatingPartialList;
	}
	public void setOperatingPartialList(List<ReportOperatingListModel> operatingPartialList) {
		OperatingPartialList = operatingPartialList;
	}
	public List<ReportOperatingListModel> getOperatingWeekTotalList() {
		return OperatingWeekTotalList;
	}
	public void setOperatingWeekTotalList(List<ReportOperatingListModel> operatingWeekTotalList) {
		OperatingWeekTotalList = operatingWeekTotalList;
	}
	public double getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(double statusCode) {
		this.statusCode = statusCode;
	}

	// **««« 2018/12/10 LBNÎì tF[Y2@BACRÎ    «««**//
	public Integer getIconType() {
		return iconType;
	}
	public void setIconType(Integer iconType) {
		this.iconType = iconType;
	}
	// **ªªª 2018/12/10 LBNÎì tF[Y2@BACRÎ    ªªª**//

}
