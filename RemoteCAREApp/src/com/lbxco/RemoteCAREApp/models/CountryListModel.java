package com.lbxco.RemoteCAREApp.models;

import java.util.List;

public class CountryListModel{
	
	private String countryCd;
	private String countryName;
	private Double countryIdo;
	private Double countryKeido;
	private Double countryMapScaleSize;
	private List<StateListModel> stateList;
	
	
	
	public CountryListModel() {
		
	}
	
	
	public void setCountryCd(String countryCd) {
		this.countryCd = countryCd;
	}
	public String getCountryShortCd() {
		return countryCd;
	}
	
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getCountryName() {
		return countryName;
	}

	public void setCountryIdo(Double countryIdo) {
		this.countryIdo = countryIdo;
	}
	public Double getCountryIdo() {
		return countryIdo;
	}
	
	public void setCountryKeido(Double countryKeido) {
		this.countryKeido = countryKeido;
	}
	public Double getCountryKeido() {
		return countryKeido;
	}

	// countryMapScaleSize
	public void setCountryMapScaleSize(Double countryMapScaleSize) {
		this.countryMapScaleSize = countryMapScaleSize;
	}
	public Double getCountryMapScaleSize() {
		return countryMapScaleSize;
	}
	
//	private List<StateListModel> stateList;
	public void setStateList(List<StateListModel> stateList) {
		this.stateList = stateList;
	}
	public List<StateListModel> getStateList(){
		return stateList;
	}
	
	
	
	
}