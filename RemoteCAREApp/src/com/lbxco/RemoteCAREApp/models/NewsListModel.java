package com.lbxco.RemoteCAREApp.models;

public class NewsListModel {


	private String newsDate;
	private String newsContents;

	// **«««@2019/01/31 LBNÎì  Xebv2tF[Y3 ãX¨mç¹Î ÇÁ «««**//
	private Integer newsId;
	private String newsDateFrom;
	private String newsDateTo;
	private String newsUrl;
	private Integer newsOpenFlg;
	private String dealerCd;

	private String newsLogoUrl;
	private Integer newsType;
	// **ªªª@2019/01/31 LBNÎì  Xebv2tF[Y3 ãX¨mç¹Î ÇÁ  ªªª**//




	public String getNewsContents() {
		return newsContents;
	}
	public void setNewsContents(String newsContents) {
		this.newsContents = newsContents;
	}

	public String getNewsDate() {
		return newsDate;
	}
	public void setNewsDate(String newsDate) {
		this.newsDate = newsDate;
	}

	// **«««@2019/01/31 LBNÎì  Xebv2tF[Y3 ãX¨mç¹Î ÇÁ «««**//

	public Integer getNewsId() {
		return newsId;
	}
	public void setNewsId(Integer newsId) {
		this.newsId = newsId;
	}

	public String getNewsDateFrom() {
		return newsDateFrom;
	}
	public void setNewsDateFrom(String newsDateFrom) {
		this.newsDateFrom = newsDateFrom;
	}

	public String getNewsDateTo() {
		return newsDateTo;
	}
	public void setNewsDateTo(String newsDateTo) {
		this.newsDateTo = newsDateTo;
	}

	public String getNewsUrl() {
		return newsUrl;
	}
	public void setNewsUrl(String newsUrl) {
		this.newsUrl = newsUrl;
	}

	public Integer getNewsOpenFlg() {
		return newsOpenFlg;
	}
	public void setNewsOpenFlg(Integer newsOpenFlg) {
		this.newsOpenFlg = newsOpenFlg;
	}

	public String getDealerCd() {
		return dealerCd;
	}
	public void setDealerCd(String dealerCd) {
		this.dealerCd = dealerCd;
	}


	public String getNewsLogoUrl() {
		return newsLogoUrl;
	}
	public void setNewsLogoUrl(String newsLogoUrl) {
		this.newsLogoUrl = newsLogoUrl;
	}

	public Integer getNewsType() {
		return newsType;
	}
	public void setNewsType(Integer newsType) {
		this.newsType = newsType;
	}
	// **ªªª@2019/01/31 LBNÎì  Xebv2tF[Y3 ãX¨mç¹Î ÇÁEXV  ªªª**//





}
