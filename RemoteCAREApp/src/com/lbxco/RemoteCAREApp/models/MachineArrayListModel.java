package com.lbxco.RemoteCAREApp.models;

//import java.sql.Date;

public class MachineArrayListModel extends BaseModel {

	private Integer serialNumber;
	private Integer machineModelCategory;
	private String manufacturerSerialNumber;
	private String lbxSerialNumber;
	private String customerManagementNo;
	private String customerManagementName;
	private String scmModel;
	private String lbxModel;
	private String latestLocation;
	private Double ido;
	private Double keido;
	private Double hourMeter;
	private String latestUtcCommonDateTime;
	private Integer fuelLevel;
	private Integer defLevel;
	private Integer favoriteDelFlg;
	// **««« 2018/12/10 LBNÎì tF[Y2@BACRÎ    «««**//
	private Integer iconType;
	// **ªªª 2018/12/10 LBNÎì tF[Y2@BACRÎ    ªªª**//
	// **«««@2019/01/07  iDEARº  Xebv2@èú®õõÇÁ «««**//
	private Integer periodicServiceIconType;
	// **ªªª@2019/01/07  iDEARº  Xebv2@èú®õõÇÁ ªªª**//
	// **««« 2019/08/01 iDEARº tF[Y3 êDTCACRíÊtO@ÇÁ «««**//
	private Integer dtcIconType;
	// **ªªª 2019/08/01 iDEARº tF[Y3 êDTCACRíÊtO@ÇÁ ªªª**//



	public Integer getSerialNumber() {
		return this.serialNumber;
	}
	public void setSerialNumber(Integer serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Integer getMachineModelCategory() {
		return this.machineModelCategory;
	}
	public void setMachineModelCategory(Integer machineModelCategory) {
		this.machineModelCategory = machineModelCategory;
	}

	public String getManufacturerSerialNumber() {
		return this.manufacturerSerialNumber;
	}
	public void setManufacturerSerialNumber(String manufacturerSerialNumber) {
		this.manufacturerSerialNumber = manufacturerSerialNumber;
	}

	public String getLbxSerialNumber() {
		return this.lbxSerialNumber;
	}
	public void setLbxSerialNumber(String lbxSerialNumber) {
		this.lbxSerialNumber = lbxSerialNumber;
	}

	public String getCustomerManagementNo() {
		return this.customerManagementNo;
	}
	public void setCustomerManagementNo(String customerManagementNo) {
		this.customerManagementNo = customerManagementNo;
	}

	public String getCustomerManagementName() {
		return this.customerManagementName;
	}
	public void setCustomerManagementName(String customerManagementName) {
		this.customerManagementName = customerManagementName;
	}
	public String getScmModel() {
		return scmModel;
	}
	public void setScmModel(String scmModel) {
		this.scmModel = scmModel;
	}

	public String getLbxModel() {
		return lbxModel;
	}
	public void setLbxModel(String lbxModel) {
		this.lbxModel = lbxModel;
	}

	public String getLatestLocation() {
		return latestLocation;
	}
	public void setLatestLocation(String latestLocation) {
		this.latestLocation = latestLocation;
	}

	public Double getIdo() {
		return ido;
	}
	public void setIdo(Double ido) {
		this.ido = ido;
	}

	public Double getKeido() {
		return keido;
	}
	public void setKeido(Double keido) {
		this.keido = keido;
	}

	public Double getHourMeter() {
		return hourMeter;
	}
	public void setHourMeter(Double hourMeter) {
		this.hourMeter = hourMeter;
	}

	public String getLatestUtcCommonDateTime() {
		return latestUtcCommonDateTime;
	}
	public void setLatestUtcCommonDateTime(String latestUtcCommonString) {
		this.latestUtcCommonDateTime = latestUtcCommonString;
	}

	public Integer getFuelLevel() {
		return fuelLevel;
	}
	public void setFuelLevel(Integer fuelLevel) {
		this.fuelLevel = fuelLevel;
	}

	public Integer getDefLevel() {
		return defLevel;
	}
	public void setDefLevel(Integer defLevel) {
		this.defLevel = defLevel;
	}

	public Integer getFavoriteDelFlg() {
		return favoriteDelFlg;
	}
	public void setFavoriteDelFlg(Integer favoriteDelFlg) {
		this.favoriteDelFlg = favoriteDelFlg;
	}

	// **««« 2018/12/10 LBNÎì tF[Y2@BACRÎ    «««**//
	public Integer getIconType() {
		return iconType;
	}
	public void setIconType(Integer iconType) {
		this.iconType = iconType;
	}
	// **ªªª 2018/12/10 LBNÎì tF[Y2@BACRÎ    ªªª**//

	// **«««@2019/01/07  iDEARº  Xebv2@èú®õõÇÁ «««**//
	public Integer getPeriodicServiceIconType() {
		return periodicServiceIconType;
	}
	public void setPeriodicServiceIconType(Integer periodicServiceIconType) {
		this.periodicServiceIconType = periodicServiceIconType;
	}
	// **ªªª@2019/01/07  iDEARº  Xebv2@èú®õõÇÁ ªªª**//

	// **««« 2019/08/01 iDEARº tF[Y3 êDTCACRíÊtO@ÇÁ «««**//
	public Integer getDtcIconType() {
		return dtcIconType;
	}
	public void setDtcIconType(Integer dtcIconType) {
		this.dtcIconType = dtcIconType;
	}
	// **««« 2019/08/01 iDEARº tF[Y3 êDTCACRíÊtO@ÇÁ «««**//

}
