package com.lbxco.RemoteCAREApp.models;

public class MachineFavoriteArraylistModel {

	private Integer serialNumber;
	private String manufacturerSerialNumber;
	private String lbxSerialNumber;
	private String customerManagementNo;

	public Integer getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(Integer serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getManufacturerSerialNumber() {
		return manufacturerSerialNumber;
	}
	public void setManufacturerSerialNumber(String manufacturerSerialNumber) {
		this.manufacturerSerialNumber = manufacturerSerialNumber;
	}
	public String getLbxSerialNumber() {
		return lbxSerialNumber;
	}
	public void setLbxSerialNumber(String lbxSerialNumber) {
		this.lbxSerialNumber = lbxSerialNumber;
	}
	public String getCustomerManagementNo() {
		return customerManagementNo;
	}
	public void setCustomerManagementNo(String customerManagementNo) {
		this.customerManagementNo = customerManagementNo;
	}

}
