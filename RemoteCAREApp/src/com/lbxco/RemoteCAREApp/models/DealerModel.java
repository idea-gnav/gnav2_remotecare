package com.lbxco.RemoteCAREApp.models;

public class DealerModel{

	private String dealerCd;
	private String dealerName;
	private String dealerLogoUrl;

	public DealerModel() {}

	public void setDealerCd(String dealerCd) {
		this.dealerCd = dealerCd;
	}
	public String getDealerCd() {
		return dealerCd;
	}

	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}
	public String getDealerName() {
		return dealerName;
	}

	public void setDealerLogoUrl(String dealerLogoUrl) {
		this.dealerLogoUrl = dealerLogoUrl;
	}
	public String getDealerLogoUrl() {
		return dealerLogoUrl;
	}




}