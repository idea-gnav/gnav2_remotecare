package com.lbxco.RemoteCAREApp.models;

public class MaintainecePartItemArrayListModel{

	private Integer maintainecePartNumber;
    private String maintainecePartName;

	public Integer getMaintainecePartNumber() {
		return maintainecePartNumber;
	}
	public void setMaintainecePartNumber(Integer maintainecePartNumber) {
		this.maintainecePartNumber = maintainecePartNumber;
	}

	public String getMaintainecePartName() {
		return maintainecePartName;
	}
	public void setMaintainecePartName(String maintainecePartName) {
		this.maintainecePartName = maintainecePartName;
	}


}