package com.lbxco.RemoteCAREApp.models;



public class ReportOperatingListModel extends BaseModel{
	String date;
	String dateFrom;
	String dateTo;
	double engineOperatingTime;
	double machineOperatingTime;
	double idlePercentage;
	double fuelConsumption;
	double fuelEffciency;
	double defConsumpiton;
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	
	public String getDateTo() {
		return dateTo;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
	
	public double getEngineOperatingTime() {
		return engineOperatingTime;
	}
	public void setEngineOperatingTime(double engineOperatingTime) {
		this.engineOperatingTime = engineOperatingTime;
	}
	
	public double getMachineOperatingTime() {
		return machineOperatingTime;
	}
	public void setMachineOperatingTime(double machineOperatingTime) {
		this.machineOperatingTime = machineOperatingTime;
	}
	
	public double getIdlePercentage() {
		return idlePercentage;
	}
	public void setIdlePercentage(double idlePercentage) {
		this.idlePercentage = idlePercentage;
	}
	
	public double getFuelConsumption() {
		return fuelConsumption;
	}
	public void setFuelConsumption(double fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	}
	
	public double getFuelEffciency() {
		return fuelEffciency;
	}
	public void setFuelEffciency(double fuelEffciency) {
		this.fuelEffciency = fuelEffciency;
	}
	
	public double getDefConsumpiton() {
		return defConsumpiton;
	}
	public void setDefConsumpiton(double defConsumpiton) {
		this.defConsumpiton = defConsumpiton;
	}
}
