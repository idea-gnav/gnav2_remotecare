package com.lbxco.RemoteCAREApp.models;

public class PsmModel{
	
	private String psmId;
	private String psmName;
	
	public PsmModel() {}
	
	public void setPsmId(String psmId) {
		this.psmId = psmId;
	}
	public String getPsmId() {
		return psmId;
	}
	
	public void setPsmName(String psmName) {
		this.psmName = psmName;
	}
	public String getPsmName() {
		return psmName;
	}

}