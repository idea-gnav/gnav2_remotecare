package com.lbxco.RemoteCAREApp.models;

public class DailyReportModel extends BaseModel{

	private Integer serialNumber;
	private Integer machineModelCategory;
	private String manufacturerSerialNumber;
	private String lbxSerialNumber;
	private String customerManagementNo;
	private String date;
	private String nippoData;
	private double engineOperatingTime;
	private double machineOperatingTime;
	private double idlePercentage;
	private double fuelConsumption;
	private Integer fuelLevelDaily;
	private Integer defLevelDaily;
	private double fuelEffciency;
	private double defConsumpiton;
	private double coolantMax;
	private double oilTempartureMax;
	private Integer autoRegenStart;
	private Integer autoRegenCompleted;
	private String customerManagementName;
	private String scmModel;
	private String lbxModel;
	private String latestLocation;
	private Double ido;
	private Double keido;
	private Double hourMeter;
	private Integer fuelLevel;
	private String latestUtcCommonDateTime;
	private Integer defLevelAdblueLevel;
	// **««« 2018/12/10 LBNÎì tF[Y2@BACRÎ    «««**//
	private Integer iconType;
	// **ªªª 2018/12/10 LBNÎì tF[Y2@BACRÎ    ªªª**//


	public Integer getserialNumber() {
		return this.serialNumber;
	}
	public void setserialNumber(Integer serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Integer getMachineModelCategory() {
		return this.machineModelCategory;
	}
	public void setMachineModelCategory(Integer machineModelCategory) {
		this.machineModelCategory = machineModelCategory;
	}

	public String getManufacturerSerialNumber() {
		return this.manufacturerSerialNumber;
	}
	public void setManufacturerSerialNumber(String manufacturerSerialNumber) {
		this.manufacturerSerialNumber = manufacturerSerialNumber;
	}

	public String getLbxSerialNumber() {
		return this.lbxSerialNumber;
	}
	public void setLbxSerialNumber(String lbxSerialNumber) {
		this.lbxSerialNumber = lbxSerialNumber;
	}

	public String getCustomerManagementNo() {
		return this.customerManagementNo;
	}
	public void setCustomerManagementNo(String customerManagementNo) {
		this.customerManagementNo = customerManagementNo;
	}

	public String getDate() {
		return this.date;
	}
	public void setDate(String date) {
		this.date = date;
	}

	public String getNippoData() {
		return this.nippoData;
	}
	public void setNippoData(String nippoData) {
		this.nippoData = nippoData;
	}

	public double getEngineOperatingTime() {
		return this.engineOperatingTime;
	}
	public void setEngineOperatingTime(double engineOperatingTime) {
		this.engineOperatingTime = engineOperatingTime;
	}

	public double getMachineOperatingTime() {
		return this.machineOperatingTime;
	}
	public void setMachineOperatingTime(double machineOperatingTime) {
		this.machineOperatingTime = machineOperatingTime;
	}

	public Double getIdlePercentage() {
		return this.idlePercentage;
	}
	public void setIdlePercentage(Double idlePercentage) {
		this.idlePercentage = idlePercentage;
	}

	public Double getFuelConsumption() {
		return this.fuelConsumption;
	}
	public void setFuelConsumption(Double fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	}

	public Double getFuelEffciency() {
		return this.fuelEffciency;
	}
	public void setFuelEffciency(Double fuelEffciency) {
		this.fuelEffciency = fuelEffciency;
	}

	public Integer getFuelLevelDaily() {
		return this.fuelLevelDaily;
	}
	public void setFuelLevelDaily(Integer fuelLevelDaily) {
		this.fuelLevelDaily = fuelLevelDaily;
	}

	public double getDefConsumpiton() {
		return this.defConsumpiton;
	}
	public void setDefConsumpiton(double defConsumpiton) {
		this.defConsumpiton = defConsumpiton;
	}

	public Integer getDefLevelDaily() {
		return this.defLevelDaily;
	}
	public void setDefLevelDaily(Integer defLevelDaily) {
		this.defLevelDaily = defLevelDaily;
	}

	public double getCoolantMax() {
		return this.coolantMax;
	}
	public void setCoolantMax(double coolantMax) {
		this.coolantMax = coolantMax;
	}

	public double getOilTempartureMax() {
		return this.oilTempartureMax;
	}
	public void setOilTempartureMax(double oilTempartureMax) {
		this.oilTempartureMax = oilTempartureMax;
	}

	public Integer getAutoRegenStart() {
		return this.autoRegenStart;
	}
	public void setAutoRegenStart(Integer autoRegenStart) {
		this.autoRegenStart = autoRegenStart;
	}

	public Integer getAutoRegenCompleted() {
		return this.autoRegenCompleted;
	}
	public void setAutoRegenCompleted(Integer autoRegenCompleted) {
		this.autoRegenCompleted = autoRegenCompleted;
	}

	public String getCustomerManagementName() {
		return this.customerManagementName;
	}
	public void setCustomerManagementName(String customerManagementName) {
		this.customerManagementName = customerManagementName;
	}

	public String getScmModel() {
		return this.scmModel;
	}
	public void setScmModel(String scmModel) {
		this.scmModel = scmModel;
	}

	public String getLbxModel() {
		return this.lbxModel;
	}
	public void setLbxModel(String lbxModel) {
		this.lbxModel = lbxModel;
	}

	public String getLatestLocation() {
		return this.latestLocation;
	}
	public void setLatestLocation(String latestLocation) {
		this.latestLocation = latestLocation;
	}

	public Double getIdo() {
		return this.ido;
	}
	public void setIdo(Double ido) {
		this.ido = ido;
	}

	public Double getKeido() {
		return this.keido;
	}
	public void setKeido(Double keido) {
		this.keido = keido;
	}

	public Double getHourMeter() {
		return this.hourMeter;
	}
	public void setHourMeter(Double hourMeter) {
		this.hourMeter = hourMeter;
	}

	public Integer getFuelLevel() {
		return this.fuelLevel;
	}
	public void setFuelLevel(Integer fuelLevel) {
		this.fuelLevel = fuelLevel;
	}

	public String getLatestUtcCommonDateTime() {
		return this.latestUtcCommonDateTime;
	}
	public void setLatestUtcCommonDateTime(String latestUtcCommonDateTime) {
		this.latestUtcCommonDateTime = latestUtcCommonDateTime;
	}

	public Integer getDefLevelAdblueLevel() {
		return this.defLevelAdblueLevel;
	}
	public void setDefLevelAdblueLevel(Integer defLevelAdblueLevel) {
		this.defLevelAdblueLevel = defLevelAdblueLevel;
	}

	// **««« 2018/12/10 LBNÎì tF[Y2@BACRÎ    «««**//
	public Integer getIconType() {
		return iconType;
	}
	public void setIconType(Integer iconType) {
		this.iconType = iconType;
	}
	// **ªªª 2018/12/10 LBNÎì tF[Y2@BACRÎ    ªªª**//

}
