package com.lbxco.RemoteCAREApp.models;

import java.util.List;

public class LbxModelListModel {

	private Integer statusCode;  
	private List<String> lbxModelList;
	
	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public List<String> getLbxModelList() {
		return lbxModelList;
	}
	public void setLbxModelList(List<String> lbxModelList) {
		this.lbxModelList = lbxModelList;
	}
	

}
