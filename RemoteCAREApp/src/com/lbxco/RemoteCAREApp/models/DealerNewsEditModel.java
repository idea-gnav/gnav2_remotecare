package com.lbxco.RemoteCAREApp.models;

import java.util.ArrayList;

public class DealerNewsEditModel {

	private String userId;
	private String dealerLogoImage;
	private Integer dealerLogoImageEditFlg;
	private ArrayList<NewsModel> newsList;



	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getDealerLogoImage() {
		return dealerLogoImage;
	}
	public void setDealerLogoImage(String dealerLogoImage) {
		this.dealerLogoImage = dealerLogoImage;
	}


	public Integer getDealerLogoImageEditFlg() {
		return dealerLogoImageEditFlg;
	}
	public void setDealerLogoImageEditFlg(Integer dealerLogoImageEditFlg) {
		this.dealerLogoImageEditFlg = dealerLogoImageEditFlg;
	}


	public ArrayList<NewsModel> getNewsList() {
		return newsList;
	}
	public void setNewsList(ArrayList<NewsModel> newsList) {
		this.newsList = newsList;
	}



}
