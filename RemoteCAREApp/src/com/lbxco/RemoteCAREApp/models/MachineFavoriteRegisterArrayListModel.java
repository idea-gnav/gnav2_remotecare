package com.lbxco.RemoteCAREApp.models;

public class MachineFavoriteRegisterArrayListModel {
	private Integer serialNumber;
	private Integer favoriteDelFlg;

	public Integer getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(Integer serialNumber) {
		this.serialNumber = serialNumber;
	}
	public Integer getFavoriteFlg() {
		return favoriteDelFlg;
	}
	public void setFavoriteFlg(Integer favoriteFlg) {
		this.favoriteDelFlg = favoriteFlg;
	}
}
