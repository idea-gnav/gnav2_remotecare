package com.lbxco.RemoteCAREApp.models;

public class NewsModel {


	private Integer newsId;
	private String newsDateFrom;
	private String newsDateTo;
	private String newsContents;
	private String newsUrl;
	private Integer newsDelFlg;
	private String dealerCd;



	public String getNewsContents() {
		return newsContents;
	}
	public void setNewsContents(String newsContents) {
		this.newsContents = newsContents;
	}

	public Integer getNewsId() {
		return newsId;
	}
	public void setNewsId(Integer newsId) {
		this.newsId = newsId;
	}

	public String getNewsDateFrom() {
		return newsDateFrom;
	}
	public void setNewsDateFrom(String newsDateFrom) {
		this.newsDateFrom = newsDateFrom;
	}

	public String getNewsDateTo() {
		return newsDateTo;
	}
	public void setNewsDateTo(String newsDateTo) {
		this.newsDateTo = newsDateTo;
	}

	public String getNewsUrl() {
		return newsUrl;
	}
	public void setNewsUrl(String newsUrl) {
		this.newsUrl = newsUrl;
	}

	public Integer getNewsDelFlg() {
		return newsDelFlg;
	}
	public void setNewsDelFlg(Integer newsDelFlg) {
		this.newsDelFlg = newsDelFlg;
	}

	public String getDealerCd() {
		return dealerCd;
	}
	public void setDealerCd(String dealerCd) {
		this.dealerCd = dealerCd;
	}



}
