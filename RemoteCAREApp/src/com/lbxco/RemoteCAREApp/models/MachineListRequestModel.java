package com.lbxco.RemoteCAREApp.models;

import javax.ws.rs.FormParam;

public class MachineListRequestModel extends BaseModel {

	@FormParam("userId")
	private String userId;
	
	@FormParam("kibanSelect")
	private Integer kibanSelect;
	
	@FormParam("kibanInput")
	private String kibanInput;

	@FormParam("searchFlg")
	private Integer searchFlg;
	
	@FormParam("sortFlg")
	private Integer sortFlg;
	
	@FormParam("manufacturerSerialNumberFrom")
	private String manufacturerSerialNumberFrom;
	
	@FormParam("manufacturerSerialNumberTo")
	private String manufacturerSerialNumberTo;
	
	@FormParam("customerManagementNoFrom")
	private String customerManagementNoFrom;
	
	@FormParam("customerManagementNoTo")
	private String customerManagementNoTo;
	
	@FormParam("lbxSerialNumberFrom")
	private String lbxSerialNumberFrom; 
	
	@FormParam("lbxSerialNumberTo")
	private String lbxSerialNumberTo;
	
	@FormParam("lbxModel")
	private String lbxModel;
	
	@FormParam("dtcCode")
	private String dtcCode;
	
	@FormParam("warningCurrentlyInProgress")
	private String warningCurrentlyInProgress;
	
	@FormParam("warningDateFrom")
	private String warningDateFrom;
	
	@FormParam("warningDateTo")
	private String warningDateTo;
	
	@FormParam("favoriteSearchFlg")
	private String favoriteSearchFlg;
	
	@FormParam("startRecord")
	private Integer startRecord;

	
	
	
}
