package com.lbxco.RemoteCAREApp.models;

import java.util.List;

public class DealerNewsListModel extends BaseModel{

	private List<DealerModel> dealerList;
	private List<NewsListModel> newsList;
	private Integer newsCount;

	public DealerNewsListModel() {}

	public void setDealerList(List<DealerModel> dealerList) {
		this.dealerList = dealerList;
	}
	public List<DealerModel> getDealerList() {
		return dealerList;
	}

	public void setNewsList(List<NewsListModel> newsList) {
		this.newsList = newsList;
	}
	public List<NewsListModel> getNewsList() {
		return newsList;
	}

	public void setNewsCount(Integer newsCount) {
		this.newsCount = newsCount;
	}
	public Integer getNewsCount() {
		return newsCount;
	}


}