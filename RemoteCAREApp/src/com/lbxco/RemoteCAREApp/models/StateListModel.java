package com.lbxco.RemoteCAREApp.models;

import java.util.List;

public class StateListModel{

	private String stateCd;
	private String stateName;
	private Double stateIdo;
	private Double stateKeido;
	// **««« 2018/11/27  LBNÎì  tF[Y2ãXü¯vÎ ÇÁ  «««**//
	private String stateBubbleName;
	private List<String> regionNameList;
	// **ªªª 2018/11/27  LBNÎì  tF[Y2ãXü¯vÎ ÇÁ  ªªª**//

	public StateListModel() {}

	public void setStateCd(String stateCd) {
		this.stateCd = stateCd;
	}
	public String getStateCd() {
		return stateCd;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getStateName() {
		return stateName;
	}

	public void setStateIdo(Double stateIdo) {
		this.stateIdo = stateIdo;
	}
	public Double getStateIdo() {
		return stateIdo;
	}

	public void setStateKeido(Double stateKeido) {
		this.stateKeido = stateKeido;
	}
	public Double getStateKeido() {
		return stateKeido;
	}

	// **««« 2018/11/27  LBNÎì  tF[Y2ãXü¯vÎ ÇÁ  «««**//
	public void setStateBubbleName(String stateBubbleName) {
		this.stateBubbleName = stateBubbleName;
	}
	public String getStateBubbleName() {
		return stateBubbleName;
	}

	public void setRegionNameList(List<String> regionNameList) {
		this.regionNameList = regionNameList;
	}
	public List<String> getRegionNameList(){
		return regionNameList;
	}
	// **ªªª 2018/11/27  LBNÎì  tF[Y2ãXü¯vÎ ÇÁ  ªªª**//


}