package com.lbxco.RemoteCAREApp.models;

public class RmModel{
	
	private String rmId;
	private String rmName;
	
	public RmModel() {}
	
	public void setRmId(String rmId) {
		this.rmId = rmId;
	}
	public String getRmId() {
		return rmId;
	}
	
	public void setRmName(String rmName) {
		this.rmName = rmName;
	}
	public String getRmName() {
		return rmName;
	}

}
