package com.lbxco.RemoteCAREApp.resources;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.lbxco.RemoteCAREApp.Constants;
import com.lbxco.RemoteCAREApp.ConvertUtil;
import com.lbxco.RemoteCAREApp.annotations.Authorized;
import com.lbxco.RemoteCAREApp.ejbs.statistics.IStatisticsService;
import com.lbxco.RemoteCAREApp.ejbs.user.IUserService;
import com.lbxco.RemoteCAREApp.models.ReturnContainer;
import com.lbxco.RemoteCAREApp.models.StatisticsInfoModel;
import com.lbxco.RemoteCAREApp.models.StatisticsListModel;
import com.lbxco.RemoteCAREApp.util.PropertyUtil;


@Path("/statisticsInfo")
@Stateless
public class StatisticsInfoResource {

	private static final Logger logger = Logger.getLogger(StatisticsInfoResource.class.getName());

	private String languageCd;

	private String countryCds;

	private String timeFrom;
	private String timeTo;
	private String groupTitleFormat;

	private String scaleType;
	private double maxDayAvg;
	private double maxMonthAvg;
	private double maxUnit;

	private List<String> countrySizePattern;
	private List<String> countryMaxScale;
	private List<String> countryMinRatio;
	private List<String> stateSizePattern;
	private List<String> stateMaxScale;
	private List<String> stateMinRatio;

	@EJB
	IStatisticsService statisticsService;

	@EJB
	IUserService userService;

	/**
	 * [API No.20] vîñæ¾
	 * @param userId
	 * @param statisticsCd
	 * @param countryCd
	 * @param timeFilter
	 * @param dealerName
	 * @param customerName
	 * @param lbxModel
	 * @param psmId
	 * @param rmId
	 * @param mode
	 * @return Response.JSON MachineSearchInfoModel
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
					@FormParam("userId") String userId,
					@FormParam("statisticsCd") Integer statisticsCd,
					@FormParam("countryCd") String countryCd,
					@FormParam("stateCd") String stateCd,
					// **««« 2018/11/27  LBNÎì  tF[Y2ãXü¯vÎ ÇÁ  «««**//
					@FormParam("regionName") String regionName,
					// **ªªª 2018/11/27  LBNÎì  tF[Y2ãXü¯vÎ ÇÁ  ªªª**//
					@FormParam("timeFilter") Integer timeFilter,
					@FormParam("timeYear") String timeYear,
					@FormParam("dealerName") String dealerName,
					@FormParam("customerName") String customerName,
					@FormParam("lbxModel") String lbxModel,
					@FormParam("psmId") String psmId,
					@FormParam("rmId") String rmId,
					@FormParam("mode") Integer mode,
					// **««« 2018/11/28 LBNÎì  tF[Y2ãXü¯vÎ ÇÁ  «««**//
					@FormParam("sortFlg") Integer sortFlg
					// **ªªª 2018/11/28  LBNÎì  tF[Y2ãXü¯vÎ ÇÁ  ªªª**//
				) {

		logger.info("[POST] userId="+userId+", statisticsCd="+statisticsCd+", countryCd="+countryCd+", stateCd="+stateCd+", regionName="+regionName+", timeFilter="+timeFilter
					+", dealerName="+dealerName+", customerName="+customerName+", lbxModel="+lbxModel+", psmId="+psmId+", rmId="+rmId+", mode="+mode+", sortFlg="+sortFlg);

		try {

			// ú
				// Ýè¾ê
				languageCd = ConvertUtil.convertLanguageCd(Constants.CON_ENGLISH_ONLY);

				// Oinit@CÇÝÝ(1)
				countryCds = PropertyUtil.readPropertys(Constants.APP_CONFIG_PATH, "countryCds", null);

				// Oinit@CÇÝÝ(2)@~IuWFNgÝè
				countrySizePattern = Arrays.asList(PropertyUtil.readPropertys(Constants.APP_CONFIG_PATH, "countrySizePattern", null).split(","));
				countryMaxScale = Arrays.asList(PropertyUtil.readPropertys(Constants.APP_CONFIG_PATH, "countryMaxScale", null).split(","));
				countryMinRatio = Arrays.asList(PropertyUtil.readPropertys(Constants.APP_CONFIG_PATH, "countryMinRatio", null).split(","));
				stateSizePattern = Arrays.asList(PropertyUtil.readPropertys(Constants.APP_CONFIG_PATH, "stateSizePattern", null).split(","));
				stateMaxScale = Arrays.asList(PropertyUtil.readPropertys(Constants.APP_CONFIG_PATH, "stateMaxScale", null).split(","));
				stateMinRatio = Arrays.asList(PropertyUtil.readPropertys(Constants.APP_CONFIG_PATH, "stateMinRatio", null).split(","));


				List<StatisticsListModel> statisticsList = new ArrayList<StatisticsListModel>();
				StatisticsListModel statisticsListModel = new StatisticsListModel();


				// úÔoE
				if(mode==0) {
					groupTitleFormat = "YYYY-MM";
					ExtractTimeFilter(timeFilter);
				}
				if(mode==1) {
					groupTitleFormat = "YYYY";
					ExtractTimeYear(timeYear, mode);
				}
				if(mode==2) {
					groupTitleFormat = "YYYY-MM";
					ExtractTimeYear(timeYear, mode);
				}
				if(mode==3) {
					groupTitleFormat = "YYYY";
					ExtractTimeYear(timeYear, mode);
				}
				if(mode==4) {
					groupTitleFormat = "YYYY-MM";
					ExtractTimeYear(timeYear, mode);
				}

				// ~IuWFNgíÊ
				if(countryCd==null && stateCd==null)
					scaleType = "country";
				if(countryCd!=null && stateCd==null)
					scaleType = "state";



				List<Object[]> user = userService.findByUserSosikiKengenNativeQuery(userId);

				String sosikiCd = null;
				if(user.get(0)[1]!=null)
					sosikiCd = (String)user.get(0)[1];

				String kigyouCd = null;
				if(user.get(0)[2]!=null)
					kigyouCd = (String)user.get(0)[2];

				String kengenCd = null;

				// ^Cv
				Integer typeFlg = null;
				if(user.get(0)[5]!=null)
					typeFlg = ((Number)user.get(0)[5]).intValue();



				// NE»ÏØ°
				List<Object[]> summaryResults = statisticsService.findByMachineKadoSummaryNativeQuery(
								countryCd,
								stateCd,
								timeFrom,
								timeTo,
								dealerName,
								customerName,
								lbxModel,
								psmId,
								rmId,
								mode,
								groupTitleFormat,
								"YYYY-MM",
								countryCds
								// **««« 2018/11/27  LBNÎì  tF[Y2ãXü¯vÎ ÇÁ  «««**//
								,regionName
								,sortFlg
								,statisticsCd
								// **ªªª 2018/11/27  LBNÎì  tF[Y2ãXü¯vÎ ÇÁ  ªªª**//
								,typeFlg
								,kigyouCd
								,sosikiCd
							);

				if(summaryResults!=null) {
					maxDayAvg = 0;
					maxMonthAvg = 0;
					maxUnit = 0;

					for(int index=0; index < summaryResults.size(); index++) {

						// Ò­Ôú½Ï
						maxDayAvg = ExtractMaxValue(Double.parseDouble(summaryResults.get(index)[4].toString()),maxDayAvg);
						// Ò­Ô½Ï
						maxMonthAvg = ExtractMaxValue(Double.parseDouble(summaryResults.get(index)[5].toString()),maxMonthAvg);
						// Ò­äv
						maxUnit = ExtractMaxValue(Double.parseDouble(summaryResults.get(index)[2].toString()),maxUnit);
					}

					logger.config("[RemoteCAREApp][DEBUG] maxDayAvg="+maxDayAvg+", maxMonthAvg="+maxMonthAvg+", maxUnit="+maxUnit);

					for(int index=0; index < summaryResults.size(); index++) {

						statisticsListModel = new StatisticsListModel();
						// **««« 2019/08/01  iDEARº  tF[Y3 næîñNullf[^¼Ìt^Î ÇÁ  «««**//
						if(summaryResults.get(index)[0].toString().equals("  ")) {
							statisticsListModel.setGroupTitle("no location data");
						}else {
							statisticsListModel.setGroupTitle(summaryResults.get(index)[0].toString());
						}
						// **ªªª 2019/08/01  iDEARº  tF[Y3 næîñNullf[^¼Ìt^Î ÇÁ  ªªª**//


						// **««« 2018/11/27  LBNÎì  tF[Y2ãXü¯vÎ ÇÁ  «««**//
//						if(statisticsCd == 0 && (mode==0 || mode==1 || mode==2)){
						if(statisticsCd == 0){
						// **ªªª 2018/11/27  LBNÎì  tF[Y2ãXü¯vÎ ÇÁ  ªªª**//
							// Ò­Ôú½Ï
							statisticsListModel.setAggregateTime(Double.parseDouble(summaryResults.get(index)[4].toString()));
							if(mode==0)
								statisticsListModel.setCircleObjSize(ExtractCircleObjSize(Double.parseDouble(summaryResults.get(index)[4].toString()), statisticsCd, scaleType));

						}
						// **««« 2018/11/27  LBNÎì  tF[Y2ãXü¯vÎ ÇÁ  «««**//
//						if(statisticsCd == 1 && (mode==0 || mode==1 || mode==2)) {
						if(statisticsCd == 1) {
						// **ªªª 2018/11/27  LBNÎì  tF[Y2ãXü¯vÎ ÇÁ  ªªª**//
							// Ò­Ô½Ï@
							statisticsListModel.setAggregateTime(Double.parseDouble(summaryResults.get(index)[5].toString()));
							if(mode==0)
								statisticsListModel.setCircleObjSize(ExtractCircleObjSize(Double.parseDouble(summaryResults.get(index)[5].toString()), statisticsCd, scaleType));
						}

						// **««« 2018/11/27  LBNÎì  tF[Y2ãXü¯vÎ ÇÁ  «««**//
//						if((mode==0 && statisticsCd == 2) || mode==1 || mode==2) {
						if((mode==0 && statisticsCd == 2) || mode==1 || mode==2 || mode==3 || mode==4) {
						// **ªªª 2018/11/27  LBNÎì  tF[Y2ãXü¯vÎ ÇÁ  ªªª**//
							// Ò­äv

							statisticsListModel.setAggregateUnit(Double.parseDouble(summaryResults.get(index)[6].toString()));
							if(mode==0)
								statisticsListModel.setCircleObjSize(ExtractCircleObjSize(Double.parseDouble(summaryResults.get(index)[2].toString()), statisticsCd, scaleType));
						}

						// **««« 2019/01/11  LBNÎì  tF[Y2¼ÇÁvÎ ÇÁ  «««**//
						statisticsListModel.setCountryCd(summaryResults.get(index)[7].toString());
						// **ªªª 2019/01/11  LBNÎì  tF[Y2¼ÇÁvÎ ÇÁ  ªªª**//
						statisticsList.add(statisticsListModel);
					}
			}

			StatisticsInfoModel statisticsInfo = new StatisticsInfoModel(1000,statisticsList);
			return Response.ok(statisticsInfo).build();

		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
		}
	}




	/**
	 * Åålo
	 */
	public double ExtractMaxValue(double getValue, double tempValue ) {

		if(getValue>tempValue)
			return getValue;
		else
			return tempValue;

	}



	/**
	 * sNZwè§ä
	 */
	public double ExtractCircleObjSize(double getValue, int statisticsCd, String scaleType) {

		int sizePattern = 0;
		double maxValue = 0;
		double maxScale = 0;
		double minRatio = 0;


		if(scaleType.equals("country")) {
			sizePattern = Integer.parseInt(countrySizePattern.get(statisticsCd));
			maxScale = Double.parseDouble(countryMaxScale.get(statisticsCd));
			minRatio = Double.parseDouble(countryMinRatio.get(statisticsCd));
		}
		if(scaleType.equals("state")){
			sizePattern = Integer.parseInt(stateSizePattern.get(statisticsCd));
			maxScale = Double.parseDouble(stateMaxScale.get(statisticsCd));
			minRatio = Double.parseDouble(stateMinRatio.get(statisticsCd));
		}


		if(statisticsCd==0)
			maxValue = maxDayAvg;
		if(statisticsCd==1)
			maxValue = maxMonthAvg;
		if(statisticsCd==2)
			maxValue = maxUnit;

		double pixsel = 0;
		double deffRange = (maxScale-(maxScale*minRatio))/(sizePattern-1);
		double ratio = getValue/maxValue; //
		double addRange = 0;

		for(int i=0; i<sizePattern; i++) {
			addRange = addRange+(100/sizePattern);

			if(addRange>=(ratio*100)) {
				pixsel = (maxScale*minRatio)+(deffRange*i);
//				logger.config("[RemoteCAREApp][DEBUG] pixsel="+pixsel+", addRange="+addRange+", (ratio*100)="+(ratio*100));

				break;
			}
		}

		return pixsel;

	}



	/**
	 * oúÔ From - To
	 * timeFilter mode==0 ÌêÌÝ
	 * @throws Exception
	 */
	public void ExtractTimeFilter(Integer timeFilter) throws Exception{

		int filterMonthCounter = 0;

//		Date currentDate = new Date(System.currentTimeMillis());
		Date currentDate = ConvertUtil.currentDate();

		int setYear = Integer.parseInt(new SimpleDateFormat("yyyy").format(currentDate));
		int setMonth = Integer.parseInt(new SimpleDateFormat("M").format(currentDate));

		logger.config("[RemoteCAREApp][DEBUG][BEFORE] setYear="+setYear+", setMonth="+setMonth);
		if(timeFilter!=null) {
			switch(timeFilter) {

			// 0: OiftHgj
			case 0:
				setMonth = setMonth - 1;
				filterMonthCounter = 1;
				break;

			// 1: 
			case 1:
				filterMonthCounter = 1;
				break;

			// 2: ON¯
			case 2:
				setYear = setYear-1;
				filterMonthCounter = 1;
				break;

			// 3: N
			case 3:
				// N 1`12
				setMonth = 12;
				filterMonthCounter = 12;
				break;

			// 4: ß3
			case 4:
				filterMonthCounter = 3;
				break;

			// 5: ß6
			case 5:
				filterMonthCounter = 6;
				break;

			// 6: ß12
			case 6:
				filterMonthCounter = 12;
				break;

			// 7: ß3N
			case 7:
				filterMonthCounter = 36;
				break;

			// 8: ß5N
			case 8:
				filterMonthCounter = 60;
				break;

			// else
			default:
				filterMonthCounter = 1;
			}
		}


		Calendar cal = new GregorianCalendar(setYear ,setMonth-1 ,1);
		this.timeTo = new SimpleDateFormat("YYYY-MM").format(cal.getTimeInMillis());

		cal.add(Calendar.MONTH, -1*(filterMonthCounter-1));
		this.timeFrom = new SimpleDateFormat("YYYY-MM").format(cal.getTimeInMillis());

		logger.config("[RemoteCAREApp][DEBUG] timeFrom="+timeFrom+", timeTo="+timeTo);

		return;
	}



	/**
	 * oúÔ
	 * timeYear
	 */
	public void ExtractTimeYear(String timeYear, Integer mode) {

		if(mode==1) {
			Integer year = Integer.parseInt(timeYear)-4;
			this.timeFrom = year.toString().concat("-01");
			this.timeTo = timeYear.concat("-12");
		}
		if(mode==2) {
			this.timeFrom = timeYear.concat("-01");
			this.timeTo = timeYear.concat("-12");
		}
		if(mode==3) {
			this.timeFrom = timeYear.concat("-01");
			this.timeTo = timeYear.concat("-12");
		}
		if(mode==4) {
			this.timeFrom = timeYear;
			this.timeTo = timeYear;
		}

		return;
	}





}
