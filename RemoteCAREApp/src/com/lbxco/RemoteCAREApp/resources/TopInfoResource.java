package com.lbxco.RemoteCAREApp.resources;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.lbxco.RemoteCAREApp.Constants;
import com.lbxco.RemoteCAREApp.ConvertUtil;
import com.lbxco.RemoteCAREApp.annotations.Authorized;
import com.lbxco.RemoteCAREApp.ejbs.dealerNews.IDealerNewsService;
import com.lbxco.RemoteCAREApp.ejbs.dtcNotice.IDtcNoticeService;
import com.lbxco.RemoteCAREApp.ejbs.topInfo.ITopInfoService;
import com.lbxco.RemoteCAREApp.ejbs.user.IUserService;
import com.lbxco.RemoteCAREApp.entities.TblDocuments;
import com.lbxco.RemoteCAREApp.entities.TblNews;
import com.lbxco.RemoteCAREApp.models.DocumentListModel;
import com.lbxco.RemoteCAREApp.models.DtcNoticesListModel;
import com.lbxco.RemoteCAREApp.models.NewsListModel;
import com.lbxco.RemoteCAREApp.models.ReturnContainer;
import com.lbxco.RemoteCAREApp.models.TopInfoModel;


@Path("/topInfo")
@Stateless
public class TopInfoResource {

	private static final Logger logger = Logger.getLogger(TopInfoResource.class.getName());

	private String languageCd;
	private String dealerCd;
	private String dealerLogoUrl;

	@EJB
	ITopInfoService topInfoService;

	@EJB
	IUserService userService;

	@EJB
	IDtcNoticeService dtcService;

	@EJB
	IDealerNewsService dealerNewsService;


	/**
	 * [API No.4] お知らせ一覧取得
	 * @param userId
	 * @param startRecord
	 * @param deviceLanguage
	 * @return Response.JSON TopInfoModel
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
				@FormParam("userId") String userId,
				@FormParam("startRecord") Integer startRecord,
				@FormParam("deviceLanguage") Integer deviceLanguage
			) {

		logger.info("[RemoteCAREApp][POST] userId="+userId+", startRecord="+startRecord+", deviceLanguage="+deviceLanguage);

		try {

			TopInfoModel topInfoModel = new TopInfoModel();

			languageCd = ConvertUtil.convertLanguageCd(deviceLanguage);


			// **↓↓↓　2018/06/20 LBN石川  要望不具合管理台帳 NoXX 資料公開 追加 ↓↓↓**//

			//**↓↓↓ 2019/04/01 iDEA山下　組織権限取得⇒ユーザ権限取得に変更 ↓↓↓**//
			// ユーザー組織権限コード取得
//			List<Object[]> userSosikiKengen =userService.findByUserSosikiKengenNativeQuery(userId);
			List<Object[]> userKengen =userService.findByUserKengenNativeQuery(userId);
			//**↑↑↑ 2019/04/01 iDEA山下　組織権限取得⇒ユーザ権限取得に変更 ↑↑↑**//

			Integer kengenCd = null;
			if(userKengen != null) {
				kengenCd = Integer.parseInt(userKengen.get(0)[4].toString());	// [4] KENGEN_CD
			}


			// 資料テーブル一覧取得
			List<TblDocuments> tblDocuments = topInfoService.findByDocuments(languageCd, Constants.CON_APP_FLG);
			if(tblDocuments != null) {
				ArrayList<DocumentListModel> documentList = new ArrayList<DocumentListModel>();

				for(TblDocuments document : tblDocuments) {
					DocumentListModel documentListModel = new DocumentListModel();

					boolean documentCheck = checkTarget(userId, kengenCd, document.getTargetUserId(), document.getTargetKengenCd(), null);
					if(documentCheck) {
						documentListModel.setDocumentDate(document.getOpDate().toString());
						documentListModel.setDocumentName(document.getDocuName());
						documentListModel.setDocumentUrl(Constants.PDF_URL + document.getDocuPath());
						documentList.add(documentListModel);
					}
				}
				if(documentList!=null) {
					topInfoModel.setDocumentList(documentList);
					topInfoModel.setDocumentCount(documentList.size());
				}

			}else {
				topInfoModel.setDocumentCount(0);
			}
			// **↑↑↑　2018/06/20 LBN石川  要望不具合管理台帳 NoXX 資料公開 追加 ↑↑↑**//


		// ニュース一覧取得

			// add. dealer ************ start
			// 権限および代理店コードを取得
			if(kengenCd==101 || kengenCd==102 || kengenCd==103)
				dealerCd = null;
			if(kengenCd==104 || kengenCd==105)
				dealerCd = userKengen.get(0)[1].toString();	// [1]SOSIKI_CD
			if(kengenCd==106 || kengenCd==107)
				dealerCd = userKengen.get(0)[6].toString();	// [6]DAIRITENN_CD

			// 代理店ロゴ取得
			if(dealerCd!=null)
				dealerLogoUrl = dealerNewsService.findByDealerLogo(dealerCd);
			List<TblNews> tblNews = topInfoService.findByNews(languageCd, Constants.CON_APP_FLG, dealerCd, userId, kengenCd.toString());
			// add. dealer ************ end

			if(tblNews != null) {

				ArrayList<NewsListModel> newsList = new ArrayList<NewsListModel>();

				//キャッシュクリア 対応
				Integer iValue = (int)(Math.random() * 1000);

				for(TblNews news : tblNews) {
					NewsListModel newsListModel = new NewsListModel();

					// **↓↓↓　2018/06/20 LBN石川  要望不具合管理台帳 NoXX G@NavApp多言語対応改修のため 変更 ↓↓↓**//
//					if(deviceLanguage == 0)
//						newsListModel.setNewsContents(news.getNewsContentsEn());
//					if(deviceLanguage == 1)
//						newsListModel.setNewsContents(news.getNewsContentsPt());
//					if(deviceLanguage == 2)
//						newsListModel.setNewsContents(news.getNewsContentsEs());
//					int newsCheck = 0;
//					newsCheck = checkTarget(userId, kengenCd, news.getTargetUserId(), news.getTargetKengenCd());
					boolean newsCheck = checkTarget(userId, kengenCd, news.getTargetUserId(), news.getTargetKengenCd(),news.getTargetDairitennCd());
					if(newsCheck) {
						newsListModel.setNewsContents(news.getNewsContents());
						newsListModel.setNewsDate(new SimpleDateFormat("yyyy-MM-dd").format(news.getOpDate()));
						// add. dealer ************ start
						newsListModel.setNewsUrl(news.getNewsUrl());
						if(news.getTargetDairitennCd()!=null) {
							newsListModel.setNewsType(1);	// 1:代理店ニュース
							if(dealerLogoUrl!=null && dealerLogoUrl.length() > 0)
								newsListModel.setNewsLogoUrl(Constants.FILE_URL + dealerLogoUrl + "?"+iValue);
						}else {
							newsListModel.setNewsType(0);	// 0:通常ニュース
						}
						// add. dealer ************ end
						newsList.add(newsListModel);
					}
				}
				if(newsList!=null) {
					topInfoModel.setNewsList(newsList);
					topInfoModel.setNewsCount(newsList.size());
				}
				// **↑↑↑　2018/06/20 LBN石川  要望不具合管理台帳 NoXX G@NavApp多言語対応改修のため 変更 ↑↑↑**//
			}else {
				topInfoModel.setNewsCount(0);

			}

			// **↓↓↓　2018/08/19 LBN石川  要望対応 お気に入りDTC履歴の既読処理 追加 ↓↓↓**//
			// キー不整合、処理フラグ９対応
			dtcService.updateNoMatchTblFavoriteDtcHistory(userId);
			// 復旧済みの既読処理をする。
			dtcService.updateFukkyuDtcTblFavoriteDtcHistory(userId);

			// **↓↓↓ 2019/02/28 iDEA Yamashita　お気に入りDTCの件数更新はバッチでのみ実施 削除 ↓↓↓**//
			//  お気に入り登録されている機械でDTC未復旧の直近2日分のDTCを取得し、該当がある場合は記録する。
			//dtcService.insertByNotExistsTblFavoriteDtcHistory(userId, kengenCd, null);
			// **↑↑↑ 2019/02/28 iDEA Yamashita　お気に入りDTCの件数更新はバッチでのみ実施 削除 ↑↑↑**//


			// 未読件数取得
			Integer badgeCount = dtcService.findByNoReadCount(userId);
			if(badgeCount!=null) {
				if(badgeCount>=1000)
					badgeCount=999;	// max
				topInfoModel.setDtcBadgeCount(badgeCount);
			}
			// **↑↑↑　2018/08/19 LBN石川  要望対応 お気に入りDTC履歴の既読処理 追加 ↑↑↑**//



			// Favorite DTC 2days and No read
			List<Object[]> dtcNotice = dtcService.findByDtcNoticesNativeQuery(userId, kengenCd);
			ArrayList<DtcNoticesListModel> dtcNoticesList = new ArrayList<DtcNoticesListModel>();
			if(dtcNotice != null) {

				topInfoModel.setDtcCount(dtcNotice.size());
				Integer record = startRecord-1;			// 1 - 51 - 101 -
				Integer maxRecord = dtcNotice.size();

				for(int counter = record; counter < (record+50) ; counter++ ) {

					DtcNoticesListModel dtcNoticesListModel = new DtcNoticesListModel();

					if(counter==(maxRecord))
						break;

					Object[] dtc = dtcNotice.get(counter);

					Integer kibanSerno = 0;
					if(dtc[0]!=null) {
						kibanSerno = ((Number)dtc[0]).intValue();
						dtcNoticesListModel.setSerialNumber(kibanSerno);
					}

					String manufacturerSerialNumber = "";
					if(dtc[15]!=null)
						manufacturerSerialNumber = (String)dtc[15];
					dtcNoticesListModel.setManufacturerSerialNumber(manufacturerSerialNumber);

					if(manufacturerSerialNumber.substring(7,8).equals("6"))
						dtcNoticesListModel.setMachineModelCategory(1); 	//1: model 6
					else
						dtcNoticesListModel.setMachineModelCategory(2);		//2: model 7


					String lbxSerialNumber = "";
					if(dtc[16]!=null)
						lbxSerialNumber = (String)dtc[16];
					dtcNoticesListModel.setLbxSerialNumber(lbxSerialNumber);

					String customerManagementNo = "";
					if(dtc[17]!=null)
						customerManagementNo = (String)dtc[17];
					dtcNoticesListModel.setCustomerManagementNo(customerManagementNo);

					String alertLv = "";
					if(dtc[8]!=null) {
						String al = (String)dtc[8];
						if(al.equals("1"))
							alertLv = "0";
						else if(al.equals("2"))
							alertLv = "1";
						else if(al.equals("3"))
							alertLv = "2";
					}
					dtcNoticesListModel.setAlertLv(alertLv);


					//発生日時現地時間
					SimpleDateFormat sdf = new  SimpleDateFormat("yyyy-MM-dd HH:mm");
					if((Timestamp)dtc[10]!=null)
						dtcNoticesListModel.setHasseiDate(sdf.format((Timestamp)dtc[10]));

					//DTC内容
					StringBuilder sb = new StringBuilder();
					if(dtc[3]!=null)
						sb.append(dtc[3].toString());	// FLG_VISIBLE
					else
						sb.append(dtc[4].toString());	// EVENT_CODE 文字列書式 0000
					sb.append(":");
					if(dtc[5]!=null)
						sb.append(dtc[5].toString());	// KUBUN_NAME
					sb.append("-");
					if(dtc[6]!=null)
						sb.append(dtc[6].toString());	// EVENT_NAME
					dtcNoticesListModel.setWarnHistoryContent(new String(sb));


					// **↓↓↓　2018/08/19 LBN石川  要望対応 お気に入りDTC履歴の既読処理 追加 ↓↓↓**//
					if(dtc[19]!=null)
						dtcNoticesListModel.setReadFlg(Integer.parseInt(dtc[19].toString()));
					else
						dtcNoticesListModel.setReadFlg(1);
					// **↑↑↑　2018/08/19 LBN石川  要望対応 お気に入りDTC履歴の既読処理 追加 ↑↑↑**//

					dtcNoticesList.add(dtcNoticesListModel);
				}

				topInfoModel.setDtcNoticesList(dtcNoticesList);

			}else {
				topInfoModel.setDtcCount(0);
				topInfoModel.setDtcNoticesList(new ArrayList<DtcNoticesListModel>());

			}






			topInfoModel.setStatusCode(Constants.CON_OK);
			return Response.ok(topInfoModel).build();


		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
		}


	}





	// **↓↓↓　2018/06/20 LBN石川  要望不具合管理台帳 NoXX G@NavApp多言語対応改修のため 変更 ↓↓↓**//
	/**
	 * ニュース・資料ターゲット
	 *
	 */
//	private int checkTarget(String userId, Integer kengenCd, String targetUserId, String targetKengenCd) {
	private boolean checkTarget(String userId, Integer kengenCd, String targetUserId, String targetKengenCd, String dealerCd) {

		boolean flg = false;

		if(dealerCd!=null)
			flg = true;
//		if(targetUserId==null && targetKengenCd==null)
		else if(targetUserId==null && targetKengenCd==null)
			flg = true;
		else if(targetUserId!=null && Arrays.asList(targetUserId.split(",")).contains(userId))
			flg = true;
		else if(targetKengenCd!=null && Arrays.asList(targetKengenCd.split(",")).contains(kengenCd.toString()))
			flg = true;

		return flg;
	}
	// **↑↑↑　2018/06/20 LBN石川  要望不具合管理台帳 NoXX G@NavApp多言語対応改修のため 変更 ↑↑↑**//
}
