package com.lbxco.RemoteCAREApp.resources;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.imageio.ImageIO;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.lbxco.RemoteCAREApp.Constants;
import com.lbxco.RemoteCAREApp.annotations.Authorized;
import com.lbxco.RemoteCAREApp.ejbs.example.IExample;
import com.lbxco.RemoteCAREApp.ejbs.mailer.Mailer;
import com.lbxco.RemoteCAREApp.models.ReturnContainer;



@Path("/samples")
@Stateless
public class ExampleResource {

	@EJB
	IExample exampleService;

	@EJB
	Mailer mailer;


	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response res() {

		return Response.ok(new ReturnContainer(Constants.CON_OK, "ok")).build();
	}




	@GET
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	@Produces(MediaType.TEXT_PLAIN)	// text/plain (.txt) ブラウザ表示後、保存対応とする。 2018/06/07
//	@Produces(MediaType.MULTIPART_FORM_DATA)	// 拡張子無し Downlode
//	@Produces("text/csv")						// 拡張子無し Downlode
//	@Produces("text/comma-separated-values")	// 拡張子無し Downlode
	@Path("loginLog")
	public Response loginlog(
				@QueryParam("appKey") String appKey,
				@QueryParam("ymd") String ymd
			) {


		if(!appKey.equals("remotecare::xxxxxsec")) {
			return Response.ok("can not access").build();
		}

		String contents =  null;

		if(ymd.equals("20180607")) {

			contents = ""
				+ "2018/06/07,09:54:01,inoku1999\n"
				+ "2018/06/07,10:18:23,yama9999\n"
				+ "2018/06/07,10:31:47,tana1111\n"
				+ "2018/06/07,15:11:09,inoku1999\n"
				+ "\n";
		}else {
			contents = "no data";
		}

		return Response.ok(contents).build();
	}



	@GET
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	@Path("image")
	public Response test() throws IOException {


		BufferedImage bufferedImageFROM = ImageIO.read(new File("/opt/dealerLogo.jpg"));
		ByteArrayOutputStream outPutStreamTO = new ByteArrayOutputStream();
		ImageIO.write(bufferedImageFROM, "jpg", outPutStreamTO);
		// ByteArrayOutPutStreamを用いてbyte[]に変換
		byte[] imageBinary = outPutStreamTO.toByteArray();

        BufferedImage bufImage = ImageIO.read(new ByteArrayInputStream(imageBinary));

//		InputStreamをImageIOクラスのreadメソッドを用いてBufferedImage型に変換
        FileOutputStream out = new FileOutputStream("/opt/abc.jpg");
        ImageIO.write(bufImage, "jpg", out);

		return Response.ok("OK").build();
	}



	@GET
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	@Path("test2")
	public Response test2() {

		String contents =  "test data.\n";

		return Response.ok(contents).build();
	}



	@GET
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	@Path("mail")
	public Response mail(@Context UriInfo uriInfo) throws AddressException, MessagingException {

		mailer.sendMail("aishikawa@lbn.co.jp", "lbx test", "mail message test! OK! \n hello !", new ArrayList<String>());

		return Response.ok(new ReturnContainer(Constants.CON_OK, "ok")).build();
	}



}
