package com.lbxco.RemoteCAREApp.resources;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.lbxco.RemoteCAREApp.Constants;
import com.lbxco.RemoteCAREApp.annotations.Authorized;
import com.lbxco.RemoteCAREApp.ejbs.inspectionPdf.IInspectionPDFService;
import com.lbxco.RemoteCAREApp.models.InspectionPDFModel;
import com.lbxco.RemoteCAREApp.models.ReturnContainer;




@Path("/inspectionPDF")
@Stateless
public class InspectionPDFResource {

	private static final Logger logger = Logger.getLogger(InspectionPDFResource.class.getName());

	@EJB
	IInspectionPDFService inspectionPDFService;


	/**
	 * [API No.12] A3_PDFæ¾
	 * @param serialNumber
	 * @param dtcEventNo
	 * @return Response.JSON pdfModel
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
					@FormParam("serialNumber") Integer _serialNumber,
					@FormParam("dtcEventNo") String _dtcEventNo
				) {

		logger.info("[RemoteCAREApp][POST] serialNumber="+_serialNumber+", dtcEventNo="+_dtcEventNo);

		try {
			String pdfUrl = null;
			InspectionPDFModel pdfModel = new InspectionPDFModel();


			pdfUrl = inspectionPDFService.getPDFUrl(_serialNumber, _dtcEventNo);

			if(pdfUrl!=null) {
				logger.config("[RemoteCAREApp][DEBUG] pdfUrl="+pdfUrl);
				pdfModel.setUrl(Constants.PDF_URL + pdfUrl);
				pdfModel.setStatusCode(Constants.CON_OK);
			}else{
				return Response.ok(new ReturnContainer(Constants.CON_OK)).build();
			}

			return Response.ok(pdfModel).build();

		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
		}
	}
}
