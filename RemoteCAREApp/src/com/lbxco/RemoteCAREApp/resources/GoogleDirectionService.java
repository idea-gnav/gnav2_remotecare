package com.lbxco.RemoteCAREApp.resources;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.logging.Logger;
import java.util.zip.GZIPOutputStream;

import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.DatatypeConverter;

import com.lbxco.RemoteCAREApp.Constants;
import com.lbxco.RemoteCAREApp.annotations.Authorized;
import com.lbxco.RemoteCAREApp.models.GoogleDirectionServiceModel;
import com.lbxco.RemoteCAREApp.models.ReturnContainer;


@Path("/googleDirectionService")
@Stateless
public class GoogleDirectionService {

	private static final Logger logger = Logger.getLogger(GoogleDirectionService.class.getName());
	private static final String CON_MODE_CAR = "driving";
	private static final String CON_MODE_WALK = "walking";
	private static String CON_googleApiUrl = "https://maps.googleapis.com/maps/api/directions/json";


	/**
	 * [API No.18] GoogleMapルート検索サービス
	 * @param origin
	 * @param destination
	 * @param transitMode
	 * @param units
	 * @param language
	 * @param key
	 * @param compress
	 * @return Response.JSON GoogleDirectionServiceModel
	 * @throws IOException
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response searchRouter(
			@FormParam("origin") String origin,
			@FormParam("destination") String destination,
			@FormParam("transit_mode") String transitMode,
			@FormParam("units") String units,
			@FormParam("language") String language,
			@FormParam("key") String key,
			@FormParam("compress") String compress
			) throws IOException {

		logger.info("[RemoteCAREApp][POST] origin="+origin+", destination="+destination+", units="+units+", transit_mode="+transitMode+", language="+language+", key="+key+", compress="+compress);

		boolean isCompress = false;

		StringBuilder query = new StringBuilder();
		query.append("?origin=").append(origin);
		query.append("&destination=").append(destination);

		if (units != null)
			query.append("&units=").append(units);
		if (language != null)
			query.append("&language=").append(language);
		if (key != null)
			query.append("&key=").append(URLEncoder.encode(key, "UTF-8"));
		if (transitMode != null)
			query.append("&transit_mode=").append(transitMode);

		if (compress != null && compress.equals("yes"))
			isCompress = true;

		String carRouter = GetRouterData(CON_MODE_CAR, query.toString(),isCompress);
		String walkRouter = GetRouterData(CON_MODE_WALK, query.toString(),isCompress);

		if (carRouter == null || walkRouter == null) {
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
		}


		GoogleDirectionServiceModel returnData = new GoogleDirectionServiceModel();
		returnData.setStatusCode(Constants.CON_OK);
		returnData.setCarRouterData(carRouter);
		returnData.setWalkRouterData(walkRouter);


		logger.config("carRouter="+carRouter.toString());
		logger.config("walkRouter="+walkRouter.toString());

		return Response.ok(returnData).build();

	}

	private String GetRouterData(String mode, String query, boolean compressed) throws IOException {
		URL url = new URL(CON_googleApiUrl + query + "&mode=" + mode);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");

		int responseCode;
		responseCode = con.getResponseCode();

		if (responseCode != 200) {
			return null;
		}

		InputStream responseStream = con.getInputStream();



		if (compressed) {
			ByteArrayOutputStream result = new ByteArrayOutputStream();
			GZIPOutputStream gzipStream = new GZIPOutputStream(result);
			byte[] buffer = new byte[2048];
			int length;
			while ((length = responseStream.read(buffer)) != -1) {
				gzipStream.write(buffer, 0, length);
			}

			gzipStream.flush();
			gzipStream.close();
			responseStream.close();

			return DatatypeConverter.printBase64Binary(result.toByteArray());
		}
		else {
			ByteArrayOutputStream result = new ByteArrayOutputStream();
			byte[] buffer = new byte[2048];
			int length;
			while ((length = responseStream.read(buffer)) != -1) {
			    result.write(buffer, 0, length);
			}
			responseStream.close();
			return  result.toString("UTF-8");
		}

	}

}
