package com.lbxco.RemoteCAREApp.resources;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.lbxco.RemoteCAREApp.Constants;
import com.lbxco.RemoteCAREApp.ConvertUtil;
import com.lbxco.RemoteCAREApp.annotations.Authorized;
import com.lbxco.RemoteCAREApp.ejbs.statistics.IStatisticsService;
import com.lbxco.RemoteCAREApp.entities.MstSummaryArea;
import com.lbxco.RemoteCAREApp.entities.MstSummaryCountry;
import com.lbxco.RemoteCAREApp.models.CountryListModel;
import com.lbxco.RemoteCAREApp.models.CountryStateListModel;
import com.lbxco.RemoteCAREApp.models.ReturnContainer;
import com.lbxco.RemoteCAREApp.models.StateListModel;
import com.lbxco.RemoteCAREApp.util.PropertyUtil;



@Path("/countryStateList")
@Stateless
public class CountryStateListResource {

	private static final Logger logger = Logger.getLogger(CountryStateListResource.class.getName());

	private String languageCd;



	@EJB
	IStatisticsService statisticsService;




	/**
	 * [API No.21] BκζΎ
	 * @param userId
	 * @param deviceLanguage
	 * @return Response.JSON MachineSearchInfoModel
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
					@FormParam("userId") String userId,
					@FormParam("deviceLanguage") Integer deviceLanguage,
					@FormParam("mode") Integer mode,
					@FormParam("countryCd") String countryCd
				) {

		logger.info("[RemoteCAREApp][POST] userId="+userId+", deviceLanguage="+deviceLanguage+", mode="+mode+", countryCd="+countryCd);

		try {


			languageCd = ConvertUtil.convertLanguageCd(Constants.CON_ENGLISH_ONLY);

			CountryStateListModel countryStateListModel = new CountryStateListModel();


			List<String> countryCds = new ArrayList<>();
			if(mode==0)
				// Oinit@CΗέέ(1)
				countryCds = Arrays.asList(PropertyUtil.readPropertys(Constants.APP_CONFIG_PATH, "countryCds", null).split(","));
			if(mode==1)
				countryCds.add(countryCd);

			// Oinit@CΗέέ(2)
			Integer mapIdo = Integer.parseInt(PropertyUtil.readPropertys(Constants.APP_CONFIG_PATH, "mapIdo", null));
			Integer mapKeido = Integer.parseInt(PropertyUtil.readPropertys(Constants.APP_CONFIG_PATH, "mapKeido", null));
			countryStateListModel.setMapIdo(ConvertUtil.parseLatLng2(mapIdo));		// 10i@
			countryStateListModel.setMapKeido(ConvertUtil.parseLatLng2(mapKeido));	// 10i@

			// Oinit@CΗέέ(3)
			Double mapScaleSize = Double.parseDouble(PropertyUtil.readPropertys(Constants.APP_CONFIG_PATH, "mapScaleSize", null));
			countryStateListModel.setMapScaleSize(mapScaleSize);

			// Oinit@CΗέέ(4)
			List<String> countryMapScaleSize = Arrays.asList(PropertyUtil.readPropertys(Constants.APP_CONFIG_PATH, "countryMapScaleSize", null).split(","));


			// }X^©ηκζΎ
			List<MstSummaryCountry> country = statisticsService.findBySummaryCountry(languageCd, countryCds);

			if(country!=null) {

				List<CountryListModel> countryList = new ArrayList<CountryListModel>();
				CountryListModel countryListModel = new CountryListModel();

				for(int countryIndex = 0; countryIndex < country.size(); countryIndex++) {
					countryListModel = new CountryListModel();

					logger.config("[RemoteCAREApp][DEBUG]CountryName"+country.get(countryIndex).getCountryNm()+", CountryIdo="+country.get(countryIndex).getIdo());

					// country_cd
					countryListModel.setCountryCd(country.get(countryIndex).getCountryCd());
					// country_name
					countryListModel.setCountryName(country.get(countryIndex).getCountryNm());

//					// άx
					if(country.get(countryIndex).getIdo()!=null)
						countryListModel.setCountryIdo(ConvertUtil.parseLatLng(country.get(countryIndex).getIdo()));
					// ox
					if(country.get(countryIndex).getKeido()!=null)
						countryListModel.setCountryKeido(ConvertUtil.parseLatLng(country.get(countryIndex).getKeido()));

					// B}bvζΚ kΪx Ot@CQΖ
					Integer countryMapIndex = countryCds.indexOf(country.get(countryIndex).getCountryCd());
					countryListModel.setCountryMapScaleSize(Double.parseDouble(countryMapScaleSize.get(countryMapIndex)));

					// nζ}X^©ηBκζΎ
					List<MstSummaryArea> area = statisticsService.findBySummaryArea(languageCd, country.get(countryIndex).getCountryCd());

					if(area!=null) {
						List<StateListModel> stateList = new ArrayList<StateListModel>();
						StateListModel stateListModel = new StateListModel();

						for(int areaIndex=0; areaIndex < area.size(); areaIndex++) {
							stateListModel = new StateListModel();

							stateListModel.setStateCd(area.get(areaIndex).getAreaCd());
							stateListModel.setStateName(area.get(areaIndex).getAreaNm());
							// **««« 2018/11/27  LBNΞμ  tF[Y2γXό―vΞ ΗΑ  «««**//
							stateListModel.setStateBubbleName(area.get(areaIndex).getAreaBubbleNm());
							// **ͺͺͺ 2018/11/27  LBNΞμ  tF[Y2γXό―vΞ ΗΑ  ͺͺͺ**//

							if(area.get(areaIndex).getIdo()!=null)
								stateListModel.setStateIdo(ConvertUtil.parseLatLng(area.get(areaIndex).getIdo()));
							if(area.get(areaIndex).getKeido()!=null)
								stateListModel.setStateKeido(ConvertUtil.parseLatLng(area.get(areaIndex).getKeido()));

							stateList.add(stateListModel);


							// **««« 2018/11/27  LBNΞμ  tF[Y2γXό―vΞ ΗΑ  «««**//
							if(mode==1) {
								// nζΌκ
								List<String> regionNameList = statisticsService.findByRegionName(area.get(areaIndex).getCountryCd(), area.get(areaIndex).getAreaCd());
								stateListModel.setRegionNameList(regionNameList);
							}
							// **ͺͺͺ 2018/11/27  LBNΞμ  tF[Y2γXό―vΞ ΗΑ  ͺͺͺ**//

						}
						countryListModel.setStateList(stateList);
						countryList.add(countryListModel);
					}

				}

				countryStateListModel.setCountryList(countryList);
			}

			countryStateListModel.setStatusCode(Constants.CON_OK);
			return Response.ok(countryStateListModel).build();

		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
		}
	}
}
