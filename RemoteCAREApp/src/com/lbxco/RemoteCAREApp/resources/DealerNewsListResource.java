package com.lbxco.RemoteCAREApp.resources;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.lbxco.RemoteCAREApp.Constants;
import com.lbxco.RemoteCAREApp.ConvertUtil;
import com.lbxco.RemoteCAREApp.annotations.Authorized;
import com.lbxco.RemoteCAREApp.ejbs.dealerNews.IDealerNewsService;
import com.lbxco.RemoteCAREApp.ejbs.user.IUserService;
import com.lbxco.RemoteCAREApp.entities.TblNews;
import com.lbxco.RemoteCAREApp.models.DealerModel;
import com.lbxco.RemoteCAREApp.models.DealerNewsListModel;
import com.lbxco.RemoteCAREApp.models.NewsListModel;
import com.lbxco.RemoteCAREApp.models.ReturnContainer;


@Path("/dealerNewsList")
@Stateless
public class DealerNewsListResource {

	private static final Logger logger = Logger.getLogger(DealerNewsListResource.class.getName());

	private String languageCd;
	private Integer kengenCd;
	private String dealerCd;


	@EJB
	IDealerNewsService dealerNewsService;

	@EJB
	IUserService userService;


	/**
	 * [API No.25] 代理店向けお知らせ取得
	 * @param userId
	 * @param mode
	 * @return Response.JSON statusCode
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
				@FormParam("userId") String userId,
				@FormParam("mode") Integer mode
			) {

		logger.info("userId="+userId+", mode="+mode);

		DealerNewsListModel dealerNewsListModel = new DealerNewsListModel();

		try {

			// 設定言語
			languageCd = ConvertUtil.convertLanguageCd(Constants.CON_ENGLISH_ONLY);

			// ユーザー組織・権限
			List<Object[]> userSosikiKengen = userService.findByUserSosikiKengenNativeQuery(userId);
			if(userSosikiKengen!=null)
				kengenCd = Integer.parseInt(userSosikiKengen.get(0)[4].toString()); // [4]KENGEN_CD
			// 権限および代理店コードを取得
//			if(kengenCd==101 || kengenCd==102 || kengenCd==103)
//				dealerCd = userSosikiKengen.get(0)[1].toString();	// [1]SOSIKI_CD
			if(kengenCd==104 || kengenCd==105)
				dealerCd = userSosikiKengen.get(0)[1].toString();	// [1]SOSIKI_CD

			// mode
			//   1:LBX権限 代理店ニュース画面
			//   0:代理店権限 代理店ニュース編集画面
			List<String> dealerCds = new ArrayList<String>();;
			if(mode==1) {
				//LBX権限：ニュース登録している代理店一覧取得
				dealerCds = dealerNewsService.findByDealers(Constants.CON_APP_FLG);
			}else if(mode==0) {
				//代理店権限： 組織コードをセット
				dealerCds.add(dealerCd);
			}

			// 代理店一覧を取得
			List<Object[]> dealerList = null;
			if(dealerCds!=null)
				dealerList = dealerNewsService.findByDealerInfoList(dealerCds);

			ArrayList<DealerModel> dealerModelList = new ArrayList<DealerModel>();
			//キャッシュクリア 対応
			Integer iValue = (int)(Math.random() * 1000);

			if(dealerList!=null) {

				for(int index=0; index < dealerList.size(); index++) {

					DealerModel dealerModel = new DealerModel();
					dealerModel.setDealerCd(dealerList.get(index)[0].toString());				// 代理店コード
					dealerModel.setDealerName(dealerList.get(index)[1].toString());				// 代理店名
					if(dealerList.get(index)[2]!=null)
						dealerModel.setDealerLogoUrl(Constants.FILE_URL
												+ dealerList.get(index)[2].toString()+"?"+iValue.toString());	// 代理店ロゴ画像URL
	//				else
	//					dealerModel.setDealerUrl(Constants.FILE_URL
	//											+ Constants.DEALER_LOGO_DEFULT_IMG + "?"+iValue.toString());	// defultは不要
					dealerModelList.add(dealerModel);
				}
				dealerNewsListModel.setDealerList(dealerModelList);


				// ニュース一覧を取得
				List<TblNews> tblNewsList = dealerNewsService.findByDealerNewsList(languageCd, Constants.CON_APP_FLG, dealerCds, mode);
				ArrayList<NewsListModel> newsList = new ArrayList<NewsListModel>();

				if(tblNewsList!=null){

					for(TblNews tblNews: tblNewsList) {

						NewsListModel newsListModel = new NewsListModel();
						newsListModel.setNewsId(tblNews.getNewsId());
						if(tblNews.getOpDate()!=null)
							newsListModel.setNewsDateFrom(ConvertUtil.formatYMD(new Timestamp(tblNews.getOpDate().getTime())));
						if(tblNews.getEdDate()!=null)
							newsListModel.setNewsDateTo(ConvertUtil.formatYMD(new Timestamp(tblNews.getEdDate().getTime())));
						if(tblNews.getNewsContents()!=null)
							newsListModel.setNewsContents(tblNews.getNewsContents());
						if(tblNews.getNewsUrl()!=null)
							newsListModel.setNewsUrl(tblNews.getNewsUrl());

						if(tblNews.getOpDate()!=null) {
							// ニュース公開フラグ 0:非公開 1:公開
							String today = ConvertUtil.formatYMD(new Timestamp(System.currentTimeMillis()));
							String opDate = ConvertUtil.formatYMD(new Timestamp(tblNews.getOpDate().getTime()));
							String edDate = null;
							if(tblNews.getEdDate()!=null)
								edDate = ConvertUtil.formatYMD(new Timestamp(tblNews.getEdDate().getTime()));

							if(tblNews.getDelFlg()==1)
								newsListModel.setNewsOpenFlg(0);
							else if((opDate.compareTo(today)<=0) && edDate==null)
								newsListModel.setNewsOpenFlg(1);
							else if((opDate.compareTo(today)<=0) && (edDate.compareTo(today)>=0))
								newsListModel.setNewsOpenFlg(1);
							else
								newsListModel.setNewsOpenFlg(0);
						}
						newsListModel.setDealerCd(tblNews.getTargetDairitennCd());
						newsList.add(newsListModel);
					}
				}

				dealerNewsListModel.setNewsList(newsList);
				dealerNewsListModel.setNewsCount(newsList.size());

			//**↓↓↓ 2019/04/01 iDEA山下　対象件数0件の場合newsCount=0で返却  ↓↓↓**//
			}else {
				dealerNewsListModel.setNewsCount(0);
			}
			//**↑↑↑ 2019/04/01 iDEA山下　対象件数0件の場合newsCount=0で返却  ↑↑↑**//

		}catch (Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}
		dealerNewsListModel.setStatusCode(Constants.CON_OK);
		return Response.ok(dealerNewsListModel).build();

	}

}
