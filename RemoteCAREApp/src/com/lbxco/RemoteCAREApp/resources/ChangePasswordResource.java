package com.lbxco.RemoteCAREApp.resources;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.lbxco.RemoteCAREApp.Constants;
import com.lbxco.RemoteCAREApp.annotations.Authorized;
import com.lbxco.RemoteCAREApp.ejbs.changePassword.IChangePasswordService;
import com.lbxco.RemoteCAREApp.ejbs.user.IUserService;
import com.lbxco.RemoteCAREApp.entities.MstUser;
import com.lbxco.RemoteCAREApp.models.ReturnContainer;


@Path("/changePassword")
@Stateless
public class ChangePasswordResource {

	private static final Logger logger = Logger.getLogger(ChangePasswordResource.class.getName());

	@EJB
	IChangePasswordService changePasswordService;

	@EJB
	IUserService userService;


	/**
	 * [API No.3] パスワード変更
	 * @param userId
	 * @param passwordOld
	 * @param passwordNew
	 * @return Response.JSON statusCode
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
					@FormParam("userId") String userId,
					@FormParam("passwordOld") String passwordOld,
					@FormParam("passwordNew") String passwordNew
				) {

		logger.info("[RemoteCAREApp][POST] userId="+userId+", passwordOld="+passwordOld+", passwordNew="+passwordNew);

		try {
			MstUser user = userService.findByUser(userId, passwordOld);
			if (user != null) {

				changePasswordService.updateByPassword(userId, passwordNew);

				return Response.ok(new ReturnContainer(Constants.CON_OK)).build();

			}else {
				return Response.ok(new ReturnContainer(Constants.CON_WARNING_NO_USER_PASSWORD)).build();
			}

		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
		}


	}
}
