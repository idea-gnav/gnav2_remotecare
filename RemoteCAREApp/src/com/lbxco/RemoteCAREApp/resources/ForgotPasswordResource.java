package com.lbxco.RemoteCAREApp.resources;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.lbxco.RemoteCAREApp.Constants;
import com.lbxco.RemoteCAREApp.annotations.Authorized;
import com.lbxco.RemoteCAREApp.ejbs.mailer.Mailer;
import com.lbxco.RemoteCAREApp.ejbs.user.IUserService;
import com.lbxco.RemoteCAREApp.entities.MstUser;
import com.lbxco.RemoteCAREApp.models.ForgetPasswordModel;
import com.lbxco.RemoteCAREApp.models.ReturnContainer;


@Path("/forgotPassword")
@Stateless
public class ForgotPasswordResource {

	private static final Logger logger = Logger.getLogger(ForgotPasswordResource.class.getName());

	@EJB
	IUserService userService;

	@EJB
	Mailer mailer;


	/**
	 * [API No.2] パスワード送信
	 * @param userId
	 * @param deviceLanguage
	 * @param mode
	 * @return Response.JSON ForgetPasswordModel
	 * @throws AddressException
	 * @throws MessagingException
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
			@FormParam("userId") String userId,
			@FormParam("deviceLanguage") Integer deviceLanguage,
			@FormParam("mode") Integer mode
			) throws AddressException, MessagingException {

		logger.info("[RemoteCAREApp][POST] userId="+userId+", deviceLanguage="+deviceLanguage+", mode="+mode);


		try {

			MstUser user = userService.findByUser(userId);
			if (user != null) {

				// **↓↓↓ 2019/02/19  LBN石川 不具合対応 メールアドレス登録なし、不正メールアドレスチェック ↓↓↓**//
				ForgetPasswordModel forgetPassword = new ForgetPasswordModel();
				String mailAddress = user.getUserMailAddr();

				if(mailAddress!=null) {
					if(isCorrectMailAddr(mailAddress))
						mailAddress = maskingMailAddress(mailAddress);
					else
						mailAddress = null;
				}else {
					mailAddress = null;
				}
				// **↑↑↑ 2019/02/19  LBN石川 不具合対応 メールアドレス登録なし、不正メールアドレスチェック ↑↑↑**//


				// mode0:メールアドレス表示
				if(mode.equals(0) || mode.intValue()==0) {

					// **↓↓↓ 2019/02/19  LBN石川 不具合対応 メールアドレス登録なし、不正メールアドレスチェック ↓↓↓**//
//					ForgetPasswordModel forgetPassword = new ForgetPasswordModel();
//					String mailAddress = user.getUserMailAddr();
//
//					if(mailAddress!=null)
//						forgetPassword.setMailAddress(maskingMailAddress(mailAddress));
//					else
//						forgetPassword.setMailAddress("No mail address.");
					if(mailAddress!=null)
						forgetPassword.setMailAddress(maskingMailAddress(mailAddress));
					else
						forgetPassword.setMailAddress("No mail address.");
					// **↑↑↑ 2019/02/19  LBN石川 不具合対応 メールアドレス登録なし、不正メールアドレスチェック ↑↑↑**//

					forgetPassword.setStatusCode(Constants.CON_OK);
					return Response.ok(forgetPassword).build();


				// mdoe1:パスワードメール送信
				}else if(mode.equals(1) || mode.intValue()==1) {

					// **↓↓↓ 2019/02/19  LBN石川 不具合対応 メールアドレス登録なし、不正メールアドレスチェック ↓↓↓**//
					if(mailAddress!=null) {
					// **↑↑↑ 2019/02/19  LBN石川 不具合対応 メールアドレス登録なし、不正メールアドレスチェック ↑↑↑**//

						String contents = null;
						String title = null;
						String userName = user.getUserName();
						String mailAddr = user.getUserMailAddr();
						String password = user.getPassword();

						contents =  "\n"
								+ userName+":\n"
								+ "\n"
								+ "Your RemoteCARE Password is: "+password+"\n"
								+ "\n"
								+ "Thank you.\n"
								+ "\n"
								+ "---\n"
								+ "RemoteCARE\n"
								+ "*This email address is for sending purposes only.\n";

						if(deviceLanguage == 0) {
							title = "Forgotten Password";
						}else if(deviceLanguage == 1) {
							title = "Forgotten Password";
						}else if(deviceLanguage == 2) {
							title = "Forgotten Password";
						}else {
							return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
						}

						if(mailer.sendMail(mailAddr, title, contents, new ArrayList<String>()))
							return Response.ok(new ReturnContainer(Constants.CON_OK)).build();
						else
							return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();


					// **↓↓↓ 2019/02/19  LBN石川 不具合対応 メールアドレス登録なし、不正メールアドレスチェック ↓↓↓**//
					}else {
						// メールアドレスがNULLまたは、不正の場合は、何もせず、正常で返却。
						return Response.ok(new ReturnContainer(Constants.CON_OK)).build();
					}
					// **↑↑↑ 2019/02/19  LBN石川 不具合対応 メールアドレス登録なし、不正メールアドレスチェック ↑↑↑**//


				}else {
					return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
				}

			}else {
				return Response.ok(new ReturnContainer(Constants.CON_WARNING_NO_USER_PASSWORD)).build();

			}

		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}

	}


	// **↓↓↓ 2019/02/19  LBN石川 不具合対応 メールアドレス登録なし、不正メールアドレスチェック ↓↓↓**//
	/**
	 * メールアドレスをチェック
	 * @param mailAddress
	 * @return
	 */
	public boolean isCorrectMailAddr(String mailAddress) {
		return mailAddress.contains("@");
	}
	// **↑↑↑ 2019/02/19  LBN石川 不具合対応 メールアドレス登録なし、不正メールアドレスチェック ↑↑↑**//


	/**
	 * メールアドレスをマスク
	 * @param mailAddress
	 * @return
	 */
	public String maskingMailAddress(String mailAddress) {

		int marker = mailAddress.indexOf("@");
		StringBuilder asterisk = new StringBuilder();
		StringBuilder mailAddr = new StringBuilder(mailAddress);

		for(int counter=2; counter < marker; counter++)
			asterisk.append("*");

		mailAddr.replace(2, marker, new String(asterisk));

		return new String(mailAddr);
	}



}
