package com.lbxco.RemoteCAREApp.resources;

import java.math.BigDecimal;
import java.sql.Date;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.lbxco.RemoteCAREApp.Constants;
import com.lbxco.RemoteCAREApp.ConvertUtil;
import com.lbxco.RemoteCAREApp.annotations.Authorized;
import com.lbxco.RemoteCAREApp.ejbs.dtcNotice.IDtcNoticeService;
import com.lbxco.RemoteCAREApp.ejbs.machine.IMachineService;
import com.lbxco.RemoteCAREApp.ejbs.report.IReportService;
import com.lbxco.RemoteCAREApp.ejbs.sosiki.ISosikiService;
import com.lbxco.RemoteCAREApp.ejbs.user.IUserService;
import com.lbxco.RemoteCAREApp.entities.MstMachine;
import com.lbxco.RemoteCAREApp.entities.TblTeijiReport;
import com.lbxco.RemoteCAREApp.models.GraphReportModel;
import com.lbxco.RemoteCAREApp.models.ReportOperatingListModel;
import com.lbxco.RemoteCAREApp.models.ReturnContainer;
import com.lbxco.RemoteCAREApp.util.MachineIconType;

@Path("/graphReport")
@Stateless
public class GraphReportResource {

	private static final Logger logger = Logger.getLogger(GraphReportResource.class.getName());

	private double unitF;

	@EJB
	IReportService graphReportService;

	@EJB
	IMachineService machineService;

	@EJB
	ISosikiService sosikiService;

	@EJB
	IDtcNoticeService dtcService;

    @EJB
    IUserService userService;


	/**
	 * [API No.11] TññÚ×æ¾
	 * @param serialNumber
	 * @param searchDateFrom
	 * @param searchDateTo
	 * @param userId
	 * @return Response.JSON GraphReportModel
	 * @throws ParseException
	 */
	@SuppressWarnings("deprecation")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response getReport(
				@FormParam("serialNumber") Integer serialNumber,
				@FormParam("searchDateFrom") String searchDateFrom,
				@FormParam("searchDateTo") String searchDateTo,
				@FormParam("userId") String userId
			) throws ParseException {

		logger.info("[RemoteCAREApp][POST] userId="+userId+", serialNumber="+serialNumber+", searchDateFrom="+searchDateFrom+", searchDateTo="+searchDateTo);

		try {

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			DecimalFormat df = new DecimalFormat("#.#");

			Date dateFrom = new Date(sdf.parse(searchDateFrom).getTime());
			Date dateTo = new Date(sdf.parse(searchDateTo).getTime());

			GraphReportModel graphReport = new GraphReportModel();

			// @Bîñæ¾
			MstMachine machine = machineService.findByMachine(serialNumber);
			if (machine!=null) {

				graphReport.setSerialNumber(serialNumber);
				graphReport.setYearMonth(new SimpleDateFormat("yyyy-MM").format(dateFrom));

				graphReport.setCustomerManagementNo(machine.getUserKanriNo());
				if (machine.getKiban().length() > 0)
					graphReport.setMachineModelCategory(Double.parseDouble("0" + machine.getKiban().charAt(7)));
				graphReport.setManufacturerSerialNumber(machine.getKiban());
				if(machine.getKiban().substring(7, 8).equals("6"))
					graphReport.setMachineModelCategory(1);
				else
					graphReport.setMachineModelCategory(2);
				graphReport.setLbxSerialNumber(machine.getLbxKiban());
				graphReport.setScmModel(machine.getModelCd());
				graphReport.setLbxModel(machine.getLbxModelCd());
				graphReport.setLatestLocation(machine.getPositionEn());
				if(machine.getNewIdo() != null)
					graphReport.setIdo(ConvertUtil.parseLatLng(machine.getNewIdo()));
				if(machine.getNewKeido() != null)
					graphReport.setKeido(ConvertUtil.parseLatLng(machine.getNewKeido()));
				if(machine.getNewHourMeter()!=null)
					graphReport.setHourMeter(Math.floor(machine.getNewHourMeter()/60.0*10)/10);
				graphReport.setFuelLevel(machine.getNenryoLv());
				if(machine.getRecvTimestamp()!=null)
					graphReport.setLatestUtcCommonDateTime(sdf.format(machine.getRecvTimestamp()));
				graphReport.setDefLevelAdblueLevel(machine.getUreaWaterLevel());

				// **««« 2018/12/10 LBNÎì tF[Y2@BACRÎ    «««**//
				List<Object[]> dtcResults = dtcService.findByDtcLevelCount(serialNumber, userService.findByUserSosikiKengenCd(userId));
				graphReport.setIconType(MachineIconType.stateMachineIconType(dtcResults, machine));
				// **ªªª 2018/12/10 LBNÎì tF[Y2@BACRÎ    ªªª**//
			}


			// éÆgD¼Ì
			List<Object[]> kigyouInfo = sosikiService.findByKigyouSosikiName(serialNumber);

			if(kigyouInfo!=null) {
				if(kigyouInfo.get(0)[0]!=null && kigyouInfo.get(0)[1]!=null)
					graphReport.setCustomerManagementName(kigyouInfo.get(0)[0]+" "+kigyouInfo.get(0)[1]);
				else if(kigyouInfo.get(0)[0]!=null && kigyouInfo.get(0)[1]==null)
					graphReport.setCustomerManagementName((String)kigyouInfo.get(0)[0]);
				else if(kigyouInfo.get(0)[0]==null && kigyouInfo.get(0)[1]!=null)
					graphReport.setCustomerManagementName((String)kigyouInfo.get(0)[1]);

			}


			// [U[ÝèPÊæ¾
			unitF = 1;
			if (graphReportService.getUserSetting(userId) == 0) {
				unitF = 3.78541;
			}

			// |[gîñæ¾
			java.sql.Timestamp to = new java.sql.Timestamp(dateTo.getTime());
			to.setHours(23);
			to.setMinutes(59);
			to.setSeconds(59);

			List<Object[]> objList = graphReportService.getAllInfo(serialNumber, new java.sql.Timestamp(dateFrom.getTime()), to);


		if (objList != null && objList.size()>0) {

			List<TblTeijiReport> reportInfoList = parseReport(objList);

			int reportCount = reportInfoList.size();
			logger.config("[RemoteCAREApp][DEBUG] reportListCount="+reportCount);

			if (reportCount > 0) {

				List<ReportOperatingListModel> dailyRpList = new ArrayList<ReportOperatingListModel>();

				BigDecimal engineOperatingAllTotal = new BigDecimal(0);
				BigDecimal machineOperatingAllTotal = new BigDecimal(0);
				BigDecimal nenryoConsumAllTotal = new BigDecimal(0);
				BigDecimal waterConsumAllTotal = new BigDecimal(0);


				// úWv
				for (TblTeijiReport rp : reportInfoList) {

					ReportOperatingListModel dayRp = new ReportOperatingListModel();

					// @Bìú
					dayRp.setDate(sdf.format(rp.getKikaiKadobiLocal()));
					logger.config("[RemoteCAREApp][DEBUG][D] kikaiKadobiLocal="+sdf.format(rp.getKikaiKadobiLocal()));

					// GWÒ­Ô engineOperatingTime
					BigDecimal engineOperating = new BigDecimal(0);
					if(rp.getHourMeter() != null || rp.getHourMeter() > 0) {
						engineOperating = new BigDecimal(df.format(Math.floor(rp.getHourMeter()/60.0*10)/10));
						dayRp.setEngineOperatingTime(engineOperating.doubleValue());
						engineOperatingAllTotal = engineOperatingAllTotal.add(engineOperating);
						logger.config("[RemoteCAREApp][DEBUG][D] engineOperatingTime="+engineOperating.doubleValue());
//						engineOperatingTotal += rp.getHourMeter();
					}

					// @BìÔ machineOperatingTime
					BigDecimal machineOperating = new BigDecimal(0);
					if (rp.getKikaiSosaTime() != null || rp.getKikaiSosaTime() > 0) {
						machineOperating = new BigDecimal(df.format(Math.floor(rp.getKikaiSosaTime()/60.0*10)/10));
						dayRp.setMachineOperatingTime(machineOperating.doubleValue());
						machineOperatingAllTotal = machineOperatingAllTotal.add(machineOperating);
						logger.config("[RemoteCAREApp][DEBUG][D] machineOperatingTime="+machineOperating.doubleValue());
//						machineOperatingTotal += rp.getKikaiSosaTime();
					}

					// R¿ÁïÊ
					BigDecimal fuelConsumption = new BigDecimal(0);
					if(rp.getNenryoConsum() != null) {
						fuelConsumption = new BigDecimal(df.format(Math.floor(rp.getNenryoConsum()/unitF*10.0)/10));
						dayRp.setFuelConsumption(fuelConsumption.doubleValue());
						nenryoConsumAllTotal = nenryoConsumAllTotal.add(fuelConsumption);
						logger.config("[RemoteCAREApp][DEBUG][D] fuelConsumption="+fuelConsumption.doubleValue());
//						nenryoConsumTotal += rp.getNenryoConsum();
					}

					// AfÁïÊ
					BigDecimal defConsumption = new BigDecimal(0);
					if(rp.getUreaWaterConsum() != null) {
						if (unitF == 1) {
							// **«««@2018/05/15  LBNÎì  v]sïÇä  No36 bgÎ  «««**//
//							defConsumption = new BigDecimal(rp.getUreaWaterConsum());
							defConsumption = new BigDecimal(df.format(Math.floor(rp.getUreaWaterConsum()/100)/10)).setScale(1, BigDecimal.ROUND_HALF_UP);
							// **ªªª@2018/05/15  LBNÎì  v]sïÇä  No36 bgÎ  ªªª**//
						}else {
							defConsumption = new BigDecimal((rp.getUreaWaterConsum()/unitF)/1000).setScale(1, BigDecimal.ROUND_HALF_UP);
							logger.config("[RemoteCAREApp][DEBUG][D] defConsumption="+((rp.getUreaWaterConsum()/unitF)/1000)+" :noformat");
						}
						dayRp.setDefConsumpiton(defConsumption.doubleValue());
						waterConsumAllTotal = waterConsumAllTotal.add(defConsumption);
						logger.config("[RemoteCAREApp][DEBUG][D] defConsumption="+defConsumption.doubleValue());
					}


					// ACh¦ idlePercentage
//					if(engineOperating.doubleValue()>0 && machineOperating.doubleValue()>0) {
					if(engineOperating.doubleValue()>0) {

//						double idleTime = Math.floor((rp.getHourMeter()-rp.getKikaiSosaTime())*10)/10;	//this 180227 before
//						BigDecimal idlePercentage = new BigDecimal(df.format(Math.floor((idleTime*100/rp.getHourMeter())*10)/10));	//this 180227 before

						double idleTime = new BigDecimal(engineOperating.doubleValue()-machineOperating.doubleValue()).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();
						logger.config("[RemoteCAREApp][DEBUG][D] idleTime="+idleTime);
						if(idleTime > 0) {
							BigDecimal idlePercentage = new BigDecimal((idleTime*100/engineOperating.doubleValue())).setScale(1, BigDecimal.ROUND_HALF_UP);
							dayRp.setIdlePercentage(idlePercentage.doubleValue());
							logger.config("[RemoteCAREApp][DEBUG][D] idlePercentage="+idlePercentage.doubleValue());
						}
					}

					// Rï fuelEffciency
					if(fuelConsumption.doubleValue()>0 && engineOperating.doubleValue()>0) {
//						BigDecimal fuelEffciency = new BigDecimal(df.format(Math.floor((rp.getNenryoConsum()/unitF)/(rp.getHourMeter()/60.0)*100)/100));	//this 180227 before

						BigDecimal fuelEffciency = new BigDecimal(Math.floor(fuelConsumption.doubleValue()/engineOperating.doubleValue()*100)/100).setScale(1, BigDecimal.ROUND_HALF_UP);
						dayRp.setFuelEffciency(fuelEffciency.doubleValue());
					}

					dailyRpList.add(dayRp);
				}
				graphReport.setOperatingPartialList(dailyRpList);


				// Tv
				logger.config("[RemoteCAREApp][DEBUG][W] getWeeklyTotal()");
				graphReport.setOperatingWeekTotalList(getWeeklyTotal(reportInfoList, dateFrom, dateTo));


				// v
				graphReport.setEngineOperatingTimeTotal(engineOperatingAllTotal.doubleValue());
				graphReport.setMachineOperatingTimeTotal(machineOperatingAllTotal.doubleValue());
				graphReport.setFuelConsumptionTotal(nenryoConsumAllTotal.doubleValue());
				graphReport.setDefConsumpitonTotal(waterConsumAllTotal.doubleValue());
				logger.config("[RemoteCAREApp][DEBUG][M] engineOperating="+engineOperatingAllTotal.doubleValue());
				logger.config("[RemoteCAREApp][DEBUG][M] machineOperating="+machineOperatingAllTotal.doubleValue());
				logger.config("[RemoteCAREApp][DEBUG][M] nenryoConsum="+nenryoConsumAllTotal.doubleValue());
				logger.config("[RemoteCAREApp][DEBUG][M] waterConsum="+waterConsumAllTotal.doubleValue());

				// ACh¦
//				if(engineOperatingAllTotal.doubleValue()>0 && machineOperatingAllTotal.doubleValue()>0) {
				if(engineOperatingAllTotal.doubleValue()>0) {
					double idleTime = new BigDecimal((engineOperatingAllTotal.doubleValue()-machineOperatingAllTotal.doubleValue())).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();	// Ûßë·Î
					logger.config("[RemoteCAREApp][DEBUG][M] idleTime="+(engineOperatingAllTotal.doubleValue()-machineOperatingAllTotal.doubleValue())+" :noformat");
					logger.config("[RemoteCAREApp][DEBUG][M] idleTime="+idleTime);
					if(idleTime > 0) {
						BigDecimal idlePercentage = new BigDecimal((idleTime*100/engineOperatingAllTotal.doubleValue())).setScale(1, BigDecimal.ROUND_HALF_UP);
						graphReport.setIdlePercentageTotal(idlePercentage.doubleValue());
						logger.config("[RemoteCAREApp][DEBUG][M] idlePercentage="+idlePercentage.doubleValue());
					}
				}

				// Rï
				if(nenryoConsumAllTotal.doubleValue()>0 && engineOperatingAllTotal.doubleValue()>0) {
//					double fuelEffciencyTotal = nenryoConsumAllTotal.doubleValue() / engineOperatingAllTotal.doubleValue();

					BigDecimal fuelEffciency = new BigDecimal(Math.floor((nenryoConsumAllTotal.doubleValue()/engineOperatingAllTotal.doubleValue())*100)/100).setScale(1, BigDecimal.ROUND_HALF_UP);
					graphReport.setFuelEffciencyTotal(fuelEffciency.doubleValue());
					logger.config("[RemoteCAREApp][DEBUG][M] fuelEffciency="+fuelEffciency.doubleValue());
				}

			}
		}

		graphReport.setStatusCode(Constants.CON_OK);
		return Response.ok(graphReport).build();

		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}

	}



	/**
	 * T|[gWv
	 * @param reportInfoList
	 * @param dateFrom
	 * @param dateTo
	 * @return List<ReportOperatingListModel>
	 *
	 * BigDecimal.ROUND_HALF_UP Ûßë·Îô
	 */
	public List<ReportOperatingListModel> getWeeklyTotal(List<TblTeijiReport> reportInfoList, Date dateFrom, Date dateTo){

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			DecimalFormat df = new DecimalFormat("#.#");

			int year = Integer.parseInt(new SimpleDateFormat("yyyy").format(dateFrom));
			int month = Integer.parseInt(new SimpleDateFormat("M").format(dateFrom));
			int startDay = Integer.parseInt(new SimpleDateFormat("d").format(dateFrom));
			int endDay = Integer.parseInt(new SimpleDateFormat("d").format(dateTo));
			int reportCount = reportInfoList.size();
			logger.config("[RemoteCAREApp][DEBUG] reportCount="+reportCount);
			logger.config("[RemoteCAREApp][DEBUG] startDay="+startDay);
			logger.config("[RemoteCAREApp][DEBUG] endDay  ="+endDay);

//			double engineOperatingTotal = 0;
//			double machineOperatingTotal = 0;
//			double nenryoConsumTotal = 0;

			BigDecimal engineOperatingWeekTotal = new BigDecimal(0);
			BigDecimal machineOperatingWeekTotal = new BigDecimal(0);
			BigDecimal nenryoConsumWeekTotal = new BigDecimal(0);
			BigDecimal waterConsumWeekTotal = new BigDecimal(0);

			List<ReportOperatingListModel> weekReportList = new ArrayList<ReportOperatingListModel>();
			ReportOperatingListModel weekReport = new ReportOperatingListModel();

			int count = 0;
			Calendar cal = new GregorianCalendar(year ,month-1 ,startDay);


			for(int dayCounter = startDay; dayCounter <= endDay; dayCounter++) {

				cal = new GregorianCalendar(year ,month-1 ,dayCounter);

				if(cal.get(Calendar.DAY_OF_WEEK)==2 || cal.get(Calendar.DATE)==1) {
					engineOperatingWeekTotal = new BigDecimal(0);
					machineOperatingWeekTotal = new BigDecimal(0);
					nenryoConsumWeekTotal = new BigDecimal(0);
					waterConsumWeekTotal = new BigDecimal(0);

//					engineOperatingTotal = 0;
//					machineOperatingTotal = 0;
//					nenryoConsumTotal = 0;

					weekReport = new ReportOperatingListModel();

					weekReport.setDateFrom(sdf.format(cal.getTime()));

				}

				if(count<reportCount) {
					if(sdf.format(cal.getTime()).equals(sdf.format(reportInfoList.get(count).getKikaiKadobiLocal()))) {

						TblTeijiReport rp = reportInfoList.get(count);

						if(rp.getHourMeter() != null || rp.getHourMeter() > 0) {
							BigDecimal hourMeter = new BigDecimal(df.format(Math.floor(rp.getHourMeter()/60.0*10)/10));
							engineOperatingWeekTotal = engineOperatingWeekTotal.add(hourMeter);
//							engineOperatingTotal += rp.getHourMeter();
						}

						if (rp.getKikaiSosaTime() != null || rp.getKikaiSosaTime() > 0) {
							BigDecimal kikaiSosa = new BigDecimal(df.format(Math.floor(rp.getKikaiSosaTime()/60.0*10)/10));
							machineOperatingWeekTotal = machineOperatingWeekTotal.add(kikaiSosa);
//							machineOperatingTotal += rp.getKikaiSosaTime();
						}

						if(rp.getNenryoConsum() != null) {
							BigDecimal nenryoConsum = new BigDecimal(df.format(Math.floor(rp.getNenryoConsum()/unitF*10.0)/10));
							nenryoConsumWeekTotal = nenryoConsumWeekTotal.add(nenryoConsum);
//							nenryoConsumTotal += rp.getNenryoConsum()/unitF;
						}



						if(rp.getUreaWaterConsum() != null) {
							BigDecimal defConsum = new BigDecimal(0);
							if (unitF == 1)
								// **«««@2018/05/15  LBNÎì  v]sïÇä  No36 bgÎ  «««**//
//								defConsum = new BigDecimal(df.format(rp.getUreaWaterConsum()));
								defConsum = new BigDecimal(df.format(Math.floor(rp.getUreaWaterConsum()/100)/10)).setScale(1, BigDecimal.ROUND_HALF_UP);
								// **ªªª@2018/05/15  LBNÎì  v]sïÇä  No36 bgÎ  ªªª**//
							else
								defConsum = new BigDecimal((rp.getUreaWaterConsum()/unitF)/1000).setScale(1, BigDecimal.ROUND_HALF_UP);
//								defConsum = new BigDecimal(df.format(Math.floor((rp.getUreaWaterConsum()/unitF)/1000*10.0)/10));
							waterConsumWeekTotal = waterConsumWeekTotal.add(defConsum);
						}
						count++;	// <-----this!
					}				// <-----this!
				}

				if(cal.get(Calendar.DAY_OF_WEEK)==1 || cal.get(Calendar.DATE)==endDay) {

					logger.config("[RemoteCAREApp][DEBUG] dateTo="+sdf.format(cal.getTime()));
					weekReport.setDateTo(sdf.format(cal.getTime()));

					weekReport.setEngineOperatingTime(engineOperatingWeekTotal.doubleValue());
					weekReport.setMachineOperatingTime(machineOperatingWeekTotal.doubleValue());
					weekReport.setFuelConsumption(nenryoConsumWeekTotal.doubleValue());
					weekReport.setDefConsumpiton(waterConsumWeekTotal.doubleValue());
					logger.config("[RemoteCAREApp][DEBUG][W] engineOperating="+engineOperatingWeekTotal.doubleValue());
					logger.config("[RemoteCAREApp][DEBUG][W] machineOperating="+machineOperatingWeekTotal.doubleValue());
					logger.config("[RemoteCAREApp][DEBUG][W] nenryoConsum="+nenryoConsumWeekTotal.doubleValue());
					logger.config("[RemoteCAREApp][DEBUG][W] waterConsum="+waterConsumWeekTotal.doubleValue());

					// ACh¦
//					if(engineOperatingWeekTotal.doubleValue() > 0 && machineOperatingWeekTotal.doubleValue() > 0) {
					if(engineOperatingWeekTotal.doubleValue() > 0) {
//						double idleTime = Math.floor((engineOperatingTotal-machineOperatingTotal)*10)/10;
//						BigDecimal idlePercentage = new BigDecimal(df.format(Math.floor((idleTime*100/engineOperatingTotal)*10)/10));

						double idleTime = new BigDecimal(engineOperatingWeekTotal.doubleValue()-machineOperatingWeekTotal.doubleValue()).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();	// Ûßë·Î
						logger.config("[RemoteCAREApp][DEBUG][W] idleTime="+(engineOperatingWeekTotal.doubleValue()-machineOperatingWeekTotal.doubleValue())+" :noformat");
						logger.config("[RemoteCAREApp][DEBUG][W] idleTime="+idleTime);
						if(idleTime>0) {
							BigDecimal idlePercentage = new BigDecimal((idleTime*100/engineOperatingWeekTotal.doubleValue())).setScale(1, BigDecimal.ROUND_HALF_UP);;
							weekReport.setIdlePercentage(idlePercentage.doubleValue());
							logger.config("[RemoteCAREApp][DEBUG][W] idlePercentage="+idlePercentage.doubleValue());
						}
					}

					// Rï
					if(nenryoConsumWeekTotal.doubleValue() > 0 && engineOperatingWeekTotal.doubleValue() > 0) {

						BigDecimal fuelEffciency = new BigDecimal(Math.floor((nenryoConsumWeekTotal.doubleValue()/engineOperatingWeekTotal.doubleValue())*100)/100).setScale(1, BigDecimal.ROUND_HALF_UP);
//						BigDecimal fuelEffciency = new BigDecimal(df.format(
//								Math.floor(nenryoConsumWeekTotal.doubleValue()/engineOperatingWeekTotal.doubleValue()*10)/10));

						weekReport.setFuelEffciency(fuelEffciency.doubleValue());
						logger.config("[RemoteCAREApp][DEBUG][W] fuelEffciency="+fuelEffciency.doubleValue());
					}

					weekReportList.add(weekReport);
				}
			}
			return weekReportList;
	}



	/**
	 *
	 * @param list
	 * @return
	 */
	public List<TblTeijiReport> parseReport(List<Object[]> list) {

		List<TblTeijiReport> rst = new ArrayList<TblTeijiReport>();

		for (Object[] item : list) {
			TblTeijiReport rp = new TblTeijiReport();
			rp.setKikaiKadobiLocal((java.sql.Timestamp) item[0]);
			rp.setHourMeter((Integer) item[1]);
			rp.setKikaiSosaTime((Integer) item[2]);
			rp.setNenryoConsusum((Double) item[3]);
			rp.setUreaWaterConsum((Integer) item[4]);
			rst.add(rp);
		}
		return rst;
	}



}
