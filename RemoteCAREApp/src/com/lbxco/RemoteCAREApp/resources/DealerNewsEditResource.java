package com.lbxco.RemoteCAREApp.resources;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.imageio.ImageIO;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.lbxco.RemoteCAREApp.Constants;
import com.lbxco.RemoteCAREApp.ConvertUtil;
import com.lbxco.RemoteCAREApp.annotations.Authorized;
import com.lbxco.RemoteCAREApp.ejbs.dealerNews.IDealerNewsService;
import com.lbxco.RemoteCAREApp.ejbs.user.IUserService;
import com.lbxco.RemoteCAREApp.models.DealerNewsEditModel;
import com.lbxco.RemoteCAREApp.models.NewsModel;
import com.lbxco.RemoteCAREApp.models.ReturnContainer;
import com.lbxco.RemoteCAREApp.util.Base64;
import com.lbxco.RemoteCAREApp.util.BunaryToBufferedImage;

@Path("/dealerNewsEdit")
@Stateless
public class DealerNewsEditResource {

	private static final Logger logger = Logger.getLogger(DealerNewsEditResource.class.getName());


	private String languageCd;
	private Integer kengenCd;
	private String dealerCd;

	@EJB
	IDealerNewsService dealerNewsService;

	@EJB
	IUserService userService;


	/**
	 * [API No.24] 代理店向けお知らせ編集
	 * @param userId
	 * @param dealerLogoImage
	 * @param dealerLogoImageEditFlg
	 * @param newsList
	 * @return Response.JSON statusCode
	 */
	@POST
//	@Consumes("application/json")	// add
	@Consumes(MediaType.APPLICATION_JSON)	// add
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(DealerNewsEditModel dealerNewsEdit) {


		logger.config("userId="+dealerNewsEdit.getUserId()+", dealerLogoImageEditFlg="+dealerNewsEdit.getDealerLogoImageEditFlg()+", newsList="+dealerNewsEdit.getNewsList());

		try {

			// 設定言語
			languageCd = ConvertUtil.convertLanguageCd(Constants.CON_ENGLISH_ONLY);

			// 所属代理店を取得
			// ユーザー組織・権限
			List<Object[]> userSosikiKengen = userService.findByUserSosikiKengenNativeQuery(dealerNewsEdit.getUserId());
			if(userSosikiKengen!=null) {
				kengenCd = Integer.parseInt(userSosikiKengen.get(0)[4].toString()); // [4]KENGEN_CD
				dealerCd = userSosikiKengen.get(0)[1].toString();					// [1]SOSIKI_CD
			}


			/* **
			 *  代理店ロゴ画像   代理店ロゴ画像編集フラグ 1:編集あり
			 */
			if(dealerNewsEdit.getDealerLogoImageEditFlg()==1) {

				// ファイル名および参照パスを生成
				String dbImagepath = Constants.FILE_STORAGE_DEALER_LOGO_SAVE_PATH + dealerCd + ".jpg";
				String outputImagePath = Constants.FILE_STORAGE_PATH + Constants.FILE_STORAGE_DEALER_LOGO_SAVE_PATH + dealerCd + ".jpg";

				// ファイルサーバーにjpg画像を保存
				try {

//					リンクをはるので作らない
//					File theDir = new File(Constants.DEALER_LOGO_SAVE_PATH);
//					if (!theDir.exists()) {
//						theDir.mkdir();
//					}

					if(dealerNewsEdit.getDealerLogoImage()!=null) {

						// ファイルサーバーに画像を保存
						saveStorageImageFile(outputImagePath, dealerNewsEdit.getDealerLogoImage());

						// 代理店ロゴの参照先をテーブルに新規登録または更新
						String mstAppDealerLogoDealerCd = dealerNewsService.findByDealerCd(dealerCd);
						dealerNewsService.updateToMstAppDealerLogo(dealerCd, kengenCd, dbImagepath, mstAppDealerLogoDealerCd, dealerNewsEdit.getUserId());

					}else {

						// 画像削除に伴い、代理店ロゴ管理テーブルから代理店情報を削除
						logger.config("getDealerLogoImage Dealer logo image is null : deleteToTblNews");
						dealerNewsService.deleteToTblNews(dealerCd);

						// ストレージからファイルを削除
						deleteStorageImageFile(outputImagePath);

					}
				}catch(IOException e) {
					logger.log(Level.WARNING, e.getMessage());
					return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
				}
			}



			/* **
			 * ニュース新規登録・更新処理
			 */
			if(dealerNewsEdit.getNewsList()!=null) {
				logger.config("NewsList().size()="+dealerNewsEdit.getNewsList().size());

				for(NewsModel newsModel: dealerNewsEdit.getNewsList()) {

					// 代理店ニュース更新
					dealerNewsService.updateToTblNews(
							Constants.CON_APP_FLG,
							languageCd,
							newsModel.getNewsId(),
							newsModel.getNewsDateFrom(),
							newsModel.getNewsDateTo(),
							newsModel.getNewsContents(),
							newsModel.getNewsUrl(),
							newsModel.getNewsDelFlg(),
							dealerCd,
							dealerNewsEdit.getUserId()
							);
					}
			}
			return Response.ok(new ReturnContainer(Constants.CON_OK)).build();

		}catch (Exception e) {
			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}
	}






	/**
	 * ファイルサーバーに画像を保存
	 * @param outputImagePath
	 * @param dealerLogoImage
	 * @throws IOException
	 */
	private void saveStorageImageFile(String outputImagePath, String dealerLogoImage) throws IOException {

		logger.config("DealerLogoImage decode : ImageIO.write : Start.");
		// String を Base64デコードしbyte[]に変換
		byte[] decodeBinary = Base64.decodeBase64(dealerLogoImage);
		logger.config("DealerLogoImage decode : ImageIO.write : decodeBinary OK!");

//		InputStream byteArrayStream = new ByteArrayInputStream(decodeBinary);
//		BufferedImage bufferedImage = ImageIO.read(byteArrayStream);
//		byteArrayStream.close();
		BufferedImage bufferedImage = BunaryToBufferedImage.readBinary(decodeBinary);
		logger.config("DealerLogoImage decode : ImageIO.write : ImageIO.read OK! ");

		ByteArrayOutputStream outPutStream = new ByteArrayOutputStream();
		ImageIO.write(bufferedImage, "jpg", outPutStream);
//		byte[] imageBinary = outPutStream.toByteArray();
		logger.config("DealerLogoImage decode : ImageIO.write : OutPutStream OK!");

		// BufferedImage型に変換しFileOutputでjpg保存
		BufferedImage bufImage = ImageIO.read(new ByteArrayInputStream(outPutStream.toByteArray()));
		FileOutputStream out = new FileOutputStream(outputImagePath);
		ImageIO.write(bufImage, "jpg", out);
		out.close();
		logger.config("DealerLogoImage decode : ImageIO.write : FileOutputStream OK!");
		logger.config("DealerLogoImage decode : ImageIO.write : End.");

		return;
	}


	/**
	 * ファイルサーバーから画像を削除
	 * @param outputImagePath
	 * @throws IOException
	 */
	private void deleteStorageImageFile(String outputImagePath) throws IOException {

		logger.config("deleteStorageImageFile : outputImagePath="+outputImagePath);

		File file = new File(outputImagePath);
		if (file.exists()){
			if (file.delete()){
				logger.config("Dealer image file deleted.");
			}else{
				logger.warning("Dealer image file deletion failed.");
			}
		}else{
			logger.warning("Dealer image file not found.");
		}

	}


}
