package com.lbxco.RemoteCAREApp.resources;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.lbxco.RemoteCAREApp.Constants;
import com.lbxco.RemoteCAREApp.ConvertUtil;
import com.lbxco.RemoteCAREApp.annotations.Authorized;
import com.lbxco.RemoteCAREApp.ejbs.dtcNotice.IDtcNoticeService;
import com.lbxco.RemoteCAREApp.ejbs.machine.IMachineService;
import com.lbxco.RemoteCAREApp.ejbs.machineDitail.IMachineDitailService;
import com.lbxco.RemoteCAREApp.ejbs.user.IUserService;
import com.lbxco.RemoteCAREApp.entities.MstMachine;
import com.lbxco.RemoteCAREApp.models.MachineDetailModel;
import com.lbxco.RemoteCAREApp.models.ReturnContainer;
import com.lbxco.RemoteCAREApp.util.MachineIconType;

@Path("/machineDetail")
@Stateless
public class MachineDetailResource{

	private static final Logger logger = Logger.getLogger(MachineDetailResource.class.getName());

	@EJB
	IMachineDitailService machineDitailService;

		@EJB
		IDtcNoticeService dtcService;

		@EJB
		IUserService userService;

		@EJB
		IMachineService machineService;


	/**
	 * [API No.6] @BΪΧζΎ
	 * @param userId
	 * @param serialNumber
	 * @return Response.JSON machineDetailModel
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response getMachineDitail(
			@FormParam("userId") String userId,
			@FormParam("serialNumber") Integer serialNumber,
			@FormParam("topFlg") Integer topFlg
		){

		logger.info("[RemoteCAREApp][POST] userId="+userId+", serialNumber="+serialNumber);

		MachineDetailModel machineDetail = new MachineDetailModel();

		try {

			MstMachine machine = machineService.findByMachine(serialNumber);

			if(machine != null) {

				machineDetail.setSerialNumber(machine.getKibanSerno());

				if(machine.getKiban().substring(7, 8).equals("6"))
					machineDetail.setMachineModelCategory(1);
				else
					machineDetail.setMachineModelCategory(2);

				machineDetail.setManagementNo(machine.getUserKanriNo());
				machineDetail.setManufacturerSerialNumber(machine.getKiban());
				machineDetail.setLbxSerialNumber(machine.getLbxKiban());
				machineDetail.setLbxModel(machine.getLbxModelCd());
				machineDetail.setEngineSerialNumber(machine.getEngineSerno());

				if(machine.getNonyubi() != null)
					machineDetail.setDeliveryDate(ConvertUtil.formatYMD(machine.getNonyubi()));

				Double hourMeter = machine.getNewHourMeter();
				if(hourMeter != null) {
					machineDetail.setHourMeter(Math.floor(hourMeter/60.0*10)/10);
				}
				else
					machineDetail.setHourMeter(0.0);

				if(machine.getRecvTimestamp() != null)
					machineDetail.setLatestUtcCommonDateTime(ConvertUtil.formatYMDHMS(machine.getRecvTimestamp()));

				machineDetail.setMonitorFuelBar(machine.getNenryoLv());
				machineDetail.setDefLevelAdblueLevel(machine.getUreaWaterLevel());

				Double ido = null;
				Double keido = null;
				if(machine.getNewIdo()!=null)
					ido = ConvertUtil.parseLatLng(machine.getNewIdo());
				if(machine.getNewKeido()!=null)
					keido = ConvertUtil.parseLatLng(machine.getNewKeido());

				machineDetail.setIdo(ido);
				machineDetail.setKeido(keido);

				if(machine.getPositionEn()!=null && ido!=null && keido!=null)
					machineDetail.setLatestLocation(machine.getPositionEn()+" ("+ido+","+keido+")");
				else if(machine.getPositionEn()==null && ido!=null && keido!=null)
					machineDetail.setLatestLocation("("+ido+","+keido+")");
				else
					machineDetail.setLatestLocation(machine.getPositionEn());

				// CoustomerName/Branch
				List<Object[]> machineSosiki = machineDitailService.findByCustomer(serialNumber);
				if(machineSosiki != null) {

					StringBuilder sb1 = new StringBuilder();
					if((String)machineSosiki.get(0)[2] != null) {
						sb1.append((String)machineSosiki.get(0)[2]+" ");
					}
					if((String)machineSosiki.get(0)[3] != null) {
						sb1.append((String)machineSosiki.get(0)[3]);
					}
					machineDetail.setCustomerName(new String(sb1));
				}



				// **«««@2018/03/23  LBNΞμ  v]sοΗδ  No6Ξ C³ «««**//
				if((String)machineSosiki.get(0)[6] != null) {

						StringBuilder sb2 = new StringBuilder();
						String dairitenCd = (String)machineSosiki.get(0)[6];
						logger.config("[RemoteCAREApp][DEBUG] dairitenCd="+dairitenCd);

						List<Object[]> dealerInfo = machineDitailService.findByDealer(dairitenCd);
						String psmName = null;
						// **«««@2018/04/10  LBNΞμ  v]sοΗδ  No34Ξ v]ΗΑ «««**//
						String psmCd = null;
						// **ͺͺͺ@2018/04/10  LBNΞμ  v]sοΗδ  No34Ξ v]ΗΑ ͺͺͺ**//


						if(dealerInfo!=null) {

							// DealerName/Branch
							if((String)dealerInfo.get(0)[0] != null)
								sb2.append((String)dealerInfo.get(0)[0]+" ");
							if((String)dealerInfo.get(0)[1] != null)
								sb2.append((String)dealerInfo.get(0)[1]);
							machineDetail.setDealerName(new String(sb2));

							// **«««@2018/11/28  iDEARΊ  Xebv2@γXdbΤΗΑ «««**//
							// DealerTel
							if((String)dealerInfo.get(0)[5] != null)
								machineDetail.setDealerTel((String)dealerInfo.get(0)[5]);
							// **ͺͺͺ@2018/11/28  iDEARΊ  Xebv2@γXdbΤΗΑ ͺͺͺ**//

							// psmName
							if((String)dealerInfo.get(0)[2]!=null) {
								psmName = (String)dealerInfo.get(0)[2];
								psmCd = (String)dealerInfo.get(0)[4];
								machineDetail.setPsm(psmName);
							}

							// RegionalManagerName
							if((String)dealerInfo.get(0)[3]!=null) {
								machineDetail.setRegionalManager((String)dealerInfo.get(0)[3]);
							}
						}
					// **ͺͺͺ@2018/03/23  LBNΞμ  v]sοΗδ  No6Ξ C³ ͺͺͺ**//


					// **«««@2018/05/11  LBNΞμ  v]sοΗδ  No44Ξ ΗΑ «««**//
					Integer processTypeFlg = userService.findByUserSosikiProcessTypeFlg(userId);
					logger.config("[RemoteCAREApp][DEBUG] processTypeFlg="+processTypeFlg);

					if(processTypeFlg == 61 || processTypeFlg == 62) {
						// **«««@2018/04/10  LBNΞμ  v]sοΗδ  No34Ξ ΗΑ «««**//
						if(psmName!=null) {
							String psmTel =  machineDitailService.findByPsmTel(psmCd, psmName);

							if(psmTel!=null) {
									logger.config("[RemoteCAREApp][DEBUG] psmName="+psmName+", psmTel="+psmTel);
									machineDetail.setPsmTel(psmTel);
							}
						}
						// **ͺͺͺ@2018/04/10  LBNΞμ  v]sοΗδ  No34Ξ ΗΑ ͺͺͺ**//
					}
					// **ͺͺͺ@2018/05/11  LBNΞμ  v]sοΗδ  No44Ξ ΗΑ ͺͺͺ**//

				}

				// X4ControllerSerialNo
				machineDetail.setX4ControllerSerialNo(machine.getX4ControllerSerialNo());

				if(machine.getX4ControllerPartNo()!=null) {
					String mainConParts = machineDitailService
							.findByMainConParts(Integer.parseInt(machine.getX4ControllerPartNo()), machine.getConType());
					if(mainConParts!=null)
						machineDetail.setX4ControllerPartNo(mainConParts);
					else
						machineDetail.setX4ControllerPartNo(machine.getX4ControllerPartNo());
				}

				// CtrlABuhinNo
				if(machine.getCtrlABuhinNo()!=null) {
					String ctrlABuhin = machineDitailService
							.findByCtrlABuhinNo(machine.getMachineModel(),
												Integer.parseInt(machine.getCtrlABuhinNo()),
												machine.getConType());
					if(ctrlABuhin!=null)
						machineDetail.setComputerAVersion(ctrlABuhin);
					else
						machineDetail.setComputerAVersion(machine.getCtrlABuhinNo());
				}

				// CtrlBBuhinNo
				if(machine.getCtrlBBuhinNo()!=null) {
					String ctrlBBuhin = machineDitailService
							.findByCtrlBBuhinNo(machine.getMachineModel(),
												Integer.parseInt(machine.getCtrlBBuhinNo()),
												machine.getConType());
					if(ctrlBBuhin!=null)
						machineDetail.setComputerBVersion(ctrlBBuhin);
					else
						machineDetail.setComputerBVersion(machine.getCtrlBBuhinNo());
				}

				// Q4000BuhinNo
				if(machine.getQ4000BuhinNo()!=null) {
					String q4000Version = machineDitailService
							.findByq4000(Integer.parseInt(machine.getQ4000BuhinNo()), machine.getConType());
					if(q4000Version!=null)
						machineDetail.setQ4000Version(q4000Version);
					else
						machineDetail.setQ4000Version(machine.getQ4000BuhinNo());
				}

				// favorite
				Integer favoriteDelFlg = machineDitailService.findByFavoriteDelFlg(serialNumber, userId);
				if(favoriteDelFlg != null)
					machineDetail.setFavoriteDelFlg(favoriteDelFlg);
				else
					machineDetail.setFavoriteDelFlg(1);


				// **«««@2018/08/19 LBNΞμ  v]Ξ ¨CΙόθDTCπΜ ΗΑ «««**//

				//topFlgͺ1ΜκAXVπs€B
				if(topFlg!=null) {
					if(topFlg==1)
						dtcService.updateTblFavoriteDtcHistory(userId, serialNumber, 1, null);
				}

				// ’ΗζΎ
				Integer badgeCount = dtcService.findByNoReadCount(userId);
				if(badgeCount!=null) {
					if(badgeCount>=1000)
						badgeCount=999;	// max
					machineDetail.setDtcBadgeCount(badgeCount);
				}
				// **ͺͺͺ@2018/08/24 LBNΞμ v]Ξ ¨CΙόθDTCπΜ ΗΑ@ͺͺͺ**//

				// **««« 2018/12/10 LBNΞμ tF[Y2@BACRΞ    «««**//
				List<Object[]> dtcResults = dtcService.findByDtcLevelCount(serialNumber, userService.findByUserSosikiKengenCd(userId));
				machineDetail.setIconType(MachineIconType.stateMachineIconType(dtcResults, machine));
				// **ͺͺͺ 2018/12/10 LBNΞμ tF[Y2@BACRΞ    ͺͺͺ**//

			}

			machineDetail.setStatusCode(Constants.CON_OK);
			return Response.ok(machineDetail).build();


		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(), e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}

	}




}
