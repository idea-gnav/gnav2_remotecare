package com.lbxco.RemoteCAREApp.resources;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.lbxco.RemoteCAREApp.Constants;
import com.lbxco.RemoteCAREApp.annotations.Authorized;
import com.lbxco.RemoteCAREApp.ejbs.displayUpdate.IDisplayUpdateService;
import com.lbxco.RemoteCAREApp.models.ReturnContainer;



@Path("/displayUpdate")
@Stateless
public class DisplayUpdateResource {

	private static final Logger logger = Logger.getLogger(DisplayUpdateResource.class.getName());

	@EJB
	IDisplayUpdateService displayUpdateService;


	/**
	 * [API No.16] 設定情報更新
	 * @param userId
	 * @param unitSelect
	 * @param kibanSelect
	 * @return Response.JSON statusCode
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
				@FormParam("userId") String userId,
				@FormParam("unitSelect") Integer unitSelect,
				@FormParam("kibanSelect") Integer kibanSelect,
				@FormParam("searchRangeDistance") Integer searchRangeDistance
		) {

		logger.info("[RemoteCAREApp][POST] userId="+userId+", unitSelect="+unitSelect+", kibanSelect="+kibanSelect+", searchRangeDistance="+searchRangeDistance);

	try {

		displayUpdateService.displayUpdate(userId, unitSelect, kibanSelect, searchRangeDistance);
		return Response.ok(new ReturnContainer(Constants.CON_OK)).build();

	}catch(Exception e) {

		logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
		return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
	}


	}
}
