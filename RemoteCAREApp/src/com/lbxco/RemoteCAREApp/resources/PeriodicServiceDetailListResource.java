package com.lbxco.RemoteCAREApp.resources;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.lbxco.RemoteCAREApp.Constants;
import com.lbxco.RemoteCAREApp.ConvertUtil;
import com.lbxco.RemoteCAREApp.annotations.Authorized;
import com.lbxco.RemoteCAREApp.ejbs.dtcNotice.IDtcNoticeService;
import com.lbxco.RemoteCAREApp.ejbs.machine.IMachineService;
import com.lbxco.RemoteCAREApp.ejbs.periodicServiceDitail.IPeriodicServiceDitailService;
import com.lbxco.RemoteCAREApp.ejbs.sosiki.ISosikiService;
import com.lbxco.RemoteCAREApp.ejbs.user.IUserService;
import com.lbxco.RemoteCAREApp.entities.MstMachine;
import com.lbxco.RemoteCAREApp.models.PeriodicServiceDetailArrayListModel;
import com.lbxco.RemoteCAREApp.models.PeriodicServiceDetailListModel;
import com.lbxco.RemoteCAREApp.models.ReplacementHistoryArrayListModel;
import com.lbxco.RemoteCAREApp.models.ReturnContainer;
import com.lbxco.RemoteCAREApp.util.MachineIconType;

@Path("/periodicServiceDetailList")
@Stateless
public class PeriodicServiceDetailListResource {

	private static final Logger logger = Logger.getLogger(PeriodicServiceDetailListResource.class.getName());

	@EJB
	IPeriodicServiceDitailService periodicServiceDitailService;

	@EJB
	IUserService userService;

	@EJB
	IMachineService machineService;

	@EJB
	ISosikiService sosikiService;

	@EJB
	IDtcNoticeService dtcService;


	/**
	 * [API No.23] èú®õÚ×êæ¾
	 * @param userId
	 * @param serialNumber
	 * @return Response.JSON DtcDetailListModel
	 * @throws Exception
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
				@FormParam("userId") String userId,
				@FormParam("serialNumber") Integer serialNumber,
				@FormParam("deviceLanguage") Integer deviceLanguage
			) throws Exception {

		logger.info("[RemoteCAREApp][POST] userId="+userId+", serialNumber="+serialNumber);

		try {

			PeriodicServiceDetailListModel periodicServiceDetailListModel = new PeriodicServiceDetailListModel();

			MstMachine machine = machineService.findByMachine(serialNumber);

			if(machine != null) {
				periodicServiceDetailListModel.setSerialNumber(machine.getKibanSerno());

				if(machine.getKiban().substring(7,8).equals("6")) {
					periodicServiceDetailListModel.setMachineModelCategory(1);
				}else {
					periodicServiceDetailListModel.setMachineModelCategory(2);
				}

				periodicServiceDetailListModel.setManufacturerSerialNumber(machine.getKiban());
				periodicServiceDetailListModel.setLbxSerialNumber(machine.getLbxKiban());
				periodicServiceDetailListModel.setCustomerManagementNo(machine.getUserKanriNo());
				// **««« 2018/12/10 LBNÎì tF[Y2@BACRÎ    «««**//
				List<Object[]> dtcResults = dtcService.findByDtcLevelCount(serialNumber, userService.findByUserSosikiKengenCd(userId));
				periodicServiceDetailListModel.setIconType(MachineIconType.stateMachineIconType(dtcResults, machine));
				// **ªªª 2018/12/10 LBNÎì tF[Y2@BACRÎ    ªªª**//
				periodicServiceDetailListModel.setScmModel(machine.getModelCd());
				periodicServiceDetailListModel.setLbxModel(machine.getLbxModelCd());
				periodicServiceDetailListModel.setLatestLocation(machine.getPosition());
				if(machine.getNewIdo()!=null) {
					periodicServiceDetailListModel.setIdo(ConvertUtil.parseLatLng(machine.getNewIdo()));
				}
				if(machine.getNewKeido()!=null) {
					periodicServiceDetailListModel.setKeido(ConvertUtil.parseLatLng(machine.getNewKeido()));
				}
				if(machine.getNewHourMeter()!=null) {
					periodicServiceDetailListModel.setHourMeter(Math.floor(machine.getNewHourMeter()/60.0*10)/10);
				}
				if(machine.getRecvTimestamp()!=null) {
					periodicServiceDetailListModel.setLatestUtcCommonDateTime(ConvertUtil.formatYMD(machine.getRecvTimestamp()));
				}
				periodicServiceDetailListModel.setFuelLevel(machine.getNenryoLv());
				periodicServiceDetailListModel.setDefLevelAdblueLevel(machine.getUreaWaterLevel());


			}

			List<Object[]> machineSosiki = sosikiService.findByMachineSosiki(serialNumber);
			if((String)machineSosiki.get(0)[2] != null && (String)machineSosiki.get(0)[3] != null) {
				periodicServiceDetailListModel.setCustomerManagementName((String)machineSosiki.get(0)[2] + " " + (String)machineSosiki.get(0)[3]);
			}else if((String)machineSosiki.get(0)[2] != null && (String)machineSosiki.get(0)[3] == null) {
				periodicServiceDetailListModel.setCustomerManagementName((String)machineSosiki.get(0)[2]);
			}else if((String)machineSosiki.get(0)[2] == null && (String)machineSosiki.get(0)[3] != null) {
				periodicServiceDetailListModel.setCustomerManagementName((String)machineSosiki.get(0)[3]);
			}

			Integer limit = 0;
			if(machine.getConType()!=null) {
				if(machine.getConType().equals("C")) {
					limit = 13;
				}else if(machine.getConType().equals("C2")){
					limit = 14;
				}
			}

			ArrayList<PeriodicServiceDetailArrayListModel> periodicServiceDetailArrayList = new ArrayList<PeriodicServiceDetailArrayListModel>(); //èú®õÚ×Xg

			for(Integer partsNo = 0 ; partsNo < limit; partsNo++ ) {

				List<Object[]> serviceRequest = periodicServiceDitailService.findByServiceRequestDitailNativeQuery(serialNumber,machine.getConType(),machine.getMachineModel(), partsNo,ConvertUtil.convertLanguageCd(deviceLanguage));

				if(serviceRequest != null) {
					PeriodicServiceDetailArrayListModel PeriodicServiceDetailArrayListModel= new PeriodicServiceDetailArrayListModel();
					Object[] serviceRequestParts = serviceRequest.get(0);

					PeriodicServiceDetailArrayListModel.setReplacementPartNo(partsNo);

					if(serviceRequestParts[1] != null) {
						PeriodicServiceDetailArrayListModel.setReplacementPartName((String)serviceRequestParts[1]);
					}

					if(serviceRequestParts[2] != null) {
						PeriodicServiceDetailArrayListModel.setReplacementInterval(((Number)serviceRequestParts[2]).intValue());
					}

					if(serviceRequestParts[3] != null) {
						Integer nextReplacementTime = ((Number)serviceRequestParts[3]).intValue();
						PeriodicServiceDetailArrayListModel.setTimeUntilNextReplacementTime(nextReplacementTime);

						double value =  ((Number)serviceRequestParts[3]).doubleValue() / 8;
						BigDecimal bd = new BigDecimal(String.valueOf(value));
						PeriodicServiceDetailArrayListModel.setTimeUntilNextReplacementDay(((Number)bd.setScale(0, RoundingMode.UP)).intValue());

						if(nextReplacementTime <= 0) {
							PeriodicServiceDetailArrayListModel.setPeriodicServiceNotice(2);
						}else if(nextReplacementTime <= 100) {
							PeriodicServiceDetailArrayListModel.setPeriodicServiceNotice(1);
						}else{
							PeriodicServiceDetailArrayListModel.setPeriodicServiceNotice(0);
						}

						PeriodicServiceDetailArrayListModel.setReplacementScheduleDay(ConvertUtil.getDaysUTC((PeriodicServiceDetailArrayListModel.getTimeUntilNextReplacementDay())));

					}




					if(serviceRequestParts[4] != null) {
						PeriodicServiceDetailArrayListModel.setReplacementScheduleTime(((Number)serviceRequestParts[4]).doubleValue());
					}


					ArrayList<ReplacementHistoryArrayListModel> replacementHistoryArrayList = new ArrayList<ReplacementHistoryArrayListModel>();

					//i²ÆÉð·ððæ¾
					List<Object[]> replacementHistory = periodicServiceDitailService.findByReplacementHistoryNativeQuery(serialNumber, partsNo);
					if(replacementHistory != null) {

						for(Integer historyNo = 0; historyNo < replacementHistory.size();historyNo++) {
							ReplacementHistoryArrayListModel ReplacementHistoryArrayListModel = new ReplacementHistoryArrayListModel();
							Object[] replacementHistorys = replacementHistory.get(historyNo);

							if(replacementHistorys[0]!= null) {
								ReplacementHistoryArrayListModel.setReplacementNo(((Number)replacementHistorys[0]).intValue());
							}

							if(replacementHistorys[1]!= null) {
								ReplacementHistoryArrayListModel.setReplacementHour(((Number)replacementHistorys[1]).doubleValue());
							}

							if(replacementHistorys[2]!= null) {
								ReplacementHistoryArrayListModel.setReplacementDate((String)replacementHistorys[2]);
							}
							replacementHistoryArrayList.add(ReplacementHistoryArrayListModel);
						}
					}
					PeriodicServiceDetailArrayListModel.setReplacementHistoryList(replacementHistoryArrayList);
					periodicServiceDetailArrayList.add(PeriodicServiceDetailArrayListModel);
				}
			}
			if(!periodicServiceDetailArrayList.isEmpty()) {
				periodicServiceDetailListModel.setServiceRequestList(periodicServiceDetailArrayList);
			}else {
				periodicServiceDetailListModel.setServiceRequestList(null);
			}



			periodicServiceDetailListModel.setStatusCode(Constants.CON_OK);
			return Response.ok(periodicServiceDetailListModel).build();



		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}

	}

}
