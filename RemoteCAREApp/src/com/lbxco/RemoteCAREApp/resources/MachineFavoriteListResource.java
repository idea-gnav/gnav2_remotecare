package com.lbxco.RemoteCAREApp.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.lbxco.RemoteCAREApp.Constants;
import com.lbxco.RemoteCAREApp.annotations.Authorized;
import com.lbxco.RemoteCAREApp.ejbs.favorite.IMachineFavoriteListService;
import com.lbxco.RemoteCAREApp.entities.TblUserSetting;
import com.lbxco.RemoteCAREApp.models.MachineFavoriteArraylistModel;
import com.lbxco.RemoteCAREApp.models.MachineFavoriteListModel;
import com.lbxco.RemoteCAREApp.models.ReturnContainer;


@Path("/machineFavoriteList")
@Stateless
public class MachineFavoriteListResource {

	private static final Logger logger = Logger.getLogger(MachineFavoriteListResource.class.getName());


	@EJB
	IMachineFavoriteListService favoriteListService;


	/**
	 * [API No.13] お気に入り一覧取得
	 * @param userId
	 * @param sortFlg
	 * @param startRecord
	 * @return Response.JSON MachineFavoriteListModel
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response machineFavoriteList(
				@FormParam("userId") String userId,
				@FormParam("sortFlg") Integer sortFlg,
				@FormParam("startRecord") Integer startRecord
			){

		logger.info("[RemoteCAREApp][POST] userId="+userId+", sortFlg="+sortFlg+", startRecord="+startRecord);

		MachineFavoriteListModel machineFavoriteListModel = new MachineFavoriteListModel();
		ArrayList<MachineFavoriteArraylistModel> machineFavoriteArraylist = new ArrayList<MachineFavoriteArraylistModel>();

		try {

			//選択機番取得
			TblUserSetting ks = favoriteListService.findKibanSelect(userId);


			//お気に入り機械一覧取得
			List<Object[]> favoriteMachineList = favoriteListService.findFavoriteMachineNativeQuery(userId, ks.getKibanSelect(), sortFlg);

			if(favoriteMachineList!=null) {

				machineFavoriteListModel.setMachineFavoriteCount(favoriteMachineList.size());
				Integer record = startRecord-1;			// 1 - 51 - 101 -
				Integer maxRecord = favoriteMachineList.size();

				for(int counter = record; counter < (record+50) ; counter++ ) {

					if(counter==(maxRecord))
						break;

					MachineFavoriteArraylistModel machineFavoriteArrayListModel = new MachineFavoriteArraylistModel();


					Object[] result = favoriteMachineList.get(counter);

					Integer serialNumber = ((Number)result[1]).intValue();
					machineFavoriteArrayListModel.setSerialNumber(serialNumber);
					machineFavoriteArrayListModel.setManufacturerSerialNumber((String)result[2]);

					if((String)result[3] != null)
						machineFavoriteArrayListModel.setLbxSerialNumber((String)result[3]);
					else
						machineFavoriteArrayListModel.setLbxSerialNumber("");

					if((String)result[4] != null)
						machineFavoriteArrayListModel.setCustomerManagementNo((String)result[4]);
					else
						machineFavoriteArrayListModel.setCustomerManagementNo("");

					machineFavoriteArraylist.add(machineFavoriteArrayListModel);
				}


				machineFavoriteListModel.setMachineList(machineFavoriteArraylist);

			}else {
				machineFavoriteListModel.setMachineFavoriteCount(0);

			}
			machineFavoriteListModel.setStatusCode(Constants.CON_OK);
			return Response.ok(machineFavoriteListModel).build();


		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}


	}
}
