package com.lbxco.RemoteCAREApp.resources;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.lbxco.RemoteCAREApp.Constants;
import com.lbxco.RemoteCAREApp.ConvertUtil;
import com.lbxco.RemoteCAREApp.annotations.Authorized;
import com.lbxco.RemoteCAREApp.ejbs.dtcNotice.IDtcNoticeService;
import com.lbxco.RemoteCAREApp.ejbs.machine.IMachineService;
import com.lbxco.RemoteCAREApp.ejbs.user.IUserService;
import com.lbxco.RemoteCAREApp.entities.MstMachine;
import com.lbxco.RemoteCAREApp.models.MachineArrayListModel;
import com.lbxco.RemoteCAREApp.models.MachineListModel;
import com.lbxco.RemoteCAREApp.models.ReturnContainer;
import com.lbxco.RemoteCAREApp.util.MachineIconType;



@Path("/reportList")
@Stateless
public class ReportListResource {

	private static final Logger logger = Logger.getLogger(ReportListResource.class.getName());

	@EJB
	IMachineService machineListService;

	@EJB
	IUserService userService;

	@EJB
	IDtcNoticeService dtcService;

	@EJB
	IMachineService machineService;



	/**
	 * [API No.9] レポート一覧取得
	 * @param userId
	 * @param kibanSelect
	 * @param kibanInput
	 * @param searchFlg
	 * @param sortFlg
	 * @param manufacturerSerialNumberFrom
	 * @param manufacturerSerialNumberTo
	 * @param customerManagementNoFrom
	 * @param customerManagementNoTo
	 * @param lbxSerialNumberFrom
	 * @param lbxSerialNumberTo
	 * @param lbxModel
	 * @param psmId
	 * @param rmId
	 * @param dealerName
	 * @param warningCurrentlyInProgress
	 * @param warningDateFrom
	 * @param warningDateTo
	 * @param periodicServiceNotice
	 * @param periodicServiceNowDue
	 * @param replacementPart
	 * @param favoriteSearchFlg
	 * @param startRecord
	 * @return Response.JSON MachineListModel
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
				@FormParam("userId") String userId,
				@FormParam("kibanSelect") Integer kibanSelect,
				@FormParam("kibanInput") String kibanInput,
				@FormParam("searchFlg") Integer searchFlg,
				@FormParam("sortFlg") Integer sortFlg,
				@FormParam("manufacturerSerialNumberFrom") String manufacturerSerialNumberFrom,
				@FormParam("manufacturerSerialNumberTo") String manufacturerSerialNumberTo,
				@FormParam("customerManagementNoFrom") String customerManagementNoFrom,
				@FormParam("customerManagementNoTo") String customerManagementNoTo,
				@FormParam("lbxSerialNumberFrom") String lbxSerialNumberFrom,
				@FormParam("lbxSerialNumberTo") String lbxSerialNumberTo,
				@FormParam("lbxModel") String lbxModel,
				@FormParam("psmId") String psmId,
				@FormParam("rmId") String rmId,
				@FormParam("dealerName") String dealerName,
				// **↓↓↓　2018/11/28  iDEA山下  ステップ2　お客様名検索追加 ↓↓↓**//
				@FormParam("customerName") String customerName,
				// **↑↑↑　2018/11/28  iDEA山下  ステップ2　お客様名検索追加 ↑↑↑**//
				@FormParam("warningCurrentlyInProgress") Integer warningCurrentlyInProgress,
				@FormParam("warningDateFrom") String warningDateFrom,
				@FormParam("warningDateTo") String warningDateTo,
				// **↓↓↓　2019/01/07  iDEA山下  ステップ2　定期整備検索追加 ↓↓↓**//
				@FormParam("periodicServiceNotice") Integer periodicServiceNotice,
				@FormParam("periodicServiceNowDue") Integer periodicServiceNowDue,
				@FormParam("replacementPart") String replacementPart,
				// **↑↑↑　2019/01/07  iDEA山下  ステップ2　定期整備検索追加 ↑↑↑**//
				@FormParam("favoriteSearchFlg") Integer favoriteSearchFlg,
				// **↓↓↓　2018/05/22 LBN石川  要望不具合管理台帳 No45 特定位置検索対応 追加 ↓↓↓**//
				@FormParam("searchRangeFromIdo") Double searchRangeFromIdo,
				@FormParam("searchRangeFromKeido") Double searchRangeFromKeido,
				@FormParam("searchKibanInput") String searchKibanInput,
				@FormParam("searchRangeDistance") Integer searchRangeDistance,
				@FormParam("searchRangeUnit") Integer searchRangeUnit,
				// **↑↑↑　2018/05/22 LBN石川  要望不具合管理台帳 No45 特定位置検索対応 追加 ↑↑↑**//
				@FormParam("startRecord") Integer startRecord
			) {

		logger.info("[RemoteCAREApp][POST] userId="+userId+", kibanSelect="+kibanSelect+", kibanInput="+kibanInput
				+", searchFlg="+searchFlg+", sortFlg="+sortFlg+", manufacturerSerialNumberFrom="+manufacturerSerialNumberFrom+", manufacturerSerialNumberTo="+manufacturerSerialNumberTo
				+", customerManagementNoFrom="+customerManagementNoFrom+", customerManagementNoTo="+customerManagementNoTo+", lbxSerialNumberFrom="+lbxSerialNumberFrom+", lbxSerialNumberTo="+lbxSerialNumberTo
				+", lbxModel="+lbxModel+", psmId="+psmId+", rmId="+rmId+", dealerName="+dealerName+", customerName="+customerName+", warningDateFrom="+warningDateFrom+", warningDateTo="+warningDateTo
				+", warningCurrentlyInProgress="+warningCurrentlyInProgress+", favoriteSearchFlg="+favoriteSearchFlg
				+", searchRangeFromIdo="+searchRangeFromIdo+", searchRangeFromKeido="+searchRangeFromKeido+", searchKibanInput="+searchKibanInput
				+", searchRangeDistance="+searchRangeDistance+", searchRangeUnit="+searchRangeUnit+", startRecord="+startRecord);


		try {
			MachineListModel machineListModel = new MachineListModel();


			// ユーザー組織権限取得
//			List<Object[]> user = machineListService.findUserSosikiByKengenNativeQuery(userId);
			List<Object[]> user = userService.findByUserSosikiKengenNativeQuery(userId);

			String sosikiCd = null;
			if(user.get(0)[1]!=null)
				sosikiCd = (String)user.get(0)[1];

			String kigyouCd = null;
			if(user.get(0)[2]!=null)
				kigyouCd = (String)user.get(0)[2];

			String kengenCd = null;

			// 処理タイプ
			Integer typeFlg = null;
			if(user.get(0)[5]!=null)
				typeFlg = ((Number)user.get(0)[5]).intValue();
			else
				return Response.ok(new ReturnContainer(Constants.CON_OK)).build();


			// **↓↓↓　2018/5/22  LBN石川  要望不具合管理台帳 No45 特定位置検索対応 追加  ↓↓↓**//
			// 検索点中央位置の緯度・経度をセット
			Double _searchRangeFromIdo = searchRangeFromIdo;
			Double _searchRangeFromKeido = searchRangeFromKeido;

			if(searchKibanInput != null) {

				List<Object[]> latlng = machineListService.findByLatlngNativeQuery(searchKibanInput);

				if(latlng!=null && (latlng.get(0)[0]!=null && latlng.get(0)[1]!=null)) {
					_searchRangeFromIdo = Double.parseDouble(latlng.get(0)[0].toString());
					_searchRangeFromKeido = Double.parseDouble(latlng.get(0)[1].toString());
				}else {
					// 特定位置検索の機番が存在しない又は複数ある、又は機番に緯度経度がない場合は処理ステータス1601を返却
					return Response.ok(new ReturnContainer(Constants.CON_WARNING_NO_LATLNG)).build();
				}
			}
			machineListModel.setSearchRangeFromIdo(_searchRangeFromIdo);
			machineListModel.setSearchRangeFromKeido(_searchRangeFromKeido);
			// **↑↑↑　2018/5/22  LBN石川  要望不具合管理台帳 No45 特定位置検索対応 追加  ↑↑↑**//




			/*
			 * Get Machines
			 */
			List<Object[]> machines = machineListService.findByMachineListNativeQuery(
					userId,
//					serialNumber,
					kibanSelect,
					kibanInput,
					searchFlg,
					sortFlg,
					manufacturerSerialNumberFrom,
					manufacturerSerialNumberTo,
					customerManagementNoFrom,
					customerManagementNoTo,
					lbxSerialNumberFrom,
					lbxSerialNumberTo,
					lbxModel,
					psmId,
					rmId,
					dealerName,
					// **↓↓↓　2018/11/28  iDEA山下  ステップ2　お客様名検索追加 ↓↓↓**//
					customerName,
					// **↑↑↑　2018/11/28  iDEA山下  ステップ2　お客様名検索追加 ↑↑↑**//
					null,
					warningCurrentlyInProgress,
					warningDateFrom,
					warningDateTo,
					// **↓↓↓　2019/01/07  iDEA山下  ステップ2　定期整備検索追加 ↓↓↓**//
					null,
					null,
					null,
					// **↑↑↑　2019/01/07  iDEA山下  ステップ2　定期整備検索追加 ↑↑↑**//
					favoriteSearchFlg,
					typeFlg,
					kigyouCd,
					sosikiCd,
					kengenCd
					// **↓↓↓　2018/05/22 LBN石川  要望不具合管理台帳 No45 特定位置検索対応 追加 ↓↓↓**//
					, _searchRangeFromIdo,
					_searchRangeFromKeido,
					searchRangeDistance,
					searchRangeUnit
					// **↑↑↑　2018/05/22 LBN石川  要望不具合管理台帳 No45 特定位置検索対応 追加追加 ↑↑↑**//
					);

			ArrayList<MachineArrayListModel> machineList = new ArrayList<MachineArrayListModel>();


			if(machines != null) {

				// machine maximun
				Integer maxCount = Constants.CON_MAX_COUNT;
				if(machines.size() >= maxCount )
					return Response.ok(new ReturnContainer(Constants.CON_WARNING_MAXIMUN_NUMBER)).build();

				Integer record = startRecord-1;
				for(int counter =  record; counter < (record+50) ; counter++ ) {

					MachineArrayListModel machineArrayListModel = new MachineArrayListModel();

					if( machines.size() == counter) {
						break;
					}


					Object[] machine = machines.get(counter);


					Integer kibanSerno = 0;
					if(machine[0]!=null)
						kibanSerno = ((Number)machine[0]).intValue();

					String manufacturerSerialNumber = "";
					if(machine[1]!=null)
						manufacturerSerialNumber = (String)machine[1];

					String lbxSerialNumber = "";
					if(machine[2]!=null)
						lbxSerialNumber = (String)machine[2];

					String customerManagementNo = "";
					if(machine[3]!=null)
						customerManagementNo = (String)machine[3];

					String customerManagementName = "";
					if(machine[4]!=null)
						customerManagementName = (String)machine[4];

					String scmModel = null;
					if(machine[5]!=null)
						scmModel = (String)machine[5];

					String lbModel = null;
					if(machine[6]!=null)
						lbModel = (String)machine[6];

					String position = null;
					if(machine[7]!=null)
						position = (String)machine[7];

					Double ido = null;
					if(machine[8]!=null)
						ido =  ConvertUtil.parseLatLng(((Number)machine[8]).intValue());

					Double keido = null;
					if(machine[9]!=null)
						keido = ConvertUtil.parseLatLng(((Number)machine[9]).intValue());

					double hourMeter = 0;
					if(machine[10]!=null)
						hourMeter = ((Number)machine[10]).doubleValue();

					Integer nenryoLv = null;
					if(machine[11]!=null)
						nenryoLv = ((Number)machine[11]).intValue();

					Integer uraWaterLv = null;
					if(machine[12]!=null)
						uraWaterLv = ((Number)machine[12]).intValue();
					Timestamp latestUtcCommonTimestamp = null;

					String latestUtcCommonString = null;
					if(machine[13]!=null) {
						latestUtcCommonTimestamp = (Timestamp)machine[13];
						latestUtcCommonString = ConvertUtil.formatYMD(latestUtcCommonTimestamp);
					}

					machineArrayListModel.setSerialNumber(kibanSerno);

					if(((String)machine[1]).substring(7,8).equals("6"))
						machineArrayListModel.setMachineModelCategory(1);
					else
						machineArrayListModel.setMachineModelCategory(2);


					machineArrayListModel.setManufacturerSerialNumber(manufacturerSerialNumber);
					machineArrayListModel.setLbxSerialNumber(lbxSerialNumber);
					machineArrayListModel.setCustomerManagementNo(customerManagementNo);
					machineArrayListModel.setCustomerManagementName(customerManagementName);
					machineArrayListModel.setScmModel(scmModel);
					machineArrayListModel.setLbxModel(lbModel);
					machineArrayListModel.setLatestLocation(position);

					machineArrayListModel.setIdo(ido);
					machineArrayListModel.setKeido(keido);
					machineArrayListModel.setHourMeter(Math.floor(hourMeter/60.0*10)/10);
					machineArrayListModel.setLatestUtcCommonDateTime(latestUtcCommonString);
					machineArrayListModel.setFuelLevel(nenryoLv);
					machineArrayListModel.setDefLevel(uraWaterLv);


					// **↓↓↓ 2018/12/10 LBN石川 フェーズ2機械アイコン対応    ↓↓↓**//
					MstMachine mstMachine = machineService.findByMachine(kibanSerno);
					List<Object[]> dtcResults = dtcService.findByDtcLevelCount(kibanSerno, userService.findByUserSosikiKengenCd(userId));
					machineArrayListModel.setIconType(MachineIconType.stateMachineIconType(dtcResults, mstMachine));
					// **↑↑↑ 2018/12/10 LBN石川 フェーズ2機械アイコン対応    ↑↑↑**//

					machineList.add(machineArrayListModel);

				}

				machineListModel.setMachineList(machineList);
				machineListModel.setMachineCount(machines.size());

			}else {
				machineListModel.setMachineCount(0);

			}

			machineListModel.setStatusCode(Constants.CON_OK);
			return Response.ok(machineListModel).build();


		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}

	}
}
