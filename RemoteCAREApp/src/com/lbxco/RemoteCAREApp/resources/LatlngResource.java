package com.lbxco.RemoteCAREApp.resources;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.xml.sax.InputSource;

import com.lbxco.RemoteCAREApp.Constants;
import com.lbxco.RemoteCAREApp.ConvertUtil;
import com.lbxco.RemoteCAREApp.annotations.Authorized;
import com.lbxco.RemoteCAREApp.ejbs.latLng.ILatLngService;
import com.lbxco.RemoteCAREApp.entities.MstSummaryArea;
import com.lbxco.RemoteCAREApp.util.ReadXml;



@Path("/latLng")
@Stateless
public class LatlngResource {


	private final static String GOOGLE_MAPS_XML_URL = "https://maps.googleapis.com/maps/api/geocode/xml";
	private final static String LINE_SEPARATOR_PATTERN =  "\r\n|[\n\r\u2028\u2029\u0085]";

	private final static String RESULT = "result";
	private final static String LOCATION ="location";
	private final static String LAT ="lat";
	private final static String LNG ="lng";
	private final static String SHORT_NAME = "short_name";


	@EJB
	ILatLngService latLngService;


	@POST
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
//	@Produces(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
//	@Produces("text/csv")
	public Response postMethod(
				@FormParam("countryCd") String countryCd,
				@FormParam("googleKey") String googleKey
			) throws Exception {



		StringBuilder readXml = new StringBuilder();
		try {


			List<MstSummaryArea> summaryAreaList = latLngService.findByMstSummaryArea(countryCd);

			for(MstSummaryArea summaryArea :summaryAreaList) {
				String result = getGoogleMapsGeocodeXml(summaryArea.getCountryCd(), summaryArea.getAreaCd(), summaryArea.getAreaNm(), googleKey);

				String strLat = ReadXml.ReadLatLangXml(new InputSource(new StringReader(new String(result))), RESULT, LAT, 0);
				String strLng = ReadXml.ReadLatLangXml(new InputSource(new StringReader(new String(result))), RESULT, LNG, 0);
				String strName = ReadXml.ReadLatLangXml(new InputSource(new StringReader(new String(result))), RESULT, SHORT_NAME, 0);

//				InputSource xmlFile = new InputSource(new StringReader(new String(result)));
//	            String strLat = ReadXml.ReadLatLangXml(xmlFile, RESULT, LAT, 0);
//				String strLng = ReadXml.ReadLatLangXml(xmlFile, RESULT, LNG, 0);
//				String strName = ReadXml.ReadLatLangXml(xmlFile, RESULT, SHORT_NAME, 0);

	            readXml.append(strLat).append(",").append(strLng);
				readXml.append(",").append(summaryArea.getAreaCd());
				readXml.append(",").append(strName).append("\n");

				if(ConvertUtil.isNumberDouble(strLat)&&ConvertUtil.isNumberDouble(strLng)) {
					Integer lat = ConvertUtil.parseLatLngInt(Double.parseDouble(strLat));
					Integer lng = ConvertUtil.parseLatLngInt(Double.parseDouble(strLng));

					latLngService.updateMstSummaryArea(summaryArea.getCountryCd(), summaryArea.getAreaCd(), lat, lng, strName);
				}
			}

		} catch (Exception e) {
			throw new Exception();

		}

		return Response.ok(new String(readXml)).build();
	}



	private String getGoogleMapsGeocodeXml(String CountryCd, String areaCd, String areaName, String googleKey) throws Exception {

		StringBuilder reqUrl = new StringBuilder();

		reqUrl.append(GOOGLE_MAPS_XML_URL);
		reqUrl.append("?address=").append(areaName).append("+").append(areaCd).append("+").append(CountryCd);
		reqUrl.append("&key=").append(googleKey);

		URL url = new URL(new String(reqUrl).replace(" ", "%20"));
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		int responseCode;
		responseCode = conn.getResponseCode();

		if(responseCode == HttpURLConnection.HTTP_OK){
			StringBuilder result = new StringBuilder();
			//responseの読み込み
			InputStream in = conn.getInputStream();
			InputStreamReader inReader = new InputStreamReader(in, "utf-8");
			BufferedReader bufferedReader = new BufferedReader(inReader);
			String line = null;
			while((line = bufferedReader.readLine()) != null) {
				result.append(line);
			}
			bufferedReader.close();
			inReader.close();
			in.close();

			return new String(result);

		}else {
			return null;
		}
	}




}
