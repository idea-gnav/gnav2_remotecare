package com.lbxco.RemoteCAREApp.resources;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.lbxco.RemoteCAREApp.Constants;
import com.lbxco.RemoteCAREApp.annotations.Authorized;
import com.lbxco.RemoteCAREApp.ejbs.dtcNotice.IDtcNoticeService;
import com.lbxco.RemoteCAREApp.ejbs.favorite.IMachineFavoriteRegisterService;
import com.lbxco.RemoteCAREApp.models.ReturnContainer;



@Path("/machineFavoriteRegister")
@Stateless
public class MachineFavoriteRegisterResource {

	private static final Logger logger = Logger.getLogger(MachineFavoriteRegisterResource.class.getName());


	@EJB
	IMachineFavoriteRegisterService favoriteRegisterService;

	@EJB
	IDtcNoticeService dtcService;



	/**
	 * [API No.14] お気に入り登録/削除
	 * @param userId
	 * @param addSerialNumber
	 * @param removeSerialNumber
	 * @return Response.JSON statusCode
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
			@FormParam("userId") String userId,
			@FormParam("addSerialNumber") String _addSerialNumber,
			@FormParam("removeSerialNumber") String _removeSerialNumber
		)	{

		logger.info("[RemoteCAREApp][POST] userId="+userId+", addSerialNumber="+_addSerialNumber+", removeSerialNumber="+_removeSerialNumber);

		try {


			// お気に入り登録処理
			String[] addSerialNumbers = null;
			if(_addSerialNumber != null && !_addSerialNumber.equals("")) {
				addSerialNumbers = _addSerialNumber.split(",");
				for(String addSerialNumber : addSerialNumbers){
					favoriteRegisterService.addFavoriteMachine(Integer.parseInt(addSerialNumber), userId);

					// **↓↓↓　2018/07/19 LBN石川  要望対応 お気に入りDTC履歴の既読処理 追加 ↓↓↓**//
					Integer kengenCd = null;
					if(favoriteRegisterService.findByUserSosikiKengenNativeQuery(userId).get(0)[4]!=null)
						kengenCd = Integer.parseInt(favoriteRegisterService.findByUserSosikiKengenNativeQuery(userId).get(0)[4].toString());

					// お気に入り登録されている機械でDTC未復旧の直近2日分のDTCを取得し、該当がある場合は記録する。
					// 以前登録してた機械があった場合に更新する　DEL_FLGを更新する。
					dtcService.updateTblFavoriteDtcHistory(userId, Integer.parseInt(addSerialNumber), null, 0);

					dtcService.insertByNotExistsTblFavoriteDtcHistory(userId, kengenCd, Integer.parseInt(addSerialNumber));

					// **↑↑↑　2018/07/19 LBN石川  要望対応 お気に入りDTC履歴の既読処理 追加 ↑↑↑**//
				}

			}

			// お気に入り削除処理
			String[] removeSerialNumbers = null;
			if(_removeSerialNumber != null && !_removeSerialNumber.equals("")) {
				removeSerialNumbers = _removeSerialNumber.split(",");
				for(String removeSerialNumber : removeSerialNumbers){
					favoriteRegisterService.removeFavoriteMachine(Integer.parseInt(removeSerialNumber), userId);

					// **↓↓↓　2018/07/19 LBN石川  要望対応 お気に入りDTC履歴の既読処理 追加 ↓↓↓**//
					dtcService.updateTblFavoriteDtcHistory(userId, Integer.parseInt(removeSerialNumber), null, 1);
					// **↑↑↑　2018/07/19 LBN石川  要望対応 お気に入りDTC履歴の既読処理 追加 ↑↑↑**//
				}
			}



			// 処理結果返却処理

			// **↓↓↓　2018/07/24 LBN石川  要望対応 お気に入りDTC履歴処理を追加↓↓↓**//

			// 件数追加に伴い返却値をHashMapで対応。key, value
			HashMap<String, Object> responseMap = new HashMap<String, Object>();
			Integer badgeCount = dtcService.findByNoReadCount(userId);
			if(badgeCount!=null) {
				if(badgeCount>=1000)
					badgeCount = 999;
				responseMap.put("dtcBadgeCount", badgeCount);
			}

			responseMap.put("statusCode", 1000);

			return Response.ok(responseMap).build();
//			return Response.ok(new ReturnContainer(Constants.CON_OK)).build();
			// **↑↑↑　2018/07/24 LBN石川  要望対応 お気に入りDTC履歴処理を追加 ↑↑↑**//

		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}


	}
}
