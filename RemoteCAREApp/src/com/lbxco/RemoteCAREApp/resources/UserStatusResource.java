package com.lbxco.RemoteCAREApp.resources;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.imageio.ImageIO;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.lbxco.RemoteCAREApp.Constants;
import com.lbxco.RemoteCAREApp.ConvertUtil;
import com.lbxco.RemoteCAREApp.annotations.Authorized;
import com.lbxco.RemoteCAREApp.ejbs.user.IUserService;
import com.lbxco.RemoteCAREApp.ejbs.userStatus.IUserStatusService;
import com.lbxco.RemoteCAREApp.entities.MstUser;
import com.lbxco.RemoteCAREApp.entities.TblUserDevice;
import com.lbxco.RemoteCAREApp.entities.TblUserSetting;
import com.lbxco.RemoteCAREApp.models.ReturnContainer;
import com.lbxco.RemoteCAREApp.models.UserStatusModel;
import com.lbxco.RemoteCAREApp.util.Base64;
import com.lbxco.RemoteCAREApp.util.PropertyUtil;




@Path("/userStatus")
@Stateless
public class UserStatusResource {

	private static final Logger logger = Logger.getLogger(UserStatusResource.class.getName());

	@EJB
	IUserStatusService userStatusService;

	@EJB
	IUserService userService;

	private String languageCd;
	private Integer processTypeFlg;

	/**
	 * [API No.1] ユーザー認証
	 * @param userId
	 * @param password
	 * @param deviceToken
	 * @param appVersion
	 * @param deviceType
	 * @param mode
	 * @return Response.JSON　UserStatusModel
	 * @throws Exception
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
				@FormParam("userId") String userId,
				@FormParam("password") String password,
				@FormParam("deviceToken") String deviceToken,
				@FormParam("appVersion") String appVersion,
				@FormParam("deviceType") Integer deviceType,
				@FormParam("deviceLanguage") Integer deviceLanguage,
				@FormParam("mode") Integer mode
			) throws Exception{

		logger.info("[RemoteCAREApp][POST] userId="+userId+", password="+password+", deviceToken="+deviceToken+", appVersion="+appVersion+", deviceType="+deviceType+", mode="+mode);


		UserStatusModel userStatusModel = new UserStatusModel();

		try {

			// ユーザー認証管理 ログイン日時取得
			String loginDtm = ConvertUtil.getDaysUtcYmdHms(0);



			// 1) アプリケーションバージョンチェック
			if(mode == 0 || mode == 1 || mode == 2) {
				String minVertion = null;
				try{

					// **↓↓↓　2018/6/26  LBN石川   Utility化に伴い修正  ↓↓↓**//
					minVertion = PropertyUtil.readPropertys(Constants.APP_VERTION_PATH, "applicationVersion", null);
					if(minVertion==null)
						minVertion = "99999";

//					File file = new File(Constants.APP_VERTION_PATH);
//					if (file.exists()){
//						BufferedReader br = new BufferedReader(new FileReader(file));
//						String str = br.readLine();
//						while(str != null){
//							str = br.readLine();
//							if(str.startsWith("applicationVersion")) {
//								minVertion = str.toString().split("=")[1];
//								break;
//							}
//						}
//						br .close();
//					}else{
//						minVertion = "99999";
//					}

					// **↑↑↑　2018/6/26  LBN石川   Utility化に伴い修正  ↑↑↑**//
			    }catch(Exception e){
			    	logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			    	return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();
			    }

				logger.config("[RemoteCAREApp][DEBUG] appVersion="+appVersion+", minVertion="+minVertion);

				if(appVersion.compareTo(minVertion) < 0) {
					return Response.ok(new ReturnContainer(Constants.CON_WARNING_APP_VERSION)).build();

				}else {
					if(mode==2)
						return Response.ok(new ReturnContainer(Constants.CON_OK)).build();
				}

			}else {
				userStatusModel.setStatusCode(Constants.CON_UNKNOWN_ERROR);
			}


			if(mode == 0 || mode == 1) {

				// 2) ユーザー認証
				MstUser user = userStatusService.findByUser(userId, password);
				TblUserSetting userSetting = null;
				Timestamp newPasswordDate = null;

				if(user != null) {

					// 3) パスワード有効期限切れチェック
					newPasswordDate = user.getNewPasswordDate();
					if(newPasswordDate==null){
						logger.config("[RemoteCAREApp][DEBUG] WARNING_PASSWORD_EXPIRED 1101");
						return Response.ok(new ReturnContainer(Constants.CON_WARNING_PASSWORD_EXPIRED)).build();
					}

					// ユーザー組織権限取得
					List<Object[]> userSosikiKengen = userService.findByUserSosikiKengenNativeQuery(userId);
					if(userSosikiKengen.get(0)[5]!=null)
						processTypeFlg = Integer.parseInt(userSosikiKengen.get(0)[5].toString());

					// ユーザー設定
					userSetting = checkTblUserSetting(user, processTypeFlg);

				}
				if(user==null) {
					logger.config("[RemoteCAREApp][DEBUG] WARNING_NO_USER_PASSWORD 1201");
					return Response.ok(new ReturnContainer(Constants.CON_WARNING_NO_USER_PASSWORD)).build();
				}



					Integer days = 0;
					if(newPasswordDate!=null){
						days = checkPasswordDays(newPasswordDate);

						if(days>=60) {
							logger.config("[RemoteCAREApp][DEBUG] WARNING_PASSWORD_EXPIRED 1101");
							return Response.ok(new ReturnContainer(Constants.CON_WARNING_PASSWORD_EXPIRED)).build();

						}else {
							// **↓↓↓　2018/04/05  LBN石川  要望不具合管理台帳 No40対応 追加  ↓↓↓**//
							String updpwdFlg = user.getUpdpwdFlg();
							if(updpwdFlg == null)
								updpwdFlg = "0";
							// **↑↑↑　2018/04/05  LBN石川  要望不具合管理台帳 No40対応 追加  ↑↑↑**//

							if(days <= 52) {
								userStatusModel.setStatusCode(Constants.CON_OK);
								if(updpwdFlg.equals("1") || updpwdFlg.equals("2") || updpwdFlg==null)
									userStatusService.updateByPasswordFlg(userId, "0");
							}
							if(days >= 53 && days <= 56) {
								if(!updpwdFlg.equals("1")) {
									logger.config("[RemoteCAREApp][DEBUG] WARNING_PASSWORD_7DAYS　1501");
									userStatusModel.setStatusCode(Constants.CON_WARNING_PASSWORD_7DAYS);
									userStatusService.updateByPasswordFlg(userId, "1");

								}else {
									userStatusModel.setStatusCode(Constants.CON_OK);
								}
							}
							if(days >= 57 && days <= 59) {
								if(!updpwdFlg.equals("2")) {
									logger.config("[RemoteCAREApp][DEBUG] WARNING_PASSWORD_3DAYS　1001");
									userStatusModel.setStatusCode(Constants.CON_WARNING_PASSWORD_3DAYS);
									userStatusService.updateByPasswordFlg(userId, "2");

								}else {
									userStatusModel.setStatusCode(Constants.CON_OK);
								}
							}
						}
					}



					// デバイストークンNULLチェック
					if(deviceToken!=null && deviceToken.length()>0) {

						// **↓↓↓ 2018/06/22  LBN石川  プッシュ通知お知らせ追加およびG＠NavApp対応に伴いlanguageCd, appFlg 追加  ↓↓↓**//
						// デバイス言語
						languageCd = ConvertUtil.convertLanguageCd(deviceLanguage);

						TblUserDevice userDevice = userStatusService.findByDevice(deviceToken, null);
						if(userDevice != null)
							userStatusService.updateByUserDevice(deviceToken, userId, languageCd, loginDtm);
						else
							userStatusService.insertByUserDevice(deviceToken, deviceType, userId, languageCd, Constants.CON_APP_FLG, loginDtm);

						// **↑↑↑ 2018/06/22  LBN石川  プッシュ通知お知らせ追加およびG＠NavApp対応に伴いlanguageCd, appFlg 追加   ↑↑↑**//
					}


					// ユーザー設定情報取得
					userSetting = userStatusService.findByUserSetting(user.getUserId());
					if(userSetting!=null) {
						userStatusModel.setUnitSelect(userSetting.getUnitSelect());
						userStatusModel.setKibanSelect(userSetting.getKibanSelect());
						userStatusModel.setPdfFlg(userSetting.getPdfFlg());
						userStatusModel.setAppLogFlg(userSetting.getApplogFlg());
						// **↓↓↓　2018/05/22  LBN石川  要望不具合管理台帳 No45 特定位置検索対応 追加  ↓↓↓**//
						userStatusModel.setSearchRangeDistance(userSetting.getSearchRange());
						// **↑↑↑　2018/05/22  LBN石川  要望不具合管理台帳 No45 特定位置検索対応 追加  ↑↑↑**//
					}else {
						logger.warning("[WARNING] There is no TBL_USER_SETTING.");
						userStatusModel.setStatusCode(Constants.CON_UNKNOWN_ERROR);
					}

					//**↓↓↓ 2019/04/01 iDEA山下　組織権限取得⇒ユーザ権限取得に変更 ↓↓↓**//
					// ユーザー組織権限コード取得
//					List<Object[]> userSosikiKengen =userStatusService.findByUserSosikiKengenNativeQuery(userId);
					List<Object[]> userKengen =userStatusService.findByUserKengenNativeQuery(userId);
					//**↑↑↑ 2019/04/01 iDEA山下　組織権限取得⇒ユーザ権限取得に変更 ↑↑↑**//

					Integer kengenCd = null;
					if(userKengen != null) {
						kengenCd = Integer.parseInt(userKengen.get(0)[4].toString());	// [4] KENGEN_CD
					}

					// 統計メニュー制御フラグ制御
					if(kengenCd!=null) {
						// **↓↓↓ 2019/03/26  iDEA山下  代理店権限　統計情報公開延期 ↓↓↓**//
						// **↓↓↓ 2018/12/06  LBN石川  フェーズ2代理店向け統計対応 ↓↓↓**//
//						if(kengenCd==101 || kengenCd==102 || kengenCd==103 || kengenCd==104 || kengenCd==105)
						if(kengenCd==101 || kengenCd==102 || kengenCd==103)
						// **↑↑↑ 2018/12/06  LBN石川  フェーズ2代理店向け統計対応 ↑↑↑**//
						// **↑↑↑ 2019/03/26  iDEA山下  代理店権限　統計情報公開延期 ↑↑↑**//
							userStatusModel.setStatisticsFlg(1);
						else
							userStatusModel.setStatisticsFlg(0);
					}

					// **↓↓↓ 2019/01/31  LBN石川  ステップ2フェーズ3代理店向けお知らせ 追加  ↓↓↓**//
					if(kengenCd!=null) {
						if(kengenCd==101 || kengenCd==102 || kengenCd==103)
							userStatusModel.setDealerNewsFlg(2);
						// **↓↓↓ 2019/04/01  iDEA山下  代理店ニュース編集権限 代理店ADMINのみ許可　変更  ↓↓↓**//
//						else if(kengenCd==104 || kengenCd==105)
						else if(kengenCd==104)
						// **↑↑↑ 2019/04/01  iDEA山下  代理店ニュース編集権限 代理店ADMINのみ許可　変更  ↑↑↑**//
							userStatusModel.setDealerNewsFlg(1);
						else
							userStatusModel.setDealerNewsFlg(0);

					}
					// **↑↑↑ 2019/01/31  LBN石川  ステップ2フェーズ3代理店向けお知らせ 追加  ↑↑↑**//


					// **↓↓↓ 2019/01/21  LBN石川  ステップ2フェーズ3機械アイコン対応 追加  ↓↓↓**//
					if(mode==0 || mode==1){
						HashMap<String, Object> hashMapIcon = new HashMap<String, Object>();
						ArrayList<HashMap<String, Object>> hashMapIconList = new ArrayList<>();

						String[] iconType = PropertyUtil.readPropertys(Constants.APP_CONFIG_PATH, Constants.TYPE_ICON_LIST, null).split(",");

						for(int index = 0; index < iconType.length; index++) {
							// 外部ファイルから画像のパスを取得
							String iconPath = PropertyUtil.readPropertys(Constants.APP_CONFIG_PATH, Constants.TYPE_ICON+iconType[index], null);

							// バイナリ変換
							BufferedImage bufferedImage = ImageIO.read(new File(iconPath));
							ByteArrayOutputStream outPutStream = new ByteArrayOutputStream();
							ImageIO.write(bufferedImage, "png", outPutStream);
							byte[] binary = outPutStream.toByteArray();
							String iconImageBinary = Base64.encodeBase64(binary);

							hashMapIcon.put("iconType", iconType[index]);
							hashMapIcon.put("iconImage", iconImageBinary);
							hashMapIconList.add(hashMapIcon);
							hashMapIcon = new HashMap<String, Object>();
							userStatusModel.setMachineIconList(hashMapIconList);
						}
					}
					// **↑↑↑ 2019/01/21  LBN石川  ステップ2フェーズ3機械アイコン対応 追加  ↑↑↑**//

			}else {
				userStatusModel.setStatusCode(Constants.CON_UNKNOWN_ERROR);
			}


			// **↓↓↓ 2018/12/17 ユーザー認証管理テーブルにログを追加 ↓↓↓**//
			if(mode==0 || mode==1)
				userStatusService.insertByTblUserStatusAppLog(
						userId,
						deviceType,
						ConvertUtil.convertLanguageCd(deviceLanguage),
						mode,
						Constants.CON_APP_FLG,
						loginDtm);
			// **↑↑↑ 2018/12/17 ユーザー認証管理テーブルにログを追加 ↑↑↑**//


			return Response.ok(userStatusModel).build();

		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}

	}




	/**
	 * パスワード有効期限日数
	 * @param newPasswordDate
	 * @return days
	 */
	public Integer checkPasswordDays(Timestamp newPasswordDate) {

		Date datNow = new Date(System.currentTimeMillis());
		Date datLastPass = new Date(newPasswordDate.getTime());

		long diff = datNow.getTime() - datLastPass.getTime();
		long days = diff / (1000 * 60 * 60 * 24);
		logger.config("[RemoteCAREApp][DEBUG] Password days="+days);
		/*
		 * パスワード変更日の7日前　days==53
		 * パスワード変更日の6日前　days==54
		 * パスワード変更日の5日前　days==55
		 * パスワード変更日の4日前　days==56
		 * パスワード変更日の3日前　days==57
		 * パスワード変更日の2日前　days==58
		 * パスワード変更日の1日前　days==59
		 * パスワード変更日期限切れ　days>=60
		 *
		 */
		return (int)days;
	}




	/**
	 * ユーザー設定テーブルチェック
	 * 存在しない場合、PDFフラグをセットし、ユーザー設定テーブルに登録する。
	 * @param MstUser ユーザーマスタ
	 * @return TblUserSetting
	 */
	public TblUserSetting checkTblUserSetting(MstUser user, Integer ProcessTypeFlg) {

		int pdfFlg = 0;

		TblUserSetting userSetting = userStatusService.findByUserSetting(user.getUserId());

		// 設定テーブルに登録が無い場合
		if(userSetting == null) {
			if(ProcessTypeFlg!=null) {
				if( ProcessTypeFlg == 61 	// LBX
					|| ProcessTypeFlg==62 	// 代理店
					|| ProcessTypeFlg==63	// 顧客
						) {
					if(ProcessTypeFlg==63)
						pdfFlg = 0;
					else
						pdfFlg = 1;

				// 処理タイプがLBX以外は何もせずnullで返却する。
				}else {
					return null;
				}
				/*
				 * ユーザー設定テーブルに新規登録
				 * デフォルト
				 * 	機番   0:SCM機番    user.getuserKbn()
				 * 	単位   1:ISO
				 */
				userStatusService.insertByUserSetting(user.getUserId(), 1, 0, pdfFlg);

			// 処理タイプが存在しない場合はは何もせずnullで返却する。
			}else {
				return null;
			}

		}

		// ユーザー設定テーブルを返却する
		return userStatusService.findByUserSetting(user.getUserId());
	}





}
