package com.lbxco.RemoteCAREApp.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.lbxco.RemoteCAREApp.Constants;
import com.lbxco.RemoteCAREApp.ConvertUtil;
import com.lbxco.RemoteCAREApp.annotations.Authorized;
import com.lbxco.RemoteCAREApp.ejbs.machineSearchInfo.IMachineSearchInfoService;
import com.lbxco.RemoteCAREApp.ejbs.user.IUserService;
import com.lbxco.RemoteCAREApp.entities.MstArea;
import com.lbxco.RemoteCAREApp.entities.MstResionSalesManager;
import com.lbxco.RemoteCAREApp.entities.MstUser;
import com.lbxco.RemoteCAREApp.models.MachineSearchInfoModel;
import com.lbxco.RemoteCAREApp.models.MaintainecePartItemArrayListModel;
import com.lbxco.RemoteCAREApp.models.PsmModel;
import com.lbxco.RemoteCAREApp.models.ReturnContainer;
import com.lbxco.RemoteCAREApp.models.RmModel;



@Path("/machineSearchInfo")
@Stateless
public class MachineSearchInfo {

	private static final Logger logger = Logger.getLogger(MachineSearchInfo.class.getName());

	@EJB
	IMachineSearchInfoService machineSearchInfoService;


	@EJB
	IUserService userService;


	/**
	 * [API No.19] 機械詳細検索情報
	 * @param userId
	 * @return Response.JSON MachineSearchInfoModel
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
					@FormParam("userId") String userId,
					// **↓↓↓　2019/02/01  iDEA山下  ステップ2　交換部品リスト追加 ↓↓↓**//
					@FormParam("deviceLanguage") Integer deviceLanguage
					// **↑↑↑　2019/02/01  iDEA山下  ステップ2　交換部品リスト追加 ↑↑↑**//
				) {

		logger.info("[RemoteCAREApp][POST] userId="+userId);

		MachineSearchInfoModel machineSearchInfo = new MachineSearchInfoModel();

		try {

				// ユーザー情報取得
				MstUser userInfo = userService.findByUser(userId);
				Integer kengenCd = userInfo.getKengenCd();

				// ユーザ組織権限取得
				List<Object[]> userKengen = userService.findByUserSosikiKengenNativeQuery(userId);
				String sosikiCd = null;
				if(userKengen.get(0)[1]!=null)
					sosikiCd = (String)userKengen.get(0)[1];

				String kigyouCd = null;
				if(userKengen.get(0)[2]!=null)
					kigyouCd = (String)userKengen.get(0)[2];

				String groupNo = null;
				if(userKengen.get(0)[3]!=null)
					groupNo = (String)userKengen.get(0)[3];

				Integer processTypeFlg = null;
				if(userKengen.get(0)[5]!=null)
					processTypeFlg = ((Number)userKengen.get(0)[5]).intValue();
				else
					return Response.ok(new ReturnContainer(Constants.CON_OK)).build();


				// LBＸモデル一覧取得
				// **↓↓↓　2019/11/18  iDEA山下  型式リスト表示制限 追加 ↓↓↓**//
//				List<String> lbxModelList = machineSearchInfoService.findByLbxModel();
				List<String> lbxModelList = machineSearchInfoService.findByLbxModel(processTypeFlg,kigyouCd,sosikiCd,groupNo);
				// **↑↑↑　2019/11/18  iDEA山下  型式リスト表示制限 追加 ↑↑↑**//
				if(lbxModelList!=null) {
					machineSearchInfo.setLbxModelList(lbxModelList);
				}

				// PSM、RM検索 権限制御
				if(kengenCd==101 || kengenCd==102 || kengenCd==103) {

					List<MstArea> areaList = machineSearchInfoService.findByPsm();
					if(areaList!=null) {
						ArrayList<PsmModel> psmList = new ArrayList<PsmModel>();
						for(MstArea area : areaList) {
							PsmModel psmModel = new PsmModel();
							psmModel.setPsmId(area.getAreaCd());
							psmModel.setPsmName(area.getAreaName());
							psmList.add(psmModel);
						}
						machineSearchInfo.setPsmList(psmList);
					}

					if(kengenCd==101 || kengenCd==102) {
						List<MstResionSalesManager> mrsmList = machineSearchInfoService.findByRm();
						if(mrsmList!=null) {
							ArrayList<RmModel> rmList = new ArrayList<RmModel>();
							for(MstResionSalesManager mrsm : mrsmList) {
								RmModel rmModel = new RmModel();
								rmModel.setRmId(mrsm.getResionSalesManagerCd());
								rmModel.setRmName(mrsm.getResionSalesManagerName());
								rmList.add(rmModel);
							}
							machineSearchInfo.setRmList(rmList);
						}
					}

					// 権限 102の場合、初期表示IDをセットする
					if(kengenCd==102) {
						machineSearchInfo.setPsmDisplay(userInfo.getAreaCd());
						machineSearchInfo.setRmDisplay(userInfo.getResionSalesManagerCd());
					}
				}

				// **↓↓↓　2019/02/01  iDEA山下  ステップ2　交換部品リスト追加 ↓↓↓**//
				// 交換部品一覧取得
				ArrayList<MaintainecePartItemArrayListModel> maintainecePartItemArrayList = new ArrayList<MaintainecePartItemArrayListModel>();
				List<Object[]> maintaineceParts = machineSearchInfoService.findByMaintainecePartItem(ConvertUtil.convertLanguageCd(deviceLanguage));
				if(maintaineceParts != null) {

					for(Integer listCount = 0 ; listCount < maintaineceParts.size(); listCount++ ) {
						MaintainecePartItemArrayListModel maintainecePartItemListModel = new MaintainecePartItemArrayListModel();
						Object[] listColumn = maintaineceParts.get(listCount);

						if(listColumn[0] != null) {
							maintainecePartItemListModel.setMaintainecePartNumber(((Number)listColumn[0]).intValue());
						}
						if(listColumn[1] != null) {
							maintainecePartItemListModel.setMaintainecePartName((String)listColumn[1]);
						}
						maintainecePartItemArrayList.add(maintainecePartItemListModel);
					}
				}
				if(!maintainecePartItemArrayList.isEmpty()) {
					machineSearchInfo.setMaintainecePartItemList(maintainecePartItemArrayList);
				}else {
					machineSearchInfo.setMaintainecePartItemList(null);
				}
				// **↑↑↑　2019/02/01  iDEA山下  ステップ2　交換部品リスト追加 ↑↑↑**//


				// Dealer Name 検索 権限制御
				// CustomerName 検索権限制御追加 2018/06/21 LBN石川
				if(kengenCd==101 || kengenCd==102 || kengenCd==103 || kengenCd==104 || kengenCd==105) {
					machineSearchInfo.setDealerNameFlg(1);
					machineSearchInfo.setCustomerNameFlg(1);
				}else {
					machineSearchInfo.setDealerNameFlg(0);
					machineSearchInfo.setCustomerNameFlg(0);
				}

				machineSearchInfo.setStatusCode(Constants.CON_OK);
				return Response.ok(machineSearchInfo).build();

		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(Constants.CON_UNKNOWN_ERROR).build();
		}
	}
}
