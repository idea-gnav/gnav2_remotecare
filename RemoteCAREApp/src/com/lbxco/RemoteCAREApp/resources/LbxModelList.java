package com.lbxco.RemoteCAREApp.resources;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.lbxco.RemoteCAREApp.Constants;
import com.lbxco.RemoteCAREApp.annotations.Authorized;
import com.lbxco.RemoteCAREApp.ejbs.lbxModelList.ILbxModelListService;
import com.lbxco.RemoteCAREApp.models.LbxModelListModel;



@Path("/lbxModelList")
@Stateless
public class LbxModelList {

	private static final Logger logger = Logger.getLogger(LbxModelList.class.getName());

	@EJB
	ILbxModelListService lbxModelListService;

	
	/**
	 * [API No.19] LBX型式 ---API変更---> 機械詳細検索情報 machineSerchInfo
	 * @param userId
	 * @return Response.JSON LbxModelListModel
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
					@FormParam("userId") String userId
				) {

		logger.info("[POST] userId="+userId);

		LbxModelListModel lbxModelListModel = new LbxModelListModel();
		
		try {
			
				// LBＸモデル一覧取得
				List<String> lbxModelList = lbxModelListService.findByLbxModel();
	
				if(lbxModelList!=null)
					lbxModelListModel.setLbxModelList(lbxModelList);
				
					lbxModelListModel.setStatusCode(Constants.CON_OK);
					return Response.ok(lbxModelListModel).build();
			
		}catch(Exception e) {
			
			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(Constants.CON_UNKNOWN_ERROR).build();
		}
	}
}
