package com.lbxco.RemoteCAREApp.resources;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.lbxco.RemoteCAREApp.Constants;
import com.lbxco.RemoteCAREApp.ConvertUtil;
import com.lbxco.RemoteCAREApp.annotations.Authorized;
import com.lbxco.RemoteCAREApp.ejbs.dtcNotice.IDtcNoticeService;
import com.lbxco.RemoteCAREApp.ejbs.machine.IMachineService;
import com.lbxco.RemoteCAREApp.ejbs.report.IReportService;
import com.lbxco.RemoteCAREApp.ejbs.sosiki.ISosikiService;
import com.lbxco.RemoteCAREApp.ejbs.user.IUserService;
import com.lbxco.RemoteCAREApp.ejbs.userStatus.IUserStatusService;
import com.lbxco.RemoteCAREApp.entities.MstMachine;
import com.lbxco.RemoteCAREApp.entities.TblTeijiReport;
import com.lbxco.RemoteCAREApp.entities.TblUserSetting;
import com.lbxco.RemoteCAREApp.models.DailyReportModel;
import com.lbxco.RemoteCAREApp.models.ReturnContainer;
import com.lbxco.RemoteCAREApp.util.MachineIconType;



@Path("/dailyReport")
@Stateless
public class DailyReportResource {

	private static final Logger logger = Logger.getLogger(DailyReportResource.class.getName());

	@EJB
	IReportService reportService;

	@EJB
	IMachineService machineService;

	@EJB
	IUserStatusService userStatusService;

	@EJB
	ISosikiService sosikiService;

	@EJB
	IDtcNoticeService dtcService;

    @EJB
    IUserService userService;



	/**
	 * [API No.10] úñÚ×æ¾
	 * @param serialNumber
	 * @param userId
	 * @param searchDate
	 * @return Response.JSON DailyReportModel
	 * @throws ParseException
	 */
	@SuppressWarnings("deprecation")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
				@FormParam("serialNumber") Integer serialNumber,
			    @FormParam("userId") String userId,
				@FormParam("searchDate") String searchDate
			) throws ParseException {

		logger.info("[RemoteCAREApp][POST] userId="+userId+", serialNumber:"+serialNumber+", searchDate:"+searchDate);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		DecimalFormat df = new DecimalFormat("#.#");

		try {

			DailyReportModel dailyReport = new DailyReportModel();

			MstMachine machine = machineService.findByMachine(serialNumber);
			if(machine != null) {

				dailyReport.setDate(searchDate);
				dailyReport.setserialNumber(machine.getKibanSerno());
				if(machine.getKiban().substring(7, 8).equals("6"))
					dailyReport.setMachineModelCategory(1);
				else
					dailyReport.setMachineModelCategory(2);
				dailyReport.setManufacturerSerialNumber(machine.getKiban());
				dailyReport.setLbxSerialNumber(machine.getLbxKiban());
				dailyReport.setCustomerManagementNo(machine.getUserKanriNo());
				dailyReport.setScmModel(machine.getModelCd());
				dailyReport.setLbxModel(machine.getLbxModelCd());
				dailyReport.setLatestLocation(machine.getPositionEn());
				if(machine.getNewIdo() != null)
					dailyReport.setIdo(ConvertUtil.parseLatLng(machine.getNewIdo()));
				if(machine.getNewKeido() != null)
					dailyReport.setKeido(ConvertUtil.parseLatLng(machine.getNewKeido()));
				if(machine.getNewHourMeter()!=null)
					dailyReport.setHourMeter(Math.floor(machine.getNewHourMeter()/60.0*10)/10);
				dailyReport.setFuelLevel(machine.getNenryoLv());
				if(machine.getRecvTimestamp()!=null)
					dailyReport.setLatestUtcCommonDateTime(ConvertUtil.formatYMD(machine.getRecvTimestamp()));
				dailyReport.setDefLevelAdblueLevel(machine.getUreaWaterLevel());

				// **««« 2018/12/10 LBNÎì tF[Y2@BACRÎ    «««**//
				List<Object[]> dtcResults = dtcService.findByDtcLevelCount(serialNumber, userService.findByUserSosikiKengenCd(userId));
				dailyReport.setIconType(MachineIconType.stateMachineIconType(dtcResults, machine));
				// **ªªª 2018/12/10 LBNÎì tF[Y2@BACRÎ    ªªª**//
			}

			List<Object[]> kigyouInfo = sosikiService.findByKigyouSosikiName(serialNumber);

			if(kigyouInfo!=null) {
				if(kigyouInfo.get(0)[0]!=null && kigyouInfo.get(0)[1]!=null)
					dailyReport.setCustomerManagementName(kigyouInfo.get(0)[0]+" "+kigyouInfo.get(0)[1]);
				else if(kigyouInfo.get(0)[0]!=null && kigyouInfo.get(0)[1]==null)
					dailyReport.setCustomerManagementName((String)kigyouInfo.get(0)[0]);
				else if(kigyouInfo.get(0)[0]==null && kigyouInfo.get(0)[1]!=null)
					dailyReport.setCustomerManagementName((String)kigyouInfo.get(0)[1]);

			}


			// |[gWv
			Date date;
			date = new Date(sdf.parse(searchDate).getTime());
			java.sql.Timestamp seach = new java.sql.Timestamp(date.getTime());
			seach.setHours(23);
			seach.setMinutes(59);
			seach.setSeconds(59);

			TblTeijiReport ttr = reportService.FindByReport(serialNumber, seach );
			if(ttr != null) {

				//PÊIðæ¾
				TblUserSetting tus = userStatusService.findByUserSetting(userId);
				double unitF = 1;
				if(tus.getUnitSelect() == 0)
					unitF = 3.78541;	//USA

				dailyReport.setNippoData(ttr.getNippoData());

				double engineOperating = 0;
				if(ttr.getHourMeter() != null) {
					engineOperating = new BigDecimal(df.format(Math.floor(ttr.getHourMeter()/60.0*10)/10)).doubleValue();
					dailyReport.setEngineOperatingTime(engineOperating);
					logger.config("[RemoteCAREApp][DEBUG] engineOperating="+engineOperating);
				}

				double machineOperating = 0;
				if(ttr.getKikaiSosaTime()!=null) {
					machineOperating = new BigDecimal(df.format(Math.floor(ttr.getKikaiSosaTime()/60.0*10)/10)).doubleValue();
					dailyReport.setMachineOperatingTime(machineOperating);
					logger.config("[RemoteCAREApp][DEBUG] machineOperating="+machineOperating);
				}

				double fuelConsum = 0;
				if(ttr.getNenryoConsum()!=null) {
					fuelConsum = new BigDecimal(df.format(Math.floor((ttr.getNenryoConsum()/unitF)*10)/10)).doubleValue();
					dailyReport.setFuelConsumption(fuelConsum);
					logger.config("[RemoteCAREApp][DEBUG] fuelConsum="+fuelConsum);
				}

//				double waterConsum = 0;
				BigDecimal waterConsum = new BigDecimal(0);
				if(ttr.getUreaWaterConsum()!=null) {
					if(tus.getUnitSelect() == 0)
						waterConsum = new BigDecimal(ttr.getUreaWaterConsum()/unitF/1000).setScale(1, BigDecimal.ROUND_HALF_UP);
					else
						// **«««@2018/05/15  LBNÎì  v]sïÇä  No36 bgÎ  «««**//
//						waterConsum = new BigDecimal(ttr.getUreaWaterConsum());
						waterConsum = new BigDecimal(df.format(Math.floor(ttr.getUreaWaterConsum()/100)/10));
						// **ªªª@2018/05/15  LBNÎì  v]sïÇä  No36 bgÎ  ªªª**//

					dailyReport.setDefConsumpiton(waterConsum.doubleValue());
					logger.config("[RemoteCAREApp][DEBUG] getUreaWaterConsum="+ttr.getUreaWaterConsum()+", unitF="+unitF+", waterConsum="+waterConsum);
				}

//				if((ttr.getKikaiSosaTime() != null && ttr.getHourMeter() != null) || (engineOperating > 0 && machineOperating > 0)) {
				if(ttr.getHourMeter() != null || engineOperating > 0) {

//					double idlePercentage = (ttr.getHourMeter() - ttr.getKikaiSosaTime()) * 100.0 / ttr.getHourMeter();
//					dailyReport.setIdlePercentage(Math.floor(idlePercentage*10)/10);

					double idleTime = new BigDecimal(engineOperating-machineOperating).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();
					logger.config("[RemoteCAREApp][DEBUG] idleTime="+idleTime);
					logger.config("[RemoteCAREApp][DEBUG] idlePercentage="+(idleTime*100/engineOperating)+" :noFormat");
					logger.config("[RemoteCAREApp][DEBUG] idelPercentage="+(Math.floor((idleTime*100/engineOperating)*100)/100)+" :math");
					if(idleTime > 0) {
						BigDecimal idlePercentage = new BigDecimal(idleTime*100/engineOperating).setScale(1, BigDecimal.ROUND_HALF_UP);
						dailyReport.setIdlePercentage(idlePercentage.doubleValue());
					}

				}

				if(fuelConsum > 0 && engineOperating > 0) {
//					double fuelEffciency = nenryoConsum  / (ttr.getHourMeter() /60.0 );
//					dailyReport.setFuelEffciency(Math.floor(fuelEffciency*10)/10);

					BigDecimal fuelEffciency = new BigDecimal(Math.floor(fuelConsum/engineOperating*100)/100).setScale(1, BigDecimal.ROUND_HALF_UP);
					dailyReport.setFuelEffciency(fuelEffciency.doubleValue());

				}

				dailyReport.setFuelLevelDaily(ttr.getNenryoLv());
				dailyReport.setDefLevelDaily(ttr.getUreaWaterLv());
				dailyReport.setAutoRegenStart(ttr.getJidoSaiseiStartNum());
				dailyReport.setAutoRegenCompleted(ttr.getJidoSaiseiOverNum());

				double radiatorOndoMax = 0.0;
				double sadoyuOndoMax = 0.0;

				if(ttr.getRadiatorOndoMax() != null)
					if(tus.getUnitSelect() == 0)
						radiatorOndoMax = ttr.getRadiatorOndoMax()*1.8+32;
					else
						radiatorOndoMax = ttr.getRadiatorOndoMax();
				dailyReport.setCoolantMax(Math.floor(radiatorOndoMax*10)/10);

				if(ttr.getSadoyuOndoMax() != null)
					if(tus.getUnitSelect() == 0)
						sadoyuOndoMax = ttr.getSadoyuOndoMax()*1.8+32.0;
					else
						sadoyuOndoMax = ttr.getSadoyuOndoMax();
				dailyReport.setOilTempartureMax(Math.floor(sadoyuOndoMax*10)/10);

				}

				dailyReport.setStatusCode(Constants.CON_OK);
				return Response.ok(dailyReport).build();

			}catch(Exception e) {

				logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
				return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

			}


		}
	}


