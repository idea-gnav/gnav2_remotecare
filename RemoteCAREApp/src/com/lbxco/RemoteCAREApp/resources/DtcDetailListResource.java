package com.lbxco.RemoteCAREApp.resources;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.lbxco.RemoteCAREApp.Constants;
import com.lbxco.RemoteCAREApp.ConvertUtil;
import com.lbxco.RemoteCAREApp.annotations.Authorized;
import com.lbxco.RemoteCAREApp.ejbs.dtcDitail.IDtcDitailService;
import com.lbxco.RemoteCAREApp.ejbs.dtcNotice.IDtcNoticeService;
import com.lbxco.RemoteCAREApp.ejbs.machine.IMachineService;
import com.lbxco.RemoteCAREApp.ejbs.sosiki.ISosikiService;
import com.lbxco.RemoteCAREApp.ejbs.user.IUserService;
import com.lbxco.RemoteCAREApp.entities.MstMachine;
import com.lbxco.RemoteCAREApp.models.DtcDetailArrayListModel;
import com.lbxco.RemoteCAREApp.models.DtcDetailListModel;
import com.lbxco.RemoteCAREApp.models.ReturnContainer;
import com.lbxco.RemoteCAREApp.util.MachineIconType;


@Path("/dtcDetailList")
@Stateless
public class DtcDetailListResource {

	private static final Logger logger = Logger.getLogger(DtcDetailListResource.class.getName());

	@EJB
	IDtcDitailService dtcDitailService;

	@EJB
	IUserService userService;

	@EJB
	IMachineService machineService;

	@EJB
	ISosikiService sosikiService;

	@EJB
	IDtcNoticeService dtcService;


	/**
	 * [API No.8] DTCÚ×êæ¾
	 * @param userId
	 * @param serialNumber
	 * @param sortFlg
	 * @param warningDateFrom
	 * @param warningDateTo
	 * @param warningCd
	 * @param startRecord
	 * @return Response.JSON DtcDetailListModel
	 * @throws Exception
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
				@FormParam("userId") String userId,
				@FormParam("serialNumber") Integer serialNumber,
				@FormParam("sortFlg") Integer sortFlg,
				@FormParam("warningDateFrom") String warningDateFrom,
				@FormParam("warningDateTo") String warningDateTo,
				@FormParam("warningCd") String warningCd,
				@FormParam("startRecord") Integer startRecord
			) throws Exception {

		logger.info("[RemoteCAREApp][POST] userId="+userId+", serialNumber="+serialNumber+", sortFlg="+sortFlg
				+", warningDateFrom="+warningDateFrom+", warningDateTo="+warningDateTo+", warningCd="+warningCd+", startRecord="+startRecord);

		try {

			DtcDetailListModel dtcDetailListModel = new DtcDetailListModel();

			MstMachine machine = machineService.findByMachine(serialNumber);

			if(machine != null) {
				dtcDetailListModel.setSerialNumber(machine.getKibanSerno());
				dtcDetailListModel.setManufacturerSerialNumber(machine.getKiban());

				if(machine.getKiban().substring(7, 8).equals("6"))
					dtcDetailListModel.setMachineModelCategory(1);
				else
					dtcDetailListModel.setMachineModelCategory(2);

				dtcDetailListModel.setLbxSerialNumber(machine.getLbxKiban());
				dtcDetailListModel.setCustomerManagementNo(machine.getUserKanriNo());
				dtcDetailListModel.setScmModel(machine.getModelCd());
				dtcDetailListModel.setLbxModel(machine.getLbxModelCd());
				dtcDetailListModel.setLatestLocation(machine.getPositionEn());
				if(machine.getNewIdo()!=null)
					dtcDetailListModel.setIdo(ConvertUtil.parseLatLng(machine.getNewIdo()));
				if(machine.getNewKeido()!=null)
					dtcDetailListModel.setKeido(ConvertUtil.parseLatLng(machine.getNewKeido()));
				if(machine.getNewHourMeter()!=null)
					dtcDetailListModel.setHourMeter(Math.floor(machine.getNewHourMeter()/60.0*10)/10);

				dtcDetailListModel.setFuelLevel(machine.getNenryoLv());

				if(machine.getRecvTimestamp() != null)
					dtcDetailListModel.setLatestUtcCommonDateTime(ConvertUtil.formatYMD(machine.getRecvTimestamp()));

				dtcDetailListModel.setDefLevelAdblueLevel(machine.getUreaWaterLevel());

				// **««« 2018/12/10 LBNÎì tF[Y2@BACRÎ    «««**//
				List<Object[]> dtcResults = dtcService.findByDtcLevelCount(serialNumber, userService.findByUserSosikiKengenCd(userId));
				dtcDetailListModel.setIconType(MachineIconType.stateMachineIconType(dtcResults, machine));
				// **ªªª 2018/12/10 LBNÎì tF[Y2@BACRÎ    ªªª**//

			}


			List<Object[]> machineSosiki = sosikiService.findByMachineSosiki(serialNumber);
			if((String)machineSosiki.get(0)[2] != null && (String)machineSosiki.get(0)[3] != null)
				dtcDetailListModel.setCustomerManagementName((String)machineSosiki.get(0)[2]+" "+(String)machineSosiki.get(0)[3]);
			else if((String)machineSosiki.get(0)[2] != null && (String)machineSosiki.get(0)[3] == null)
				dtcDetailListModel.setCustomerManagementName((String)machineSosiki.get(0)[2]);
			else if((String)machineSosiki.get(0)[2] == null && (String)machineSosiki.get(0)[3] != null)
				dtcDetailListModel.setCustomerManagementName((String)machineSosiki.get(0)[3]);


//			Integer kengenCd = dtcDitailService.findByUser(userId).getKengenCd();
			List<Object[]> userSosikiKengen =userService.findByUserSosikiKengenNativeQuery(userId);
			Integer kengenCd = null;
			if(userSosikiKengen != null) {
				kengenCd = Integer.parseInt(userSosikiKengen.get(0)[4].toString());	// [4] KENGEN_CD
			}
			logger.config("[RemoteCAREApp][DEBUG] userKengenCd="+kengenCd);
			List<Object[]> dtcs = dtcDitailService.findByDtcDitailNativeQuery(
							serialNumber, sortFlg, warningDateFrom, warningDateTo, warningCd, kengenCd);

			if(dtcs != null) {

				Integer maxCount = 1001;
				if(dtcs.size() >= maxCount ) {
					dtcDetailListModel.setStatusCode(Constants.CON_WARNING_MAXIMUN_NUMBER);
					return Response.ok(dtcDetailListModel).build();
				}



				ArrayList<DtcDetailArrayListModel> dtcDetailArrayList = new ArrayList<DtcDetailArrayListModel>();

				Integer record = startRecord-1;
				for(int counter =  record; counter < (record+50) ; counter++ ) {

					if( dtcs.size() == counter) {
						break;}

					DtcDetailArrayListModel dtcDetailArrayListModel = new DtcDetailArrayListModel();

					Object[] dtc = dtcs.get(counter);

					double hourMeter = 0;
					if(dtc[9]!=null) {
						hourMeter = ((Number)dtc[9]).intValue();
						dtcDetailArrayListModel.setHourMeter(Math.floor(hourMeter/60.0*10)/10);
					}

					String alertLv = null;
					if(dtc[8]!=null) {
						String al = (String)dtc[8];
						if(al.equals("1"))
							alertLv = "0";
						else if(al.equals("2"))
							alertLv = "1";
						else if(al.equals("3"))
							alertLv = "2";
					}
//					if(dtc[8]!=null)
//						alertLv = (String)dtc[8];
					dtcDetailArrayListModel.setAlertLv(alertLv);

					//­¶ú»nÔ
					//ú»nÔ
					SimpleDateFormat sdf = new  SimpleDateFormat("yyyy-MM-dd HH:mm");
					if((Timestamp)dtc[10]!=null) {
						dtcDetailArrayListModel.setHasseiDate(sdf.format((Timestamp)dtcs.get(counter)[10]));
					}
					if((Timestamp)dtc[11]!=null)
						dtcDetailArrayListModel.setFukkyuDate(sdf.format((Timestamp)dtcs.get(counter)[11]));

					//warnHistoryContent
					StringBuilder sb = new StringBuilder();

					String dtcEventNo = null;
					if(dtc[16]!=null) {
						dtcEventNo = (String)dtc[16];
//						sb.append(dtcEventNo);	// FLG_VISIBLE
					}
					dtcDetailArrayListModel.setDtcEventNo(dtcEventNo);

					if(dtc[5]!=null)
						sb.append((String)dtc[5]);	// KUBUN_NAME
					sb.append("-");
					if(dtc[6]!=null)
						sb.append((String)dtc[6]);	// EVENT_NAME


					dtcDetailArrayListModel.setWarnHistoryContent(new String(sb));		//DTCàe


					// pdf¶Ý`FbN@0:PDF³ 1:PDFL
					String pdf = dtcDitailService.findByPDF(serialNumber, dtcEventNo);
					if(pdf!=null)
						dtcDetailArrayListModel.setPdfFlg(1);
					else
						dtcDetailArrayListModel.setPdfFlg(0);

					dtcDetailArrayList.add(dtcDetailArrayListModel);

				}


				dtcDetailListModel.setDtcDetailList(dtcDetailArrayList);
				dtcDetailListModel.setDtcCount(dtcs.size());

			}else {
				// count 0
				dtcDetailListModel.setDtcDetailList(null);
				dtcDetailListModel.setDtcCount(0);

			}

			dtcDetailListModel.setStatusCode(Constants.CON_OK);
			return Response.ok(dtcDetailListModel).build();


		}catch(Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}

	}
}


