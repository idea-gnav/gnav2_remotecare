package com.lbxco.RemoteCAREApp.resources;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.lbxco.RemoteCAREApp.Constants;
import com.lbxco.RemoteCAREApp.annotations.Authorized;
import com.lbxco.RemoteCAREApp.ejbs.mailer.Mailer;
import com.lbxco.RemoteCAREApp.models.ReturnContainer;


@Path("/appLogList")
@Stateless
public class ApplogListResource {

	private static final Logger logger = Logger.getLogger(ApplogListResource.class.getName());

	@EJB
	Mailer mailer;


	/**
	 * [API No.15] アプリケーションログ送信
	 * @param OperatingLog
	 * @param ErrorLog
	 * @param userId
	 * @return Response.JSON statusCode
	 * @throws AddressException
	 * @throws MessagingException
	 * @throws IOException
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Authorized(Constants.CON_AUTHORIZED_PUBLIC)
	public Response postMethod(
				@FormParam("appOperatingLog") String OperatingLog,
				@FormParam("appErrorLog") String ErrorLog,
				@FormParam("userId") String userId
			) throws AddressException, MessagingException, IOException {

		logger.info("[RemoteCAREApp][POST] userId="+userId);

		try {

			String MailTo = Constants.APP_LOG_MAIL_ADDRESS;
			String MailSubject = "RemoteCARE App logs! ["+userId+"]";
			String MailContent = userId + ": \nThis is log file LBX application logs.";

			String DirPath = Constants.APP_LOG_PATH;
			String OperaFilePath = "";
			String ErrFilePath = "";
			List<String> lstAttachFile = new ArrayList<String>();
			File theDir = new File(DirPath);

			if (!theDir.exists()) {
				theDir.mkdir();
			}

			BufferedWriter writer1 = null;
			BufferedWriter writer2 = null;

			try {

				if (OperatingLog.length() > 0 || OperatingLog != null) {
					OperaFilePath = DirPath + "/appOperatingLog.log";

					writer1 = new BufferedWriter(
							new OutputStreamWriter(new FileOutputStream(""), StandardCharsets.UTF_8));
					String[] wordsOpe = OperatingLog.split("'");
					for (String word : wordsOpe) {
						writer1.write(word);
						writer1.newLine();
					}
					lstAttachFile.add(OperaFilePath);
					writer1.close();
				}

				if (ErrorLog.length() > 0 || ErrorLog !=null) {
					ErrFilePath = DirPath + "/appErrorLog.log";
					writer2 = new BufferedWriter(
							new OutputStreamWriter(new FileOutputStream(ErrFilePath), StandardCharsets.UTF_8));
					String[] wordsErro = ErrorLog.split("'");
					for (String word : wordsErro) {
						writer2.write(word);
						writer2.newLine();
					}

					lstAttachFile.add(ErrFilePath);
					writer2.close();
				}

			} catch (Exception e) {

				logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
				return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

			} finally {
				if(writer1 != null) writer1.close();
				if(writer2 != null) writer2.close();
			}

			if(mailer.sendMail(MailTo, MailSubject, MailContent, lstAttachFile))
				return Response.ok(new ReturnContainer(Constants.CON_OK)).build();
			else
				return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		} catch (Exception e) {

			logger.log(Level.WARNING, e.fillInStackTrace().toString(),e);
			return Response.ok(new ReturnContainer(Constants.CON_UNKNOWN_ERROR)).build();

		}



	}
}
