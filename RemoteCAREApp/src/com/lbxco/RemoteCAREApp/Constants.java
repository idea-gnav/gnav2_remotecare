package com.lbxco.RemoteCAREApp;

public class Constants {

	public static final String CON_AUTHORIZED_PUBLIC="Public";
	public static final String CON_AUTHORIZED_REQUIRED="Required";

	public static final int CON_OK = 1000;							// 成功
	public static final int CON_WARNING_PASSWORD_3DAYS = 1001;		// 認証成功、パスワード有効期限切れ間近(3日）
	public static final int CON_WARNING_PASSWORD_EXPIRED = 1101;	// パスワード有効期限切れ、初回利用時パスワード変更
	public static final int CON_WARNING_NO_USER_PASSWORD = 1201;	// ユーザーIDまたはパスワード誤り
	public static final int CON_WARNING_APP_VERSION = 1301;			// アプリケーションバージョン警告
	public static final int CON_WARNING_MAXIMUN_NUMBER = 1401;		// 検索件数が最大件数を超える
	public static final int CON_WARNING_PASSWORD_7DAYS = 1501;		// 認証成功、パスワード有効期限切れ間近(7日）
	public static final int CON_WARNING_NO_LATLNG = 1601;			// 特定位置集権検索の機番が存在しない又は機番に緯度経度がない
	public static final int CON_UNKNOWN_ERROR = 1002;				// 例外

	public static final int CON_APP_FLG = 0;						// 0: RemoteCAREApp
	public static final int CON_MAX_COUNT = 1001;					// 最大レコード数
	public static final int GOOGLE_MAP_LOG_FLAG = 0;				// 0:記録しない  1:記録する
	public static final int CON_ENGLISH_ONLY = 0;					// 0:英語

	public static final String TYPE_ICON = "iconType_";				// ログイン時 機械アイコン
	public static final String TYPE_ICON_LIST = "iconTypeList";		// ログイン時 機械アイコンリスト



/* ************************************
 * 開発(RASIS)サーバー設定

	public static final String HOST_URL = "http://113.33.116.157:14908/RemoteCAREApp/";
	public static final String APP_VERTION_PATH = "/home/profile01/RemoteCAREApp/app.properties";
	public static final String APP_LOG_PATH = "/home/profile01/RemoteCAREApp";
	public static final String APP_CONFIG_PATH = "/home/profile01/RemoteCAREApp/config.properties";
	public static final String APP_LOG_MAIL_ADDRESS = "sumiken.dev.02@outlook.jp";

	public static final String PDF_URL = "http://113.33.116.157:14908/Fileserver/GnavAppFiles/PDF/";
	public static final String FILE_URL = "http://113.33.116.157:14908/Fileserver/GnavAppFiles/";
	public static final String FILE_STORAGE_PATH = "/opt/IBM/WebSphere/AppServer/profiles/AppSrv01/installedApps/centos7Node01Cell/Fileserver.ear/META-INF/GnavAppFiles/";
	public static final String FILE_STORAGE_DEALER_LOGO_SAVE_PATH = "Images/LbxDealerLogo/";

*/



/* ************************************
 * テストサーバー設定
 * info@gnav2.scm.shi.co.jp

	public static final String HOST_URL = "https://scmgnavapptest.shi.co.jp/RemoteCAREApp/";
	public static final String APP_VERTION_PATH = "/home/profile11/RemoteCAREApp/app.properties";
	public static final String APP_LOG_PATH = "/home/profile11/RemoteCAREApp";
	public static final String APP_CONFIG_PATH = "/home/profile11/RemoteCAREApp/config.properties";
	public static final String APP_LOG_MAIL_ADDRESS = "idea_remotecare_ap@ideacns.co.jp";

	public static final String PDF_URL = "https://scmgnavapptest.shi.co.jp/GnavAppFilesTest/PDF/";
	public static final String FILE_URL = "https://scmgnavapptest.shi.co.jp/GnavAppFilesTest/";
	public static final String FILE_STORAGE_PATH = "/opt/IBM/HTTPServer85/htdocs/profile11/GnavAppFilesTest/";
	public static final String FILE_STORAGE_DEALER_LOGO_SAVE_PATH = "Images/LbxDealerLogo/";
*/


/* ************************************
 * 本番設定
 * info@gnav2.scm.shi.co.jp
 */
	public static final String HOST_URL = "https://scmgnavApp.shi.co.jp/RemoteCAREApp/";
	public static final String APP_VERTION_PATH = "/home/profile16/RemoteCAREApp/app.properties";
	public static final String APP_LOG_PATH = "/home/profile16/RemoteCAREApp";
	public static final String APP_CONFIG_PATH = "/home/profile16/RemoteCAREApp/config.properties";
	public static final String APP_LOG_MAIL_ADDRESS = "GR.SCM.USER.GNAV_DEVELOPER@shi-g.com";

	public static final String PDF_URL = "https://scmgnavApp.shi.co.jp/apfile/PDF/";
	public static final String FILE_URL = "https://scmgnavApp.shi.co.jp/apfile/";
	public static final String FILE_STORAGE_PATH = "/opt/IBM/HTTPServer85/htdocs/profile11/apfile/";
	public static final String FILE_STORAGE_DEALER_LOGO_SAVE_PATH = "Images/LbxDealerLogo/";









}
