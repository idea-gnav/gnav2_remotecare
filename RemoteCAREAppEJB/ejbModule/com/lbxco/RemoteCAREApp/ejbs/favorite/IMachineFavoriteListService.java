package com.lbxco.RemoteCAREApp.ejbs.favorite;

import java.util.List;

import com.lbxco.RemoteCAREApp.entities.TblUserSetting;

public interface IMachineFavoriteListService {
	
	TblUserSetting findKibanSelect(String userId);
	List<Object[]> findFavoriteMachine(String userId, Integer kibanSelect, Integer sortFlg);
	List<Object[]> findFavoriteMachineNativeQuery(String userId, Integer kibanSelect, Integer sortFlg);
}
