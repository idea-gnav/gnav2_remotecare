package com.lbxco.RemoteCAREApp.ejbs.favorite;

import java.sql.Timestamp;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.lbxco.RemoteCAREApp.ejbs.user.UserService;
import com.lbxco.RemoteCAREApp.entities.TblUserFavorite;
import com.lbxco.RemoteCAREApp.entities.TblUserFavoritePK;


@Stateless
@Local(IMachineFavoriteRegisterService.class)
public class MachineFavoriteRegisterService extends UserService implements IMachineFavoriteRegisterService{

	@PersistenceContext
    EntityManager em;



	/**
	 * JPA
	 * addFavoriteMachine
	 */
	@Override
	public void addFavoriteMachine(Integer addSerialNumber, String userId){

		/*
		 * UPDATE
		 */
		TblUserFavorite tuf = new TblUserFavorite();
		TblUserFavoritePK pk = new TblUserFavoritePK();
		pk.setKibanSerno(addSerialNumber);
		pk.setUserId(userId);
		tuf = em.find(TblUserFavorite.class,pk);

		if(tuf!=null){
			tuf.setDelFlg(0);
			tuf.setUpdatePrg("RemoteCAREApp");
			tuf.setUpdateDtm(new Timestamp(System.currentTimeMillis()));
			tuf.setUpdateUser(userId);
			em.persist(tuf);
		}
		else{
			/*
			 * INSERT
			 */
			tuf = new TblUserFavorite();
				tuf.setKibanSerno(addSerialNumber);
				tuf.setUserId(userId);
				tuf.setDelFlg(0);
				tuf.setRegistPrg("RemoteCAREApp");
				tuf.setRegistDtm(new Timestamp(System.currentTimeMillis()));
				tuf.setRegistUser(userId);
				tuf.setUpdatePrg("RemoteCAREApp");
				tuf.setUpdateDtm(new Timestamp(System.currentTimeMillis()));
				tuf.setUpdateUser(userId);
				em.persist(tuf);
		}

		em.flush();
		em.clear();

		return;
	}



	/**
	 * JPA
	 * removeFavoriteMachine
	 */
	@Override
	public void removeFavoriteMachine(Integer removeSerialNumber,String userId) {


		/*
		 * UPDATE
		 */
		TblUserFavorite tuf = new TblUserFavorite();
		TblUserFavoritePK pk = new TblUserFavoritePK();
		pk.setKibanSerno(removeSerialNumber);
		pk.setUserId(userId);
		tuf = em.find(TblUserFavorite.class,pk);
		tuf.setDelFlg(1);
		tuf.setUpdatePrg("RemoteCAREApp");
		tuf.setUpdateDtm(new Timestamp(System.currentTimeMillis()));
		tuf.setUpdateUser(userId);
		em.persist(tuf);

		em.flush();
		em.clear();

		return;
	}





}
