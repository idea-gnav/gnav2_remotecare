package com.lbxco.RemoteCAREApp.ejbs.favorite;


import com.lbxco.RemoteCAREApp.ejbs.user.IUserService;

public interface IMachineFavoriteRegisterService extends IUserService{


	/**
	 * JPA
	 * addFavoriteMachine
	 */
	void addFavoriteMachine(Integer addSerialNumber, String userId);


	/**
	 * JPA
	 * removeFavoriteMachine
	 */
	void removeFavoriteMachine(Integer removeSerialNumber,String userId);




}
