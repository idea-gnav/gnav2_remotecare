package com.lbxco.RemoteCAREApp.ejbs.dtcNotice;

import java.util.List;

public interface IDtcNoticeService {


	// **↓↓↓　2018/07/19 LBN石川  要望対応 お気に入りDTC履歴の既読処理 追加 ↓↓↓**//

	/**
	 * findByNotExistsTblFavoriteDtcHistory
	 * お気に入りDTC履歴テーブルに存在しない、お気に入り登録済の未復旧DTCを取得
	 */
	int insertByNotExistsTblFavoriteDtcHistory(String userId, Integer kengenCd, Integer serialNumber);


	/**
	 * 既読フラグ0、または削除フラグを更新
	 * updateTblFavoriteDtcHistory　
	 */
	void updateTblFavoriteDtcHistory(String userId, Integer serialNumber, Integer readFlg, Integer delFlg);


	/**
	 * 復旧済みの既読処理
	 * updateByFukkyuReadFlg
	 * DEL_FLG 0, 1 に関わらず、お気に入りDTCの復旧済み既読フラグ更新
	 */
	void updateFukkyuDtcTblFavoriteDtcHistory(String userId);

	// **↑↑↑　2018/07/19 LBN石川  要望対応 お気に入りDTC履歴の既読処理 追加 ↑↑↑**//


	/**
	 * TBL_EVENT_SEND に存在しないイベントデータの考慮
	 * 過去に履歴テーブルに記録されたが、イベントテーブルのキー整合性とれない場合の対応
	 * 既読フラグ1、削除フラグ1に更新
	 */
	void updateNoMatchTblFavoriteDtcHistory(String userId);
	// **↑↑↑　2018/09/19 LBN石川  イベントテーブルのキー整合性とれない場合の対応 追加 ↑↑↑**//


	/**
	 * findByDtcNotices: トップお気に入りDTC一覧取得
	 * 					  発生後直近2日DTCおよび3日以前の未復旧の未読DTC
	 */
	List<Object[]> findByDtcNoticesNativeQuery(String userId, Integer kengenCd);



	// **↓↓↓　2018/07/19 LBN石川  要望対応 お気に入りDTC履歴の未読件数鵜取得処理 追加 ↓↓↓**//
	/**
	 * findByNoReadCount:未読件数を取得
	 */
	int findByNoReadCount(String userId);
	// **↑↑↑　2018/07/19 LBN石川  要望対応 お気に入りDTC履歴の未読件数鵜取得処理 追加 ↑↑↑**//


	// **↓↓↓　2019/01/31 LBN石川  ステップ2フェーズ3 機械アイコン対応追加 ↓↓↓**//
	/**
	 * findByDtcLevelCount: DTCのレベル別件数を取得
	 */
	List<Object[]> findByDtcLevelCount(Integer serialNumber, Integer kengenCd);
	// **↑↑↑　2019/01/31 LBN石川  ステップ2フェーズ3 機械アイコン対応追加 ↑↑↑**//

}
