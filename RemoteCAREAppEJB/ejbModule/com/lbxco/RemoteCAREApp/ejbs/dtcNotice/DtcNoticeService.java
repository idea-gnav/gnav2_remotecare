package com.lbxco.RemoteCAREApp.ejbs.dtcNotice;


import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.lbxco.RemoteCAREApp.ConvertUtil;


@Stateless
@Local(IDtcNoticeService.class)
public class DtcNoticeService implements IDtcNoticeService{


	@PersistenceContext
    EntityManager em;



	/**
	 * findByNotExistsTblFavoriteDtcHistory
	 * お気に入りDTC履歴テーブルに存在しない、お気に入り登録済の未復旧DTCを取得
	 */
	@Override
	public int insertByNotExistsTblFavoriteDtcHistory(String userId, Integer kengenCd, Integer serialNumber) {


		StringBuilder sb = new StringBuilder();


		sb.append("INSERT INTO TBL_FAVORITE_DTC_HISTORY( ");
		sb.append("USER_ID, ");
		sb.append("KIBAN_SERNO, ");
		sb.append("KIBAN_ADDR, ");
		sb.append("EVENT_NO, ");
		sb.append("EVENT_SHUBETU, ");
		sb.append("RECV_TIMESTAMP, ");
		sb.append("HASSEI_TIMESTAMP, ");
		sb.append("CON_TYPE, ");
		sb.append("READ_FLG, ");
		sb.append("DEL_FLG, ");
		sb.append("REGIST_DTM, ");
		sb.append("REGIST_USER, ");
		sb.append("REGIST_PRG, ");
		sb.append("UPDATE_DTM, ");
		sb.append("UPDATE_USER, ");
		sb.append("UPDATE_PRG ");
		sb.append(") ");

		sb.append("SELECT '"+userId+"', ");
		sb.append("TES.KIBAN_SERNO, ");
		sb.append("TES.KIBAN_ADDR, ");
		sb.append("TES.EVENT_NO, ");
		sb.append("TES.EVENT_SHUBETU, ");
		sb.append("TES.RECV_TIMESTAMP, ");
		sb.append("TES.HASSEI_TIMESTAMP, ");
		sb.append("TES.CON_TYPE, ");
		sb.append("0, 0, SYSDATE, '"+userId+"', 'RemoteCAREApp', SYSDATE, '"+userId+"', 'RemoteCAREApp' ");

		sb.append("FROM TBL_EVENT_SEND TES ");

		sb.append("JOIN TBL_USER_FAVORITE TUF ");
		sb.append("ON TES.KIBAN_SERNO = TUF.KIBAN_SERNO ");
		sb.append("AND TUF.DEL_FLG = 0 ");
		sb.append("AND TUF.USER_ID = '"+userId+"' ");

		sb.append("INNER JOIN TBL_KEIHO_HYOJI_SET TK  ");
		sb.append("ON (TO_CHAR(TES.EVENT_NO) = TK.EVENT_CODE OR NVL(TES.N_EVENT_NO,'N_EVENT_NO') = TO_CHAR(TK.EVENT_CODE)) ");
		sb.append("AND TES.EVENT_SHUBETU = TK.SHUBETU_CODE ");
		sb.append("AND TES.CON_TYPE = TK.CON_TYPE ");
		sb.append("AND TES.MACHINE_KBN = TK.MACHINE_KBN ");
		sb.append("AND TK.KENGEN_CD = "+kengenCd+" ");

		sb.append("INNER JOIN MST_EVENT_SEND MES ");
		sb.append("ON TES.EVENT_NO = MES.EVENT_CODE ");
		sb.append("AND TES.EVENT_SHUBETU = MES.SHUBETU_CODE ");
		sb.append("AND TES.CON_TYPE = MES.CON_TYPE ");
		sb.append("AND TES.MACHINE_KBN = MES.MACHINE_KBN ");

		sb.append("AND MES.DELETE_FLAG = 0 ");
		sb.append("AND MES.LANGUAGE_CD = 'en' ");
		sb.append("AND TES.EVENT_SHUBETU = 8 ");
		sb.append("AND TES.CON_TYPE IN ('C','C2') ");
		sb.append("AND TES.SYORI_FLG <> 9 ");

		if(serialNumber!=null)
			sb.append("AND TES.KIBAN_SERNO = "+serialNumber+" ");

		sb.append("AND TES.FUKKYU_TIMESTAMP IS NULL ");

		sb.append("AND TES.HASSEI_TIMESTAMP >=  TO_TIMESTAMP('" + ConvertUtil.getDays(-1) + " 00:00:00.0', 'yyyy/MM/dd hh24:mi:ss.FF6') ");
//		sb.append("AND TES.REGIST_DTM >=  TO_TIMESTAMP('" + Utilitys.getDaysUTC(-1) + " 00:00:00.0', 'yyyy/MM/dd hh24:mi:ss.FF6') ");

		sb.append("AND NOT EXISTS ( ");
		sb.append("SELECT * FROM TBL_FAVORITE_DTC_HISTORY TFDH ");
		sb.append("WHERE DEL_FLG = 0 ");
		sb.append("AND TFDH.USER_ID = '"+userId+"'");
		sb.append("AND TES.KIBAN_ADDR = TFDH.KIBAN_ADDR ");
		sb.append("AND TES.EVENT_NO = TFDH.EVENT_NO ");
		sb.append("AND TES.EVENT_SHUBETU = TFDH.EVENT_SHUBETU ");
		sb.append("AND TES.RECV_TIMESTAMP = TFDH.RECV_TIMESTAMP ");
		sb.append("AND TES.HASSEI_TIMESTAMP = TFDH.HASSEI_TIMESTAMP ");
		sb.append("AND TES.CON_TYPE = TFDH.CON_TYPE ");
		sb.append("AND TES.KIBAN_SERNO = TFDH.KIBAN_SERNO ");
		sb.append(") ");

		sb.append("AND TES.FUKKYU_TIMESTAMP IS NULL ");
		sb.append("ORDER BY TES.HASSEI_TIMESTAMP_LOCAL DESC ");


		em.getEntityManagerFactory().getCache().evictAll();
		int insert =  em.createNativeQuery(new String(sb)).executeUpdate();

		if(insert>0)
			em.flush();
		em.clear();

		return insert;

	}


	/**
	 * 既読フラグ0、または削除フラグを更新
	 * updateTblFavoriteDtcHistory　
	 */
	@Override
	public void updateTblFavoriteDtcHistory(String userId, Integer serialNumber, Integer readFlg, Integer delFlg) {

		StringBuilder sb = new StringBuilder();

		sb.append("UPDATE TBL_FAVORITE_DTC_HISTORY ");
		if(delFlg!=null)
			sb.append("SET DEL_FLG = " + delFlg + ", ");
		if(readFlg!=null)
			sb.append("SET READ_FLG = " + readFlg + ", ");

		sb.append("UPDATE_USER = '" + userId + "', ");
		sb.append("UPDATE_PRG = 'RemoteCAREApp', ");
		sb.append("UPDATE_DTM = SYSDATE ");
		sb.append("WHERE USER_ID = '" + userId + "' ");
		sb.append("AND KIBAN_SERNO = " + serialNumber);

		int updated = em.createNativeQuery(new String(sb)).executeUpdate();

		if(updated>0)
			em.flush();
		em.clear();

		return;
	}


	/**
	 * 復旧済みの既読処理
	 * updateByFukkyuReadFlg
	 * DEL_FLG 0, 1 に関わらず、お気に入りDTCの復旧済み既読フラグ更新
	 */
	@Override
	public void updateFukkyuDtcTblFavoriteDtcHistory(String userId) {

		StringBuilder sb = new StringBuilder();

		sb.append("UPDATE TBL_FAVORITE_DTC_HISTORY TFDH SET READ_FLG = 1, ");
		sb.append("UPDATE_USER = '"+userId+"', ");
		sb.append("UPDATE_PRG = 'RemoteCAREApp', ");
		sb.append("UPDATE_DTM = SYSDATE ");
		sb.append("WHERE EXISTS ( ");
		sb.append("SELECT * FROM TBL_EVENT_SEND TES ");
		sb.append("WHERE TES.KIBAN_ADDR = TFDH.KIBAN_ADDR ");
		sb.append("AND TES.EVENT_NO = TFDH.EVENT_NO ");
		sb.append("AND TES.EVENT_SHUBETU = TFDH.EVENT_SHUBETU ");
		sb.append("AND TES.RECV_TIMESTAMP = TFDH.RECV_TIMESTAMP ");
		sb.append("AND TES.HASSEI_TIMESTAMP = TFDH.HASSEI_TIMESTAMP ");
		sb.append("AND TES.KIBAN_SERNO = TFDH.KIBAN_SERNO ");
		sb.append("AND TES.FUKKYU_TIMESTAMP IS NOT NULL ");
		sb.append("AND TFDH.USER_ID = '"+userId+"' ");
		sb.append(")");

		em.createNativeQuery(new String(sb)).executeUpdate();

		return;
	}
	// **↑↑↑　2018/08/19 LBN石川  要望対応 お気に入りDTC履歴の既読処理 追加 ↑↑↑**//



	/**
	 * TBL_EVENT_SEND に存在しないイベントデータの考慮
	 * 過去に履歴テーブルに記録されたが、イベントテーブルのキー整合性とれない場合の対応
	 * 既読フラグ1、削除フラグ1に更新
	 */
	@Override
	public void updateNoMatchTblFavoriteDtcHistory(String userId) {

		StringBuilder sb = new StringBuilder();

		sb.append("UPDATE TBL_FAVORITE_DTC_HISTORY TFDH ");
		sb.append("SET DEL_FLG = 1, ");
		sb.append("READ_FLG = 1, ");
		sb.append("UPDATE_DTM = sysdate, ");
		sb.append("UPDATE_USER = '"+userId+"', ");
		sb.append("UPDATE_PRG = 'RemoteCAREApp' ");
		sb.append("WHERE DEL_FLG = 0 ");
		sb.append("AND USER_ID = '"+userId+"' ");
		sb.append("AND NOT EXISTS ( ");
		sb.append("SELECT * FROM TBL_EVENT_SEND TES ");
		sb.append("WHERE TES.KIBAN_ADDR = TFDH.KIBAN_ADDR ");
		sb.append("AND TES.EVENT_NO = TFDH.EVENT_NO ");
		sb.append("AND TES.EVENT_SHUBETU = TFDH.EVENT_SHUBETU ");
		sb.append("AND TES.RECV_TIMESTAMP = TFDH.RECV_TIMESTAMP ");
		sb.append("AND TES.HASSEI_TIMESTAMP = TFDH.HASSEI_TIMESTAMP ");
		sb.append("AND TES.KIBAN_SERNO = TFDH.KIBAN_SERNO ");
		sb.append(") OR EXISTS ( ");
		sb.append("SELECT * FROM TBL_EVENT_SEND TES ");
		sb.append("WHERE TES.KIBAN_ADDR = TFDH.KIBAN_ADDR ");
		sb.append("AND TES.EVENT_NO = TFDH.EVENT_NO ");
		sb.append("AND TES.EVENT_SHUBETU = TFDH.EVENT_SHUBETU ");
		sb.append("AND TES.RECV_TIMESTAMP = TFDH.RECV_TIMESTAMP ");
		sb.append("AND TES.HASSEI_TIMESTAMP = TFDH.HASSEI_TIMESTAMP ");
		sb.append("AND TES.KIBAN_SERNO = TFDH.KIBAN_SERNO ");
		sb.append("AND TFDH.DEL_FLG = 0 ");
		sb.append("AND TFDH.USER_ID = '"+userId+"' ");
		sb.append("AND TES.SYORI_FLG = 9 ");
		sb.append(") ");

		em.createNativeQuery(new String(sb)).executeUpdate();

		return;

	}





	/**
	 * NativeQuery
	 * findByDtcNotices: トップお気に入りDTC一覧取得
	 * 					  発生後直近2日DTCおよび3日以前の未復旧の未読DTC
	 */
	@Override
	public List<Object[]> findByDtcNoticesNativeQuery(String userId, Integer kengenCd){


		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");

		sb.append("TES.KIBAN_SERNO, ");				// 0
		sb.append("TES.CON_TYPE, ");				// 1
		sb.append("TES.EVENT_NO, ");				// 2
		sb.append("MES.FLG_VISIBLE, ");				// 3
		sb.append("MES.EVENT_CODE, ");				// 4
		sb.append("MES.KUBUN_NAME, ");				// 5
		sb.append("MES.EVENT_NAME, ");				// 6
		sb.append("MES.MESSAGE, ");					// 7
		sb.append("MES.ALERT_LV, ");				// 8
		sb.append("TES.HOUR_METER, ");				// 9
		sb.append("TES.HASSEI_TIMESTAMP_LOCAL, ");	// 10
		sb.append("TES.FUKKYU_TIMESTAMP_LOCAL, ");	// 11
		sb.append("TES.SHOZAITI_EN, ");				// 12
		sb.append("TES.IDO, ");						// 13
		sb.append("TES.KEIDO, ");					// 14
		sb.append("MM.KIBAN, ");					// 15
		sb.append("MM.LBX_KIBAN, ");				// 16
		sb.append("MM.USER_KANRI_NO, ");			// 17
		sb.append("TES.RECV_TIMESTAMP_LOCAL ");		// 18
		// **↓↓↓　2018/08/19 LBN石川  要望対応 お気に入りDTC履歴の既読フラグ 追加 ↓↓↓**//
		sb.append(",TFDH.READ_FLG ");				// 19
		// **↑↑↑　2018/08/19 LBN石川  要望対応 お気に入りDTC履歴の既読フラグ 追加 ↑↑↑**//

		sb.append("FROM TBL_EVENT_SEND TES ");

		sb.append("JOIN TBL_USER_FAVORITE TUF ");
		sb.append("ON TES.KIBAN_SERNO = TUF.KIBAN_SERNO ");
		sb.append("AND TUF.USER_ID = '"+userId+"' ");
		sb.append("AND TUF.DEL_FLG = 0 ");

		// **↓↓↓　2018/08/19 LBN石川  要望対応 お気に入りDTC履歴の既読フラグ 追加 ↓↓↓**//
		sb.append("JOIN TBL_FAVORITE_DTC_HISTORY TFDH ");
		sb.append("ON TFDH.DEL_FLG = 0 ");
		sb.append("AND TES.KIBAN_ADDR = TFDH.KIBAN_ADDR ");
		sb.append("AND TES.EVENT_NO = TFDH.EVENT_NO ");
		sb.append("AND TES.EVENT_SHUBETU = TFDH.EVENT_SHUBETU ");
		sb.append("AND TES.RECV_TIMESTAMP = TFDH.RECV_TIMESTAMP ");
		sb.append("AND TES.HASSEI_TIMESTAMP = TFDH.HASSEI_TIMESTAMP ");
		sb.append("AND TES.KIBAN_SERNO = TFDH.KIBAN_SERNO ");
		sb.append("AND TFDH.USER_ID = '"+userId+"' ");
		// **↑↑↑　2018/08/19 LBN石川  要望対応 お気に入りDTC履歴の既読フラグ 追加 ↑↑↑**//

		sb.append("INNER JOIN TBL_KEIHO_HYOJI_SET TK  ");
		sb.append("ON (TO_CHAR(TES.EVENT_NO) = TK.EVENT_CODE OR NVL(TES.N_EVENT_NO,'N_EVENT_NO') = TO_CHAR(TK.EVENT_CODE)) ");
		sb.append("AND TES.EVENT_SHUBETU = TK.SHUBETU_CODE ");
		sb.append("AND TES.CON_TYPE = TK.CON_TYPE ");
		sb.append("AND TES.MACHINE_KBN = TK.MACHINE_KBN ");
		sb.append("AND TK.KENGEN_CD = "+kengenCd+" ");

		sb.append("INNER JOIN MST_EVENT_SEND MES ");
		sb.append("ON TES.EVENT_NO = MES.EVENT_CODE ");
		sb.append("AND TES.EVENT_SHUBETU = MES.SHUBETU_CODE ");
		sb.append("AND TES.CON_TYPE = MES.CON_TYPE ");
		sb.append("AND TES.MACHINE_KBN = MES.MACHINE_KBN ");

		sb.append("JOIN MST_MACHINE MM ON TES.KIBAN_SERNO = MM.KIBAN_SERNO ");
		sb.append("AND MES.DELETE_FLAG = 0 ");
		sb.append("AND MES.LANGUAGE_CD = 'en' ");
		sb.append("AND TES.EVENT_SHUBETU = 8 ");
		sb.append("AND TES.CON_TYPE IN ('C','C2') ");
		sb.append("AND TES.SYORI_FLG <> 9 ");

//		sb.append("AND EXISTS (SELECT * FROM TBL_USER_FAVORITE TUF WHERE MM.KIBAN_SERNO = TUF.KIBAN_SERNO AND USER_ID = '"  + userId + "' AND DEL_FLG = 0) ");
//		sb.append("AND TES.FUKKYU_TIMESTAMP IS NULL ");
//		sb.append("AND TES.HASSEI_TIMESTAMP >= TO_TIMESTAMP('" + Utilitys.getDays(-1) + " 00:00:00.0', 'yyyy/MM/dd hh24:mi:ss.FF6') ");

		sb.append("AND ( ( ");
		sb.append("TES.FUKKYU_TIMESTAMP IS NULL ");
		sb.append("AND TES.HASSEI_TIMESTAMP >= TO_TIMESTAMP('" + ConvertUtil.getDays(-1) + " 00:00:00.0', 'yyyy/MM/dd hh24:mi:ss.FF6') ");
		sb.append(") OR ( ");
		sb.append("EXISTS( ");
		sb.append("SELECT * FROM TBL_FAVORITE_DTC_HISTORY TFDH ");
		sb.append("WHERE TFDH.DEL_FLG = 0 ");
		sb.append("AND TFDH.USER_ID = '"+userId+"' ");
		sb.append("AND TFDH.READ_FLG = 0 ) ");
		sb.append("AND TFDH.READ_FLG = 0 ");
		sb.append("AND TES.FUKKYU_TIMESTAMP IS NULL ");
		sb.append("AND TES.HASSEI_TIMESTAMP < TO_TIMESTAMP('" + ConvertUtil.getDays(-1) + " 00:00:00.0', 'yyyy/MM/dd hh24:mi:ss.FF6') ");
		sb.append(") ) ");

		sb.append("ORDER BY TES.HASSEI_TIMESTAMP_LOCAL DESC ");

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<Object[]> result = em.createNativeQuery(new String(sb)).getResultList();

		if(result.size() > 0)
			return result;
		else
			return null;
	}




	// **↓↓↓　2018/08/19 LBN石川  要望対応 お気に入りDTC履歴の未読件数鵜取得処理 追加 ↓↓↓**//
	/**
	 * findByNoReadCount:未読件数を取得
	 *
	 */
	@Override
	public int findByNoReadCount(String userId) {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT COUNT(*) FROM TBL_FAVORITE_DTC_HISTORY TFDH ");
		sb.append("WHERE TFDH.DEL_FLG = 0 ");
		sb.append("AND TFDH.READ_FLG = 0 ");
		sb.append("AND TFDH.USER_ID = '"+userId+"' ");


		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<Object[]> result =  em.createNativeQuery(new String(sb)).getResultList();

		Object dtcNoReadCount = result.get(0);
		String count = dtcNoReadCount.toString();

		if(count!=null)
			return Integer.parseInt(count);
		else
			return 0;
	}
	// **↑↑↑　2018/08/19 LBN石川  要望対応 お気に入りDTC履歴の未読件数鵜取得処理 追加 ↑↑↑**//



	// **↓↓↓　2019/01/31 LBN石川  ステップ2フェーズ3 機械アイコン対応追加 ↓↓↓**//
	/**
	 * findByDtcLevelCount: DTCのレベル別件数を取得
	 */
	@Override
	public List<Object[]> findByDtcLevelCount(Integer serialNumber, Integer kengenCd){

		StringBuilder sb = new StringBuilder();


		sb.append("SELECT ");
		sb.append("MES.ALERT_LV, ");			// [0]ALERT_LV
		sb.append("COUNT(MES.ALERT_LV) ");		// [1]COUNT
		sb.append("FROM TBL_EVENT_SEND TES ");

		sb.append("INNER JOIN TBL_KEIHO_HYOJI_SET TK ");
		sb.append("ON (TO_CHAR(TES.EVENT_NO) = TK.EVENT_CODE OR NVL(TES.N_EVENT_NO,'N_EVENT_NO') = TO_CHAR(TK.EVENT_CODE)) ");
		sb.append("AND TES.EVENT_SHUBETU = TK.SHUBETU_CODE ");
		sb.append("AND TES.CON_TYPE = TK.CON_TYPE ");
		sb.append("AND TES.MACHINE_KBN = TK.MACHINE_KBN ");
		sb.append("AND TK.KENGEN_CD = "+kengenCd+" ");

		sb.append("INNER JOIN MST_EVENT_SEND MES ");
		sb.append("ON TES.EVENT_NO = MES.EVENT_CODE ");
		sb.append("AND TES.EVENT_SHUBETU = MES.SHUBETU_CODE ");
		sb.append("AND TES.CON_TYPE = MES.CON_TYPE ");
		sb.append("AND TES.MACHINE_KBN = MES.MACHINE_KBN ");
		sb.append("AND MES.DELETE_FLAG = 0 ");
		sb.append("AND MES.LANGUAGE_CD = 'en' ");
		sb.append("AND TES.EVENT_SHUBETU = 8 ");
		sb.append("AND TES.CON_TYPE IN ('C','C2') ");
		sb.append("AND TES.SYORI_FLG <> 9 ");
		sb.append("AND TES.FUKKYU_TIMESTAMP IS NULL ");

		sb.append("JOIN MST_MACHINE MM ");
		sb.append("ON TES.KIBAN_SERNO = MM.KIBAN_SERNO ");
		sb.append("WHERE MM.DELETE_FLAG = 0 ");
		sb.append("AND MM.KIBAN_SERNO = "+serialNumber+" ");

		sb.append("GROUP BY TES.KIBAN_SERNO, MES.ALERT_LV ");
		sb.append("");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> result =  em.createNativeQuery(new String(sb)).getResultList();

		if(result!=null && result.size()>0)
			return result;
		else
			return null;

	}
	// **↑↑↑　2019/01/31 LBN石川  ステップ2フェーズ3 機械アイコン対応追加 ↑↑↑**//

}
