package com.lbxco.RemoteCAREApp.ejbs.topInfo;

import java.util.List;

import com.lbxco.RemoteCAREApp.entities.TblDocuments;
import com.lbxco.RemoteCAREApp.entities.TblNews;

public interface ITopInfoService{


	/**
	 * JPA-JPQL
	 * findByNews: ニュース取得
	 */
	List<TblNews> findByNews(String languageCd, int appFlg, String dealerCd, String userId, String kengenCd);

	/**
	 * JPA-JPQL
	 * findByNews: ニュース取得
	 */
	List<TblNews> findByNews(String languageCd, int appFlg);

	/**
	 * JPA-JPQL
	 * findByDocuments: 資料一覧取得
	 */
	List<TblDocuments> findByDocuments(String languageCd, int appFlg);


	// **↓↓↓ 2019/01/31  LBN石川  ステップ2フェーズ3代理店向けお知らせ 追加  ↓↓↓**//
	/**
	 * JPA-JPQL
	 * findByDealerNews: 1 代理店向けニュース一覧取得
	 */
	List<TblNews> findByDealerNews(String languageCd, int appFlg, String dealerCd);
	// **↑↑↑ 2019/01/31  LBN石川  ステップ2フェーズ3代理店向けお知らせ 追加  ↑↑↑**//



}
