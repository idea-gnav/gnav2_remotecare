package com.lbxco.RemoteCAREApp.ejbs.topInfo;


import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.lbxco.RemoteCAREApp.entities.TblDocuments;
import com.lbxco.RemoteCAREApp.entities.TblNews;


@Stateless
@Local(ITopInfoService.class)
public class TopInfoService implements ITopInfoService{


	@PersistenceContext
    EntityManager em;


	/**
	 * JPA-JPQL
	 * findByNews: ニュース取得
	 */
	@Override
	public List<TblNews> findByNews(String languageCd, int appFlg, String dealerCd, String userId, String kengenCd) {

		String fmtDate = new SimpleDateFormat("yyyy-MM-dd")
				.format(new Timestamp(System.currentTimeMillis()));	// JST

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT t FROM TblNews t ");
		sb.append("WHERE t.delFlg = 0 ");
		sb.append("AND t.appFlg = :appFlg ");
		sb.append("AND t.languageCd = :languageCd ");

		if(dealerCd!=null) {
			sb.append("AND (( ");
			sb.append("t.targetDairitennCd = '"+dealerCd+"' ");
			sb.append("OR t.targetUserId LIKE '%"+userId+"%' ");
			sb.append("OR t.targetKengenCd LIKE '%"+kengenCd+"%' ");
			sb.append(") OR ( ");
			sb.append("t.targetDairitennCd IS NULL ");
			sb.append("AND t.targetUserId IS NULL ");
			sb.append("AND t.targetKengenCd IS NULL ");
			sb.append(")) ");
		}else {
			sb.append("AND t.targetDairitennCd IS NULL ");
		}

		sb.append("AND t.opDate <= {ts '" + fmtDate + " 23:59:59'} ");	// JST
		sb.append("AND (t.edDate >= {ts '" + fmtDate + " 00:00:00'} OR t.edDate IS NULL) ");	// JST

		sb.append("ORDER BY t.opDate desc, t.newsContents ");

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<TblNews> result = em.createQuery(new String(sb))
				.setParameter("appFlg", appFlg)
				.setParameter("languageCd", languageCd)
				.getResultList();


		if (result.size() > 0)
			return result;
		else
			return null;
	}



	/**
	 * JPA-JPQL
	 * findByNews: ニュース取得
	 */
	@Override
	public List<TblNews> findByNews(String languageCd, int appFlg) {

		String fmtDate = new SimpleDateFormat("yyyy-MM-dd")
				.format(new Timestamp(System.currentTimeMillis()));	// JST

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT t FROM TblNews t ");
		sb.append("WHERE t.delFlg = 0 ");
		sb.append("AND t.appFlg = :appFlg ");
		sb.append("AND t.languageCd = :languageCd ");
		// **↓↓↓ 2019/01/31  LBN石川  ステップ2フェーズ3代理店向けお知らせ 追加  ↓↓↓**//
		// 代理店向けニュースを除外
		sb.append("AND t.targetDairitennCd IS NULL ");
		// **↑↑↑ 2019/01/31  LBN石川  ステップ2フェーズ3代理店向けお知らせ 追加  ↑↑↑**//
		sb.append("AND t.opDate <= {ts '" + fmtDate + " 23:59:59'} ");	// JST
		sb.append("AND (t.edDate >= {ts '" + fmtDate + " 00:00:00'} OR t.edDate IS NULL) ");	// JST
		sb.append("ORDER BY t.opDate desc, t.newsContents ");

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<TblNews> result = em.createQuery(new String(sb))
				.setParameter("appFlg", appFlg)
				.setParameter("languageCd", languageCd)
				.getResultList();

		if (result.size() > 0)
			return result;
		else
			return null;
	}



	/**
	 * JPA-JPQL
	 * findByDocuments: 資料一覧取得
	 */
	@Override
	public List<TblDocuments> findByDocuments(String languageCd, int appFlg) {

		String fmtDate = new SimpleDateFormat("yyyy-MM-dd")
				.format(new Timestamp(System.currentTimeMillis()));

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT t FROM TblDocuments t ");
		sb.append("WHERE t.delFlg = :delFlg ");
		sb.append("AND t.appFlg = :appFlg ");
		sb.append("AND t.languageCd = :languageCd ");
		sb.append("AND t.opDate <= {ts '" + fmtDate + " 23:59:59'} ");	// JST
		sb.append("AND (t.edDate >= {ts '" + fmtDate + " 00:00:00'} OR t.edDate IS NULL) ");	// JST
		sb.append("ORDER BY t.opDate desc, t.docuName ");

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<TblDocuments> result = em.createQuery(new String(sb))
				.setParameter("delFlg", 0)
				.setParameter("appFlg", appFlg)
				.setParameter("languageCd", languageCd)
				.getResultList();

		if (result.size() > 0)
			return result;
		else
			return null;
	}




	/**
	 * JPA-JPQL
	 * findByDealerNews: 1 代理店向けニュース一覧取得
	 */
	@Override
	public List<TblNews> findByDealerNews(String languageCd, int appFlg, String dealerCd) {

		String fmtDate = new SimpleDateFormat("yyyy-MM-dd")
				.format(new Timestamp(System.currentTimeMillis()));	// JST

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT t FROM TblNews t ");
		sb.append("WHERE t.delFlg = 0 ");
		sb.append("AND t.appFlg = :appFlg ");
		sb.append("AND t.targetDairitennCd = :dealerCd ");
		sb.append("AND t.languageCd = :languageCd ");
		sb.append("AND t.opDate <= {ts '" + fmtDate + " 00:00:00'} ");	// JST
		sb.append("AND (t.edDate >= {ts '" + fmtDate + " 23:59:59'} OR t.edDate IS NULL) ");	// JST
		sb.append("ORDER BY t.opDate desc, t.newsContents ");


		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<TblNews> result = em.createQuery(new String(sb))
				.setParameter("appFlg", appFlg)
				.setParameter("dealerCd", dealerCd)
				.setParameter("languageCd", languageCd)
				.getResultList();


		if (result.size() > 0)
			return result;
		else
			return null;
	}



}
