package com.lbxco.RemoteCAREApp.ejbs.machineDitail;


import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Stateless
@Local(IMachineDitailService.class)
public class MachineDitailService implements IMachineDitailService {


	@PersistenceContext
    EntityManager em;



	/**
	 * JPA-JPQL
	 * findByCustomer: @BLgDξρζΎ
	 */
	@Override
	public List<Object[]> findByCustomer(Integer serialNumber) {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("mm.sosikiCd, ");					// 0
		sb.append("ms.kengenCd, ");					// 1
		sb.append("mk.kigyouName, ");				// 2
		sb.append("ms.sosikiName, ");				// 3
		sb.append("ma.areaName, ");					// 4 PSM
		sb.append("mrsm.resionSalesManagerName, ");	// 5 resionSalesManager
		sb.append("mm.dairitennCd, ");				// 6
		// **«««@2018/04/11  LBNΞμ  v]sοΗδ  No34Ξ ΗΑ «««**//
		sb.append("ma.areaCd ");					// 7
		// **ͺͺͺ@2018/04/11  LBNΞμ  v]sοΗδ  No34Ξ ΗΑ ͺͺͺ**//
		sb.append("FROM MstMachine mm ");
		sb.append("LEFT JOIN mm.mstSosiki ms ");
		sb.append("LEFT JOIN ms.mstKigyou mk ");
		sb.append("LEFT JOIN ms.mstArea ma ");
		sb.append("LEFT JOIN ms.mstResionSalesManager mrsm ");
		sb.append("WHERE mm.kibanSerno = :kibanSerno ");

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<Object[]> result = em.createQuery(new String(sb))
				.setParameter("kibanSerno", serialNumber)
				.getResultList();

		if (result.size() > 0)
			return result;
		else
			return null;
	}



	/**
	 * JPA-JPQL
	 * findByDealer: gDγXξρζΎ
	 */
	@Override
	public List<Object[]> findByDealer(String dairitenCd) {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("mk.kigyouName, ");				// 0
		sb.append("ms.sosikiName, ");				// 1
		sb.append("ma.areaName, ");					// 2
		sb.append("mrsm.resionSalesManagerName, ");	// 3
		// **«««@2018/04/11  LBNΞμ  v]sοΗδ  No34Ξ ΗΑ «««**//
		sb.append("ma.areaCd, ");					// 4
		// **ͺͺͺ@2018/04/11  LBNΞμ  v]sοΗδ  No34Ξ ΗΑ ͺͺͺ**//
		// **«««@2018/11/28  iDEARΊ  Xebv2@γXdbΤΗΑ «««**//
		sb.append("ms.sosikiTel1 ");				// 5
		// **ͺͺͺ@2018/11/28  iDEARΊ  Xebv2@γXdbΤΗΑ ͺͺͺ**//
		sb.append("FROM MstSosiki ms ");
		sb.append("LEFT JOIN ms.mstKigyou mk ");
		sb.append("LEFT JOIN ms.mstArea ma ");
		sb.append("LEFT JOIN ms.mstResionSalesManager mrsm ");
		sb.append("WHERE ms.sosikiCd = :param ");


		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<Object[]> result = em.createQuery(new String(sb))
				.setParameter("param", dairitenCd)
				.getResultList();


		if (result.size() > 0)
			return result;
		else
			return null;

	}




	/**
	 * JPA-JPQL
	 * findByPsmTel: PSMdbΤζΎ
	 */
	@Override
	public String findByPsmTel(String areaCd, String userName){

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT m.userTel FROM MstUser m ");
		sb.append("WHERE m.delFlg = 0 ");
		sb.append("AND m.areaCd = :areaCd ");
		sb.append("AND m.userName = :userName ");
		sb.append("AND m.userTel IS NOT NULL ");
		sb.append("ORDER BY m.newPasswordDate DESC ");

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<String> result = em.createQuery(new String(sb))
				.setParameter("areaCd", areaCd)
				.setParameter("userName", userName)
				.getResultList();

		if (result.size() > 0)
			return (String)result.get(0);
		else
			return null;
	}





	/**
	 * JPA-JPQL
	 * findByCtrlABuhinNoF Rg[[AiΌζΎ
	 */
	@Override
	public String findByCtrlABuhinNo(Integer machineModel, Integer BuhinNo, String conType) {

			em.getEntityManagerFactory().getCache().evictAll();
			@SuppressWarnings("unchecked")
			List<String> result = em.createQuery("SELECT t.name FROM MstTypeA t "
					+ "WHERE t.deleteFlag = 0 "
					+ "AND t.machineKbn = :machineModel "
					+ "AND t.code = :buhinNo "
					+ "AND t.conType = :conType ")
					.setParameter("machineModel", machineModel)
					.setParameter("buhinNo", BuhinNo)
					.setParameter("conType", conType)
					.getResultList();

			if (result.size() > 0)
				return (String)result.get(0);
			else
				return null;
	}



	/**
	 * JPA-JPQL
	 * findByCtrlBBuhinNoF Rg[[BiΌζΎ
	 */
	@Override
	public String findByCtrlBBuhinNo(Integer machineModel, Integer BuhinNo, String conType) {

			em.getEntityManagerFactory().getCache().evictAll();
			@SuppressWarnings("unchecked")
			List<String> result = em.createQuery("SELECT t.name FROM MstTypeB t "
					+ "WHERE t.deleteFlag = 0 "
					+ "AND t.machineKbn = :machineModel "
					+ "AND t.code = :buhinNo "
					+ "AND t.conType = :conType ")
					.setParameter("machineModel", machineModel)
					.setParameter("buhinNo", BuhinNo)
					.setParameter("conType", conType)
					.getResultList();

			if (result.size() > 0)
				return (String)result.get(0);
			else
				return null;
	}


	/**
	 * JPA-JPQL
	 * findByMainConPartsF Rg[[VAiΌζΎ
	 */
	@Override
	public String findByMainConParts(Integer mainConParts, String conType) {

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<String> result = em.createQuery("SELECT t.name FROM MstTypeMainCon t "
				+ "WHERE t.deleteFlag = 0 "
				+ "AND t.conType = :conType "
				+ "AND t.code = :buhinNo ")
				.setParameter("conType", conType)
				.setParameter("buhinNo", mainConParts)
				.getResultList();

		if (result.size() > 0)
			return (String)result.get(0);
		else
			return null;
	}


	/**
	 * JPA-JPQL
	 * findByTypeQ4000F Q4000iΌζΎ
	 */
	@Override
	public String findByq4000(Integer q4000BuhinNo, String conType) {

			em.getEntityManagerFactory().getCache().evictAll();
			@SuppressWarnings("unchecked")
			List<String> result = em.createQuery("SELECT t.name FROM MstTypeQ4000 t "
					+ "WHERE t.deleteFlag = 0 "
					+ "AND t.conType = :conType "
					+ "AND t.code = :buhinNo ")
					.setParameter("conType", conType)
					.setParameter("buhinNo", q4000BuhinNo)
					.getResultList();

			if (result.size() > 0)
				return (String)result.get(0);
			else
				return null;
	}



	 /** JPA-JPQL
	 * findByFavoriteFlg: ¨CΙόθ@Bυ
	 */
	@Override
	public Integer findByFavoriteDelFlg(Integer serialNumber, String userId) {

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<Integer> result = em.createQuery("SELECT t.delFlg FROM TblUserFavorite t WHERE t.kibanSerno = :kibanSerno AND t.userId = :userId ")
				.setParameter("kibanSerno", serialNumber)
				.setParameter("userId", userId)
				.getResultList();

		if (result.size() > 0)
			return (Integer)result.get(0);
		else
			return null;
	}




}
