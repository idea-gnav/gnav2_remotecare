package com.lbxco.RemoteCAREApp.ejbs.machineDitail;


import java.util.List;



public interface IMachineDitailService{


	/**
	 * JPA-JPQL
	 * findByCustomer: 機械所有組織情報取得
	 */
	List<Object[]> findByCustomer(Integer serialNumber);

	/**
	 * JPA-JPQL
	 * findByDealer: 組織代理店情報取得
	 */
	List<Object[]> findByDealer(String dairitenCd);


	// **↓↓↓　2018/04/11  LBN石川  要望不具合管理台帳 No34対応 追加 ↓↓↓**//
	/**
	 * JPA-JPQL
	 * findByPsmTel: PSM電話番号取得
	 */
	String findByPsmTel(String areaCd, String userName);
	// **↑↑↑　2018/04/11  LBN石川  要望不具合管理台帳 No34対応 追加 ↑↑↑**//


	/**
	 * JPA-JPQL
	 * findByCtrlBBuhinNo： コントローラーA部品名取得
	 */
	String findByCtrlABuhinNo(Integer machineModel, Integer BuhinNo, String conType);

	/**
	 * JPA-JPQL
	 * findByCtrlBBuhinNo： コントローラーB部品名取得
	 */
	String findByCtrlBBuhinNo(Integer machineModel, Integer BuhinNo, String conType);

	/**
	 * JPA-JPQL
	 * findByMainConParts： コントローラーシリアル部品名取得
	 */
	String findByMainConParts(Integer mainConParts, String conType);

	/**
	 * JPA-JPQL
	 * findByTypeQ4000： Q4000部品名取得
	 */
	String findByq4000(Integer q4000BuhinNo, String conType);

	 /** JPA-JPQL
	 * findByFavoriteFlg: お気に入り機械検索
	 */
	Integer findByFavoriteDelFlg(Integer serialNumber, String userId);



}

