package com.lbxco.RemoteCAREApp.ejbs.lbxModelList;

import java.util.List;

public interface ILbxModelListService {
	
	/**
	 * NativeQuery
	 * findByLbxModel : LBXモデル一覧取得
	 */
	List<String> findByLbxModel();

}
