package com.lbxco.RemoteCAREApp.ejbs.lbxModelList;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Stateless
@Local(ILbxModelListService.class)
public class LbxModelListService implements ILbxModelListService{

	@PersistenceContext
    EntityManager em;

	/**
	 * NativeQuery
	 * findByLbxModel : LBXモデル一覧取得
	 */
	@Override
	public List<String> findByLbxModel(){

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT DISTINCT LBX_MODEL_CD FROM MST_MACHINE ");
		sb.append("WHERE DELETE_FLAG = 0 ");
		sb.append("AND NLS_UPPER(KIBAN) LIKE 'LBX%'　");
		sb.append("AND LBX_MODEL_CD IS NOT NULL ");
		sb.append("ORDER BY LBX_MODEL_CD ");

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<String> result =  em.createNativeQuery(new String(sb)).getResultList();


		if(result.size() > 0)
			return result;
		else
			return null;
	}
}
