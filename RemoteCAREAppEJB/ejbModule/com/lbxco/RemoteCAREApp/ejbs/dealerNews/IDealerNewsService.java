package com.lbxco.RemoteCAREApp.ejbs.dealerNews;

import java.util.List;

import com.lbxco.RemoteCAREApp.entities.TblNews;

public interface IDealerNewsService{


	/**
	 * JPA-JPQL
	 * findByDealerLogo: 代理店ロゴ
	 */
	String findByDealerLogo(String dealerCd);


	/**
	 * JPA-JPQL
	 * findByDealers: ニュース登録している代理店一覧取得
	 */
	List<String> findByDealers(Integer appFlg);


	/**
	 * JPA-JPQL
	 * findByDealerInfoList: 代理店情報一覧
	 */
	List<Object[]> findByDealerInfoList(List<String> dealerCds);


	/**
	 * JPA-JPQL
	 * findByDealerNews: 代理店向けニュース一覧取得
	 */
	List<TblNews> findByDealerNewsList(String languageCd, int appFlg, List<String> dealerCds, Integer mode);


	/**
	 * JPA-JPQL
	 * 代理店向けニュースの新規登録・更新
	 */
	void updateToTblNews(
					Integer appFlg,
					String languageCd,
					Integer newsId,
					String newsDateFrom,
					String newsDateTo,
					String  newsContents,
					String  newsUrl,
					Integer newsDelFlg,
					String dealerCd,
					String userId
				);


	/**
	 * JPA-JPQL
	 * findByDealerCd: 代理店コード存在チェック
	 */
	String findByDealerCd(String dealerCd);


	/**
	 * JPA-JPQL
	 * 代理店ロゴ管理テーブルの新規登録・更新
	 */
	void updateToMstAppDealerLogo(
			String dealerCd,
			Integer kengenCd,
			String imagePath,
			String mstAppDealerLogoDealerCd,
			String userId
		);


	/**
	 * JPA-JPQL
	 * 代理店ロゴ削除
	 */
	void deleteToTblNews(String dealerCd);




}
