package com.lbxco.RemoteCAREApp.ejbs.dealerNews;


import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.lbxco.RemoteCAREApp.ConvertUtil;
import com.lbxco.RemoteCAREApp.entities.MstAppDealerLogo;
import com.lbxco.RemoteCAREApp.entities.TblNews;


@Stateless
@Local(IDealerNewsService.class)
public class DealerNewsService implements IDealerNewsService{


	@PersistenceContext
    EntityManager em;



	/**
	 * JPA-JPQL
	 * findByDealerLogo: 代理店ロゴ
	 */
	@Override
	public String findByDealerLogo(String dealerCd) {


		StringBuilder sb = new StringBuilder();

		sb.append("SELECT m.logoImagePath FROM MstAppDealerLogo m ");
		sb.append("WHERE m.dairitennCd = '" + dealerCd + "' ");

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<String> result = em.createQuery(new String(sb))
				.getResultList();

		if (result.size() > 0)
			return (String)result.get(0);
		else
			return null;

//		return "Images/LbxDealerLogo/defult.jpg";
	}


	/**
	 * JPA-JPQL
	 * findByDealers: ニュース登録している代理店一覧取得
	 */
	@Override
	public List<String> findByDealers(Integer appFlg){

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT DISTINCT t.targetDairitennCd FROM TblNews t ");
		sb.append("WHERE t.delFlg = 0  ");
		sb.append("AND t.appFlg = " + appFlg + " ");
		sb.append("AND t.targetDairitennCd IS NOT NULL ");

		em.getEntityManagerFactory().getCache().evictAll();

		@SuppressWarnings("unchecked")
		List<String> result = em.createQuery(new String(sb))
				.getResultList();

		if (result.size() > 0)
			return result;
		else
			return null;

	}



	/**
	 * JPA-JPQL
	 * findByTargetDealers: 代理店情報一覧
	 */
	@Override
	public List<Object[]> findByDealerInfoList(List<String> dealerCds){

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT DISTINCT ");
		sb.append("MS.SOSIKI_CD AS DEALER_CD, ");
		sb.append("MK.KIGYOU_NAME || NVL2(MS.SOSIKI_NAME,' ','') || MS.SOSIKI_NAME AS DEALER_NAME, ");
		sb.append("MADL.LOGO_IMAGE_PATH AS DEALER_URL ");
		sb.append("FROM MST_SOSIKI MS ");
		sb.append("LEFT OUTER JOIN MST_APP_DEALER_LOGO MADL ");
		sb.append("ON MS.SOSIKI_CD = MADL.DAIRITENN_CD ");
		sb.append("JOIN MST_KIGYOU MK ");
		sb.append("ON MS.KIGYOU_CD = MK.KIGYOU_CD ");
		sb.append("WHERE MS.DELETE_FLG = 0 ");
		sb.append("AND MS.SOSIKI_CD IN (" + ConvertUtil.queryInStr(dealerCds) + ") ");
		sb.append("ORDER BY MS.SOSIKI_CD ");
		sb.append("");

		em.getEntityManagerFactory().getCache().evictAll();
		List<Object[]> result =  em.createNativeQuery(new String(sb)).getResultList();

		if(result.size() > 0)
			return result;
		else
			return null;

	}


	/**
	 * JPA-JPQL
	 * findByDealerNews: 代理店向けニュース一覧取得
	 *
	 */
	@Override
	public List<TblNews> findByDealerNewsList(String languageCd, int appFlg, List<String> dealerCds, Integer mode){


		String fmtDate = new SimpleDateFormat("yyyy-MM-dd")
				.format(new Timestamp(System.currentTimeMillis()));	// JST


		StringBuilder sb = new StringBuilder();

		sb.append("SELECT t FROM TblNews t ");
		sb.append("WHERE t.delFlg = 0  ");
		sb.append("AND t.appFlg = " + appFlg + " ");
		sb.append("AND t.languageCd = '" + languageCd + "' ");
		sb.append("AND t.targetDairitennCd IN (" + ConvertUtil.queryInStr(dealerCds) + ") ");
		// mode
		//   1:LBX権限 代理店ニュース画面 公開中のみ
		//   0:代理店権限 代理店ニュース編集画面
		if(mode == 1) {
			sb.append("AND t.opDate <= {ts '" + fmtDate + " 23:59:59'} ");	// JST
			sb.append("AND (t.edDate >= {ts '" + fmtDate + " 00:00:00'} OR t.edDate IS NULL) ");	// JST
		}
		sb.append("ORDER BY t.opDate DESC, t.newsContents ");

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<TblNews> result = em.createQuery(new String(sb)).getResultList();

		if (result.size() > 0)
			return result;
		else
			return null;

	}



	/**
	 * JPA-JPQL
	 * 代理店向けニュースの新規登録・更新
	 */
	@Override
	public void updateToTblNews(
					Integer appFlg,
					String languageCd,
					Integer newsId,
					String newsDateFrom,
					String newsDateTo,
					String  newsContents,
					String  newsUrl,
					Integer newsDelFlg,
					String dealerCd,
					String userId
				) {

        // String -> Date
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date dateFrom = null;
        Date dateTo = null;
		try {
			if(newsDateFrom!=null)
				dateFrom = sdf.parse(newsDateFrom);
			if(newsDateTo!=null)
				dateTo = sdf.parse(newsDateTo);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		if(newsId==null) {

			// newsId 最大値を取得
			em.getEntityManagerFactory().getCache().evictAll();
			List<Integer> maxNewsId = em.createQuery("SELECT t.newsId FROM TblNews t ORDER BY t.newsId DESC").getResultList();
			em.clear();

			TblNews tblNews = new TblNews();
			tblNews.setNewsId(maxNewsId.get(0)+1);
			tblNews.setAppFlg(appFlg);
			tblNews.setTargetUserId(null);
			tblNews.setTargetKengenCd(null);
			tblNews.setLanguageCd(languageCd);
			tblNews.setNewsContents(newsContents);
			tblNews.setOpDate(dateFrom);
			tblNews.setEdDate(dateTo);
			tblNews.setRemarks(null);
			tblNews.setTargetDairitennCd(dealerCd);
			tblNews.setNewsUrl(newsUrl);
			tblNews.setDelFlg(0);
			tblNews.setRegistUser(userId);
			tblNews.setRegistDtm(new Timestamp(System.currentTimeMillis()));
			tblNews.setRegistPrg("RemoteCAREApp");
			tblNews.setUpdateUser(userId);
			tblNews.setUpdateDtm(new Timestamp(System.currentTimeMillis()));
			tblNews.setUpdatePrg("RemoteCAREApp");
			em.persist(tblNews);

		}else if(newsId!=null){

			TblNews tblNews = em.find(TblNews.class, newsId);

			tblNews.setNewsContents(newsContents);
			if(dateFrom!=null)
				tblNews.setOpDate(dateFrom);
			tblNews.setEdDate(dateTo);
			if(dealerCd!=null)
				tblNews.setTargetDairitennCd(dealerCd);
			tblNews.setNewsUrl(newsUrl);
			if(newsDelFlg!=null)
				tblNews.setDelFlg(newsDelFlg);
			tblNews.setUpdateUser(userId);
			tblNews.setUpdateDtm(new Timestamp(System.currentTimeMillis()));
			tblNews.setUpdatePrg("RemoteCAREApp");
			em.persist(tblNews);

		}

		em.flush();
		em.clear();

		return;
	}



	/**
	 * JPA-JPQL
	 * findByDealerCd: 代理店コード存在チェック
	 */
	@Override
	public String findByDealerCd(String dealerCd) {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT m.dairitennCd FROM MstAppDealerLogo m ");
		sb.append("WHERE m.dairitennCd = '" + dealerCd + "' ");

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<String> result = em.createQuery(new String(sb))
				.getResultList();

		if (result.size() > 0)
			return (String)result.get(0);
		else
			return null;

	}




	/**
	 * JPA-JPQL
	 * 代理店ロゴ管理テーブルの新規登録・更新
	 */
	@Override
	public void updateToMstAppDealerLogo(
			String dealerCd,
			Integer kengenCd,
			String imagePath,
			String mstAppDealerLogoDealerCd,
			String userId
		) {

		if(mstAppDealerLogoDealerCd==null) {

			MstAppDealerLogo mstAppDealerLogo = new MstAppDealerLogo();

			mstAppDealerLogo.setDairitennCd(dealerCd);
			mstAppDealerLogo.setKengenCd(kengenCd);
			mstAppDealerLogo.setLogoImagePath(imagePath);
			mstAppDealerLogo.setRegistUser(userId);
			mstAppDealerLogo.setRegistDtm(new Timestamp(System.currentTimeMillis()));
			mstAppDealerLogo.setRegistPrg("RemoteCAREApp");
			mstAppDealerLogo.setUpdateUser(userId);
			mstAppDealerLogo.setUpdateDtm(new Timestamp(System.currentTimeMillis()));
			mstAppDealerLogo.setUpdatePrg("RemoteCAREApp");
			em.persist(mstAppDealerLogo);

		}else if(mstAppDealerLogoDealerCd!=null){

			MstAppDealerLogo mstAppDealerLogo = em.find(MstAppDealerLogo.class, dealerCd);
			mstAppDealerLogo.setLogoImagePath(imagePath);
			mstAppDealerLogo.setUpdateUser(userId);
			mstAppDealerLogo.setUpdateDtm(new Timestamp(System.currentTimeMillis()));
			mstAppDealerLogo.setUpdateUser("RemoteCAREApp");
			em.persist(mstAppDealerLogo);

		}

		em.flush();
		em.clear();
		return;
	}




	/**
	 * JPA-JPQL
	 * 代理店ロゴ削除
	 */
	@Override
	public void deleteToTblNews(String dealerCd) {

		MstAppDealerLogo mstAppDealerLogo = em.find(MstAppDealerLogo.class, dealerCd);
		em.remove(mstAppDealerLogo);

		em.flush();
		em.clear();

		return;
	}


}
