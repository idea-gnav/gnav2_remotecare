package com.lbxco.RemoteCAREApp.ejbs.authentication;

import com.lbxco.RemoteCAREApp.entities.AuthenticationToken;

public interface IAuthentication {
	public AuthenticationToken generateToken(String DeviceId, String loginId);
	
	public boolean verifyToken(String token);
}
