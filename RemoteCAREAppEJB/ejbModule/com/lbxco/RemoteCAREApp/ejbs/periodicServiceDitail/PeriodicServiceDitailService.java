package com.lbxco.RemoteCAREApp.ejbs.periodicServiceDitail;


import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Stateless
@Local(IPeriodicServiceDitailService.class)
public class PeriodicServiceDitailService implements IPeriodicServiceDitailService{

	@PersistenceContext
    EntityManager em;

	/**
	 * NativeQuery
	 * findByServiceRequestDitail
	 */
	@Override
	public List<Object[]> findByServiceRequestDitailNativeQuery(
			Integer serialNumberibanNo,
			String conType,
			Integer machineModel,
			Integer buhinNo,
			String languageCd){

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");

		// [0] KIBAN_SERNO
		sb.append("MM.KIBAN_SERNO , ");

		// [1] replacementPartName
		sb.append("TMP.MAINTAINECE_PART_NAME , ");

		// [2] REPLACEMENT_INTERVAL
		sb.append("CASE ");
		sb.append("WHEN " + buhinNo + " = 0 THEN ROUND(NVL(TMT.NENRYOU_FILTER_PRE_TIME,0)) ");
		sb.append("WHEN " + buhinNo + " = 1 THEN ROUND(NVL(TMT.NENRYOU_FILTER_MAIN_TIME,0)) ");
		sb.append("WHEN " + buhinNo + " = 2 THEN ROUND(NVL(TMT.ENGINE_OILTIME,0)) ");
		sb.append("WHEN " + buhinNo + " = 3 THEN ROUND(NVL(TMT.ENGINE_OIL_FILTER_TIME,0)) ");
		sb.append("WHEN " + buhinNo + " = 4 THEN ROUND(NVL(TMT.AIR_CLEANER_ELEMENT_TIME,0)) ");
		sb.append("WHEN " + buhinNo + " = 5 THEN ROUND(NVL(TMT.AIR_BREATHER_ELEMENT_TIME,0)) ");
		sb.append("WHEN " + buhinNo + " = 6 THEN ROUND(NVL(TMT.PILOT_OIL_FILTER_TIME,0)) ");
		sb.append("WHEN " + buhinNo + " = 7 THEN ROUND(NVL(TMT.RETURN_FILTER_TIME,0)) ");
		sb.append("WHEN " + buhinNo + " = 8 THEN ROUND(NVL(TMT.SUCTION_TIME,0)) ");
		sb.append("WHEN " + buhinNo + " = 9 THEN ROUND(NVL(TMT.MOVEMENT_OIL_TIME,0)) ");
		sb.append("WHEN " + buhinNo + " = 10 THEN ROUND(NVL(TMT.TURNING_SPDDOWN_GEAROIL_TIME,0)) ");
		sb.append("WHEN " + buhinNo + " = 11 THEN ROUND(NVL(TMT.TRAVEL_SPDDOWN_GEAROIL_TIME,0)) ");
		sb.append("WHEN " + buhinNo + " = 12 THEN ROUND(NVL(TMT.REPLACE_AIL_CON_FILTER_TIME,0)) ");
		sb.append("WHEN " + buhinNo + " = 13 THEN ROUND(NVL(TMT.SEIBI_JIKI_TIME,0)) ");
		sb.append("ELSE null END AS \"REPLACEMENT_INTERVAL\", ");

		// [3] NEXT_REPLACEMENT_TIME
		sb.append("CASE ");
		sb.append("WHEN " + buhinNo + " = 0 THEN ");
		sb.append("NVL( ROUND((TMT.NENRYOU_FILTER_PRE_TIME -( NVL(MM.NEW_HOUR_METER/60 ,0) - TMRT.NENRYOU_FILTER_PRER_TIME - NVL(TMRT.NENRYOU_FILTER_PRE_OFFSET,0)))) ");
		sb.append(",ROUND((TMT.NENRYOU_FILTER_PRE_TIME -( NVL(MM.NEW_HOUR_METER/60 ,0) ))) )  " );
		sb.append("WHEN " + buhinNo + " = 1 THEN ");
		sb.append("NVL( ROUND((TMT.NENRYOU_FILTER_MAIN_TIME -( NVL(MM.NEW_HOUR_METER/60 ,0) - TMRT.NENRYOU_FILTER_MAINR_TIME - NVL(TMRT.NENRYOU_FILTER_MAIN_OFFSET,0)))) ");
		sb.append(",ROUND((TMT.NENRYOU_FILTER_MAIN_TIME -( NVL(MM.NEW_HOUR_METER/60 ,0) ))) )  " );
		sb.append("WHEN " + buhinNo + " = 2 THEN ");
		sb.append("NVL( ROUND((TMT.ENGINE_OILTIME -( NVL(MM.NEW_HOUR_METER/60 ,0)  - TMRT.ENGINE_OILTIMER_TIME - NVL(TMRT.ENGINE_OIL_OFFSET,0)))) ");
		sb.append(",ROUND((TMT.ENGINE_OILTIME -( NVL(MM.NEW_HOUR_METER/60 ,0) ))) )  " );
		sb.append("WHEN " + buhinNo + " = 3 THEN ");
		sb.append("NVL( ROUND((TMT.ENGINE_OIL_FILTER_TIME -(  NVL(MM.NEW_HOUR_METER/60 ,0)  - TMRT.ENGINE_OIL_FILTERR_TIME - NVL(TMRT.ENGINE_OIL_FILTER_OFFSET,0)))) ");
		sb.append(",ROUND((TMT.ENGINE_OIL_FILTER_TIME -(  NVL(MM.NEW_HOUR_METER/60 ,0) ))))  " );
		sb.append("WHEN " + buhinNo + " = 4 THEN ");
		sb.append("NVL( ROUND((TMT.AIR_CLEANER_ELEMENT_TIME -( NVL(MM.NEW_HOUR_METER/60 ,0)  - TMRT.AIR_CLEANER_ELEMENTR_TIME - NVL(TMRT.AIR_CLEANER_ELEMENT_OFFSET,0)))) ");
		sb.append(",ROUND((TMT.AIR_CLEANER_ELEMENT_TIME -( NVL(MM.NEW_HOUR_METER/60 ,0) ))))  " );
		sb.append("WHEN " + buhinNo + " = 5 THEN ");
		sb.append("NVL( ROUND((TMT.AIR_BREATHER_ELEMENT_TIME -( NVL(MM.NEW_HOUR_METER/60 ,0)  - TMRT.AIR_BREATHER_ELEMENTR_TIME - NVL(TMRT.AIR_BREATHER_ELEMENT_OFFSET,0)))) ");
		sb.append(",ROUND((TMT.AIR_BREATHER_ELEMENT_TIME -( NVL(MM.NEW_HOUR_METER/60 ,0) ))))  " );
		sb.append("WHEN " + buhinNo + " = 6 THEN ");
		sb.append("NVL( ROUND((TMT.PILOT_OIL_FILTER_TIME -( NVL(MM.NEW_HOUR_METER/60 ,0)  - TMRT.PILOT_OIL_FILTERR_TIME - NVL(TMRT.PILOT_OIL_FILTER_OFFSET,0)))) ");
		sb.append(",ROUND((TMT.PILOT_OIL_FILTER_TIME -( NVL(MM.NEW_HOUR_METER/60 ,0) ))))  " );
		sb.append("WHEN " + buhinNo + " = 7 THEN ");
		sb.append("NVL( ROUND((TMT.RETURN_FILTER_TIME -( NVL(MM.NEW_HOUR_METER/60 ,0)  - TMRT.RETURN_FILTER_TIME - NVL(TMRT.RETURN_FILTER_OFFSET,0)))) ");
		sb.append(",ROUND((TMT.RETURN_FILTER_TIME -( NVL(MM.NEW_HOUR_METER/60 ,0) ))) ) " );
		sb.append("WHEN " + buhinNo + " = 8 THEN ");
		sb.append("NVL( ROUND((TMT.SUCTION_TIME -( NVL(MM.NEW_HOUR_METER/60 ,0)  - TMRT.SUCTIONR_TIME - NVL(TMRT.SUCTION_OFFSET,0)))) ");
		sb.append(",ROUND((TMT.SUCTION_TIME -( NVL(MM.NEW_HOUR_METER/60 ,0) ))))  " );
		sb.append("WHEN " + buhinNo + " = 9 THEN ");
		sb.append("NVL( ROUND((TMT.MOVEMENT_OIL_TIME -( NVL(MM.NEW_HOUR_METER/60 ,0)  - TMRT.MOVEMENT_OILR_TIME - NVL(TMRT.MOVEMENT_OIL_OFFSET,0)))) ");
		sb.append(",ROUND((TMT.MOVEMENT_OIL_TIME -( NVL(MM.NEW_HOUR_METER/60 ,0) ))))  " );
		sb.append("WHEN " + buhinNo + " = 10 THEN ");
		sb.append("NVL( ROUND((TMT.TURNING_SPDDOWN_GEAROIL_TIME -( NVL(MM.NEW_HOUR_METER/60 ,0)  - TMRT.TURNING_SPDDOWN_GEAROILR_TIME - NVL(TMRT.TURNING_SPDDOWN_GEAROIL_OFFSET,0)))) ");
		sb.append(",ROUND((TMT.TURNING_SPDDOWN_GEAROIL_TIME -( NVL(MM.NEW_HOUR_METER/60 ,0) ))) )  " );
		sb.append("WHEN " + buhinNo + " = 11 THEN ");
		sb.append("NVL( ROUND((TMT.TRAVEL_SPDDOWN_GEAROIL_TIME -( NVL(MM.NEW_HOUR_METER/60 ,0)  - TMRT.TRAVEL_SPDDOWN_GEAROILR_TIME - NVL(TMRT.TRAVEL_SPDDOWN_GEAROIL_OFFSET,0)))) ");
		sb.append(",ROUND((TMT.TRAVEL_SPDDOWN_GEAROIL_TIME -( NVL(MM.NEW_HOUR_METER/60 ,0) )))) " );
		sb.append("WHEN " + buhinNo + " = 12 THEN ");
		sb.append("NVL( ROUND((TMT.REPLACE_AIL_CON_FILTER_TIME -( NVL(MM.NEW_HOUR_METER/60 ,0) - TMRT.REPLACE_AIL_CON_FILTERR_TIME - NVL(TMRT.REPLACE_AIL_CON_FILTER_OFFSET,0)))) ");
		sb.append(",ROUND((TMT.REPLACE_AIL_CON_FILTER_TIME -( NVL(MM.NEW_HOUR_METER/60 ,0) ))))  " );
		sb.append("WHEN " + buhinNo + " = 13 THEN  ");
		sb.append("NVL( ROUND((TMT.SEIBI_JIKI_TIME -( NVL(MM.NEW_HOUR_METER/60 ,0)  - TMRT.SEIBI_JIKI_R_TIME - NVL(TMRT.SEIBI_JIKI_OFFSET,0)))) ");
		sb.append(",ROUND((TMT.SEIBI_JIKI_TIME -( NVL(MM.NEW_HOUR_METER/60 ,0) )))  )  " );
		sb.append("ELSE null END AS \"NEXT_REPLACEMENT_TIME\", ");

		// [4] REPLACEMENT_SCHEDULE
		sb.append("CASE ");
		sb.append("WHEN " + buhinNo + " = 0 THEN NVL( TMRT.NENRYOU_FILTER_PRER_TIME + TMT.NENRYOU_FILTER_PRE_TIME ,TMT.NENRYOU_FILTER_PRE_TIME ) ");
		sb.append("WHEN " + buhinNo + " = 1 THEN NVL( TMRT.NENRYOU_FILTER_MAINR_TIME + TMT.NENRYOU_FILTER_MAIN_TIME, TMT.NENRYOU_FILTER_MAIN_TIME ) ");
		sb.append("WHEN " + buhinNo + " = 2 THEN NVL( TMRT.ENGINE_OILTIMER_TIME + TMT.ENGINE_OILTIME , TMT.ENGINE_OILTIME ) ");
		sb.append("WHEN " + buhinNo + " = 3 THEN NVL( TMRT.ENGINE_OIL_FILTERR_TIME + TMT.ENGINE_OIL_FILTER_TIME, TMT.ENGINE_OIL_FILTER_TIME ) ");
		sb.append("WHEN " + buhinNo + " = 4 THEN NVL( TMRT.AIR_CLEANER_ELEMENTR_TIME + TMT.AIR_CLEANER_ELEMENT_TIME, TMT.AIR_CLEANER_ELEMENT_TIME ) ");
		sb.append("WHEN " + buhinNo + " = 5 THEN NVL( TMRT.AIR_BREATHER_ELEMENTR_TIME + TMT.AIR_BREATHER_ELEMENT_TIME, TMT.AIR_BREATHER_ELEMENT_TIME ) ");
		sb.append("WHEN " + buhinNo + " = 6 THEN NVL( TMRT.PILOT_OIL_FILTERR_TIME + TMT.PILOT_OIL_FILTER_TIME, TMT.PILOT_OIL_FILTER_TIME) ");
		sb.append("WHEN " + buhinNo + " = 7 THEN NVL( TMRT.RETURN_FILTER_TIME + TMT.RETURN_FILTER_TIME, TMT.RETURN_FILTER_TIME ) ");
		sb.append("WHEN " + buhinNo + " = 8 THEN NVL( TMRT.SUCTIONR_TIME + TMT.SUCTION_TIME, TMT.SUCTION_TIME ) ");
		sb.append("WHEN " + buhinNo + " = 9 THEN NVL( TMRT.MOVEMENT_OILR_TIME + TMT.MOVEMENT_OIL_TIME, TMT.MOVEMENT_OIL_TIME ) ");
		sb.append("WHEN " + buhinNo + " = 10 THEN NVL( TMRT.TURNING_SPDDOWN_GEAROILR_TIME + TMT.TURNING_SPDDOWN_GEAROIL_TIME, TMT.TURNING_SPDDOWN_GEAROIL_TIME ) ");
		sb.append("WHEN " + buhinNo + " = 11 THEN NVL( TMRT.TRAVEL_SPDDOWN_GEAROILR_TIME + TMT.TRAVEL_SPDDOWN_GEAROIL_TIME, TMT.TRAVEL_SPDDOWN_GEAROIL_TIME ) ");
		sb.append("WHEN " + buhinNo + " = 12 THEN NVL( TMRT.REPLACE_AIL_CON_FILTERR_TIME + TMT.REPLACE_AIL_CON_FILTER_TIME, TMT.REPLACE_AIL_CON_FILTER_TIME ) ");
		sb.append("WHEN " + buhinNo + " = 13 THEN NVL( TMRT.SEIBI_JIKI_R_TIME + TMT.SEIBI_JIKI_TIME, TMT.SEIBI_JIKI_TIME ) ");
		sb.append("ELSE null END AS \"REPLACEMENT_SCHEDULE\" ");

		sb.append("FROM MST_MACHINE MM ");
		sb.append("LEFT JOIN TBL_MAINTAINECE_TIME TMT ON ( MM.KIBAN_SERNO = TMT.KIBAN_SERNO AND TMT.DELETE_FLAG = 0 )  ");
		sb.append("LEFT JOIN TBL_MAINTAINECE_RESET_TIME TMRT ON ( TMT.KIBAN_SERNO = TMRT.KIBAN_SERNO AND TMRT.DELETE_FLAG = 0 ) , ");
		sb.append("MST_MAINTAINECE_PART TMP ");
		sb.append("WHERE MM.CON_TYPE = TMP.CON_TYPE ");
		sb.append("AND MM.MACHINE_MODEL = TMP.MACHINE_MODEL ");
		sb.append("AND MM.KIBAN_SERNO = " +  serialNumberibanNo + " " );
		sb.append("AND TMP.CON_TYPE = \'" +  conType + "\' " );
		sb.append("AND TMP.MACHINE_MODEL = " +  machineModel + " " );
		sb.append("AND TMP.MAINTAINECE_PART_NUMBER = " +  buhinNo + " " );
		sb.append("AND TMP.LANGUAGE_CD = \'" +  languageCd + "\' " );
		sb.append("AND TMP.DEL_FLG = 0 ");

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<Object[]> result =  em.createNativeQuery(new String(sb)).getResultList();


		if(result.size() > 0) {
			return result;
		}else {
			return null;
		}

	}


	/*
	 * findByReplacementHistoryNativeQuery
	 */

	public List<Object[]> findByReplacementHistoryNativeQuery(
			Integer serialNumber,
			Integer buhinNo){
		StringBuilder sb = new StringBuilder();


		sb.append("SELECT ");
		sb.append("TMRTL.NO, ");
		sb.append("TMRTL.RESET_TIME, ");
		sb.append("TO_CHAR(TMRTL.REGIST_DTM,'YYYY-MM-DD') ");
		sb.append("FROM TBL_MAINTAINECE_RESET_TIME_LOG TMRTL, ");
		sb.append("MST_MACHINE MM ");
		sb.append("WHERE MM.KIBAN_SERNO = TMRTL.KIBAN_SERNO ");
		sb.append("AND TMRTL.MAINTAINECE_PART_NUMBER = " + buhinNo + " ");
		sb.append("AND MM.KIBAN_SERNO = " + serialNumber + " ");
		sb.append("ORDER BY TMRTL.NO ");


		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<Object[]> result =  em.createNativeQuery(new String(sb)).getResultList();

		if(result.size() > 0) {
			return result;
		}else {
			return null;
		}
	}





}
