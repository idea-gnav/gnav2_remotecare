package com.lbxco.RemoteCAREApp.ejbs.periodicServiceDitail;

import java.util.List;


public interface IPeriodicServiceDitailService {


	/**
	 * NativeQuery
	 * findByDtcDitail
	 */
	List<Object[]> findByServiceRequestDitailNativeQuery(
			Integer serialNumber,
			String conType,
			Integer machineModel,
			Integer buhinNo,
			String languageCd
			);

	/**
	 * NativeQuery
	 * findByReplacementHistory
	 */
	List<Object[]>findByReplacementHistoryNativeQuery(
			Integer serialNumber,
			Integer buhinNo
			);





}
