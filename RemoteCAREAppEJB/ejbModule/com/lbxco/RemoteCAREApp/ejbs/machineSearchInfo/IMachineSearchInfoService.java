package com.lbxco.RemoteCAREApp.ejbs.machineSearchInfo;

import java.util.List;

import com.lbxco.RemoteCAREApp.entities.MstArea;
import com.lbxco.RemoteCAREApp.entities.MstResionSalesManager;

public interface IMachineSearchInfoService {

	/**
	 * NativeQuery
	 * findByLbxModel : LBXfêæ¾
	 */
	// **«««@2019/11/18  iDEARº  ^®Xg\¦§À ÇÁ «««**//
//	List<String> findByLbxModel();
	List<String> findByLbxModel(Integer processTypeFlg, String kigyouCd, String sosikiCd, String groupNo);
	// **ªªª@2019/11/18  iDEARº  ^®Xg\¦§À ÇÁ ªªª**//


	/**
	 * JPA-JPQL
	 * findByPsm: PSMêæ¾
	 */
	List<MstArea> findByPsm();


	/**
	 * JPA-JPQL
	 * findByRm: RMêæ¾
	 */
	List<MstResionSalesManager> findByRm();


	// **«««@2019/02/01  iDEARº  Xebv2@ð·iXgÇÁ «««**//
	/**
	 * NativeQuery
	 * findByMaintainecePartItem: ð·iêæ¾
	 */
	List<Object[]> findByMaintainecePartItem(String languageCd);
	// **ªªª@2019/02/01  iDEARº  Xebv2@ð·iXgÇÁ ªªª**//


}
