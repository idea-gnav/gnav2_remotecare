package com.lbxco.RemoteCAREApp.ejbs.machineSearchInfo;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.lbxco.RemoteCAREApp.entities.MstArea;
import com.lbxco.RemoteCAREApp.entities.MstResionSalesManager;


@Stateless
@Local(IMachineSearchInfoService.class)
public class MachineSearchInfoService implements IMachineSearchInfoService{


	@PersistenceContext
    EntityManager em;

	/**
	 * NativeQuery
	 * findByLbxModel : LBXfêæ¾
	 */

	// **«««@2019/11/18  iDEARº  ^®Xg\¦§À ÇÁ «««**//
	@Override
//	public List<String> findByLbxModel(){
	public List<String> findByLbxModel(Integer processTypeFlg, String kigyouCd, String sosikiCd, String groupNo){

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT DISTINCT MM.LBX_MODEL_CD FROM MST_MACHINE MM ");
		if(processTypeFlg == 62 || processTypeFlg == 63) {
			sb.append("LEFT JOIN MST_SOSIKI MS ON MM.SOSIKI_CD= MS.SOSIKI_CD ");
		}
		sb.append("WHERE MM.DELETE_FLAG = 0 ");
		sb.append("AND NLS_UPPER(MM.KIBAN) LIKE 'LBX%' ");
		sb.append("AND MM.LBX_MODEL_CD IS NOT NULL ");
		if(processTypeFlg == 62 || processTypeFlg == 63) {
			sb.append("AND MS.LBX_FLG = 1 ");
			sb.append("AND ( ");
			sb.append("MM.SOSIKI_CD IN ( ");
			sb.append("SELECT SOSIKI_CD ");
			sb.append("FROM MST_SOSIKI ");
			sb.append("WHERE DELETE_FLG <> 1 ");
			sb.append("AND ( ");
			sb.append("KIGYOU_CD = '" + kigyouCd + "' ");
			sb.append("OR SOSIKI_CD = '" + sosikiCd + "') ");
			sb.append(") ");
			sb.append("OR MM.KIBAN_SERNO IN ( ");
			sb.append("SELECT KIBAN_SERNO ");
			sb.append("FROM MST_MACHINE_KOUKAI ");
			sb.append("WHERE KOUKAI_SOSIKI_CD = '" + sosikiCd + "' ");
			sb.append(") ");
			if(processTypeFlg == 62) {
				sb.append("OR MM.DAIRITENN_CD IN ( ");
				sb.append("SELECT SOSIKI_CD ");
				sb.append("FROM MST_SOSIKI ");
				sb.append("WHERE DELETE_FLG <> 1 ");
				sb.append("AND KIGYOU_CD = '" + kigyouCd +"' " );
				sb.append(") ");
			}
			sb.append(") ");
		}
		if(processTypeFlg != 61 && processTypeFlg != 62 && processTypeFlg != 63) {
			sb.append("AND ROWNUM <= 0 ");
		}
		sb.append("ORDER BY MM.LBX_MODEL_CD ");

	// **ªªª@2019/11/18  iDEARº  ^®Xg\¦§À ÇÁ ªªª**//

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<String> result =  em.createNativeQuery(new String(sb)).getResultList();

		if(result.size() > 0)
			return result;
		else
			return null;
	}



	/**
	 * JPA-JPQL
	 * findByPsm: PSMêæ¾
	 */
	@Override
	public List<MstArea> findByPsm() {

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<MstArea> result =  em.createQuery("SELECT m FROM MstArea m order by m.areaCd")
				    .getResultList();

		if (result.size() > 0)
			return result;
		else
			return null;
	}


	/**
	 * JPA-JPQL
	 * findByRm: RMêæ¾
	 */
	@Override
	public List<MstResionSalesManager> findByRm() {

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<MstResionSalesManager> result =  em.createQuery("SELECT m FROM MstResionSalesManager m order by m.resionSalesManagerCd")
												.getResultList();

		if (result.size() > 0)
			return result;
		else
			return null;
	}


	// **«««@2019/02/01  iDEARº  Xebv2@ð·iXgÇÁ «««**//
	/**
	 * NativeQuery
	 * findByMaintainecePartItem : ð·iêæ¾
	 */
	@Override
	public List<Object[]> findByMaintainecePartItem(String languageCd){

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT DISTINCT MAINTAINECE_PART_NUMBER,MAINTAINECE_PART_NAME FROM MST_MAINTAINECE_PART ");
		sb.append("WHERE LANGUAGE_CD = '" + languageCd + "' ");
		sb.append("AND CON_TYPE IN ('C','C2') ");
		sb.append("ORDER BY MAINTAINECE_PART_NUMBER,MAINTAINECE_PART_NAME ");

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<Object[]> result =  em.createNativeQuery(new String(sb)).getResultList();

		if(result.size() > 0)
			return result;
		else
			return null;
	}
	// **ªªª@2019/02/01  iDEARº  Xebv2@ð·iXgÇÁ ªªª**//



}
