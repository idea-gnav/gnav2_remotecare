package com.lbxco.RemoteCAREApp.ejbs.statistics;

import java.util.List;

import com.lbxco.RemoteCAREApp.ejbs.user.IUserService;
import com.lbxco.RemoteCAREApp.entities.MstSummaryArea;
import com.lbxco.RemoteCAREApp.entities.MstSummaryCountry;

public interface IStatisticsService extends IUserService{



	/**
	 * JPA-JPQL
	 * findBySummaryCountry: WvÎÛæ¾
	 */
	List<MstSummaryCountry> findBySummaryCountry(String languageCd, List<String> countryCds);



	/**
	 * JPA-JPQL
	 * findBySummaryArea: WvÎÛnæêæ¾
	 */
	List<MstSummaryArea> findBySummaryArea(String languageCd, String countryCd);



	// **««« 2018/11/27  LBNÎì  tF[Y2ãXü¯vÎ ÇÁ  «««**//
	/**
	 * JPA-JPQL
	 * findByRegionName: WvÎÛnæ¼Ìêæ¾
	 */
	List<String> findByRegionName(String countryCd, String areaCd);
	// **ªªª 2018/11/27  LBNÎì  tF[Y2ãXü¯vÎ ÇÁ  ªªª**//




	/**
	 * NativeQuery
	 * findByMachineKadoSummaryNativeQuery: @BÒ­ÔWvpT}[
	 */
	List<Object[]> findByMachineKadoSummaryNativeQuery(
			String countryCd,
			String stateCd,
			String timeFrom,
			String timeTo,
			String dealerName,
			String customerName,
			String lbxModel,
			String psmId,
			String rmId,
			Integer mode,
			String groupTitleFormat,
			String timeFormat,
			String countryCds
			// **««« 2018/11/27  LBNÎì  tF[Y2ãXü¯vÎ ÇÁ  «««**//
			,String regionName
			,Integer sortFlg
			,Integer statisticsCd
			// **ªªª 2018/11/27  LBNÎì  tF[Y2ãXü¯vÎ ÇÁ  ªªª**//
			,Integer typeFlg
			,String kigyouCd
			,String sosikiCd
			);




}
