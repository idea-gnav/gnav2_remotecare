package com.lbxco.RemoteCAREApp.ejbs.statistics;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.lbxco.RemoteCAREApp.ConvertUtil;
import com.lbxco.RemoteCAREApp.ejbs.user.UserService;
import com.lbxco.RemoteCAREApp.entities.MstSummaryArea;
import com.lbxco.RemoteCAREApp.entities.MstSummaryCountry;


@Stateless
@Local(IStatisticsService.class)
public class StatisticsService extends UserService implements IStatisticsService {

	private static final Logger logger = Logger.getLogger(StatisticsService.class.getName());

	@PersistenceContext
    EntityManager em;




	/**
	 * JPA-JPQL
	 * findBySummaryCountry: 集計対象国取得
	 */
	@Override
	public List<MstSummaryCountry> findBySummaryCountry(String languageCd, List<String> countryCds){

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<MstSummaryCountry> result =  em.createQuery("SELECT m FROM MstSummaryCountry m "
						+ "WHERE m.deleteFlg = 0 "
						+ "AND m.languageCd = :languageCd "
						+ "AND m.countryCd IN :countryCds "
						+ "ORDER BY m.countryNm "
						+ "")
				.setParameter("languageCd", languageCd)
				.setParameter("countryCds", countryCds)
				.getResultList();

		if(result.size() > 0)
			return result;
		else
			return null;
	}




	/**
	 * JPA-JPQL
	 * findBySummaryArea: 集計対象地区一覧取得
	 */
	@Override
	public List<MstSummaryArea> findBySummaryArea(String languageCd, String countryCd){

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<MstSummaryArea> result =  em.createQuery("SELECT m FROM MstSummaryArea m "
					+ "WHERE m.deleteFlg = 0 "
					+ "AND m.languageCd = :languageCd "
					+ "AND m.countryCd = :countryCd "
					+ "ORDER BY m.areaNm ")
				.setParameter("languageCd", languageCd)
				.setParameter("countryCd", countryCd)
				.getResultList();

		if(result.size() > 0)
			return result;
		else
			return null;
	}




	// **↓↓↓ 2018/11/27  LBN石川  フェーズ2代理店向け統計対応 追加  ↓↓↓**//
	/**
	 * JPA-JPQL
	 * findByRegionName: 集計対象地域名称一覧取得
	 */
	@Override
	public List<String> findByRegionName(String countryCd, String areaCd){


		StringBuilder sb = new StringBuilder();

		sb.append("SELECT DISTINCT POSITION_AREA2 ");
		sb.append("FROM TBL_MACHINE_KADO_TIME_SUM ");
		sb.append("WHERE POSITION_AREA2 IS NOT NULL ");
		sb.append("AND COUNTRY_CD = '"+countryCd+"' ");
		sb.append("AND AREA_CD = '"+areaCd+"' ");
		sb.append("ORDER BY NLSSORT(POSITION_AREA2,'NLS_SORT=Japanese') ");	// No6 sort

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<String> result =  em.createNativeQuery(new String(sb)).getResultList();

		if(result.size() > 0)
			return result;
		else
			return null;

	}
	// **↑↑↑ 2018/11/27  LBN石川  フェーズ2代理店向け統計対応 追加  ↑↑↑**//




	/**
	 * NativeQuery
	 * findByMachineKadoSummaryNativeQuery: 機械稼働時間集計用サマリー
	 */
	@Override
	public List<Object[]> findByMachineKadoSummaryNativeQuery(
				String countryCd,
				String stateCd,
				String timeFrom,
				String timeTo,
				String dealerName,
				String customerName,
				String lbxModel,
				String psmId,
				String rmId,
				Integer mode,
				String groupTitleFormat,
				String timeFormat,
				String countryCds
				// **↓↓↓ 2018/11/27  LBN石川  フェーズ2代理店向け統計対応 追加  ↓↓↓**//
				,String regionName
				,Integer sortFlg
				,Integer statisticsCd
				// **↑↑↑ 2018/11/27  LBN石川  フェーズ2代理店向け統計対応 追加  ↑↑↑**//
				,Integer typeFlg
				,String kigyouCd
				,String sosikiCd
			){

		logger.config("[RemoteCAREApp][DEBUG] mode=" + mode + ", countryCd=" + countryCd + ", stateCd=" + stateCd);

		StringBuilder sb = new StringBuilder();

		// **↓↓↓ 2019/08/09 RASIS DUCNKT フェーズ3統計台数集計 追加 ↓↓↓**//

		sb.append("SELECT ");
		if (mode == 0) {
			if (countryCd == null)
				sb.append("T1.COUNTRY_CD, "); // [0] COUNTRY_CD
			if (countryCd != null)
				sb.append("T1.AREA_CD, "); // [0] AREA_CD
		}
		if (mode == 1 || mode == 2)
			sb.append("T1.KIKAI_KADO_TIME, "); // [0] TIME_FILTER

		if (mode == 3 || mode == 4) {
			if (countryCd != null && stateCd != null)
				sb.append("T1.REGION_NM,");// [0] REGION_NM
		}

		sb.append("T1.UNIT_DAY_TOTAL, "); // [1] UNIT_DAY_TOTAL
		sb.append("T1.UNIT_MONTH_TOTAL, "); // [2] UNIT_MONTH_TOTAL
		sb.append("T1.TIME_TOTAL, "); // [3] TIME_TOTAL
		sb.append("T1.DAY_AVG, "); // [4] DAY_AVG

		if (mode == 0 || mode == 1 || mode == 3)
			sb.append("T1.MONTH_AVG, "); // [5] MONTH_AVG

		if (mode == 2 || mode == 4)
			sb.append("T1.MONTH_AVG, "); // [5] MONTH_AVG

		sb.append("NVL(T2.KIKAI_TOTAL,0) KIKAI_TOTAL "); // [6] KIKAI_TOTAL
		sb.append(",T1.KEY_COUNTRY_CD "); // [7] COUNTRY_CD

		sb.append("FROM ");
		sb.append("( ");

		// **↑↑↑ 2019/08/09 RASIS DUCNKT フェーズ3統計台数集計 追加 ↑↑↑**//

		sb.append("SELECT ");
		if (mode == 0) {
			if (countryCd == null)
				sb.append("COUNTRY_CD, "); // [0] COUNTRY_CD
			if (countryCd != null)
				sb.append("AREA_CD, "); // [0] AREA_CD
		}
		if (mode == 1 || mode == 2)
			sb.append("TO_CHAR(KIKAI_KADO_NENGETU,'" + groupTitleFormat + "') KIKAI_KADO_TIME, "); // [0] TIME_FILTER

		// **↓↓↓ 2018/11/27 LBN石川 フェーズ2代理店向け統計対応 追加 ↓↓↓**//
		if (mode == 3 || mode == 4) {
			if (countryCd != null && stateCd != null)
//				sb.append("POSITION_AREA2 AS REGION_NM, ");										// [0] REGION_NM
				sb.append("NVL(POSITION_AREA2,'  ') AS REGION_NM,");
		}
		// **↑↑↑ 2018/11/27 LBN石川 フェーズ2代理店向け統計対応 追加 ↑↑↑**//

		sb.append("SUM(UNIT_DAY_TOTAL) AS UNIT_DAY_TOTAL, "); // [1] UNIT_DAY_TOTAL
		sb.append("SUM(UNIT_MONTH_TOTAL) AS UNIT_MONTH_TOTAL, "); // [2] UNIT_MONTH_TOTAL
		sb.append("ROUND(SUM(TIME_TOTAL),1) AS TIME_TOTAL, "); // [3] TIME_TOTAL
		sb.append("ROUND(SUM(TIME_TOTAL)/SUM(UNIT_DAY_TOTAL),1) AS DAY_AVG, "); // [4] DAY_AVG

		// **↓↓↓ 2018/11/27 LBN石川 フェーズ2代理店向け統計対応 追加 ↓↓↓**//
//		if(mode==0 || mode==1)
		if (mode == 0 || mode == 1 || mode == 3)
			sb.append("ROUND(SUM(TIME_TOTAL)/SUM(UNIT_MONTH_TOTAL),1) AS MONTH_AVG, "); // [5] MONTH_AVG
//		if(mode==2)
		if (mode == 2 || mode == 4)
			sb.append("ROUND(SUM(TIME_TOTAL)/COUNT(DISTINCT KIBAN_SERNO),1) AS MONTH_AVG, "); // [5] MONTH_AVG
		// **↑↑↑ 2018/11/27 LBN石川 フェーズ2代理店向け統計対応 追加 ↑↑↑**//

		sb.append("COUNT(DISTINCT KIBAN_SERNO) "); // [6] KIKAI_TOTAL

		// **↓↓↓ 2019/01/11 LBN石川 フェーズ2他国追加統計対応 追加 ↓↓↓**//
//		if(countryCd != null) {
		sb.append(", COUNTRY_CD AS KEY_COUNTRY_CD "); // [7] COUNTRY_CD
//		}
		// **↑↑↑ 2019/01/11 LBN石川 フェーズ2他国追加統計対応 追加 ↑↑↑**//
		sb.append("FROM ");
		sb.append("( ");
		sb.append("SELECT ");
		if (mode == 0) {
			if (countryCd == null)
				sb.append("MKTS.COUNTRY_CD, ");
			if (countryCd != null)
				sb.append("MKTS.AREA_CD, ");
		}
		if (mode == 1 || mode == 2) {
			if (stateCd == null)
				sb.append("MKTS.COUNTRY_CD, ");
			if (stateCd != null)
				sb.append("MKTS.AREA_CD, ");
		}
		// **↓↓↓ 2018/11/27 LBN石川 フェーズ2代理店向け統計対応 追加 ↓↓↓**//
		if (mode == 3 || mode == 4) {
			sb.append("MKTS.POSITION_AREA2, ");
		}
		// **↑↑↑ 2018/11/27 LBN石川 フェーズ2代理店向け統計対応 追加 ↑↑↑**//

		sb.append("MKTS.KIKAI_KADO_NENGETU, ");
		sb.append("SUM(MKTS.KADO_DAY) AS UNIT_DAY_TOTAL,  ");
		sb.append("COUNT(DISTINCT MKTS.KIBAN_SERNO) AS UNIT_MONTH_TOTAL, ");
		sb.append("SUM(MKTS.KADO_TIME/60) AS TIME_TOTAL, ");
		sb.append("MKTS.KIBAN_SERNO ");

		// // **↓↓↓ 2019/01/11 LBN石川 フェーズ2他国追加統計対応 追加 ↓↓↓**//
		if (countryCd != null) {
			if (mode == 0 || stateCd != null)
				sb.append(", MKTS.COUNTRY_CD ");
			else
				sb.append(", MKTS.COUNTRY_CD AS KEY_COUNTRY_CD ");
		}
		// **↑↑↑ 2019/01/11 LBN石川 フェーズ2他国追加統計対応 追加 ↑↑↑**//

		sb.append("FROM ");
		sb.append("TBL_MACHINE_KADO_TIME_SUM MKTS ");

		sb.append("JOIN MST_SUMMARY_AREA MSA ON MKTS.AREA_CD = MSA.AREA_CD ");
		sb.append("AND MSA.DELETE_FLG = 0 ");
		sb.append("");

		if (mode == 0)
			sb.append("AND MKTS.COUNTRY_CD IN (" + ConvertUtil.queryInStr(countryCds) + ") ");
		// **↓↓↓ 2018/11/27 LBN石川 フェーズ2代理店向け統計対応 追加 ↓↓↓**//
//		if(mode==1 || mode==2) {
		if (mode == 1 || mode == 2 || mode == 3 || mode == 4) {
			// **↑↑↑ 2018/11/27 LBN石川 フェーズ2代理店向け統計対応 追加 ↑↑↑**//
			if (countryCd != null)
				sb.append("AND MKTS.COUNTRY_CD = '" + countryCd + "' ");
			if (stateCd != null)
				sb.append("AND MKTS.AREA_CD = '" + stateCd + "' ");
			// **↓↓↓ 2018/11/27 LBN石川 フェーズ2代理店向け統計対応 追加 ↓↓↓**//
			if (regionName != null)
				sb.append("AND MKTS.POSITION_AREA2 = '" + regionName + "' ");
			// **↑↑↑ 2018/11/27 LBN石川 フェーズ2代理店向け統計対応 追加 ↑↑↑**//
		}

// No Reagin 対応
//		if(mode==3 || mode==4) {
//			sb.append("AND MKTS.POSITION_AREA2 IS NOT NULL ");
//		}

		// 機械の参照範囲ス絞り込み

		sb.append("AND EXISTS ( ");
		sb.append("SELECT * FROM MST_MACHINE MM ");
		sb.append("LEFT JOIN MST_SOSIKI MS ");
		sb.append("ON MM.SOSIKI_CD = MS.SOSIKI_CD ");
		sb.append("LEFT JOIN MST_KIGYOU MK ");
		sb.append("ON MS.KIGYOU_CD = MK.KIGYOU_CD ");
		sb.append("WHERE MM.DELETE_FLAG = 0 ");
		sb.append("AND MM.CON_TYPE <> 'S' ");

		// ユーザー権限 機械公開制御
		if (typeFlg == 61) {
			sb.append("AND NLS_UPPER(MM.KIBAN) LIKE 'LBX%' ");

		} else if (typeFlg == 62) {
			sb.append("AND NLS_UPPER(MM.KIBAN) LIKE 'LBX%' ");
			sb.append("AND MS.LBX_FLG = 1 ");
			sb.append("AND (MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"
					+ kigyouCd + "' OR SOSIKI_CD = '" + sosikiCd + "')) ");
			sb.append("OR MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"
					+ sosikiCd + "') ");
			sb.append("OR MM.DAIRITENN_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND KIGYOU_CD = '"
					+ kigyouCd + "')) ");

		} else if (typeFlg == 63) {
			sb.append("AND NLS_UPPER(MM.KIBAN) LIKE 'LBX%' ");
			sb.append("AND MS.LBX_FLG = 1 ");
			sb.append("AND (MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"
					+ kigyouCd + "' OR SOSIKI_CD = '" + sosikiCd + "')) ");
			sb.append("OR MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"
					+ sosikiCd + "')) ");

		} else {
			sb.append("AND ROWNUM <= 0 ");
		}
		sb.append("AND MM.KIBAN_SERNO = MKTS.KIBAN_SERNO ");
		sb.append(") ");

//		lbxModel
		if (lbxModel != null)
			sb.append("AND MKTS.LBX_MODEL_CD IN (" + ConvertUtil.queryInStr(lbxModel) + ") ");

//		dealerName
		if (dealerName != null && dealerName.length() > 0) {
			sb.append("AND EXISTS ( ");
			sb.append("SELECT * FROM TBL_MACHINE_KADO_TIME_SUM MMKTS ");
			sb.append("JOIN MST_SOSIKI MS ON  MMKTS.DAIRITEN_CD = MS.SOSIKI_CD ");
			sb.append("JOIN MST_KIGYOU MK ON  MS.KIGYOU_CD = MK.KIGYOU_CD ");
			sb.append(
					"AND ( NLS_UPPER(REGEXP_REPLACE(MK.KIGYOU_NAME || MS.SOSIKI_NAME,' ','')) LIKE NLS_UPPER(REGEXP_REPLACE('%"
							+ ConvertUtil.rpStr(dealerName) + "%',' ','')) ) ");
			sb.append("WHERE MKTS.DAIRITEN_CD = MMKTS.DAIRITEN_CD ");
			sb.append(") ");
		}

//		customerName
		if (customerName != null && customerName.length() > 0) {
			sb.append("AND EXISTS ( ");
			sb.append("SELECT * FROM TBL_MACHINE_KADO_TIME_SUM MMKTS ");
			sb.append("JOIN MST_SOSIKI MS ON  MMKTS.SOSHIKI_CD = MS.SOSIKI_CD ");
			sb.append("JOIN MST_KIGYOU MK ON  MS.KIGYOU_CD = MK.KIGYOU_CD ");
			sb.append(
					"AND ( NLS_UPPER(REGEXP_REPLACE(MK.KIGYOU_NAME || MS.SOSIKI_NAME,' ','')) LIKE NLS_UPPER(REGEXP_REPLACE('%"
							+ ConvertUtil.rpStr(customerName) + "%',' ','')) ) ");
			sb.append("WHERE MKTS.SOSHIKI_CD = MMKTS.SOSHIKI_CD ");
			sb.append(") ");
		}

//		psmId
//		rmId
		if (psmId != null || rmId != null) {
			sb.append("AND EXISTS ( ");
			sb.append("SELECT * FROM TBL_MACHINE_KADO_TIME_SUM MMKTS ");
			sb.append("JOIN MST_SOSIKI MS ON MMKTS.DAIRITEN_CD = MS.SOSIKI_CD ");
			sb.append("LEFT JOIN MST_KIGYOU MK ON MS.KIGYOU_CD = MK.KIGYOU_CD ");

			// PSM
			if (psmId != null) {
				sb.append("JOIN MST_AREA MA ");
				sb.append("ON MS.AREA_CD = MA.AREA_CD ");
				sb.append("AND MS.AREA_CD = '" + psmId + "' ");
			}
			// RM
			if (rmId != null) {
				sb.append("JOIN MST_RESION_SALES_MANAGER MRSM ");
				sb.append("ON MS.RESION_SALES_MANAGER_CD = MRSM.RESION_SALES_MANAGER_CD ");
				sb.append("AND MS.RESION_SALES_MANAGER_CD = '" + rmId + "' ");
			}
			sb.append("WHERE MKTS.DAIRITEN_CD = MMKTS.DAIRITEN_CD ");
			sb.append(") ");
		}

		sb.append("AND ");
		sb.append("MKTS.KIKAI_KADO_NENGETU BETWEEN TO_DATE('" + timeFrom + "','" + timeFormat + "') AND TO_DATE('"
				+ timeTo + "','" + timeFormat + "') ");
		sb.append("GROUP BY ");
		sb.append("MKTS.KIKAI_KADO_NENGETU, MKTS.KIBAN_SERNO, ");

		// **↓↓↓ 2019/01/11 LBN石川 フェーズ2他国追加統計対応 追加 ↓↓↓**//
		if (countryCd != null) {
			sb.append("MKTS.COUNTRY_CD, ");
		}
		// **↑↑↑ 2019/01/11 LBN石川 フェーズ2他国追加統計対応 追加 ↑↑↑**//

		if (mode == 0) {
			if (countryCd == null) {
				sb.append("MKTS.COUNTRY_CD ");
			//	sb.append("ORDER BY MKTS.COUNTRY_CD ");
			}
			if (countryCd != null) {
				sb.append("MKTS.AREA_CD ");
				// sb.append("ORDER BY MKTS.AREA_CD ");
			}
		}
		if (mode == 1 || mode == 2) {
			if (stateCd == null) {
				sb.append("MKTS.COUNTRY_CD ");
			//	sb.append("ORDER BY MKTS.COUNTRY_CD ");
			}
			if (stateCd != null) {
				sb.append("MKTS.AREA_CD ");
			//	sb.append("ORDER BY MKTS.AREA_CD ");
			}
		}
		// **↓↓↓ 2018/11/27 LBN石川 フェーズ2代理店向け統計対応 追加 ↓↓↓**//
		if (mode == 3 || mode == 4) {
			sb.append("MKTS.POSITION_AREA2 ");
		//	sb.append("ORDER BY POSITION_AREA2 ");
		}
		// **↑↑↑ 2018/11/27 LBN石川 フェーズ2代理店向け統計対応 追加 ↑↑↑**//
		sb.append(") ");

		sb.append("GROUP BY ");

		// **↓↓↓ 2019/01/11 LBN石川 フェーズ2他国追加統計対応 追加 ↓↓↓**//
		if (countryCd != null) {
			sb.append("COUNTRY_CD,  ");
		}
		// **↑↑↑ 2019/01/11 LBN石川 フェーズ2他国追加統計対応 追加 ↑↑↑**//

		if (mode == 0) {
			if (countryCd == null) {
				sb.append("COUNTRY_CD ");
			//	sb.append("ORDER BY COUNTRY_CD ");
			}
			if (countryCd != null) {
				sb.append("AREA_CD ");
			//	sb.append("ORDER BY AREA_CD ");
			}

		}
		if (mode == 1 || mode == 2) {
			sb.append("TO_CHAR(KIKAI_KADO_NENGETU,'" + groupTitleFormat + "') ");
			if (stateCd == null) {
				sb.append(", COUNTRY_CD ");
			}
			if (stateCd != null) {
				sb.append(", AREA_CD ");
			}
		//	sb.append("ORDER BY TO_CHAR(KIKAI_KADO_NENGETU,'" + groupTitleFormat + "') ASC ");
		}
		// **↓↓↓ 2018/11/27 LBN石川 フェーズ2代理店向け統計対応 追加 ↓↓↓**//
		if (mode == 3 || mode == 4) {
			sb.append("POSITION_AREA2 ");
/*
			switch (sortFlg) {
			case 0: // 地域名称 昇順（デフォルト）
				sb.append("ORDER BY POSITION_AREA2 ");
				break;
			case 1: // 地域名称 降順
				sb.append("ORDER BY POSITION_AREA2 DESC ");
				break;
			case 2: // 時間 昇順
				if (statisticsCd == 0)
					sb.append("ORDER BY DAY_AVG, POSITION_AREA2 ");
				if (statisticsCd == 1)
					sb.append("ORDER BY MONTH_AVG, POSITION_AREA2 ");
				break;
			case 3: // 時間 降順
				if (statisticsCd == 0)
					sb.append("ORDER BY DAY_AVG DESC, POSITION_AREA2 ");
				if (statisticsCd == 1)
					sb.append("ORDER BY MONTH_AVG DESC, POSITION_AREA2 ");
				break;
			case 4: // 台数 昇順
				if (statisticsCd == 0)
					sb.append("ORDER BY KIKAI_TOTAL, POSITION_AREA2 ");
				if (statisticsCd == 1)
					sb.append("ORDER BY KIKAI_TOTAL, POSITION_AREA2 ");
				break;
			case 5: // 台数 降順
				if (statisticsCd == 0)
					sb.append("ORDER BY KIKAI_TOTAL DESC, POSITION_AREA2 ");
				if (statisticsCd == 1)
					sb.append("ORDER BY KIKAI_TOTAL DESC, POSITION_AREA2 ");
				break;
			default:
				sb.append("ORDER BY POSITION_AREA2 ");
			}
			*/
		}
		// **↑↑↑ 2018/11/27 LBN石川 フェーズ2代理店向け統計対応 追加 ↑↑↑**//


		// **↓↓↓ 2019/08/09 RASIS DUCNKT フェーズ3統計台数集計 追加 ↓↓↓**//
		sb.append(") T1 ");
		sb.append("LEFT JOIN ");
		sb.append("( ");

		sb.append("SELECT COUNT(KIBAN_SERNO) as KIKAI_TOTAL, ");

		if (mode == 0 && countryCd != null) {
			sb.append("AREA_CD, ");
		}
		if (mode == 1 || mode == 2)
			sb.append("TO_CHAR(KIKAI_KADO_NENGETU,'" + groupTitleFormat + "') KIKAI_KADO_TIME, ");

		if (mode == 3 || mode == 4) {
			if (countryCd != null && stateCd != null)
				sb.append("NVL(POSITION_AREA2,'  ') AS REGION_NM, ");
		}
		sb.append("COUNTRY_CD ");

		sb.append("FROM ");

		sb.append("( ");
		sb.append("SELECT TMLP.KIBAN_SERNO, ");
		sb.append("TMLP.KIKAI_KADO_NENGETU, ");
		sb.append("TMLP.COUNTRY_CD, ");
		sb.append("TMLP.AREA_CD, ");
		sb.append("TMLP.POSITION_AREA2, ");
		sb.append("ROW_NUMBER() OVER ( PARTITION BY TMLP.KIBAN_SERNO ");
		if(mode == 1 || mode == 2)
			sb.append(",TO_CHAR(TMLP.KIKAI_KADO_NENGETU,'" + groupTitleFormat + "') ");
		sb.append("ORDER BY TMLP.KIKAI_KADO_NENGETU DESC ) AS NUM ");
		sb.append("FROM ");
		sb.append("TBL_MACHINE_LAST_POSITION TMLP ");
		sb.append("JOIN  MST_MACHINE MM ");
		sb.append("ON TMLP.KIBAN_SERNO = MM.KIBAN_SERNO ");
		// **↓↓↓ 2019/09/03 iDEA山下 州情報なし機番を集計対象外にする ↓↓↓**//
		sb.append("JOIN MST_SUMMARY_AREA MSA2 ON (TMLP.AREA_CD = MSA2.AREA_CD AND MSA2.DELETE_FLG = 0) ");
		// **↑↑↑ 2019/09/03 iDEA山下 州情報なし機番を集計対象外にする ↑↑↑**//


		sb.append("WHERE MM.DELETE_FLAG = 0 ");
		sb.append("AND MM.CON_TYPE <> 'S' ");
		// **↓↓↓ 2019/08/19 iDEA 山下 組織権限制御 追加 ↓↓↓**//
		//sb.append("AND NLS_UPPER(MM.KIBAN) LIKE 'LBX%' ");
		if (typeFlg == 61) {
			sb.append("AND NLS_UPPER(MM.KIBAN) LIKE 'LBX%' ");
		} else if (typeFlg == 62) {
			sb.append("AND NLS_UPPER(MM.KIBAN) LIKE 'LBX%' ");
			sb.append("AND MS.LBX_FLG = 1 ");
			sb.append("AND (MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"
					+ kigyouCd + "' OR SOSIKI_CD = '" + sosikiCd + "')) ");
			sb.append("OR MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"
					+ sosikiCd + "') ");
			sb.append("OR MM.DAIRITENN_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND KIGYOU_CD = '"
					+ kigyouCd + "')) ");
		} else if (typeFlg == 63) {
			sb.append("AND NLS_UPPER(MM.KIBAN) LIKE 'LBX%' ");
			sb.append("AND MS.LBX_FLG = 1 ");
			sb.append("AND (MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '"
					+ kigyouCd + "' OR SOSIKI_CD = '" + sosikiCd + "')) ");
			sb.append("OR MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='"
					+ sosikiCd + "')) ");
		}
		// **↑↑↑ 2019/08/19 iDEA 山下 組織権限制御 追加 ↑↑↑**//

		sb.append("AND TMLP.KIKAI_KADO_NENGETU BETWEEN TO_DATE('" + timeFrom + "','" + timeFormat + "') AND TO_DATE('"
				+ timeTo + "','" + timeFormat + "') ");
		sb.append(") ");
		sb.append("WHERE NUM = 1 ");
		if (mode == 0)
			sb.append("AND COUNTRY_CD IN (" + ConvertUtil.queryInStr(countryCds) + ") ");

		if (mode == 1 || mode == 2 || mode == 3 || mode == 4) {
			if (countryCd != null)
				sb.append("AND COUNTRY_CD = '" + countryCd + "' ");
			if (stateCd != null)
				sb.append("AND AREA_CD = '" + stateCd + "' ");
			if (regionName != null)
				sb.append("AND POSITION_AREA2 = '" + regionName + "' ");
		}

		sb.append("GROUP BY ");
		sb.append("COUNTRY_CD ");
		if (mode == 0 && countryCd != null) {
			sb.append(",AREA_CD ");
		}
		if (mode == 1 || mode == 2)
			sb.append(",TO_CHAR(KIKAI_KADO_NENGETU,'" + groupTitleFormat + "') ");

		if (mode == 3 || mode == 4) {
			if (countryCd != null && stateCd != null)
				sb.append(",POSITION_AREA2 ");
		}

		sb.append(") T2 ");
		sb.append("ON ");

		sb.append("T1.KEY_COUNTRY_CD = T2.COUNTRY_CD ");

		if (mode == 0) {

			if (countryCd != null)
				sb.append("AND T1.AREA_CD = T2.AREA_CD ");
		}
		if (mode == 1 || mode == 2)
			sb.append("AND T1.KIKAI_KADO_TIME = T2.KIKAI_KADO_TIME ");

		if (mode == 3 || mode == 4) {
			if (countryCd != null && stateCd != null)
				sb.append("AND T1.REGION_NM = T2.REGION_NM ");
		}




		if (mode == 0) {
			if (countryCd == null) {
				sb.append("ORDER BY COUNTRY_CD ");
			}
			if (countryCd != null) {
				sb.append("ORDER BY AREA_CD ");
			}

		}
		if (mode == 1 || mode == 2) {
			sb.append("ORDER BY KIKAI_KADO_TIME ASC ");
		}

		if (mode == 3 || mode == 4) {
			switch (sortFlg) {
			case 0: // 地域名称 昇順（デフォルト）
				sb.append("ORDER BY CASE WHEN REGION_NM = '  ' THEN n'zzzzzz' ELSE REGION_NM END  ");
				break;
			case 1: // 地域名称 降順
				sb.append("ORDER BY CASE WHEN REGION_NM = '  ' THEN n'zzzzzz' ELSE REGION_NM END  DESC ");
				break;
			case 2: // 時間 昇順
				if (statisticsCd == 0)
					sb.append("ORDER BY DAY_AVG, CASE WHEN REGION_NM = '  ' THEN n'zzzzzz' ELSE REGION_NM END  ");
				if (statisticsCd == 1)
					sb.append("ORDER BY MONTH_AVG, CASE WHEN REGION_NM = '  ' THEN n'zzzzzz' ELSE REGION_NM END  ");
				break;
			case 3: // 時間 降順
				if (statisticsCd == 0)
					sb.append("ORDER BY DAY_AVG DESC, CASE WHEN REGION_NM = '  ' THEN n'zzzzzz' ELSE REGION_NM END  ");
				if (statisticsCd == 1)
					sb.append("ORDER BY MONTH_AVG DESC, CASE WHEN REGION_NM = '  ' THEN n'zzzzzz' ELSE REGION_NM END  ");
				break;
			case 4: // 台数 昇順
				if (statisticsCd == 0)
					sb.append("ORDER BY KIKAI_TOTAL, CASE WHEN REGION_NM = '  ' THEN n'zzzzzz' ELSE REGION_NM END  ");
				if (statisticsCd == 1)
					sb.append("ORDER BY KIKAI_TOTAL, CASE WHEN REGION_NM = '  ' THEN n'zzzzzz' ELSE REGION_NM END  ");
				break;
			case 5: // 台数 降順
				if (statisticsCd == 0)
					sb.append("ORDER BY KIKAI_TOTAL DESC, CASE WHEN REGION_NM = '  ' THEN n'zzzzzz' ELSE REGION_NM END  ");
				if (statisticsCd == 1)
					sb.append("ORDER BY KIKAI_TOTAL DESC, CASE WHEN REGION_NM = '  ' THEN n'zzzzzz' ELSE REGION_NM END  ");
				break;
			default:
				sb.append("ORDER BY CASE WHEN REGION_NM = '  ' THEN n'zzzzzz' ELSE REGION_NM END  ");
			}
		}


		// **↑↑↑ 2019/08/09 RASIS DUCNKT フェーズ3統計台数集計 更新 ↑↑↑**//

		logger.config(new String(sb));

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<Object[]> result = em.createNativeQuery(new String(sb)).getResultList();

		if (result.size() > 0)
			return result;
		else
			return null;
	}

}
