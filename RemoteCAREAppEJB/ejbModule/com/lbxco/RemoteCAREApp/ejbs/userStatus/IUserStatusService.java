package com.lbxco.RemoteCAREApp.ejbs.userStatus;

import com.lbxco.RemoteCAREApp.ejbs.user.IUserService;
import com.lbxco.RemoteCAREApp.entities.TblUserDevice;
import com.lbxco.RemoteCAREApp.entities.TblUserSetting;

public interface IUserStatusService extends IUserService{





	/**
	 * JPA-JPQL
	 * findByUserSetting: ユーザー設定情報取得
	 */
	TblUserSetting findByUserSetting(String userId);


	/**
	 * JPA-JPQL
	 * insertByUserSetting: ユーザー設定情報登録
	 */
	void insertByUserSetting(
					String userId,
					Integer unitSelect,
					Integer kibanSelect,
					Integer pdfFlg
				);


	/**
	 * JPA-JPQL
	 * updateByUserSetting: ユーザー設定情報登録
	 */
	void updateByUserSetting(String userId, Integer kibanSelect);


	/**
	 * JPA-JPQL
	 * findByDevice: デバイス情報取得
	 */
	TblUserDevice findByDevice(String deviceToken, String userId);

	/**
	 * JPA-JPQL
	 * insertByUserDevice: デバイス情報新規登録
	 */
	void insertByUserDevice(String deviceToken, Integer deviceType, String userId, String languageCd, int appFlg, String loginDtm);

	/**
	 * JPA-JPQL
	 * insertByUserDevice: デバイス情報更新
	 */
	void updateByUserDevice(String deviceToken, String userId, String languageCd, String loginDtm);

	/**
	 * JPA-JPQL
	 * updateByPasswordFlg: パスワード通知フラグ更新
	 * updpwdFlg 0:通常 1:7日前通知済 2:3日前通知済
	 */
	void updateByPasswordFlg(String userId, String updpwdFlg);


	// **↓↓↓ 2018/12/18 LBN石川 ユーザー認証管理テーブル追加 ↓↓↓**//
	/**
	 * JPA-JPQL
	 * insertByTblAppUserStatusLog: アプリユーザー認証ログ登録
	 */
	void insertByTblUserStatusAppLog(
				String userId,
				Integer deviceType,
				String languageCd,
				Integer mode,
				Integer appFlg,
				String loginDtm
			);
	// **↑↑↑ 2018/12/18 LBN石川 ユーザー認証管理テーブル追加 ↑↑↑**//



}
