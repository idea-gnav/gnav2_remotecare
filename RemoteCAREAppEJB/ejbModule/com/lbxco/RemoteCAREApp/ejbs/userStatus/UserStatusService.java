package com.lbxco.RemoteCAREApp.ejbs.userStatus;


import java.sql.Timestamp;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.lbxco.RemoteCAREApp.ConvertUtil;
import com.lbxco.RemoteCAREApp.ejbs.user.UserService;
import com.lbxco.RemoteCAREApp.entities.MstUser;
import com.lbxco.RemoteCAREApp.entities.TblUserDevice;
import com.lbxco.RemoteCAREApp.entities.TblUserSetting;
import com.lbxco.RemoteCAREApp.entities.TblUserStatusAppLog;

@Stateless
@Local(IUserStatusService.class)
public class UserStatusService extends UserService implements IUserStatusService{

	@PersistenceContext
    EntityManager em;



	/**
	 * JPA-JPQL
	 * findByUserSetting: ユーザー設定情報取得
	 */
	@Override
	public TblUserSetting findByUserSetting(String userId) {

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<TblUserSetting> result =  em.createQuery(
				"SELECT us FROM TblUserSetting us WHERE us.userId LIKE :userId")
			    .setParameter("userId", userId)
			    .getResultList();

		if (result.size() > 0)
			return (TblUserSetting)result.get(0);
		else
			return null;
	}


	/**
	 * JPA-JPQL
	 * insertByUserSetting: ユーザー設定情報登録　有効ユーザーのみ
	 */
	@Override
	public void insertByUserSetting(
					String userId,
					Integer unitSelect,
					Integer kibanSelect,
					Integer pdfFlg
				) {

		Timestamp dtm = new Timestamp(System.currentTimeMillis());
		TblUserSetting userSetting = new TblUserSetting();

		userSetting.setUserId(userId);
		userSetting.setUnitSelect(unitSelect);
		userSetting.setKibanSelect(kibanSelect);
		userSetting.setPdfFlg(pdfFlg);
		userSetting.setApplogFlg(0);	// 通常0で登録
		userSetting.setRegistPrg("RemoteCAREApp");
		userSetting.setRegistDtm(dtm);
		userSetting.setRegistUser(userId);
		userSetting.setUpdatePrg("RemoteCAREApp");
		userSetting.setUpdateDtm(dtm);
		userSetting.setUpdateUser(userId);

		em.persist(userSetting);

		return;
	}




	/**
	 * JPA-JPQL
	 * updateByUserSetting: ユーザー設定情報更新
	 */
	@Override
	public void updateByUserSetting(String userId, Integer kibanSelect) {

		TblUserSetting userSetting = em.find(TblUserSetting.class, userId);

		userSetting.setKibanSelect(kibanSelect);
		userSetting.setUpdatePrg("RemoteCAREApp");
		userSetting.setUpdateDtm(new Timestamp(System.currentTimeMillis()));
		userSetting.setUpdateUser(userId);

		em.persist(userSetting);
		em.flush();
		em.clear();

		return;
	}




	/**
	 * JPA-JPQL
	 * findByDevice: デバイス情報取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public TblUserDevice findByDevice(String deviceToken, String userId) {

		List<TblUserDevice> result = null;
		if(userId != null) {
			result =  em.createQuery(
			    "SELECT ud FROM TblUserDevice ud WHERE ud.delFlg = 0 AND ud.deviceToken LIKE :deviceToken AND ud.userId LIKE :userId")
			    .setParameter("userId", userId)
			    .setParameter("deviceToken", deviceToken)
			    .getResultList();
		}else {
			result =  em.createQuery(
				    "SELECT ud FROM TblUserDevice ud WHERE ud.delFlg = 0 AND ud.deviceToken LIKE :deviceToken")
				    .setParameter("deviceToken", deviceToken)
				    .getResultList();
		}

		if (result.size() > 0)
			return (TblUserDevice)result.get(0);
		else
			return null;
	}


	/**
	 * JPA-JPQL
	 * insertByUserDevice: デバイス情報新規登録
	 */
	@Override
	public void insertByUserDevice(String deviceToken, Integer deviceType, String userId, String languageCd, int appFlg, String loginDtm) {

		TblUserDevice userDevice = new TblUserDevice();
		userDevice.setDeviceToken(deviceToken);
		userDevice.setDeviceType(deviceType);
		userDevice.setUserId(userId);
//		userDevice.setUserLoginDtm(new Timestamp(System.currentTimeMillis()));
		userDevice.setUserLoginDtm(loginDtm);
		userDevice.setDelFlg(0);
		// **↓↓↓　2018/06/22  LBN石川  プッシュ通知お知らせ追加およびG＠NavApp対応に伴いdeviceLanguage, appFlg 追加  ↓↓↓**//
		userDevice.setLanguageCd(languageCd);
		userDevice.setAppFlg(appFlg);
		// **↑↑↑　2018/06/22  LBN石川  プッシュ通知お知らせ追加およびG＠NavApp対応に伴いdeviceLanguage, appFlg 追加  ↑↑↑**//
		userDevice.setRegistUser(userId);
		userDevice.setRegistPrg("RemoteCAREApp");
		userDevice.setRegistDtm(new Timestamp(System.currentTimeMillis()));
		userDevice.setUpdateUser(userId);
		userDevice.setUpdatePrg("RemoteCAREApp");
		userDevice.setUpdateDtm(new Timestamp(System.currentTimeMillis()));
		em.persist(userDevice);

//		em.refresh(userDevice);
		em.flush();
		em.clear();

		return;
	}


	/**
	 * JPA-JPQL
	 * insertByUserDevice: デバイス情報更新
	 */
	@Override
	public void updateByUserDevice(String deviceToken, String userId, String languageCd, String loginDtm) {

		TblUserDevice userDevice = em.find(TblUserDevice.class, deviceToken);
		userDevice.setUserId(userId);
//		userDevice.setUserLoginDtm(new Timestamp(System.currentTimeMillis()));
		userDevice.setUserLoginDtm(loginDtm);
		userDevice.setUpdateUser(userId);
		// **↓↓↓　2018/06/22  LBN石川  プッシュ通知お知らせ追加およびG＠NavApp対応に伴いdeviceLanguage, appFlg 追加  ↓↓↓**//
		userDevice.setLanguageCd(languageCd);
		// **↑↑↑　2018/06/22  LBN石川  プッシュ通知お知らせ追加およびG＠NavApp対応に伴いdeviceLanguage, appFlg 追加  ↑↑↑**//
		userDevice.setUpdatePrg("RemoteCAREApp");
		userDevice.setUpdateDtm(new Timestamp(System.currentTimeMillis()));

		em.persist(userDevice);

		em.flush();
		em.clear();

		return;
	}

	/**
	 * JPA-JPQL
	 * updateByPasswordFlg: パスワード通知フラグ更新
	 * updpwdFlg 0:通常 1:7日前通知済 2:3日前通知済
	 */
	@Override
	public void updateByPasswordFlg(String userId, String updpwdFlg) {

			MstUser user = em.find(MstUser.class, userId);
			user.setUpdpwdFlg(updpwdFlg);
			user.setUpdateUser(userId);
			user.setUpdatePrg("RemoteCAREApp");
			user.setUpdateDtm(new Timestamp(System.currentTimeMillis()));
			em.persist(user);

			return;
	}





	// **↓↓↓ 2018/12/12 LBN石川 ユーザー認証管理テーブル追加 ↓↓↓**//
	/**
	 * JPA-JPQL
	 * insertByTblAppUserStatusLog: ユーザー認証アプリケーションログ記録
	 */
	@Override
	public void insertByTblUserStatusAppLog(
				String userId,
				Integer deviceType,
				String languageCd,
				Integer mode,
				Integer appFlg,
				String loginDtm
			) {

		TblUserStatusAppLog userStatuslog = new TblUserStatusAppLog();
		userStatuslog.setUserId(userId);
		userStatuslog.setUserLoginDtm(loginDtm); // UTC
		userStatuslog.setAppFlg(appFlg);
		userStatuslog.setAppMode(mode);
		userStatuslog.setDeviceType(deviceType);
		userStatuslog.setDeviceLanguage(languageCd);
		userStatuslog.setRegistPrg("RemoteCAREApp");
		userStatuslog.setRegistDtm(ConvertUtil.getDaysUtcYmdHms(0));
		userStatuslog.setRegistUser(userId);

//		em.getEntityManagerFactory().getCache().evictAll();
		em.persist(userStatuslog);

		return;
	}
	// **↑↑↑ 2018/12/12 LBN石川 ユーザー認証管理テーブル追加 ↑↑↑**//





}
