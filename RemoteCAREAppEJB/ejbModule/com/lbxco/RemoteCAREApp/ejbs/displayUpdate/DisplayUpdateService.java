package com.lbxco.RemoteCAREApp.ejbs.displayUpdate;


import java.sql.Timestamp;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.lbxco.RemoteCAREApp.entities.TblUserSetting;

@Stateless
@Local(IDisplayUpdateService.class)
public class DisplayUpdateService implements IDisplayUpdateService{

	@PersistenceContext
    EntityManager em;


	/*
	 * JPA
	 * displayUpdate
	 */
	@Override
	public void displayUpdate(String userId,Integer unitSelect,Integer kibanSelect, Integer searchRangeDistance) {

		TblUserSetting tus = em.find(TblUserSetting.class,userId);
		tus.setUnitSelect(unitSelect);
		tus.setKibanSelect(kibanSelect);
		// **«««@2018/05/22  LBNÎì  v]sïÇä  No45 ÁèÊuõÎ ÇÁ  «««**//
		tus.setSearchRange(searchRangeDistance);
		// **ªªª@2018/05/22  LBNÎì  v]sïÇä  No45 ÁèÊuõÎ ÇÁ  ªªª**//
		tus.setUpdateUser(userId);
		tus.setUpdatePrg("RemoteCAREApp");
		tus.setUpdateDtm(new Timestamp(System.currentTimeMillis()));

		em.persist(tus);
	}




}
