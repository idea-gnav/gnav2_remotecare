package com.lbxco.RemoteCAREApp.ejbs.machine;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.lbxco.RemoteCAREApp.ConvertUtil;
import com.lbxco.RemoteCAREApp.entities.MstMachine;



/**
 * Session Bean implementation class MachineService
 */
@Stateless
@Local(IMachineService.class)
public class MachineService implements IMachineService {

	private static final Logger logger = Logger.getLogger(MachineService.class.getName());

	@PersistenceContext
    EntityManager em;



	/**
	 * JPA
	 * FindByMachine: @Bîñæ¾
	 */
	@Override
	public MstMachine findByMachine(Integer serialNumber) {

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<MstMachine> listMachine = em.createQuery("SELECT m FROM MstMachine m WHERE m.kibanSerno=:kibanSerno ")
				.setParameter("kibanSerno", serialNumber)
				.getResultList();

		if(listMachine.size() > 0)
			return listMachine.get(0);
		else
			return null;

	}



	/**
	 * NativeQuery
	 * findByMachineList: @Bêæ¾
	 */
	public List<Object[]> findByMachineListNativeQuery(
			String userId,
//			Integer serialNumber,
			Integer kibanSelect,
			String kibanInput,
			Integer searchFlg,
			Integer sortFlg,
			String manufacturerSerialNumberFrom,
			String manufacturerSerialNumberTo,
			String customerManagementNoFrom,
			String customerManagementNoTo,
			String lbxSerialNumberFrom,
			String lbxSerialNumberTo,
			String lbxModel,
			String psmId,
			String rmId,
			String dealerName,
			// **«««@2018/11/28  iDEARº  Xebv2@¨ql¼õÇÁ «««**//
			String customerName,
			// **ªªª@2018/11/28  iDEARº  Xebv2@¨ql¼õÇÁ ªªª**//
			String dtcCode,
			Integer warningCurrentlyInProgress,
			String warningDateFrom,
			String warningDateTo,
			// **«««@2019/01/07  iDEARº  Xebv2@èú®õõÇÁ «««**//
			Integer periodicServiceNotice,
			Integer periodicServiceNowDue,
			String replacementPart,
			// **ªªª@2019/01/07  iDEARº  Xebv2@èú®õõÇÁ ªªª**//
			Integer favoriteSearchFlg,
			Integer typeFlg,
			String kigyouCd,
			String sosikiCd,
			String kengenCd,
			// **«««@2018/05/22 LBNÎì  v]sïÇä  No45Î v]ÇÁ «««**//
			Double searchRangeFromIdo,
			Double searchRangeFromKeido,
			Integer searchRangeDistance,
			Integer searchRangeUnit
			// **ªªª@2018/05/22 LBNÎì  v]sïÇä  No45Î v]ÇÁ ªªª**//
		){

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("MM.KIBAN_SERNO, ");										// 0
		sb.append("MM.KIBAN, ");											// 1
		sb.append("MM.LBX_KIBAN, ");										// 2
		sb.append("MM.USER_KANRI_NO, ");									// 3
		sb.append("CONCAT(MK.KIGYOU_NAME || ' ', MS.SOSIKI_NAME), ");		// 4
		sb.append("MM.MODEL_CD, ");											// 5
		sb.append("MM.LBX_MODEL_CD, ");										// 6
		sb.append("MM.POSITION_EN, ");										// 7
		sb.append("MM.NEW_IDO, ");											// 8
		sb.append("MM.NEW_KEIDO, ");										// 9
		sb.append("NVL(MM.NEW_HOUR_METER, 0.0) AS HOUR_METER, ");			// 10
		sb.append("MM.NENRYO_LV, ");										// 11
		sb.append("MM.UREA_WATER_LEVEL, ");									// 12
		sb.append("MM.RECV_TIMESTAMP, ");									// 13
		// **«««@2019/01/07  iDEARº  Xebv2@èú®õõÇÁ «««**//
		sb.append("CASE ");
		sb.append("WHEN MM.KEYIKOKU_COUNT > 0 THEN 2 ");
		sb.append("WHEN MM.YOKOKU_COUNT > 0 THEN 1 ");
		sb.append("ELSE 0 ");
		sb.append("END AS PERIODIC_SERVICE_ICON_TYPE, ");					// 14
		sb.append("MM.CON_TYPE ");											// 15
		// **ªªª@2019/01/07  iDEARº  Xebv2@èú®õõÇÁ ªªª**//

		sb.append("FROM MST_MACHINE MM ");
		sb.append("LEFT JOIN MST_SOSIKI MS ");
		sb.append("ON MM.SOSIKI_CD = MS.SOSIKI_CD ");
		sb.append("LEFT JOIN MST_KIGYOU MK ");
		sb.append("ON MS.KIGYOU_CD = MK.KIGYOU_CD ");
		sb.append("WHERE MM.DELETE_FLAG = 0 ");
		sb.append("AND MM.CON_TYPE <> 'S' ");


		// [U[ À @BöJ§ä
		if(typeFlg == 61) {
			sb.append("AND NLS_UPPER(MM.KIBAN) LIKE 'LBX%' ");

		}else if(typeFlg == 62) {
			sb.append("AND NLS_UPPER(MM.KIBAN) LIKE 'LBX%' ");
			sb.append("AND MS.LBX_FLG = 1 ");
			sb.append("AND (MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '" + kigyouCd + "' OR SOSIKI_CD = '" + sosikiCd + "')) ");
			sb.append("OR MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='" + sosikiCd + "') ");
			sb.append("OR MM.DAIRITENN_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1@AND@KIGYOU_CD = '" + kigyouCd + "')) ");

		}else if(typeFlg == 63) {
			sb.append("AND NLS_UPPER(MM.KIBAN) LIKE 'LBX%' ");
			sb.append("AND MS.LBX_FLG = 1 ");
			sb.append("AND (MM.SOSIKI_CD IN (SELECT SOSIKI_CD FROM MST_SOSIKI WHERE DELETE_FLG <> 1 AND (KIGYOU_CD = '" + kigyouCd + "' OR SOSIKI_CD = '" + sosikiCd + "')) ");
			sb.append("OR MM.KIBAN_SERNO IN (SELECT KIBAN_SERNO FROM MST_MACHINE_KOUKAI WHERE KOUKAI_SOSIKI_CD='" + sosikiCd + "')) ");

		}else {
			sb.append("AND ROWNUM <= 0 ");
		}


		// Êíõ : @BIðæª
		if( kibanInput != null) {
			if(kibanSelect == 0)
				sb.append("AND NLS_UPPER(MM.KIBAN) ");
			if(kibanSelect == 1)
				sb.append("AND NLS_UPPER(MM.LBX_KIBAN) ");
			if(kibanSelect == 2)
				sb.append("AND NLS_UPPER(MM.USER_KANRI_NO) ");

			if(searchFlg == 0)
				sb.append("LIKE NLS_UPPER('%" + ConvertUtil.rpStr(kibanInput) + "%') ");
			if(searchFlg == 1)
				sb.append("LIKE NLS_UPPER('" + ConvertUtil.rpStr(kibanInput) + "%') ");
			if(searchFlg == 2)
				sb.append("LIKE NLS_UPPER('%" + ConvertUtil.rpStr(kibanInput) + "') ");
			if(searchFlg == 3)
				sb.append("= NLS_UPPER('" + ConvertUtil.rpStr(kibanInput) + "') ");
		}

		// Ú×õ : @Ô
		// 3.SCM@Ô
		if( manufacturerSerialNumberFrom != null && manufacturerSerialNumberTo == null)
			sb.append("AND NLS_UPPER(MM.KIBAN) LIKE NLS_UPPER('" + ConvertUtil.rpStr(ConvertUtil.searchStrKiban(manufacturerSerialNumberFrom)) + "') ");
		if( manufacturerSerialNumberFrom == null && manufacturerSerialNumberTo != null)
			sb.append("AND NLS_UPPER(MM.KIBAN) LIKE NLS_UPPER('" + ConvertUtil.rpStr(ConvertUtil.searchStrKiban(manufacturerSerialNumberTo)) + "') ");

		if( manufacturerSerialNumberFrom != null && manufacturerSerialNumberTo != null)
			sb.append("AND NLS_UPPER(MM.KIBAN) BETWEEN NLS_UPPER('" + ConvertUtil.rpStr(manufacturerSerialNumberFrom) + "') AND NLS_UPPER('" + ConvertUtil.rpStr(manufacturerSerialNumberTo) + "') ");

		// 1.¨qlÇÔ
		if( customerManagementNoFrom != null && customerManagementNoTo == null)
			sb.append("AND NLS_UPPER(MM.USER_KANRI_NO) LIKE NLS_UPPER('" + ConvertUtil.rpStr(ConvertUtil.searchStr(customerManagementNoFrom)) + "') ");
		if( customerManagementNoFrom == null && customerManagementNoTo != null)
			sb.append("AND NLS_UPPER(MM.USER_KANRI_NO) LIKE NLS_UPPER('" + ConvertUtil.rpStr(ConvertUtil.searchStr(customerManagementNoTo)) + "') ");

		if( customerManagementNoFrom != null && customerManagementNoTo != null)
		//	sb.append("AND MM.USER_KANRI_NO BETWEEN NLS_UPPER('" + u.rpStr(customerManagementNoFrom) + "') AND NLS_UPPER('" + u.rpStr(customerManagementNoTo) + "') ");
			sb.append("AND NLS_UPPER(MM.USER_KANRI_NO) BETWEEN NLS_UPPER('" + ConvertUtil.rpStr(customerManagementNoFrom) + "') AND NLS_UPPER('" + ConvertUtil.rpStr(customerManagementNoTo) + "') ");

		// 2.LBX@Ô
		if( lbxSerialNumberFrom != null && lbxSerialNumberTo == null)
			sb.append("AND NLS_UPPER(MM.LBX_KIBAN) LIKE NLS_UPPER('" + ConvertUtil.rpStr(ConvertUtil.searchStrKiban(lbxSerialNumberFrom)) + "') ");
		if( lbxSerialNumberFrom == null && lbxSerialNumberTo != null)
			sb.append("AND NLS_UPPER(MM.LBX_KIBAN) LIKE NLS_UPPER('" + ConvertUtil.rpStr(ConvertUtil.searchStrKiban(lbxSerialNumberTo)) + "') ");

		if( lbxSerialNumberFrom != null && lbxSerialNumberTo != null)
			sb.append("AND NLS_UPPER(MM.LBX_KIBAN) BETWEEN NLS_UPPER('" + ConvertUtil.rpStr(lbxSerialNumberFrom) + "') AND NLS_UPPER('" + ConvertUtil.rpStr(lbxSerialNumberTo) + "') ");

		// 4.LBX^®
		if( lbxModel != null ) {
			// **«««@2018/03/14  LBNÎì  v]sïÇä  No17Î v]ÇÁ «««**//
//			String[] lbxModelCdList = lbxModel.split(",");
//			StringBuilder sbLbxModel = new StringBuilder();
//			for(int index=0; index<lbxModelCdList.length; index++) {
//				sbLbxModel.append("'"+lbxModelCdList[index]+"'");
//				if(index<(lbxModelCdList.length-1))
//					sbLbxModel.append(",");
//			}
//			sb.append("AND MM.LBX_MODEL_CD IN (" + new String(sbLbxModel) + ") ");

			sb.append("AND MM.LBX_MODEL_CD IN (" + ConvertUtil.queryInStr(lbxModel) + ") ");

			// **ªªª@2018/03/14  LBNÎì  v]sïÇä  No17Î v]ÇÁªªª**//
		}


		// **«««@2018/04/10  LBNÎì  v]sïÇä  No33Î v]ÇÁ «««**//
		// Ú×õ : }l[W[AãX
		if(psmId!=null || rmId!=null) {
			sb.append("AND EXISTS ( ");
			sb.append("SELECT * FROM MST_MACHINE MMM ");
			sb.append("JOIN MST_SOSIKI MS ON MMM.DAIRITENN_CD = MS.SOSIKI_CD ");
			sb.append("LEFT JOIN MST_KIGYOU MK ON MS.KIGYOU_CD = MK.KIGYOU_CD ");

			// 5.PSM
			if(psmId!=null) {
				sb.append("JOIN MST_AREA MA ");
				sb.append("ON MS.AREA_CD = MA.AREA_CD ");
				sb.append("AND MS.AREA_CD = '" + psmId + "' ");
			}
			// 6.RM
			if(rmId!=null) {
				sb.append("JOIN MST_RESION_SALES_MANAGER MRSM ");
				sb.append("ON MS.RESION_SALES_MANAGER_CD = MRSM.RESION_SALES_MANAGER_CD ");
				sb.append("AND MS.RESION_SALES_MANAGER_CD = '" + rmId + "' ");
			}
			sb.append("WHERE MM.KIBAN_SERNO = MMM.KIBAN_SERNO ");
			sb.append(") ");

		}
		// 7.ãX¼
		if(dealerName!=null && dealerName.length()>0) {
			sb.append("AND EXISTS ( ");
			sb.append("SELECT * FROM MST_MACHINE MMM ");

			sb.append("JOIN MST_SOSIKI MS ON  MMM.DAIRITENN_CD = MS.SOSIKI_CD ");
			sb.append("JOIN MST_KIGYOU MK ON  MS.KIGYOU_CD = MK.KIGYOU_CD ");
			sb.append("AND ( NLS_UPPER(REGEXP_REPLACE(MK.KIGYOU_NAME || MS.SOSIKI_NAME,' ','')) LIKE NLS_UPPER(REGEXP_REPLACE('%" + ConvertUtil.rpStr(dealerName) + "%',' ','')) ) ");
//			sb.append("OR NLS_UPPER(REGEXP_REPLACE(MS.SOSIKI_NAME,' ','')) LIKE NLS_UPPER(REGEXP_REPLACE('%" + u.rpStr(dealerName) + "%',' ','')) ) ");

			sb.append("WHERE MM.KIBAN_SERNO = MMM.KIBAN_SERNO ");
			sb.append(") ");
		}
		// **ªªª@2018/04/10  LBNÎì  v]sïÇä  No33Î v]ÇÁ ªªª**//



		// **«««@2018/11/28  iDEARº  Xebv2@¨ql¼õÇÁ «««**//
		// ¨ql¼
		if(customerName!=null && customerName.length()>0) {
			sb.append("AND EXISTS ( ");
			sb.append("SELECT * FROM MST_MACHINE MMM ");

			sb.append("JOIN MST_SOSIKI MS ON  MMM.SOSIKI_CD = MS.SOSIKI_CD ");
			sb.append("JOIN MST_KIGYOU MK ON  MS.KIGYOU_CD = MK.KIGYOU_CD ");
			sb.append("AND ( NLS_UPPER(REGEXP_REPLACE(MK.KIGYOU_NAME || MS.SOSIKI_NAME,' ','')) LIKE NLS_UPPER(REGEXP_REPLACE('%" + ConvertUtil.rpStr(customerName) + "%',' ','')) ) ");
			sb.append("WHERE MM.KIBAN_SERNO = MMM.KIBAN_SERNO ");
			sb.append(") ");
		}
		// **ªªª@2018/11/28  iDEARº  Xebv2@¨ql¼õÇÁªªª**//


		// Ú×õ : DTCÖA
		if( warningCurrentlyInProgress != 0 || warningDateFrom != null || warningDateTo != null || dtcCode!=null) {
			sb.append("AND EXISTS (");
			sb.append("SELECT * FROM TBL_EVENT_SEND TES ");

			sb.append("JOIN MST_EVENT_SEND MES ");
			sb.append("ON TES.EVENT_NO = MES.EVENT_CODE ");
			sb.append("AND TES.EVENT_SHUBETU = MES.SHUBETU_CODE ");
			sb.append("AND TES.CON_TYPE = MES.CON_TYPE ");
			sb.append("AND TES.MACHINE_KBN = MES.MACHINE_KBN ");

			sb.append("WHERE MES.DELETE_FLAG = 0 ");
			sb.append("AND MES.LANGUAGE_CD = 'en' ");
			sb.append("AND TES.EVENT_SHUBETU = 8 ");
			sb.append("AND TES.CON_TYPE IN ('C','C2') ");
			sb.append("AND TES.SYORI_FLG <> 9 ");	// <---

			sb.append("AND MM.KIBAN_SERNO = TES.KIBAN_SERNO ");

			// 10.DTCR[h
			if(dtcCode!=null ) {
				if(ConvertUtil.isNumber(dtcCode))
					sb.append("AND (MES.FLG_VISIBLE LIKE '%" + dtcCode + "%' OR MES.EVENT_CODE = " + dtcCode + ") ");	// <<< this 2018.3.5
				if(!ConvertUtil.isNumber(dtcCode))
					sb.append("AND MES.FLG_VISIBLE LIKE '%" + ConvertUtil.rpStr(dtcCode).toUpperCase() + "%' ");
			}

			// 9.DTC­¶ú
			if( warningDateFrom != null && warningDateTo == null) {
				sb.append("AND TES.HASSEI_TIMESTAMP_LOCAL  ");
				sb.append(">= TO_TIMESTAMP('" + warningDateFrom + " 00:00:00.0', 'yyyy/MM/dd hh24:mi:ss.FF6') ");
			}

			if( warningDateFrom != null && warningDateTo != null) {
				sb.append("AND TES.HASSEI_TIMESTAMP_LOCAL ");
				sb.append("BETWEEN TO_TIMESTAMP('" + warningDateFrom + " 00:00:00.0', 'yyyy/MM/dd hh24:mi:ss.FF6') ");
				sb.append("AND TO_TIMESTAMP('" + warningDateTo + " 23:59:59.999999', 'yyyy/MM/dd hh24:mi:ss.FF6') ");
			}

			if( warningDateFrom == null && warningDateTo != null) {
				sb.append("AND TES.HASSEI_TIMESTAMP_LOCAL  ");
				sb.append("<= TO_TIMESTAMP('" + warningDateTo + " 23:59:59.999999', 'yyyy/MM/dd hh24:mi:ss.FF6') ");
			}

			// 8.DTC­¶
			if( warningCurrentlyInProgress != 0 ) {
				sb.append("AND TES.FUKKYU_TIMESTAMP_LOCAL IS NULL ");
			}

			sb.append(") ");
		}

		// 11.¨CÉüè
		if(favoriteSearchFlg != 0)
			sb.append("AND EXISTS (SELECT * FROM TBL_USER_FAVORITE TUF WHERE DEL_FLG = 0 AND MM.KIBAN_SERNO = TUF.KIBAN_SERNO AND USER_ID = '"  + userId + "') ");



		// **«««@2018/05/22 LBNÎì  v]sïÇä  No45ÁèÊuõÎ v]ÇÁ «««**//
		// ¼aàÌ@Bðo
		if(searchRangeFromIdo!=null && searchRangeFromKeido!=null) {
			sb.append("AND EXISTS (");
			sb.append("SELECT * FROM ( ");
			sb.append("SELECT ");
			sb.append("KIBAN_SERNO, ");
			sb.append("( 6378.137 * ACOS( ROUND(  ");
			sb.append("SIN("+searchRangeFromIdo+" * ATAN2(0, -1) / 180) * SIN(NEW_IDO/3600/1000 * ATAN2(0, -1) / 180) ");
			sb.append("+ COS("+searchRangeFromIdo+" * ATAN2(0, -1) / 180)*COS(NEW_IDO/3600/1000 * ATAN2(0, -1) / 180) ");
			sb.append("* COS(ABS((NEW_KEIDO/3600/1000)-("+searchRangeFromKeido+")) * ATAN2(0, -1) / 180) , 20) ");
			sb.append(") ) as DISTANCE ");
			sb.append("FROM MST_MACHINE ");
			sb.append("WHERE DELETE_FLAG = 0 ");
			sb.append("AND NEW_IDO IS NOT NULL ");
			sb.append("AND NEW_KEIDO IS NOT NULL ");
			sb.append("AND KIBAN LIKE 'LBX%' ");
			sb.append(") SUB_MM ");

			if(searchRangeUnit==0)	// USA mi
				sb.append("WHERE SUB_MM.DISTANCE <= ("+searchRangeDistance+"*1.60934) ");
			else if(searchRangeUnit==1)	// ISO km
				sb.append("WHERE SUB_MM.DISTANCE <= "+searchRangeDistance+" ");

			sb.append("AND MM.KIBAN_SERNO = SUB_MM.KIBAN_SERNO) ");

		}
		// **ªªª@2018/05/22 LBNÎì  v]sïÇä  No45ÁèÊuõÎ v]ÇÁ ªªª**//


		// **«««@2019/01/07  iDEARº  Xebv2@èú®õõÇÁ «««**//
		// èú®õõ
		if(periodicServiceNotice!=null || periodicServiceNowDue!=null) {
			if(replacementPart!=null) {
				sb.append("AND EXISTS( ");
				sb.append("SELECT TES.* ");
				sb.append("FROM TBL_EVENT_SEND TES ");
				sb.append("WHERE TES.EVENT_SHUBETU = 6 ");
//				sb.append("AND TES.EVENT_NO != 2 ");
				sb.append("AND TES.EVENT_NO NOT IN (2,3) ");
				sb.append("AND TES.EVENT_NO IN ");
				if(periodicServiceNotice==1 && periodicServiceNowDue==1){
					sb.append("(0,1) ");
				}else if(periodicServiceNotice==1 && periodicServiceNowDue==0) {
					sb.append("(0) ");
				}else if(periodicServiceNotice==0 && periodicServiceNowDue==1) {
					sb.append("(1) ");
				}
				sb.append("AND TES.MT_BUHIN_NO IN (" + replacementPart + ") ");
				sb.append("AND TES.REGIST_DTM =( ");
				sb.append("SELECT MAX(TES2.REGIST_DTM) ");
				sb.append("FROM TBL_EVENT_SEND TES2 ");
				sb.append("WHERE TES.EVENT_SHUBETU =TES2.EVENT_SHUBETU ");
				sb.append("AND TES.MT_BUHIN_NO = TES2.MT_BUHIN_NO ");
				sb.append("AND TES.KIBAN_SERNO = TES2.KIBAN_SERNO ");
				sb.append("GROUP BY TES2.KIBAN_SERNO ) ");
				sb.append("AND MM.KIBAN_SERNO = TES.KIBAN_SERNO ) ");
			}else {
				if(periodicServiceNotice == 1 && periodicServiceNowDue == 1 ) {
					sb.append("AND ( MM.YOKOKU_COUNT > 0 OR MM.KEYIKOKU_COUNT > 0) ");
				}else if(periodicServiceNotice == 1 && periodicServiceNowDue == 0){
					sb.append("AND MM.YOKOKU_COUNT > 0 ");
			    }else if(periodicServiceNowDue>0) {
					sb.append("AND MM.KEYIKOKU_COUNT > 0 ");
				}
			}
		}
		// **ªªª@2019/01/07  iDEARº  Xebv2@èú®õõÇÁ ªªª**//


		sb.append("AND ROWNUM <= 1001 ");

		// ¤Ê : ÀÑÖ¦ð
		// 0  @Ô¸,  1 @Ô~, 2 Ýn¸  , 3 Ýn~   , 4 A[^ ¸  , 5 A[^~ , 6 SCM^®¸ , 7 SCM^®~
		if(sortFlg == 0 || sortFlg == 1){
			if(kibanSelect == 0)
				sb.append("ORDER BY NLS_UPPER(MM.KIBAN) ");
			if(kibanSelect == 1)
				sb.append("ORDER BY NLS_UPPER(MM.LBX_KIBAN) ");
			if(kibanSelect == 2)
				sb.append("ORDER BY NLS_UPPER(MM.USER_KANRI_NO) ");
		}
		if(sortFlg == 2 || sortFlg == 3)
			sb.append("ORDER BY MM.POSITION ");
		if(sortFlg == 4 || sortFlg == 5)
			sb.append("ORDER BY HOUR_METER ");
		// **««« 2018/05/22 LBNÎì  v]sïÇä  No45ÁèÊuõÎ v]ÇÁ «««**//
		if(sortFlg == 6 || sortFlg == 7)
			sb.append("ORDER BY MM.LBX_MODEL_CD ");
//		if(sortFlg == 1 || sortFlg == 3 || sortFlg == 5)
		if(sortFlg == 1 || sortFlg == 3 || sortFlg == 5 || sortFlg == 7)
			sb.append("DESC ");

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<Object[]> result =  em.createNativeQuery(new String(sb)).getResultList();

		logger.config(new String(sb));

		if(result.size() > 0)
			return result;
		else
			return null;


	}


	// **«««@2018/05/22 LBNÎì  v]sïÇä  No45Î v]ÇÁ «««**//
	/**
	 * NativeQuery
	 * findByLatlngNativeQuery: @BÜxoxæ¾
	 */
	@Override
	public List<Object[]> findByLatlngNativeQuery(String searchKibanInput){

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ROUND(NEW_IDO/3600/1000,6) IDO, ROUND(NEW_KEIDO/3600/1000,6) KEIDO ");
		sb.append("FROM MST_MACHINE ");
		sb.append("WHERE DELETE_FLAG = 0 ");
		sb.append("AND NLS_UPPER(KIBAN) LIKE NLS_UPPER('"+ ConvertUtil.rpStr(searchKibanInput)+"') ");
		sb.append("OR NLS_UPPER(LBX_KIBAN) LIKE NLS_UPPER('"+ConvertUtil.rpStr(searchKibanInput)+"') ");
		sb.append("OR NLS_UPPER(USER_KANRI_NO) LIKE NLS_UPPER('"+ConvertUtil.rpStr(searchKibanInput)+"') ");

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<Object[]> latlng = em.createNativeQuery(new String(sb)).getResultList();

		if (latlng.size() > 0)
			return latlng;
		else
			return null;
	}
	// **ªªª@2018/05/22 LBNÎì  v]sïÇä  No45Î v]ÇÁ ªªª**//




	/**
	 * JPA-JPQL
	 * findByFavoriteFlg: ¨CÉüè@Bõ
	 */
	@Override
	public Integer findByFavoriteDelFlg(Integer serialNumber, String userId) {

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<Integer> result = em.createQuery("SELECT t.delFlg FROM TblUserFavorite t "
											+ "WHERE t.kibanSerno = :kibanSerno AND t.userId = :userId ")
				.setParameter("kibanSerno", serialNumber)
				.setParameter("userId", userId)
				.getResultList();

		if (result.size() > 0)
			return (Integer)result.get(0);
		else
			return null;
	}




}
