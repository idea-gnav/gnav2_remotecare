package com.lbxco.RemoteCAREApp.ejbs.machine;

import java.util.List;

import com.lbxco.RemoteCAREApp.entities.MstMachine;

public interface IMachineService{



	/**
	 * JPA
	 * FindByMachine: @Bîñæ¾
	 */
	MstMachine findByMachine(Integer serialNumber);



	/**
	 * NativeQuery
	 * findByMachineList: @Bêæ¾
	 */
	List<Object[]> findByMachineListNativeQuery(
			String userId,
//			Integer serialNumber,
			Integer kibanSelect,
			String kibanInput,
			Integer searchFlg,
			Integer sortFlg,
			String manufacturerSerialNumberFrom,
			String manufacturerSerialNumberTo,
			String customerManagementNoFrom,
			String customerManagementNoTo,
			String lbxSerialNumberFrom,
			String lbxSerialNumberTo,
			String lbxModel,
			String psmId,
			String rmId,
			String dealerName,
			// **«««@2018/11/28  iDEARº  Xebv2@¨ql¼õÇÁ «««**//
			String customerName,
			// **ªªª@2018/11/28  iDEARº  Xebv2@¨ql¼õÇÁ ªªª**//
			String dtcCode,
			Integer warningCurrentlyInProgress,
			String warningDateFrom,
			String warningDateTo,
			// **«««@2019/01/07  iDEARº  Xebv2@èú®õõÇÁ «««**//
			Integer periodicServiceNotice,
			Integer periodicServiceNowDue,
			String replacementPart,
			// **ªªª@2019/01/07  iDEARº  Xebv2@èú®õõÇÁ ªªª**//
			Integer favoriteSearchFlg,
			Integer typeFlg,
			String kigyouCd,
			String sosikiCd,
			String kengenCd
			// **«««@2018/05/22 LBNÎì  v]sïÇä  No45Î v]ÇÁ «««**//
			, Double searchRangeFromIdo,
			Double searchRangeFromKeido,
			Integer searchRangeDistance,
			Integer searchRangeUnit
			// **ªªª@2018/05/22 LBNÎì  v]sïÇä  No45Î v]ÇÁ ªªª**//
		);



	// **«««@2018/05/22 LBNÎì  v]sïÇä  No45Î v]ÇÁ «««**//
	/**
	 * NativeQuery
	 * findByLatlngNativeQuery: @BÜxoxæ¾
	 */
	List<Object[]> findByLatlngNativeQuery(String searchKibanInput);
	// **ªªª@2018/05/22 LBNÎì  v]sïÇä  No45Î v]ÇÁ ªªª**//




	 /** JPA-JPQL
	 * findByFavoriteFlg: ¨CÉüè@Bõ
	 */
	Integer findByFavoriteDelFlg(Integer serialNumber, String userId);

}

