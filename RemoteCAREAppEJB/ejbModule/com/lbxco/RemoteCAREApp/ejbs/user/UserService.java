package com.lbxco.RemoteCAREApp.ejbs.user;


import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.lbxco.RemoteCAREApp.entities.MstUser;


@Stateless
@Local(IUserService.class)
public class UserService implements IUserService{


	@PersistenceContext
    EntityManager em;



	/**
	 * JPA-JPQL
	 * findByUser: ユーザー認証情報取得（1:LBXユーザー）
	 */
	@SuppressWarnings("unchecked")
	@Override
	public MstUser findByUser(String userId, String password) {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT mu FROM MstUser mu ");
		sb.append("JOIN mu.mstSosiki ms ");
		sb.append("WHERE mu.delFlg = 0 ");
		sb.append("AND ms.lbxFlg = :lbxFlg ");
		sb.append("AND mu.userId = :userId ");
		sb.append("AND mu.password = :password ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<MstUser> result =  em.createQuery(new String(sb))
					.setParameter("lbxFlg", 1)
					.setParameter("userId", userId)
				    .setParameter("password", password)
				    .getResultList();

		if (result.size() > 0)
			return (MstUser)result.get(0);

		else
			return null;
	}


	/**
	 * JPA-JPQL
	 * findByUser: ユーザー情報取得
	 */
	@SuppressWarnings("unchecked")
	@Override
	public MstUser findByUser(String userId) {

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT mu FROM MstUser mu ");
		sb.append("JOIN mu.mstSosiki ms ");
		sb.append("WHERE mu.delFlg = 0 ");
		sb.append("AND ms.lbxFlg = :lbxFlg ");
		sb.append("AND mu.userId = :userId ");

		em.getEntityManagerFactory().getCache().evictAll();
		List<MstUser> result =  em.createQuery(new String(sb))
					.setParameter("lbxFlg", 1)
					.setParameter("userId", userId)
				    .getResultList();

		if (result.size() > 0)
			return (MstUser)result.get(0);

		else
			return null;
	}



	/**
	 * NativeQuery
	 * findByUserSosikiKengenNativeQuery: ユーザー組織権限取得
	 */

	@Override
	public List<Object[]> findByUserSosikiKengenNativeQuery(String userId){

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("MU.USER_ID, ");			// 0
		sb.append("MS.SOSIKI_CD, ");		// 1
		sb.append("MS.KIGYOU_CD, ");		// 2
		sb.append("MS.GROUP_NO, ");			// 3
		sb.append("MK.KENGEN_CD, ");		// 4
		sb.append("MK.PROCESS_TYPE_FLG, ");	// 5
		sb.append("MS.DAIRITENN_CD ");		// 6
		sb.append("FROM MST_USER MU ");
		sb.append("JOIN MST_SOSIKI MS ");
		sb.append("ON MU.SOSIKI_CD = MS.SOSIKI_CD ");
		sb.append("JOIN MST_KENGEN MK ");
		sb.append("ON MS.KENGEN_CD = MK.KENGEN_CD ");
		sb.append("WHERE MU.USER_ID = '" + userId + "' ");

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<Object[]> result =  em.createNativeQuery(new String(sb)).getResultList();

		if(result.size() > 0)
			return result;
		else
			return null;
	}

	 /**↓↓↓ 2019/04/01 iDEA山下 ユーザ権限での制御追加(メニュー、Document、News) ↓↓↓**/
	/**
	 * NativeQuery
	 * findByUserKengenNativeQuery: ユーザー権限取得
	 */

	@Override
	public List<Object[]> findByUserKengenNativeQuery(String userId){

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("MU.USER_ID, ");			// 0
		sb.append("MS.SOSIKI_CD, ");		// 1
		sb.append("MS.KIGYOU_CD, ");		// 2
		sb.append("MS.GROUP_NO, ");			// 3
		sb.append("MK.KENGEN_CD, ");		// 4
		sb.append("MK.PROCESS_TYPE_FLG, ");	// 5
		sb.append("MS.DAIRITENN_CD ");		// 6
		sb.append("FROM MST_USER MU ");
		sb.append("JOIN MST_SOSIKI MS ");
		sb.append("ON MU.SOSIKI_CD = MS.SOSIKI_CD ");
		sb.append("JOIN MST_KENGEN MK ");
		sb.append("ON MU.KENGEN_CD = MK.KENGEN_CD ");
		sb.append("WHERE MU.USER_ID = '" + userId + "' ");

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<Object[]> result =  em.createNativeQuery(new String(sb)).getResultList();

		if(result.size() > 0)
			return result;
		else
			return null;
	}

	 /**↑↑↑ 2019/04/01 iDEA山下 ユーザ権限での制御追加(メニュー、Document、News) ↑↑↑**/



	/**
	 * NativeQuery
	 * findByUserSosikiProcessTypeFlg: ユーザー組織権限処理タイプ取得
	 */
	@Override
	public Integer findByUserSosikiProcessTypeFlg(String userId){

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("MK.PROCESS_TYPE_FLG ");
		sb.append("FROM MST_USER MU ");
		sb.append("JOIN MST_SOSIKI MS ");
		sb.append("ON MU.SOSIKI_CD = MS.SOSIKI_CD ");
		sb.append("JOIN MST_KENGEN MK ");
		sb.append("ON MS.KENGEN_CD = MK.KENGEN_CD ");
		sb.append("WHERE MU.USER_ID = '" + userId + "' ");

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<Object> result =  em.createNativeQuery(new String(sb)).getResultList();

		if(result!=null)
			return Integer.parseInt(result.get(0).toString());
		else
			return null;
	}


	/**
	 * NativeQuery
	 * findByUserSosikiKengenCd: ユーザー組織権限コード取得
	 */
	@Override
	public Integer findByUserSosikiKengenCd(String userId){

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("MK.KENGEN_CD ");
		sb.append("FROM MST_USER MU ");
		sb.append("JOIN MST_SOSIKI MS ");
		sb.append("ON MU.SOSIKI_CD = MS.SOSIKI_CD ");
		sb.append("JOIN MST_KENGEN MK ");
		sb.append("ON MS.KENGEN_CD = MK.KENGEN_CD ");
		sb.append("WHERE MU.USER_ID = '" + userId + "' ");

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<Object> result =  em.createNativeQuery(new String(sb)).getResultList();

		if(result!=null)
			return Integer.parseInt(result.get(0).toString());
		else
			return null;
	}
}
