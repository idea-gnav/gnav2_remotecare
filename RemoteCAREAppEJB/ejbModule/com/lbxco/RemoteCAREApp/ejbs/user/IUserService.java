package com.lbxco.RemoteCAREApp.ejbs.user;

import java.util.List;

import com.lbxco.RemoteCAREApp.entities.MstUser;

public interface IUserService {


	/**
	 * JPA-JPQL
	 * findByUser: ユーザー情報取得
	 */
	MstUser findByUser(String userId, String password);


	/**
	 * JPA-JPQL
	 * findByUser: ユーザー情報取得
	 */
	MstUser findByUser(String userId);


	/**
	 * NativeQuery
	 * findByUserSosikiKengenNativeQuery: ユーザー組織権限取得
	 */
	List<Object[]> findByUserSosikiKengenNativeQuery(String userId);


	 /**↓↓↓ 2019/04/01 iDEA山下 ユーザ権限での制御追加(メニュー、Document、News) ↓↓↓**/

	/**
	 * NativeQuery
	 * findByUserKengenNativeQuery: ユーザー権限取得
	 */
	List<Object[]> findByUserKengenNativeQuery(String userId);

	/**↑↑↑ 2019/04/01 iDEA山下 ユーザ権限での制御追加(メニュー、Document、News) ↑↑↑**/

	/**
	 * NativeQuery
	 * findByUserSosikiProcessTypeFlg: ユーザー組織権限処理タイプ取得
	 */
	Integer findByUserSosikiProcessTypeFlg(String userId);


	/**
	 * NativeQuery
	 * findByUserSosikiKengenCd: ユーザー組織権限コード取得
	 */
	Integer findByUserSosikiKengenCd(String userId);




}
