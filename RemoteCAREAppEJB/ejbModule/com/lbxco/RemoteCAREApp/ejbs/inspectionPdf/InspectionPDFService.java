package com.lbxco.RemoteCAREApp.ejbs.inspectionPdf;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Stateless
@Local(IInspectionPDFService.class)

public class InspectionPDFService implements IInspectionPDFService{

	private static final Logger logger = Logger.getLogger(InspectionPDFService.class.getName());

	@PersistenceContext
	EntityManager em;

	@Override
	public String getPDFUrl(Integer _serialNumber, String _dtcEventNo) {

		logger.setLevel(Level.ALL);

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<String> modelList = em.createQuery("SELECT m.modelCd FROM MstMachine m WHERE m.kibanSerno =:param0")
										.setParameter("param0", _serialNumber)
										.getResultList();
		if(modelList.size()>0) {
			String modelCd = modelList.get(0);
			logger.config("[RemoteCAREApp][DEBUG] modelCd="+modelCd+", dtcEventNo="+_dtcEventNo);

			em.getEntityManagerFactory().getCache().evictAll();
			@SuppressWarnings("unchecked")
			List<String> path = em.createQuery("SELECT p.pdfName FROM TblDtcPdf p WHERE p.modelCd = :param0 and p.dtcCode =: param1 AND p.delFlg = 0")
										.setParameter("param0", modelCd)
										.setParameter("param1", _dtcEventNo)
										.getResultList();

			logger.config("[RemoteCAREApp][DEBUG] pdfPath="+path.get(0));

			if(path.size()>0)
				return path.get(0);
			else
				return null;
		}
		return null;
	}
}

