package com.lbxco.RemoteCAREApp.ejbs.report;


import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.lbxco.RemoteCAREApp.entities.TblTeijiReport;




@Stateless
@Local(IReportService.class)
public class ReportService implements IReportService{

	@PersistenceContext
    EntityManager em;




	/**
	 * JPA-JPQL
	 * getUserSetting: ユーザー設定単位取得
	 */
	@Override
	public Integer getUserSetting(String userId) {
		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<Integer> rst = em.createQuery("Select u.unitSelect FROM TblUserSetting u WHERE u.userId =:param")
				.setParameter("param", userId).getResultList();
		if(rst.size()>0)
		return rst.get(0);
		return null;
	}


	/**
	 * JPA-JPQL
	 * getAllInfo: レポート情報取得 (グラフレポート)
	 */
	@Override
	public List<Object[]> getAllInfo(Integer serialNumber, Timestamp dateFrom, Timestamp dateTo) {

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<Object[]> result = em.createQuery("Select r.kikaiKadobiLocal,r.hourMeter,r.kikaiSosaTime,r.nenryoConsum,r.ureaWaterConsum, m "
				+ "from TblTeijiReport r JOIN r.mstMachine m "
				+ "WHERE (m.kibanSerno = :serialNumber AND r.kikaiKadobiLocal >= :dateFrom AND r.kikaiKadobiLocal <=:dateTo) "
				+ "Order by r.kikaiKadobiLocal")
					.setParameter("serialNumber", serialNumber)
					.setParameter("dateFrom", dateFrom)
					.setParameter("dateTo", dateTo)
					.getResultList();

		 return result;
	}



	/**
	 * JPA
	 * FindByReport: レポート情報取得（日報）
	 */
	@Override
	public TblTeijiReport FindByReport(Integer serialNumber, Timestamp searchDate) {

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<Timestamp> kadouResult = em.createQuery("SELECT t.kikaiKadobiLocal FROM TblTeijiReport t "
				+ "WHERE t.kibanSerno=:kibanSerno AND t.kikaiKadobiLocal <= :searchDate "
				+ "ORDER BY t.kikaiKadobiLocal DESC")
				.setParameter("kibanSerno", serialNumber)
				.setParameter("searchDate", searchDate)
				.setMaxResults(1)
				.getResultList();

		if(kadouResult.size() > 0) {
			Timestamp kadoubi = kadouResult.get(0);
			String date = df.format(searchDate);
			String kadouDate = df.format(kadoubi);

			if(date.equals(kadouDate)) {
				@SuppressWarnings("unchecked")
				List<TblTeijiReport> result = em.createQuery("SELECT t FROM TblTeijiReport t "
						+ "WHERE t.kibanSerno=:kibanSerno AND t.kikaiKadobiLocal <= :searchDate "
						+ "ORDER BY t.kikaiKadobiLocal DESC")
						.setParameter("kibanSerno", serialNumber)
						.setParameter("searchDate", searchDate)
						.setMaxResults(1)
						.getResultList();

				return result.get(0);

			}else {
				return null;
			}
		}else {
			return null;
		}

	}

}
