package com.lbxco.RemoteCAREApp.ejbs.report;


import java.sql.Timestamp;
import java.util.List;

import com.lbxco.RemoteCAREApp.entities.TblTeijiReport;

public interface IReportService {



	/**
	 * JPA-JPQL
	 * getUserSetting: ユーザー設定単位取得
	 */
	 Integer getUserSetting(String userId);

	/**
	 * JPA-JPQL
	 * getAllInfo: レポート情報取得(グラフレポート)
	 */
	 List<Object[]> getAllInfo(Integer serialNumber,Timestamp dateFrom,Timestamp dateTo);


	/**
	 * JPA
	 * FindByReport: レポート情報取得（日報）
	 */
	TblTeijiReport FindByReport(Integer serialNumber, Timestamp searchDate);




}
