package com.lbxco.RemoteCAREApp.ejbs.dtcDitail;


import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.lbxco.RemoteCAREApp.ConvertUtil;


@Stateless
@Local(IDtcDitailService.class)
public class DtcDitailService implements IDtcDitailService{

	@PersistenceContext
    EntityManager em;




	/**
	 * NativeQuery
	 * findByDtcDitail
	 */
	@Override
	public List<Object[]> findByDtcDitailNativeQuery(
			Integer serialNumber,
			Integer sortFlg,
			String warningDateFrom,
			String warningDateTo,
			String warningCd,
			Integer userKengenCd
		){

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("TES.KIBAN_SERNO, ");										// 0 KIBAN_SERNO
		sb.append("TES.CON_TYPE, ");										// 1 CON_TYPE
		sb.append("TES.EVENT_NO, ");										// 2 EVENT_NO
		sb.append("MES.FLG_VISIBLE, ");										// 3 FLG_VISIBLE
		sb.append("MES.EVENT_CODE, ");										// 4 EVENT_CODE
		sb.append("MES.KUBUN_NAME, ");										// 5 KUBUN_NAME
		sb.append("MES.EVENT_NAME, ");										// 6 EVENT_NAME
		sb.append("MES.MESSAGE, ");											// 7 MESSAGE
		sb.append("MES.ALERT_LV, ");										// 8 ALERT_LV
		sb.append("NVL(TES.HOUR_METER, 0.0) AS HOUR_METER, ");				// 9 HOUR_METER
		sb.append("TES.HASSEI_TIMESTAMP_LOCAL, ");							// 10 HASSEI_TIMESTAMP_LOCAL
		sb.append("TES.FUKKYU_TIMESTAMP_LOCAL, ");							// 11 FUKKYU_TIMESTAMP_LOCAL
		sb.append("TES.SHOZAITI_EN, ");										// 12 SHOZAITI_EN
		sb.append("TES.IDO, ");												// 13 IDO
		sb.append("TES.KEIDO, ");											// 14 KEIDO
		sb.append("TES.RECV_TIMESTAMP_LOCAL, ");							// 15 RECV_TIMESTAMP_LOCAL
		sb.append("NVL(MES.FLG_VISIBLE,MES.EVENT_CODE) AS DTC_EVENT_NO ");	// 16 DTC_EVENT_NO

		sb.append("FROM TBL_EVENT_SEND TES ");

		sb.append("INNER JOIN TBL_KEIHO_HYOJI_SET TK  ");
		sb.append("ON (TO_CHAR(TES.EVENT_NO) = TK.EVENT_CODE OR NVL(TES.N_EVENT_NO,'N_EVENT_NO') = TO_CHAR(TK.EVENT_CODE)) ");
		sb.append("AND TES.EVENT_SHUBETU = TK.SHUBETU_CODE ");
		sb.append("AND TES.CON_TYPE = TK.CON_TYPE ");
		sb.append("AND TES.MACHINE_KBN = TK.MACHINE_KBN ");
		sb.append("AND TK.KENGEN_CD = " + userKengenCd + " ");

		sb.append("INNER JOIN MST_EVENT_SEND MES ");
		sb.append("ON TES.EVENT_NO = MES.EVENT_CODE ");
		sb.append("AND TES.EVENT_SHUBETU = MES.SHUBETU_CODE ");
		sb.append("AND TES.CON_TYPE = MES.CON_TYPE ");
		sb.append("AND TES.MACHINE_KBN = MES.MACHINE_KBN ");

		sb.append("WHERE MES.DELETE_FLAG = 0 ");
		sb.append("AND MES.LANGUAGE_CD = 'en' ");
		sb.append("AND TES.EVENT_SHUBETU = 8 ");
		sb.append("AND TES.CON_TYPE IN ('C','C2') ");
		sb.append("AND TES.SYORI_FLG <> 9 ");

		sb.append("AND TES.KIBAN_SERNO = " + serialNumber + " ");

		if(warningDateFrom != null && warningDateTo != null) {
			sb.append("AND TES.HASSEI_TIMESTAMP_LOCAL ");
			sb.append("BETWEEN TO_TIMESTAMP('" + warningDateFrom + " 00:00:00.0', 'yyyy/MM/dd hh24:mi:ss.FF6') ");
			sb.append("AND TO_TIMESTAMP('" + warningDateTo + " 23:59:59.999999', 'yyyy/MM/dd hh24:mi:ss.FF6') ");
		}
		if(warningDateFrom != null && warningDateTo == null) {
			sb.append("AND TES.HASSEI_TIMESTAMP_LOCAL ");
			sb.append(">= TO_TIMESTAMP('" + warningDateFrom + " 00:00:00.0', 'yyyy/MM/dd hh24:mi:ss.FF6') ");
		}

		if( warningDateFrom == null && warningDateTo != null) {
			sb.append("AND TES.HASSEI_TIMESTAMP_LOCAL  ");
			sb.append("<= TO_TIMESTAMP('" + warningDateTo + " 23:59:59.999999', 'yyyy/MM/dd hh24:mi:ss.FF6') ");
		}

		if(warningCd != null) {
			if(ConvertUtil.isNumber(warningCd))
				sb.append("AND (MES.FLG_VISIBLE LIKE '%" + warningCd + "%' OR MES.EVENT_CODE = " + warningCd + ") ");
			if(!ConvertUtil.isNumber(warningCd))
				sb.append("AND MES.FLG_VISIBLE LIKE '%" + ConvertUtil.rpStr(warningCd.toUpperCase()) + "%' ");
		}
		sb.append("AND ROWNUM <= 1001 ");

		if(sortFlg != null) {
			// DTCレベル
			if(sortFlg == 0 || sortFlg == 1)
				sb.append("ORDER BY MES.ALERT_LV ");
			// 発生日時
			if(sortFlg == 2 || sortFlg == 3)
				sb.append("ORDER BY TES.HASSEI_TIMESTAMP_LOCAL ");
			// 復旧日時
			if(sortFlg == 4 || sortFlg == 5)
				sb.append("ORDER BY TES.FUKKYU_TIMESTAMP_LOCAL ");
			// DTC内容
			if(sortFlg == 6 || sortFlg == 7)
				sb.append("ORDER BY DTC_EVENT_NO ");
			// アワメーター
			if(sortFlg == 8 || sortFlg == 9)
				sb.append("ORDER BY HOUR_METER ");

			if(sortFlg == 1 || sortFlg == 3 || sortFlg == 5 || sortFlg == 7 || sortFlg == 9)
				sb.append("DESC ");

			sb.append(", DTC_EVENT_NO ");
		}


		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<Object[]> result =  em.createNativeQuery(new String(sb)).getResultList();

		if(result.size() > 0) {
			return result;
		}else {

			return null;
		}
	}



	/**
	 * JPA
	 * findByPDF: A3点検PDF存在チェック
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String findByPDF(Integer serialNumber, String dtcEventNo) {

		em.getEntityManagerFactory().getCache().evictAll();
		List<String> modelList = em.createQuery("SELECT m.modelCd FROM MstMachine m WHERE m.kibanSerno =:kibanSerno")
										.setParameter("kibanSerno", serialNumber)
										.getResultList();
		if(modelList.size()>0) {
			String modelCd = modelList.get(0);
			List<String> pathList = em.createQuery("SELECT p.pdfName FROM TblDtcPdf p WHERE p.modelCd = :modelCd and p.dtcCode =:dtcEventNo AND p.delFlg = 0")
										.setParameter("modelCd", modelCd)
										.setParameter("dtcEventNo", dtcEventNo)
										.getResultList();
			if(pathList.size()>0)
				return pathList.get(0);
		}
		return null;
	}


}
