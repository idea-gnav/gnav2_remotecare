package com.lbxco.RemoteCAREApp.ejbs.dtcDitail;

import java.util.List;


public interface IDtcDitailService {


	/**
	 * NativeQuery
	 * findByDtcDitail
	 */
	List<Object[]> findByDtcDitailNativeQuery(
			Integer serialNumber,
			Integer sortFlg,
			String warningDateFrom,
			String warningDateTo,
			String warningCd,
			Integer userKengenId
			);

	/**
	 * JPA
	 * findByPDF: A3点検PDF存在チェック
	 */
	String findByPDF(Integer serialNumber, String dtcEventNo);


}
