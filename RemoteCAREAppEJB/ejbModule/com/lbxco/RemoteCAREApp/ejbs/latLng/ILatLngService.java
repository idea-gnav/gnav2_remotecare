package com.lbxco.RemoteCAREApp.ejbs.latLng;

import java.util.List;

import com.lbxco.RemoteCAREApp.entities.MstSummaryArea;

public interface ILatLngService {


	/**
	 *
	 * @param countryCd
	 * @return
	 */
	List<MstSummaryArea> findByMstSummaryArea(String countryCd);


	/**
	 *
	 * @param countryCd
	 * @param areaCd
	 * @param lat
	 * @param lng
	 * @param areaShortName
	 */
	void updateMstSummaryArea(String countryCd, String areaCd, Integer lat, Integer lng, String areaShortName);

}
