package com.lbxco.RemoteCAREApp.ejbs.latLng;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.lbxco.RemoteCAREApp.entities.MstSummaryArea;


@Stateless
@Local(ILatLngService.class)

public class LatLngService implements ILatLngService{

	private static final Logger logger = Logger.getLogger(LatLngService.class.getName());

	@PersistenceContext
	EntityManager em;



	/**
	 *
	 * @param countryCd
	 * @return
	 */
	@Override
	public List<MstSummaryArea> findByMstSummaryArea(String countryCd) {

		em.getEntityManagerFactory().getCache().evictAll();
//		@SuppressWarnings("unchecked")
		List<MstSummaryArea> result = em.createQuery("SELECT m FROM MstSummaryArea m WHERE m.countryCd =:countryCd")
										.setParameter("countryCd", countryCd)
										.getResultList();

		if(result.size()>0 && result!=null)
			return result;
		else
			return null;
	}



	/**
	 *
	 * @param countryCd
	 * @param areaCd
	 * @param lat
	 * @param lng
	 * @param areaShortName
	 */
	@Override
	public void updateMstSummaryArea(String countryCd, String areaCd, Integer lat, Integer lng, String areaShortName) {

		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE MST_SUMMARY_AREA SET ");
		sb.append("IDO = " + lat + ", ");
		sb.append("KEIDO = " + lng + ", ");
//		sb.append("AREA_SHORT_NM = '" + areaShortName + "', ");
		sb.append("UPDATE_USER = 'API', ");
		sb.append("UPDATE_DTM = SYSDATE, ");
		sb.append("UPDATE_PRG = 'RemoteCAREApp' ");
		sb.append("WHERE COUNTRY_CD = '" + countryCd + "' ");
		sb.append("AND AREA_CD = '" + areaCd + "' ");

		int updated = em.createNativeQuery(new String(sb)).executeUpdate();

		if(updated>0)
			em.flush();
		em.clear();

		return;
	}



}

