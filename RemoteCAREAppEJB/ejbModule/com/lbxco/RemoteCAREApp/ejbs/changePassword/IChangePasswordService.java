package com.lbxco.RemoteCAREApp.ejbs.changePassword;

public interface IChangePasswordService {


	/**
	 * JPA-JPQL
	 * updateByPassword: パスワード更新
	 */
	void updateByPassword(String userId, String password);



}
