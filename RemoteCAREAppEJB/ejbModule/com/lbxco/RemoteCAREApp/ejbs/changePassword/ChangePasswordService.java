package com.lbxco.RemoteCAREApp.ejbs.changePassword;


import java.sql.Timestamp;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.lbxco.RemoteCAREApp.entities.MstUser;




@Stateless
@Local(IChangePasswordService.class)
public class ChangePasswordService implements IChangePasswordService{

	@PersistenceContext
    EntityManager em;



	/**
	 * JPA-JPQL
	 * updateByPassword: パスワード更新
	 */
	@Override
	public void updateByPassword(String userId, String password) {

			MstUser user = em.find(MstUser.class, userId);
			user.setPassword(password);
			user.setNewPasswordDate(new Timestamp(System.currentTimeMillis()));
			user.setUpdpwdFlg("0");
			user.setUpdateUser(userId);
			user.setUpdatePrg("RemoteCAREApp");
			user.setUpdateDtm(new Timestamp(System.currentTimeMillis()));
			em.persist(user);

			return;
	}




}
