package com.lbxco.RemoteCAREApp.ejbs.sosiki;

import java.util.List;

import com.lbxco.RemoteCAREApp.ejbs.user.IUserService;

public interface ISosikiService extends IUserService{


	/**
	 * JPA-JPQL
	 * findByKigyouSosikiName: 企業・組織名情報取得
	 */
	 List<Object[]> findByKigyouSosikiName(Integer serialNumber);


		/**
	 * JPA
	 * findByMachineSosiki: 機械所有組織情報取得
	 */
	List<Object[]> findByMachineSosiki(Integer serialNumber);


}
