package com.lbxco.RemoteCAREApp.ejbs.sosiki;


import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.lbxco.RemoteCAREApp.ejbs.user.UserService;

@Stateless
@Local(ISosikiService.class)
public class SosikiService extends UserService implements ISosikiService{


	@PersistenceContext
    EntityManager em;



	/**
	 * JPA-JPQL
	 * findByKigyouSosikiName: 企業・組織名情報取得
	 */
	@Override
	public List<Object[]> findByKigyouSosikiName(Integer serialNumber) {
		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<Object[]> result = em.createQuery("SELECT"
				+ " mk.kigyouName,"
				+ " ms.sosikiName "
				+ " FROM MstKigyou mk"
				+ " JOIN mk.mstSosikis ms"
				+ " JOIN ms.mstMachines mc"
				+ " WHERE mc.kibanSerno = :param")
				.setParameter("param",serialNumber )
				.getResultList();

		if(result.size() > 0)
			return result;
		else
			return null;
	}



	/**
	 * JPA
	 * findByMachineSosiki: 機械所有組織情報取得
	 */
	@Override
	public List<Object[]> findByMachineSosiki(Integer serialNumber) {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT ");
		sb.append("mm.sosikiCd, ");			// 0 sosikiCd
		sb.append("ms.kengenCd, ");			// 1 kengenCd
		sb.append("mk.kigyouName, ");		// 2 kigyouName
		sb.append("ms.sosikiName ");		// 3 sosikiName
		sb.append("FROM MstMachine mm ");
		sb.append("LEFT JOIN mm.mstSosiki ms ");
		sb.append("LEFT JOIN ms.mstKigyou mk ");
		sb.append("WHERE mm.kibanSerno = :kibanSerno");

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<Object[]> result = em.createQuery(new String(sb))
				.setParameter("kibanSerno", serialNumber)
				.getResultList();

		if (result.size() > 0)
			return result;
		else
			return null;
	}


}
