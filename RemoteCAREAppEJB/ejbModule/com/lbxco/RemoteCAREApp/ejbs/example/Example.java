package com.lbxco.RemoteCAREApp.ejbs.example;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.lbxco.RemoteCAREApp.entities.MstUser;


@Stateless
@Local(IExample.class)
public class Example implements IExample{


	@PersistenceContext
    EntityManager em;


	@Override
	public List<MstUser> Test1(){

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<MstUser> l =  em.createQuery(
			    "SELECT u FROM MstUser u INNER JOIN u.tblUserDevices s WHERE s.delFlg=:param1")
				.setParameter( "param1", 0)
			    .getResultList();

		return l;
	}


	@Override
	public List<Object[]> Test2(){

		em.getEntityManagerFactory().getCache().evictAll();
		@SuppressWarnings("unchecked")
		List<Object[]> l =  em.createQuery(
			    "SELECT u.userId, s.deviceToken FROM MstUser u INNER JOIN u.tblUserDevices s WHERE s.delFlg=:param1")
				.setParameter( "param1", 0)
			    .getResultList();

		return l;
	}

}
