package com.lbxco.RemoteCAREApp;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Logger;

public class ConvertUtil {

	private static final Logger logger = Logger.getLogger(ConvertUtil.class.getName());


	private ConvertUtil() {}

	/**
	 * システム日付 Date
	 * @throws Exception
	 */
	public static Date currentDate() throws Exception {
		Date nowDate = new Date(System.currentTimeMillis());
		return nowDate;
	}


	/**
	 * 対応言語変換
	 */
	public static String convertLanguageCd(int deviceLanguage) {

		String languageCd = null;

		if(deviceLanguage==0)
			languageCd ="en";
		else if(deviceLanguage==1)
			languageCd ="pt";
		else if(deviceLanguage==2)
			languageCd ="es";
		else
			languageCd ="en";

		return languageCd;
	}


	/**
	 * 緯度、経度変換
	 * 10進法⇒60進法単位変換
	 */
	public static double parseLatLng(double latlng) {

		DecimalFormat df = new DecimalFormat("#.######");
		BigDecimal bd = new BigDecimal(df.format(latlng/1000/3600));

		return bd.doubleValue();
	}


	/**
	 * 緯度、経度変換
	 * 秒単位変換
	 */
	public static double parseLatLng2(double latlng) {

		DecimalFormat df = new DecimalFormat("#.######");
		BigDecimal bd = new BigDecimal(df.format(latlng/10000000));

		return bd.doubleValue();
	}


	/**
	 * 緯度、経度変換
	 * 60進法⇒10進法単位変換
	 */
	public static Integer parseLatLngInt(double latlng) {

		DecimalFormat df = new DecimalFormat("############");
		BigDecimal bd = new BigDecimal(df.format(latlng*3600*1000));

		return bd.intValue();
	}

	/**
	 * 書式フォーマット（年月日）
	 */
	public static String formatYMD(Timestamp date) {
		SimpleDateFormat sdf = new  SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(date);
	}


	/**
	 * 書式フォーマット（年月日時分秒）
	 */
	public static String formatYMDHMS(Timestamp date) {
		SimpleDateFormat sdf = new  SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(date);
	}



	/**
	 * 任意換算日付取得
	 *
	 */
	public static String getDays(int ago) {

		SimpleDateFormat fmtYmd = new SimpleDateFormat("yyyy-MM-dd");

		Calendar cal = Calendar.getInstance();
		cal.setTime(new Timestamp(System.currentTimeMillis()));
		cal.add(Calendar.DATE, ago);

		return fmtYmd.format(cal.getTime());
	}


	/**
	 * 任意換算日付取得（UTC）
	 *
	 */
	public static String getDaysUTC(int ago) {

		SimpleDateFormat fmtYmdUTC = new SimpleDateFormat("yyyy-MM-dd");

		Calendar cal = Calendar.getInstance();
		cal.setTime(new Timestamp(System.currentTimeMillis()));
		cal.add(Calendar.DATE, ago);

		TimeZone timeZone = TimeZone.getTimeZone("UTC");
		fmtYmdUTC.setTimeZone(timeZone);

		return fmtYmdUTC.format(cal.getTime());
	}



	/**
	 * 任意換算日付取得（UTC）
	 * @param ago
	 * @return :String
	 */
	public static String getDaysUtcYmdHms(int ago) {

		SimpleDateFormat fmtUTC = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

		Calendar cal = Calendar.getInstance();
		cal.setTime(new Timestamp(System.currentTimeMillis()));
		cal.add(Calendar.DATE, ago);

		TimeZone timeZone = TimeZone.getTimeZone("UTC");
		fmtUTC.setTimeZone(timeZone);

		return fmtUTC.format(cal.getTime());
	}




	/**
	 * 数値チェック Integer
	 *
	 */
	public static boolean isNumber(String num) {
	    try {
	        Integer.parseInt(num);
	        return true;
	    } catch (NumberFormatException e) {
	        return false;
	    }
	}

	/**
	 * 数値チェック
	 *
	 */
	public static boolean isNumberDouble(String num) {
	    try {
	        Double.parseDouble(num);
	        return true;
	    } catch (NumberFormatException e) {
	        return false;
	    }
	}

	/**
	 * 検索文字列置換（機番）
	 * 数値チェック後方一致、他前方一致
	 */
	public static String searchStrKiban(String str) {

		if(str != null) {
			boolean bMatch = str.matches("\\d{4}$");
			logger.config("bMatch="+bMatch);

			str.toUpperCase();
			logger.config("searchStrKiban="+str);

			if(str.contains("*")){
			    return str.replaceAll("\\*","%");
			}else{

				if(bMatch){
			        return "%" + str;
			    }else{
			        return str + "%";
			    }
			}

		}else {
			return null;
		}
	}


	/**
	 * 検索文字列置換（共通）
	 * 前方一致のみ
	 */
	public static String searchStr(String str) {

		if(str != null) {

			str.toUpperCase();
			logger.config("searchStr="+str);

			if(str.contains("*")){
			    return str.replaceAll("\\*","%");
			}else{
			        return str + "%";
			}
		}else {
			return null;
		}
	}


	/**
	 * SQLインジェクション対策
	 * [']
	 */
	public static String rpStr(String param) {
		return param.replace("'", "''");
	}




	/**
	 * SQLクエリ IN 検索文字列変換　文字 'あり
	 * List -> 検索文字列
	 */
	public static String queryInStr(List<String> str) {

		String[] strList = str.toArray(new String[str.size()]);
		StringBuilder sb = new StringBuilder();

		for(int index=0; index<strList.length; index++) {
			sb.append("'"+strList[index]+"'");
			if(index<(strList.length-1))
				sb.append(",");
		}
		return new String(sb);
	}




	/**
	 * SQLクエリ IN 検索文字列変換　文字 'あり
	 * カンマ,区切り -> 検索文字列
	 */
	public static String queryInStr(String str) {

		String[] strList = str.split(",");
		StringBuilder sb = new StringBuilder();

		for(int index=0; index<strList.length; index++) {
			sb.append("'"+strList[index]+"'");
			if(index<(strList.length-1))
				sb.append(",");
		}
		return new String(sb);
	}


	/**
	 * SQLクエリ IN 検索文字列変換　数値 'なし
	 */
	public static String queryInNumber(String str) {

		String[] strList = str.split(",");
		StringBuilder sb = new StringBuilder();

		for(int index=0; index<strList.length; index++) {
			sb.append(strList[index]);
			if(index<(strList.length-1))
				sb.append(",");
		}
		return new String(sb);
	}






}
