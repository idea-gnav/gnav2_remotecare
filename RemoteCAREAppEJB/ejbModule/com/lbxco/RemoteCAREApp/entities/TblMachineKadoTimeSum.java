package com.lbxco.RemoteCAREApp.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * TBL_MACHINE_KADO_TIME_SUM
 *
 */
@Entity
@Table(name="TBL_MACHINE_KADO_TIME_SUM")
@NamedQuery(name="TblMachineKadoTimeSum.findAll", query="SELECT t FROM TblMachineKadoTimeSum t")
public class TblMachineKadoTimeSum implements Serializable {
	private static final long serialVersionUID = 1L;


	/*
	 * Field
	 */
	@Id
	@Column(name="KIBAN_SERNO")
	private Integer kibanSerno;

	@Column(name="KIKAI_KADO_NENGETU")
	private Timestamp kikaiKadoNengetu;

	@Column(name="COUNTRY_CD")
	private String countryCd;

	@Column(name="AREA_CD")
	private String areaCd;

	@Column(name="SOSHIKI_CD")
	private String sosikiCd;

	@Column(name="DAIRITEN_CD")
	private String dairitenCd;

	@Column(name="MODEL_CD")
	private String modelCd;

	@Column(name="LBX_MODEL_CD")
	private String lbxModelCd;

	@Column(name="KADO_DAY")
	private Integer kadoDay;

	@Column(name="KADO_TIME")
	private Integer kadoTime;

	// **««« 2018/12/03  LBNÎì  tF[Y2ãXü¯vÎ ÇÁ  «««**//
	@Column(name="POSITION_COUNTRY_CD")
	private String positionCountryCd;

	// BR[h
	@Column(name="POSITION_AREA1")
	private String positionArea1;

	// S¼Ì
	@Column(name="POSITION_AREA2")
	private String positionArea2;

	// sæ¬º
	@Column(name="POSITION_AREA3")
	private String positionArea3;
	// **ªªª 2018/12/03  LBNÎì  tF[Y2ãXü¯vÎ ÇÁ  ªªª**//


	/*
	 * Constructor
	 */
	public TblMachineKadoTimeSum() {
	}


	/*
	 * Setter, Getter
	 */
	public Integer getKibanSerno() {
		return this.kibanSerno;
	}
	public void setKibanSerno(Integer kibanSerno) {
		this.kibanSerno = kibanSerno;
	}

	public Timestamp getKikaiKadoNengetu() {
		return this.kikaiKadoNengetu;
	}
	public void setKikaiKadoNengetu(Timestamp kikaiKadoNengetu) {
		this.kikaiKadoNengetu = kikaiKadoNengetu;
	}

	public String getCountryCd() {
		return this.countryCd;
	}
	public void setCountryCd(String countryCd) {
		this.countryCd = countryCd;
	}

	public String getAreaCd() {
		return this.areaCd;
	}
	public void setAreaCd(String areaCd) {
		this.areaCd = areaCd;
	}

	public String getSosikiCd() {
		return this.sosikiCd;
	}
	public void setSosikiCd(String sosikiCd) {
		this.sosikiCd = sosikiCd;
	}

	public String getDairitenCd() {
		return this.dairitenCd;
	}
	public void setDairitenCd(String dairitenCd) {
		this.dairitenCd = dairitenCd;
	}

	public String getModelCd() {
		return this.modelCd;
	}
	public void setModelCd(String modelCd) {
		this.modelCd = modelCd;
	}

	public String getLbxModelCd() {
		return this.lbxModelCd;
	}
	public void setLbxModelCd(String lbxModelCd) {
		this.lbxModelCd = lbxModelCd;
	}

	public Integer getKadoDay() {
		return this.kadoDay;
	}
	public void setKadoDay(Integer kadoDay) {
		this.kadoDay = kadoDay;
	}

	public Integer getKadoTime() {
		return this.kadoTime;
	}
	public void setKadoTime(Integer kadoTime) {
		this.kadoTime = kadoTime;
	}


	// **««« 2018/12/03  LBNÎì  tF[Y2ãXü¯vÎ ÇÁ  «««**//
	public String getPositionCountryCd() {
		return this.positionCountryCd;
	}
	public void setPositionCountryCd(String positionCountryCd) {
		this.positionCountryCd = positionCountryCd;
	}

	public String getPositionArea1() {
		return this.positionArea1;
	}
	public void setPositionArea1(String positionArea1) {
		this.positionArea1 = positionArea1;
	}

	public String getPositionArea2() {
		return this.positionArea2;
	}
	public void setPositionArea2(String positionArea2) {
		this.positionArea2 = positionArea2;
	}

	public String getPositionArea3() {
		return this.positionArea3;
	}
	public void setPositionArea3(String positionArea3) {
		this.positionArea3 = positionArea3;
	}
	// **ªªª 2018/12/03  LBNÎì  tF[Y2ãXü¯vÎ ÇÁ  ªªª**//


}