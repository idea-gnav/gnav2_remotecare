package com.lbxco.RemoteCAREApp.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table(name="MST_MAINTAINECE_PART")
@NamedQuery(name="MstMaintainecePart.findAll", query="SELECT m FROM MstMaintainecePart m")
public class MstMaintainecePart implements Serializable {
	private static final long serialVersionUID = 1L;



	/*
	 * Field
	 */

	@Id
	@Column(name="CON_TYPE")
	private String conType;

	@Column(name="MACHINE_MODEL")
	private Integer machineModel;

	@Column(name="MAINTAINECE_PART_NUMBER")
	private Integer maintainecePartNumber;

	@Column(name="LANGUAGE_CD")
	private String languageCd;

	@Column(name="MAINTAINECE_PART_NAME")
	private String maintainecePartName;

	@Column(name="DEL_FLG")
	private Integer delFlg;



	/*
	 * Setter, Getter
	 */

	public String getConType() {
		return this.conType;
	}
	public void setConType(String conType) {
		this.conType = conType;
	}

	public Integer getMachineModel() {
		return this.machineModel;
	}
	public void setMachineModel(Integer machineModel) {
		this.machineModel = machineModel;
	}

	public Integer getMaintainecePartNumber() {
		return this.maintainecePartNumber;
	}
	public void setMaintainecePartNumber(Integer maintainecePartNumber) {
		this.maintainecePartNumber = maintainecePartNumber;
	}

	public String getLanguageCd() {
		return this.languageCd;
	}
	public void setLanguageCd(String languageCd) {
		this.languageCd = languageCd;
	}

	public String getMaintainecePartName() {
		return this.maintainecePartName;
	}
	public void setMaintainecePartName(String maintainecePartName) {
		this.maintainecePartName = maintainecePartName;
	}

	public Integer getdelFlg() {
		return this.delFlg;
	}
	public void setDelFlg(Integer delFlg) {
		this.delFlg = delFlg;
	}

}