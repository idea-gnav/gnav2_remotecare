package com.lbxco.RemoteCAREApp.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
//import javax.persistence.Embeddable;
//import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
//import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
//import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * TBL_TEIJI_REPORT : 定時レポートテーブル
 *
 */
@Entity
@Table(name="TBL_TEIJI_REPORT")
@NamedQuery(name="TblTeijiReport.findAll", query="SELECT t FROM TblTeijiReport t")
public class TblTeijiReport implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	/*
	 * Mapping
	 */
	@ManyToOne
	@JoinColumn(name="KIBAN_SERNO")
	private MstMachine mstMachine;
	public MstMachine getMstMachine() {
		return mstMachine;
	}
	public void setMstMachine(MstMachine mstMachine) {
		this.mstMachine = mstMachine;
	}
	
	/*
	 * Field 
	 */
	@Id
	@Column(name="KIBAN_SERNO")
	private Integer kibanSerno;

	@Column(name="KIKAI_KADOBI")
	private Timestamp kikaiKadobi;

	@Column(name="CON_TYPE")
	private String conType;

	@Column(name="HOUR_METER")
	private Integer hourMeter;

	@Column(name="JIDO_SAISEI_OVER_NUM")
	private Integer jidoSaiseiOverNum;

	@Column(name="JIDO_SAISEI_START_NUM")
	private Integer jidoSaiseiStartNum;

	@Column(name="KIBAN")
	private String kiban;

	@Column(name="KIKAI_KADOBI_LOCAL")
	private Timestamp kikaiKadobiLocal;

	@Column(name="KIKAI_SOSA_TIME")
	private Integer kikaiSosaTime;

	@Column(name="NENRYO_CONSUM")
	private Double nenryoConsum;

	@Column(name="NENRYO_LV")
	private Integer nenryoLv;

	@Column(name="NIPPO_DATA")
	private String nippoData;

	@Column(name="RADIATOR_ONDO_MAX")	//ラジエター冷却水温度Max = クーラントMAX
	private Integer radiatorOndoMax;

	@Column(name="RECV_TIMESTAMP")
	private Timestamp recvTimestamp;

	@Column(name="SADOYU_ONDO_MAX")
	private Double sadoyuOndoMax;

	@Column(name="UREA_WATER_CONSUM")
	private Integer ureaWaterConsum;

	@Column(name="UREA_WATER_LV")
	private Integer ureaWaterLv;

	
	
	public TblTeijiReport() {
	}

	
	
	/*
	 * setter, getter  
	 */

	public String getConType() {
		return this.conType;
	}
	public void setConType(String conType) {
		this.conType = conType;
	}


	public Integer getHourMeter() {
		return this.hourMeter;
	}
	public void setHourMeter(Integer hourMeter) {
		this.hourMeter = hourMeter;
	}

	public Integer getJidoSaiseiOverNum() {
		return this.jidoSaiseiOverNum;
	}
	public void setJidoSaiseiOverNum(Integer jidoSaiseiOverNum) {
		this.jidoSaiseiOverNum = jidoSaiseiOverNum;
	}

	public Integer getJidoSaiseiStartNum() {
		return this.jidoSaiseiStartNum;
	}
	public void setJidoSaiseiStartNum(Integer jidoSaiseiStartNum) {
		this.jidoSaiseiStartNum = jidoSaiseiStartNum;
	}

	public String getKiban() {
		return this.kiban;
	}
	public void setKiban(String kiban) {
		this.kiban = kiban;
	}

	public Integer getKibanSerno() {
		return this.kibanSerno;
	}
	public void setKibanSerno(Integer kibanSerno) {
		this.kibanSerno = kibanSerno;
	}

	public Timestamp getKikaiKadobi() {
		return this.kikaiKadobi;
	}
	public void setKikaiKadobi(Timestamp kikaiKadobi) {
		this.kikaiKadobi = kikaiKadobi;
	}

	public Timestamp getKikaiKadobiLocal() {
		return this.kikaiKadobiLocal;
	}
	public void setKikaiKadobiLocal(Timestamp kikaiKadobiLocal) {
		this.kikaiKadobiLocal = kikaiKadobiLocal;
	}

	public Integer getKikaiSosaTime() {
		return this.kikaiSosaTime;
	}
	public void setKikaiSosaTime(Integer kikaiSosaTime) {
		this.kikaiSosaTime = kikaiSosaTime;
	}

	public Double getNenryoConsum() {
		return this.nenryoConsum;
	}
	public void setNenryoConsusum(Double nenryoConsum) {
		this.nenryoConsum = nenryoConsum;
	}

	public Integer getNenryoLv() {
		return this.nenryoLv;
	}
	public void setNenryoLv(Integer nenryoLv) {
		this.nenryoLv = nenryoLv;
	}

	public String getNippoData() {
		return this.nippoData;
	}
	public void setNippoData(String nippoData) {
		this.nippoData = nippoData;
	}

	public Integer getRadiatorOndoMax() {
		return this.radiatorOndoMax;
	}
	public void setRadiatorOndoMax(Integer radiatorOndoMax) {
		this.radiatorOndoMax = radiatorOndoMax;
	}

	public Timestamp getRecvTimestamp() {
		return this.recvTimestamp;
	}
	public void setRecvTimestamp(Timestamp recvTimestamp) {
		this.recvTimestamp = recvTimestamp;
	}

	public Double getSadoyuOndoMax() {
		return this.sadoyuOndoMax;
	}
	public void setSadoyuOndoMax(Double sadoyuOndoMax) {
		this.sadoyuOndoMax = sadoyuOndoMax;
	}

	public Integer getUreaWaterConsum() {
		return this.ureaWaterConsum;
	}
	public void setUreaWaterConsum(Integer ureaWaterConsum) {
		this.ureaWaterConsum = ureaWaterConsum;
	}

	public Integer getUreaWaterLv() {
		return this.ureaWaterLv;
	}
	public void setUreaWaterLv(Integer ureaWaterLv) {
		this.ureaWaterLv = ureaWaterLv;
	}
	

	
}