package com.lbxco.RemoteCAREApp.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * TBL_USER_SETTING : [U[Ýèe[u
 * 
 */
@Entity
@Table(name="TBL_USER_SETTING")
@NamedQuery(name="TblUserSetting.findAll", query="SELECT t FROM TblUserSetting t")
public class TblUserSetting implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	/*
	 * Field
	 */
	@Id
	@Column(name="USER_ID")
	private String userId;
	
	@Column(name="UNIT_SELECT")
	private Integer unitSelect;
	
	@Column(name="KIBAN_SELECT")
	private Integer kibanSelect;
	
	@Column(name="PDF_FLG")
	private Integer pdfFlg;
	
	@Column(name="APPLOG_FLG")
	private Integer applogFlg;
	
	// **«««@2018/05/22  LBNÎì  v]sïÇä  No45 ÁèÊuõÎ ÇÁ  «««**//
	@Column(name="SEARCH_RANGE")
	private Integer searchRange;
	// **ªªª@2018/05/22  LBNÎì  v]sïÇä  No45 ÁèÊuõÎ ÇÁ  ªªª**//
	
//	@Column(name="DEL_FLG")
//	private Integer delFlg;
	
	@Column(name="REGIST_PRG")
	private String registPrg;
	
	@Column(name="REGIST_DTM")
	private Timestamp registDtm;
	
	@Column(name="REGIST_USER")
	private String registUser;
	
	@Column(name="UPDATE_PRG")
	private String updatePrg;
	
	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;
	
	@Column(name="UPDATE_USER")
	private String updateUser;


	
	
	public TblUserSetting() {
	}

	
	/*
	 * Setter, Getter
	 */
	public String getUserId() {
		return this.userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getUnitSelect() {
		return this.unitSelect;
	}
	public void setUnitSelect(Integer unitSelect) {
		this.unitSelect = unitSelect;
	}

	public Integer getKibanSelect() {
		return this.kibanSelect;
	}
	public void setKibanSelect(Integer kibanSelect) {
		this.kibanSelect = kibanSelect;
	}
	
	public Integer getPdfFlg() {
		return this.pdfFlg;
	}
	public void setPdfFlg(Integer pdfFlg) {
		this.pdfFlg = pdfFlg;
	}

	public Integer getApplogFlg() {
		return this.applogFlg;
	}
	public void setApplogFlg(Integer applogFlg) {
		this.applogFlg = applogFlg;
	}
	
	// **«««@2018/05/22  LBNÎì  v]sïÇä  No45 ÁèÊuõÎ ÇÁ  «««**//
	public Integer getSearchRange() {
		return this.searchRange;
	}
	public void setSearchRange(Integer searchRange) {
		this.searchRange = searchRange;
	}
	// **ªªª@2018/05/22  LBNÎì  v]sïÇä  No45 ÁèÊuõÎ ÇÁ  ªªª**//
	
	
//	public Integer getDelFlg() {
//		return this.delFlg;
//	}
//	public void setDelFlg(Integer delFlg) {
//		this.delFlg = delFlg;
//	}

	public String getRegistPrg() {
		return this.registPrg;
	}
	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public Timestamp getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}
	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}
	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	
	
}