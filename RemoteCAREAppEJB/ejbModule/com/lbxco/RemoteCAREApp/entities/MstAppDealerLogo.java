package com.lbxco.RemoteCAREApp.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table(name="MST_APP_DEALER_LOGO")
@NamedQuery(name="MstAppDealerLogo.findAll", query="SELECT m FROM MstAppDealerLogo m")
public class MstAppDealerLogo implements Serializable {
	private static final long serialVersionUID = 1L;



	/*
	 * Field
	 */
	@Id
	@Column(name="DAIRITENN_CD")
	private String dairitennCd;

	@Column(name="KENGEN_CD")
	private Integer kengenCd;

	@Column(name="LOGO_IMAGE_PATH")
	private String logoImagePath;

	@Column(name="REGIST_DTM")
	private Timestamp registDtm;

	@Column(name="REGIST_PRG")
	private String registPrg;

	@Column(name="REGIST_USER")
	private String registUser;

	@Column(name="UPDATE_DTM")
	private Timestamp updateDtm;

	@Column(name="UPDATE_PRG")
	private String updatePrg;

	@Column(name="UPDATE_USER")
	private String updateUser;



	public MstAppDealerLogo() {
	}


	/*
	 * Getter, Setter
	 */
	public String getDairitennCd() {
		return this.dairitennCd;
	}
	public void setDairitennCd(String dairitennCd) {
		this.dairitennCd = dairitennCd;
	}

	public Integer getKengenCd() {
		return this.kengenCd;
	}
	public void setKengenCd(Integer kengenCd) {
		this.kengenCd = kengenCd;
	}

	public String getLogoImagePath() {
		return this.logoImagePath;
	}
	public void setLogoImagePath(String logoImagePath) {
		this.logoImagePath = logoImagePath;
	}

	public Timestamp getRegistDtm() {
		return this.registDtm;
	}
	public void setRegistDtm(Timestamp registDtm) {
		this.registDtm = registDtm;
	}

	public String getRegistPrg() {
		return this.registPrg;
	}
	public void setRegistPrg(String registPrg) {
		this.registPrg = registPrg;
	}

	public String getRegistUser() {
		return this.registUser;
	}
	public void setRegistUser(String registUser) {
		this.registUser = registUser;
	}

	public Timestamp getUpdateDtm() {
		return this.updateDtm;
	}
	public void setUpdateDtm(Timestamp updateDtm) {
		this.updateDtm = updateDtm;
	}

	public String getUpdatePrg() {
		return this.updatePrg;
	}
	public void setUpdatePrg(String updatePrg) {
		this.updatePrg = updatePrg;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

}